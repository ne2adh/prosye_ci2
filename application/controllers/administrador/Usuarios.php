<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller
{

    public function __construct()
	{
		parent::__construct();
    $this->load->model('Usuarios_model');
    $this->request = json_decode(file_get_contents('php://input'));

	}

    public function index(){
      $data=array(
            'usuarios'=>$this->usuarios_model->getUsuarios(),
      );      
      $this->load->view('layouts/header');
      $this->load->view('layouts/menugestor');
      $this->load->view('admin/usuarios/list',$data);
      $this->load->view('layouts/footer');
    }

    public function agregar(){
      //listar areas
      $data_area = $this->area_model->listArea();
      $data=array(
              'persona'=>$this->usuarios_model->getPersona(),
              'roles'=>$this->usuarios_model->getRoles(),
              'area'=>$data_area
            );
      $this->load->view('layouts/header');
      $this->load->view('layouts/menugestor');
      $this->load->view('admin/usuarios/list_agregar',$data);
      $this->load->view('layouts/footer');
    }

    public function editUsuario($id_usuario){        
        $data_area = $this->area_model->listArea();
        $data=array(
                'usuario'=>$this->usuarios_model->detalleUsuario($id_usuario),
                'roles'=>$this->usuarios_model->getRoles(),
                'area'=>$data_area
              );        
        $this->load->view('layouts/header');
        $this->load->view('layouts/menugestor');
        $this->load->view('admin/usuarios/edit',$data);
        $this->load->view('layouts/footer');
    }

    public function saveEditUsuarios(){      
      $id_rol=$this->input->post('rol');
      $id_area=$this->input->post('area');
      $id_usuario=$this->input->post('id_usuario');
      $data = array();      
      $data['rol_id']=$id_rol;
      $data['id_area']=$id_area;      
      if( $this->Usuarios_model->editUsuario($id_usuario, $data) ) {
        //registro realizado ir a lista de usuarios
        redirect(base_url()."administrador/usuarios");  
      }
      else{
        //registro no realizado volver a pantalla de registro
        redirect(base_url()."administrador/usuarios/editUsuario/".$id_usuario);
      }
    }

    public function saveUsuarios(){
      $ci_persona=$this->input->post('CI');
      $cuenta=$this->input->post('cuenta');
      $clave=$this->input->post('contraseña');
      $id_rol=$this->input->post('rol');
      $id_area=$this->input->post('area');
      $data = array();
      $data['ci_persona']=$ci_persona;
      $data['cuenta']= strtolower($cuenta);
      $data['clave']=md5($clave);
      $data['rol_id']=$id_rol;
      $data['fecha_ingreso'] = date("Y-m-d");
      $data['id_area']=$id_area;
      $data['estado']=1;
      if($this->Usuarios_model->save($data)){
        //registro realizado ir a lista de usuarios
        redirect(base_url()."administrador/usuarios");  
      }
      else{
        //registro no realizado volver a pantalla de registro
        redirect(base_url()."administrador/usuarios/agregar");
      }
    }

    public function perfil(){
        $idUsuario = $this->session->id;
        $dataUsuario = array( "dataUsuario" => $this->Usuarios_model->detalleUsuario($idUsuario) );
        $this->load->view('layouts/header');
		    $this->load->view('layouts/menugestor');
		    $this->load->view('admin/usuarios/perfil', $dataUsuario);
		    $this->load->view('layouts/footer');
    }

    public function listarNombre(){
      $resultado = array();
      $ci = $this->input->get('CI');
      $consulta = $this->usuarios_model->nombre($ci);
      if(!empty($consulta)){
          foreach ($consulta as $fila){
              $data['ci'] = trim($fila->CI);      
              $data['nombres'] = trim($fila->nombres);
              $data['ap_paterno'] = trim($fila->ap_paterno);
              $data['ap_materno'] = trim($fila->ap_materno);
              $data['nombrecompleto'] = trim($fila->nombres)." ".trim($fila->ap_paterno)." ".trim($fila->ap_materno);
              array_push($resultado, $data);
          }
      }
      echo json_encode($resultado);die;
    }

      public function editRol(){
          $id_rol=$this->input->get("id");
          $nombre=$this->input->get("nombre");
              $data=array(
              'nombre'=>$nombre,
          );
          $this->rol_model->edit($id_rol,$data);
          $mensaje = "Edicion Correcta";
          echo json_encode(array(
             'status' => 201, // success or not?
             'mensaje' => $mensaje
          ));
      }

      public function listarUsuarios(){
          $resultado = array();
          $consulta = $this->Usuarios_model->getUsuarios();          
          if(!empty($consulta)){
              foreach ($consulta as $fila){
                  $data['id_usuario'] = $fila['id_usuario'];
                  $data['cuenta'] = $fila['cuenta'];
                  $data['ci_persona'] = $fila['ci_persona'];
                  $data['nombre'] = $fila['nombre'];
                  $data['nombres'] = $fila['nombres'];
                  $data['ap_paterno'] = $fila['ap_paterno'];
                  $data['ap_materno'] = $fila['ap_materno'];
                  $data['id_rol'] = $fila['id_rol'];
                  $data['nombre_area'] = $fila['nombre_area'];
                  array_push($resultado, $data);
              }
          }
          echo json_encode($resultado);
          die;
      }

      public function cambiarPassword(){
            $actual = $this->request->actual;
            $nuevo = $this->request->nuevo;            
            $verificarPassword = $this->Usuarios_model->verificarPassword($actual);
            if(!$this->Usuarios_model->verificarPassword($actual)){
               echo json_encode( array( "Error"=>"si", "Mensaje"=> "Error en la contraseña Actual.") );  
            }
            else{
              $this->usuarios_model->updatePassword($nuevo);
              echo json_encode( array( "Error"=>"no", "Mensaje"=> "Password Modificado.") ); 
            }
      }


}
