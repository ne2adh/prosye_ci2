<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seccion extends CI_Controller 
{
	public function __construct(){
        parent::__construct();
        $this->request = json_decode(file_get_contents('php://input'));
	}

	public function index(){
        $data=$this->seccion_model->index();
        $r = array();
        foreach($data as $item){
            $id = $item->id;
            $nombre = $item->nombre;
            $logo = $item->logo;
            $prioridad = $item->prioridad;
            $item_seccion = $this->menu_model->buscar($id);
            $temp = array(
                "id" => $id,
               "nombre" => $nombre,
               "logo" => $logo,
               "prioridad" =>$prioridad,
               "menu" => $item_seccion
            ); 
            array_push($r, $temp);
        }
        $r = array("seccion"=>$r);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/roles/index',$r);
		$this->load->view('layouts/footer');
    }

    public function saveSeccion(){        
        $nombre = $this->request->nombre;
        $logo = $this->request->logo;
        $prioridad = $this->request->prioridad;
        $data = array(
            "nombre"=>$nombre,
            "logo"=>$logo,
            "prioridad"=>$prioridad
        );
        return $this->seccion_model->save($data);
    }

    public function listSeccion(){
        $data=$this->seccion_model->index();
        $r = array();
        foreach($data as $item){
            $id = $item->id;
            $nombre = $item->nombre;
            $logo = $item->logo;
            $prioridad = $item->prioridad;
            $item_seccion = $this->menu_model->buscar($id);
            $temp = array(
                "id" => $id,
               "nombre" => $nombre,
               "logo" => $logo,
               "prioridad" =>$prioridad,
               "menu" => $item_seccion
            ); 
            array_push($r, $temp);
        }        
		echo json_encode($r);
    }

    public function deleteSeccion(){
        $idSeccion = $this->request->idSeccion;
        return $this->seccion_model->delete($idSeccion);        
    }
    
}