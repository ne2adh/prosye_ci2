<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rol extends CI_Controller
{
	public function __construct(){
        parent::__construct();
				if(!$this->session->userdata("login"))
        {
          redirect(base_url());
        }
				$this->load->library('form_validation','from_helper');
        $this->load->helper('date','url');
				$this->load->model('rol_model');

        //$this->request = json_decode(file_get_contents('php://input'));
	}

	public function index(){
		$this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/roles/roles');
		$this->load->view('layouts/footer');
  }

	public function listarRoles(){
      $resultado = array();
      $consulta = $this->rol_model->index();
      if(!empty($consulta)){
          foreach ($consulta as $fila){
              $data['id_rol'] = $fila['id_rol'];
              $data['nombre'] = $fila['nombre'];
              array_push($resultado, $data);
          }
      }
      echo json_encode($resultado);die;
  }


	public function saveRol(){
		if( isset( $_GET['rol'] ) ){
			$rol = $this->input->get('rol');
			$data = array();
			$data['nombre']=$rol;
			$this->rol_model->insert($data);
			$mensaje = "Registro Correcto";
			echo json_encode(array(
				 'status' => 201, // success or not?
				 'mensaje' => $mensaje
			));
		}else{
			$mensaje = "No existe rol";
			echo json_encode(array(
				'status' => 500, // success or not?
				'mensaje' => $mensaje
			));
		}

	}

public function deleteRol(){
		$id_rol = $this->input->get('id_rol');
		$this->rol_model->delete($id_rol);
		$mensaje = "Eliminacion Correcta";
		echo json_encode(array(
			 'status' => 201, // success or not?
			 'mensaje' => $mensaje
		));
	}

public function editRol(){
        $id_rol=$this->input->get("id");
        $nombre=$this->input->get("nombre");
            $data=array(
            'nombre'=>$nombre,
        );
        $this->rol_model->edit($id_rol,$data);
				$mensaje = "Edicion Correcta";
				echo json_encode(array(
					 'status' => 201, // success or not?
					 'mensaje' => $mensaje
				));
	}

}
