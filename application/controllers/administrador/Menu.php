<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller{
	public function __construct(){
        parent::__construct();
        $this->request = json_decode(file_get_contents('php://input'));
	}

	public function index(){
        $data=$this->menu_model->index();
        echo json_encode($data);
    }

    public function saveMenu(){
        $nombre_menu = $this->request->nombre_menu;
        $direccion = $this->request->direccion;
        $id_seccion = $this->request->id_seccion;
        $tipo_menu = $this->request->tipo_menu;
        $logo = $this->request->logo;
        
        $data = array(
            "nombre_menu" => $nombre_menu,
            "direccion" => $direccion,
            "tipo_menu" => $tipo_menu,
            "id_seccion" => $id_seccion,            
            "logo" => $logo
        );

        return $this->menu_model->save($data);
    }

    public function deleteMenu(){
        //eliminar el menu del menu y tambien del rol menu asignado
        $idMenu =  $this->request->idMenu;        
        return $this->menu_model->delete($idMenu) && $this->rol_menu_model->deleteMenu($idMenu);
    }
}