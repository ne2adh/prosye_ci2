<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AsignacionMenu extends CI_Controller
{
	public function __construct(){
        parent::__construct();
				if(!$this->session->userdata("login"))
        {
          redirect(base_url());
        }
		$this->load->library('form_validation','from_helper');
    $this->load->helper('date','url');
		$this->load->model('AsignacionMenu_model');
		$this->load->model('rol_model');

        //$this->request = json_decode(file_get_contents('php://input'));
	}

	public function index($id_rol){
		$data['id_rol']=$id_rol;
		$data['rol']=$this->rol_model->dameRol($id_rol);
		$data['traer_menu']=$this->AsignacionMenu_model->traer_menu($id_rol);
		$data['menuasignado']=$this->AsignacionMenu_model->menuasignado($id_rol);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/roles/lista_rol',$data);
		$this->load->view('layouts/footer');
  	}

	public function deleteAsignacionMenu($id,$id_rol){
		$id_rol=$id_rol;
		$id=$id;
		$this->AsignacionMenu_model->delete($id);
		redirect(base_url()."administrador/AsignacionMenu/index/$id_rol");
	}

	public function saveAsignacionMenu(){
		$id_rol=$this->input->post('id_rol');
		$id_menu=$this->input->post('id_menu');
		$data = array();
		$data['id_rol']=$id_rol;
		$data['id_menu']=$id_menu;
		$this->AsignacionMenu_model->insertMenu($data);

		redirect(base_url()."administrador/AsignacionMenu/index/$id_rol");
	}
}
