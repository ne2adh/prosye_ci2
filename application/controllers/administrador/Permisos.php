<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permisos extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
       

	}

	public function index()
	{
		$data=array(
					'permisos'=>$this->permisos_model->getPermisos(),
					);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/permisos/list_permiso',$data);
		$this->load->view('layouts/footer');
	}
	public function agregar()
	{
		$data=array(
					'roles'=>$this->usuarios_model->getRoles(),
					'menus'=>$this->permisos_model->getMenus(),
					);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/permisos/agregar',$data);
		$this->load->view('layouts/footer');
	}	
	public function store()
	{
        $menu=$this->input->post("menu");
		$rol_id=$this->input->post("rol");
		$leer=$this->input->post("leer");
		$agregar=$this->input->post("insertar");
		$actualizar=$this->input->post("actualizar");
		$borrar=$this->input->post("borrar");
        $data=array(
                    'menu_id'=>$menu,
                    'rol_id'=>$rol_id,
					'leer'=>$leer,
					'agregar'=>$agregar,
					'actualizar'=>$actualizar,
					'borrar'=>$borrar,
                    
                    );
        if($this->permisos_model->save($data))
        {
            redirect(base_url()."administrador/permisos");
        }
        else
        {
            $this->session->set_flshdata("error","No se pudo guardar");
            redirect(base_url()."administrador/permisos/agregar");
        }	
	}

}