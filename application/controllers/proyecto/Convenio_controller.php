<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Convenio_controller extends CI_Controller 
{
	private $gestion_temp;
	private $mes_temp;
	public function __construct()
	{
		parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('date','url');


	}
	public function index()
	{
		        
	   $id=array();
	   $id=$this->session->userdata('id');
       $data['results'] =  $this->proyectos_model->get_proyectos_mios_convenio($id);
       //$data['ruta']= $this->proyectos_model->get_ruta($id);     
       //$data['results'] =  $this->convenio_model->index($id);    
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec');
		$this->load->view('admin/proyectos/mis_proy_convenio',$data);
		$this->load->view('layouts/footer');

	}

	public function nuevo(){
		$id_proyecto=$this->input->post("id_proyecto");
		$convenio=$this->input->post("convenio");
		redirect(base_url()."proyecto/convenio_controller/ingresoConvenio");
	}
	public function ingresoConvenio($id_proyecto,$convenio)
	{
		$id_proyecto=$id_proyecto;
		$convenio=$convenio;
		//$id_tramo=$id_tramo;
		//$cont=$cont;
		//$convenio=$convenio;
		//si el $id_proyecto es mayor a 100000 se esta asignando un convenio a un tramo
		//recuperar la informacion del tramo
		if($id_proyecto>=100000){
			//recuperar datos del tramo
			$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
		}
		else{
			$data_proyecto = $this->convenio_model->traerProy($id_proyecto);
		}
		
        $data = array('proyecto' => $data_proyecto,
        			  'id_proyecto'=>$id_proyecto,
        				//'id_tipo'=>$id_tipo,
        				//'cont'=>$cont,
        				//'convenio'=>$convenio,
        				//'tramo'=>$this->convenio_model->getTramo($id_tramo),
		);
		$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));	
        if ($convenio=='SI'){
			$this->load->view('layouts/header');
			$this->load->view('layouts/aside_proyec', $tramos);
			$this->load->view('admin/proyectos/convenio_view',$data);
			$this->load->view('layouts/footer');
		} 
		else{
        	/*$this->load->view('layouts/header');
			$this->load->view('layouts/aside_proyec');
			$this->load->view('admin/proyectos/sin_convenio_view',$data);
			$this->load->view('layouts/footer');*/
			redirect(base_url()."proyecto/metas_controller/ingresoMetas/$id_proyecto");
        }
      }  
	
	public function get_convenio($id_proyecto){
	 	$id_proyecto=$id_proyecto;
	 	$id_convenio=$this->convenio_model->traer_convenio($id_proyecto);
		$id_ruta=$this->convenio_model->traer_ruta($id_proyecto);
		if(isset($id_convenio)){
			redirect(base_url()."proyecto/convenio_controller/ingresoEditConvenio/$id_proyecto/$id_convenio/$id_ruta");
		}
		else{
			$this->ingresoConvenio($id_proyecto,"SI");			
		}
	}

	public function ingresoEditConvenio($id_proyecto,$id_convenio,$id_ruta){
		$id_proyecto=$id_proyecto;
		$id_convenio=$id_convenio;
		$id_ruta=$id_ruta;
		//si el $id_proyecto es mayor a 100000 se esta asignando un convenio a un tramo
		//recuperar la informacion del tramo
		if($id_proyecto>=100000){
			//recuperar datos del tramo
			$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
			$idProyecto = $data_proyecto->id_proyecto;
		}
		else{
			$data_proyecto = $this->convenio_model->traerProy($id_proyecto);
			$idProyecto = $id_proyecto;
		}

		$result_convenio = $this->convenio_model->getConvenio($id_convenio);
		$vigencia = $result_convenio->vigencia;
		$gestion = intval($vigencia);
		$mes = round(($vigencia-$gestion)*12);
		echo $gestion." - ".$mes;
		$data_convenio=array(
			'id_convenio'=>$result_convenio->id_convenio,
			'id_proyecto'=>$result_convenio->id_proyecto,
			'nombre_convenio'=>$result_convenio->nombre_convenio,
			'descripcion_convenio'=>$result_convenio->descripcion_convenio,
			'fecha'=> $result_convenio->fecha,
			'fecha_ratificacion' => $result_convenio->fecha_ratificacion,
			'vigencia' => $result_convenio->vigencia,
			'gestion' => $gestion,
			'mes' => $mes,
			'estado'=> $result_convenio->estado
		);
		
		$data = array('proyecto'=>$data_proyecto,
        			  'id_proyecto'=>$id_proyecto,
        			  'convenio'=> (object)$data_convenio,
					  'id_ruta'=>$id_ruta,
					  'id_convenio'=>$id_convenio,
		);
		$tramos = array("tramos" => $this->tramo_model->index($idProyecto));			
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/proyectos/convenio_view_edit',$data);
		$this->load->view('layouts/footer');
    }
	
	public function verificarVigencia(){
		$gestion = $this->gestion_temp;		
		if(isset($gestion) && $gestion!=''){
			$mes = $this->mes_temp;
			$total = $gestion*12 + $mes;
			return !($total==0);
		}
		return false;		
	}
	
	public function store(){
		$id_ruta=$this->input->post("ruta");
		$id_convenio=$this->input->post("id_convenio");
		$id_proyecto=$this->input->post("id_proyecto");
		$nombre_convenio=$this->input->post("nombre_convenio");
		$descripcion_convenio=$this->input->post("descripcion_convenio");
		$fecha = $this->input->post("fecha");
		$fecha_ratificacion =$this->input->post("fecha_ratificacion");		
		$gestion = $this->input->post("gestion");
		$mes = $this->input->post("mes");
		$this->gestion_temp = $gestion;
		$this->mes_temp = $mes;		
		$this->form_validation->set_rules("nombre_convenio", "Nombre del Convenio del Proyecto","trim|required|is_unique[convenio.nombre_convenio]");
		$this->form_validation->set_rules("descripcion_convenio", "Descripcion del Convenio del Proyecto","trim|required");
		$this->form_validation->set_rules("fecha", "Fecha del Convenio del Proyecto","required");
		$this->form_validation->set_rules("gestion", "Vigencia del Convenio del Proyecto","required|callback_verificarVigencia");
		$this->form_validation->set_rules("mes", "Vigencia del Convenio del Proyecto","required|callback_verificarVigencia");

		$this->form_validation->set_message("is_unique", "El campo %s esta duplicado");
		$this->form_validation->set_message("required", "El campo %s es requerido");
		$this->form_validation->set_message("is_natural", "El campo %s debe contener solo numeros positivos mayores a cero");
		$this->form_validation->set_message("verificarVigencia", "La vigencia del contrato en años y meses es 0 revise la informacion introducida");

		if ($this->form_validation->run()){
					$vigencia = $gestion + $mes/12;
					$data=array(
						'id_convenio'=>$id_convenio,
						'id_proyecto'=>$id_proyecto,
						'nombre_convenio'=>$nombre_convenio,
						'descripcion_convenio'=>$descripcion_convenio,
						'fecha'=>$fecha,
						'fecha_ratificacion' => $fecha_ratificacion,
						'vigencia'=>$vigencia,
						'estado'=>1,
					);
					$this->convenio_model->insert($data);
					$pant=2;
					$data3=array('id_proyecto'=>$id_proyecto,
								 'pant'=>$pant,
					);
					$this->proyectos_model->update_pant($data3,$id_proyecto);
					$this->convenio_model->update($id_convenio,$data);
					if($id_proyecto>=100000){
						//detalle del tramo
						redirect(base_url()."proyecto/tramos_controller/inicio/$id_proyecto");
					}
					else{
						//ingreso metas en proyecto
						redirect(base_url()."proyecto/metas_controller/ingresoMetas/$id_proyecto");
					}
					
				 }	
		else{
				$proyecto=$this->proyectos_model->getProyecto($id_proyecto);
				//redirect(base_url()."proyecto/mis_proyectos/menu/$id_proyecto/$proyecto->convenio");
				$this->ingresoConvenio($id_proyecto,$proyecto->convenio);	
		}
	}
	
	public function storeEdit(){
		$id_ruta=$this->input->post("ruta");
		$id_convenio=$this->input->post("id_convenio");
		$id_proyecto=$this->input->post("id_proyecto");
		$nombre_convenio=$this->input->post("nombre_convenio");
		$descripcion_convenio=$this->input->post("descripcion_convenio");
		$fecha=$this->input->post("fecha");
		$fecha_ratificacion = $this->input->post("fecha_ratificacion");
		$gestion = $this->input->post("gestion");
		$mes = $this->input->post("mes");
					
		/*$this->form_validation->set_rules("nombre_convenio", "Nombre del Convenio del Proyecto","trim|required|is_unique[convenio.nombre_convenio]");
		$this->form_validation->set_rules("descripcion_convenio", "Descripcion del Convenio del Proyecto","trim|required");
		$this->form_validation->set_rules("fecha", "Fecha del Convenio del Proyecto","required");
		$this->form_validation->set_rules("vigencia", "Vigencia del Convenio del Proyecto","required|is_natural");

		$this->form_validation->set_message("is_unique", "El campo %s esta duplicado");
		$this->form_validation->set_message("required", "El campo %s es requerido");
		$this->form_validation->set_message("is_natural", "El campo %s debe contener solo numeros positivos mayores a cero");
		


		if ($this->form_validation->run())
				{*/
				$vigencia = $gestion + $mes/12;
				$data=array(
					//'id_convenio'=>$id_convenio,
					'id_proyecto'=>$id_proyecto,
					'nombre_convenio'=>$nombre_convenio,
					'descripcion_convenio'=>$descripcion_convenio,
					'fecha'=>$fecha,
					'fecha_ratificacion' => $fecha_ratificacion,
					'vigencia'=>$vigencia,
					'estado'=>1,
				);
				
				$this->convenio_model->update($id_convenio,$data);
				if($id_proyecto>=100000){
					//detalle del tramo
					redirect(base_url()."proyecto/tramos_controller/inicio/$id_proyecto");
				}
				else{
					//lista de proyectos
					redirect(base_url()."proyecto/convenio_controller");
				}
				
				 /*}	
				 else
					{
						$proyecto=$this->proyectos_model->getProyecto($id_proyecto);
						//redirect(base_url()."proyecto/mis_proyectos/menu/$id_proyecto/$proyecto->convenio");
						$this->ingresoEditConvenio($id_proyecto,$id_convenio,$id_ruta);	
					}*/
	}

	public function store_sc(){
		$id_convenio=$this->convenio_model->current_con();
		$id_proyecto=$this->input->post("id_proyecto");
		$data=array();
		$data['id_convenio']=$id_convenio;
		$data['id_proyecto']=$this->input->post("id_proyecto");
		$data['nombre_convenio']="GADOR";
		$data['descripcion_convenio']="UNICO";
		$data['fecha']=$this->input->post('fecha_inicio');
		$data['vigencia']=$this->input->post('vigencia');
		$data['monto_Bs']=$this->input->post('costo_proyecto');
		$data['estado']=1;
		$this->convenio_model->insert($data);
		redirect(base_url()."proyecto/participantes/ingreso_sc/$id_convenio/$id_proyecto/$cont/$id_tramo/$id_tipo");
	}
}	