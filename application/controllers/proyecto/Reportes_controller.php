<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    

    class Reportes_controller extends CI_Controller{

        public function __construct(){
            parent::__construct();
            $this->load->library('form_validation');
            $this->load->helper('date','url');
            $this->request = json_decode(file_get_contents('php://input'));
        }

        public function incidenciaProyecto(){
            $this->load->view('layouts/header');
            $this->load->view('layouts/menugestor');
            $this->load->view('admin/proyectos/reporte_incidencia');
            $this->load->view('layouts/footer');
        }

        //Reporte por tipo Proyecto

        public function tipoProyecto(){

            /*
                1. Recuperar los tipos de Proyectos
                2. Recuperar el monto gastado por proyecto por gestion
                3. colocar en un array y enviar JSON
                    {
                        tipo:
                        costo:
                        cantidad:
                        seguimiento:{
                            gestion: monto
                            .
                            .
                            .
                        }
                    }
            */
            //recuperar los tipos de proyectos
            
            $data_proyectos = $this->proyectos_model->proyectosPorTipo();
            $gestion_menor = 3000;
            $gestion_mayor = 0;
            foreach($data_proyectos as $data){
                $gestion_menor = min($gestion_menor, date("Y", strtotime($data["fecha_inicio"])) );
                $gestion_mayor = max($gestion_mayor, date("Y", strtotime($data["fecha_conclusion"])) );
            }

            if($gestion_mayor>date("Y")){
                $gestion_mayor=date("Y");
            }
            $tipos_proyectos = $this->proyectos_model->listaTipoProyecto();            
            $r = array();

            foreach($tipos_proyectos as $tipo){
                $tipo_proyecto = $tipo['tipo_proyecto'];
                $data_tipo_proyectos = $this->proyectos_model->buscarPorTipo($tipo_proyecto);
                $total_proyectos = 0;
                $sum_proyectos = 0;
                $gestiones = [];               
                foreach($data_tipo_proyectos AS $proyecto){
                    
                    for($i=$gestion_menor;$i<=$gestion_mayor;$i++){                        
                        if (!array_key_exists($i, $gestiones)){
                            $gestiones += [ $i => 0 ];                                                        
                        }                       
                        $monto_gastado = $this->segui_componente_model->pagoAnualProyecto($proyecto["id_proyecto"], $i);
                        $monto_aux = $monto_gastado[0]->monto_ejecutado;
                        $gestiones[$i] = $gestiones[$i] + isset($monto_aux)?$monto_aux : 0;
                    }                    
                    $sum_proyectos = $sum_proyectos + $proyecto['costo_proyecto'];
                    $total_proyectos++;
                }
                $total = 0;
                
                for($i=$gestion_menor;$i<=$gestion_mayor;$i++){                                                                      
                    $total = $total + $gestiones[$i];
                }
                
                $gestiones += [ "TOTAL" => $total];
                
                $item = array(
                    "detalle" => $tipo_proyecto,
                    "cantidad_proyectos" => $total_proyectos,
                    "monto_proyectos" => $sum_proyectos,
                    "gestiones" => $gestiones
                );
                array_push($r, $item);
               
            }
           
            $temp = array(
                "datos" => $r,
                "gestion_menor" => $gestion_menor,
                "gestion_mayor" => $gestion_mayor
            );
            echo json_encode($temp);
        }

        public function areaProyecto(){
            $area_result = $this->area_model->listArea();
            $data_proyectos = $this->proyectos_model->listProyecto();
            $gestion_menor = 3000;
            $gestion_mayor = 0;
            foreach($data_proyectos as $data){
                $gestion_menor = min($gestion_menor, date("Y", strtotime($data->fecha_inicio)) );
                $gestion_mayor = max($gestion_mayor, date("Y", strtotime($data->fecha_conclusion)) );
            }

            if($gestion_mayor>date("Y")){
                $gestion_mayor=date("Y");
            }

            $r = array();
            foreach ($area_result as $area) {
                $cod_area = $area->cod;
                $proyectos_area = $this->proyectos_model->listAreaProyecto($cod_area);
                $total_proyectos = 0;
                $sum_proyectos = 0;
                $gestiones = [];
                foreach($proyectos_area as $proyecto){
                    for($i=$gestion_menor;$i<=$gestion_mayor;$i++){                        
                        if (!array_key_exists($i, $gestiones)){
                            $gestiones += [ $i => 0 ];                                                        
                        }                       
                        $monto_gastado = $this->segui_componente_model->pagoAnualProyecto($proyecto->id_proyecto, $i);
                        $monto_aux = $monto_gastado[0]->monto_ejecutado;
                        $gestiones[$i] = $gestiones[$i] + isset($monto_aux)?$monto_aux : 0;
                    }                  

                    $sum_proyectos = $sum_proyectos + $proyecto->costo_proyecto;
                    $total_proyectos++;                
                }

                if(count($gestiones)>0){
                    $total = 0;
                    for($i=$gestion_menor;$i<=$gestion_mayor;$i++){                                               
                        $total = $total + $gestiones[$i];
                    }

                    $gestiones += [ "TOTAL" => $total];
                }
                
                $item = array(
                    "detalle" => $area->nombre,
                    "cantidad_proyectos" => $total_proyectos,
                    "monto_proyectos" => $sum_proyectos,
                    "gestiones" => $gestiones
                );
                array_push($r, $item);
            } 
            $temp = array(
                "datos" => $r,
                "gestion_menor" => $gestion_menor,
                "gestion_mayor" => $gestion_mayor
            );
            echo json_encode($temp);
        }

        public function provinciaProyecto(){
            /*
                1. Listar Provincias
                2. Listar proyectos por provincias
            */
            $data_proyectos = $this->proyectos_model->listProyecto();

            $gestion_menor = 3000;
            $gestion_mayor = 0;
            foreach($data_proyectos as $data){
                $gestion_menor = min($gestion_menor, date("Y", strtotime($data->fecha_inicio)) );
                $gestion_mayor = max($gestion_mayor, date("Y", strtotime($data->fecha_conclusion)) );
            }

            if($gestion_mayor>date("Y")){
                $gestion_mayor=date("Y");
            }
            $r = array();
            $data_provincias = $this->provincias_model->listProvincias();
            
            foreach ($data_provincias as $provincias) { 
                $id_provincia = $provincias->id_provincia;
                $nombre_provincia = $provincias->nombre_provincia;
                $proyectos_provincias = $this->proyectos_model->listProvinciasProyecto($id_provincia);                
                $total_proyectos = 0;
                $sum_proyectos = 0;
                $gestiones = [];

                foreach($proyectos_provincias as $proyecto){
                    for($i=$gestion_menor;$i<=$gestion_mayor;$i++){                        
                        if (!array_key_exists($i, $gestiones)){
                            $gestiones += [ $i => 0 ];                                                        
                        }                       
                        $monto_gastado = $this->segui_componente_model->pagoAnualProyecto($proyecto->id_proyecto, $i);
                        $monto_aux = $monto_gastado[0]->monto_ejecutado;
                        $gestiones[$i] = $gestiones[$i] + isset($monto_aux)?$monto_aux : 0;
                    }                  

                    $sum_proyectos = $sum_proyectos + $proyecto->costo_proyecto;
                    $total_proyectos++;                
                }

                if(count($gestiones)>0){
                    $total = 0;
                    for($i=$gestion_menor;$i<=$gestion_mayor;$i++){                                               
                        $total = $total + $gestiones[$i];
                    }

                    $gestiones += [ "TOTAL" => $total];
                }
                
                $item = array(
                    "detalle" => $nombre_provincia,
                    "cantidad_proyectos" => $total_proyectos,
                    "monto_proyectos" => $sum_proyectos,
                    "gestiones" => $gestiones
                );
                array_push($r, $item);
            }
            $temp = array(
                "datos" => $r,
                "gestion_menor" => $gestion_menor,
                "gestion_mayor" => $gestion_mayor
            );
            echo json_encode($temp);
        }

        public function municipioProyecto(){
            /*
                1. Listar Municipios
                2. Listar proyectos
            */

            $data_proyectos = $this->proyectos_model->listProyecto();

            $gestion_menor = 3000;
            $gestion_mayor = 0;
            foreach($data_proyectos as $data){
                $gestion_menor = min($gestion_menor, date("Y", strtotime($data->fecha_inicio)) );
                $gestion_mayor = max($gestion_mayor, date("Y", strtotime($data->fecha_conclusion)) );
            }

            if($gestion_mayor>date("Y")){
                $gestion_mayor=date("Y");
            }
            $r = array();

            $data_municipios =  $this->municipio_model->por_municipio();
            
            foreach ($data_municipios as $municipio) {
                $id_municipio = $municipio->id_municipio;
                $nombre_municipio = $municipio->municipio;
                $proyectos_municipio = $this->proyectos_model->listMunicipiosProyecto($id_municipio);                
                $total_proyectos = 0;
                $sum_proyectos = 0;
                $gestiones = [];

                foreach($proyectos_municipio as $proyecto){
                    for($i=$gestion_menor;$i<=$gestion_mayor;$i++){                        
                        if (!array_key_exists($i, $gestiones)){
                            $gestiones += [ $i => 0 ];                                                        
                        }                       
                        $monto_gastado = $this->segui_componente_model->pagoAnualProyecto($proyecto->id_proyecto, $i);
                        $monto_aux = $monto_gastado[0]->monto_ejecutado;
                        $gestiones[$i] = $gestiones[$i] + isset($monto_aux)?$monto_aux : 0;
                    }                  

                    $sum_proyectos = $sum_proyectos + $proyecto->costo_proyecto;
                    $total_proyectos++;                
                }

                if(count($gestiones)>0){
                    $total = 0;
                    for($i=$gestion_menor;$i<=$gestion_mayor;$i++){                                               
                        $total = $total + $gestiones[$i];
                    }

                    $gestiones += [ "TOTAL" => $total];
                }
                
                $item = array(
                    "detalle" => $nombre_municipio,
                    "cantidad_proyectos" => $total_proyectos,
                    "monto_proyectos" => $sum_proyectos,
                    "gestiones" => $gestiones
                );
                array_push($r, $item);
            }
            $temp = array(
                "datos" => $r,
                "gestion_menor" => $gestion_menor,
                "gestion_mayor" => $gestion_mayor
            );
            echo json_encode($temp);

        }

    }
?>