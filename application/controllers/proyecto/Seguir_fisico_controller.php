<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seguir_fisico_controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('date','url');
	}

	public function index()
	{
    $id=array();
	  $id=$this->session->userdata('id');
		$data['results'] =  $this->segui_actividad_model->index1($id);
    $this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/seguimientos/segui_proy_finan',$data);
		$this->load->view('layouts/footer');

	}
	public function index1()
	{

	    $id=array();
	    $id=$this->session->userdata('id');
		$data['results'] =  $this->segui_actividad_model->index1($id);
        $this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/seguimientos/ingreso_seguimiento_fisico',$data);
		$this->load->view('layouts/footer');

	}

	public function ingreso($id_proyecto){
		//print_r($id_proyecto);
		if($id_proyecto>=100000){
			//si es tramo
			$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
			$idProyecto = $data_proyecto->id_proyecto;
		}
		else{
			$data_proyecto = $this->metas_model->proy($id_proyecto);
			$idProyecto = $id_proyecto;
		}
		//print_r($idProyecto);
		$inicio=$this->segui_actividad_model->inicio_proyect($id_proyecto);
		$anio_inicio=$this->segui_actividad_model->anio_inicio_proyect($inicio);
		$fin=$this->segui_actividad_model->fin_proyect($id_proyecto);
		$anio_fin=$this->segui_actividad_model->anio_fin_proyect($fin);

		//$id_proyecto=$id_proyecto;
		$data=array(
			'id_proyecto'=>$id_proyecto,
			'proyectod'=> $data_proyecto,
			'proyecto'=>$this->segui_actividad_model->compo($id_proyecto),
			'nombre'=>$this->segui_actividad_model->nombreProy($idProyecto),
			'inicio'=>$anio_inicio,
			'fin'=>$anio_fin,
		);
		//print_r($idProyecto);
   		$this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/seguimientos/escoga_proy_finan',$data);
		$this->load->view('layouts/footer');

	}
	///////

	public function validaSeguimiento($id_proyecto, $mes, $gestion) {
		//Buscar seguimienfo financiero asociado a un proyecto, por mes y año
		return $this->seguimiento_financiero_model->buscarSeguimiento($id_proyecto, $mes, $gestion);
	}

	public function elegir_proy(){
		$id_proyecto = $this->input->post('id_proyecto');
		$mes = trim($this->input->post('mes'));
		$gestion = $this->input->post('gestion');
		$this->form_validation->set_rules("mes", "Mes de seguimiento del Proyecto","trim|required");
		$this->form_validation->set_rules("gestion", "Año de seguimiento del Proyecto","trim|required");		
		$this->form_validation->set_message("required", "El campo %s es requerido");
		if ($this->form_validation->run()){
			//print_r($id_proyecto);
			if($id_proyecto>=100000){
				//si es tramo
				$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
				$idProyecto = $data_proyecto->id_proyecto;
			}
			else{
				$data_proyecto = $this->metas_model->proy($id_proyecto);
				$idProyecto = $id_proyecto;
			}

			$inicio=$this->segui_actividad_model->inicio_proyect($id_proyecto);
			$anio_inicio=$this->segui_actividad_model->anio_inicio_proyect($inicio);
			$fin=$this->segui_actividad_model->fin_proyect($id_proyecto);
			$anio_fin=$this->segui_actividad_model->anio_fin_proyect($fin);
			$data=array(
				'id_proyecto'=>$idProyecto,
				'proyectod'=> $data_proyecto,
				'proyecto'=>$this->segui_actividad_model->compo($id_proyecto),
				'nombre'=>$this->segui_actividad_model->nombreProy($idProyecto),
				'inicio'=>$anio_inicio,
				'fin'=>$anio_fin,
				'mes'=>$mes,
				'gestion'=>$gestion,
				'contrato'=>$this->segui_actividad_model->indexp($id_proyecto),
			);
			$this->load->view('layouts/header');
			$this->load->view('layouts/menugestor');
			$this->load->view('admin/seguimientos/escoga_proy_finanb',$data);
			$this->load->view('layouts/footer');
		}
		else {
			$this->ingreso($id_proyecto);
		}
	}

	public function ingreso2($id_proyecto){
		//print_r($id_proyecto);
		//$descrip_compo_proy=$descrip_compo_proy;
		if($id_proyecto>=100000){
			//si es tramo
			$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
			$idProyecto = $data_proyecto->id_proyecto;
		}
		else{
			$data_proyecto = $this->metas_model->proy($id_proyecto);
			$idProyecto = $id_proyecto;
		}
		//print_r($id_proyecto);
		//$id_proyecto=$id_proyecto;
		$inicio=$this->segui_actividad_model->inicio_proyect($id_proyecto);
		$anio_inicio=$this->segui_actividad_model->anio_inicio_proyect($inicio);
		$fin=$this->segui_actividad_model->fin_proyect($id_proyecto);
		$anio_fin=$this->segui_actividad_model->anio_fin_proyect($fin);

		$data=array(
			'id_proyecto'=>$id_proyecto,
			'proyectod'=> $data_proyecto,
			'proyecto'=>$this->segui_actividad_model->nombreProy($idProyecto),
			'fisico'=>$this->segui_actividad_model->traer_fisico($id_proyecto),
			'inicio'=>$anio_inicio,
			'fin'=>$anio_fin,
			'falta_porcentaje'=>$this->segui_actividad_model->falta_porcentaje($id_proyecto)
		);
		//print_r($id_proyecto);
    	$this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/seguimientos/segui_fisico',$data);
		$this->load->view('layouts/footer');
	}

	public function ing_seg_fin($id_proyecto,$descrip_compo_proy,$mes,$gestion){
		$descrip_compo_proy=str_replace("%20", " ", $descrip_compo_proy);
		//$descrip_compo_proy=$descrip_compo_proy;
		$id_proyecto=$id_proyecto;
		$mes=$mes;
		$gestion=$gestion;
		$inicio=$this->segui_actividad_model->inicio_proyect($id_proyecto);
		$anio_inicio=$this->segui_actividad_model->anio_inicio_proyect($inicio);
		$fin=$this->segui_actividad_model->fin_proyect($id_proyecto);
		$anio_fin=$this->segui_actividad_model->anio_fin_proyect($fin);

		$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
		//$contrato=$this->segui_actividad_model->contratoG($id_proyecto,$descrip_compo_proy)
		$estado = $this->segui_actividad_model->verificarSeguimientoFinanciero($id_proyecto, $descrip_compo_proy, $mes, $gestion);
		
		$data=array(
			'id_proyecto'=>$id_proyecto,
			'proyectod'=> $data_proyecto,
			'mes'=>$mes,
			'gestion'=>$gestion,
			'descrip_compo_proy'=>$descrip_compo_proy,
			'proyecto'=>$this->segui_actividad_model->actividad($id_proyecto,$descrip_compo_proy),
			'gestion2'=>$this->segui_actividad_model->proy_gestion2($id_proyecto,$descrip_compo_proy),
			'contrato'=>$this->segui_actividad_model->contratoG($id_proyecto,$descrip_compo_proy),
			//'monto_ejecutado_mes'=>$this->segui_actividad_model->traer_mes_monto($id_proyecto,$mes,$descrip_compo_proy),
			//'monto_ejecutado_gestion'=>$this->segui_actividad_model->costo_ejecutado($id_proyecto,$descrip_compo_proy),
			'inicio'=>$anio_inicio,
			'fin'=>$anio_fin,
			'estado' => $estado 
			//'inicio'=>$this->segui_actividad_model->inicio_proyect($id_proyecto),
			//'unidad_metrica'=>$this->segui_actividad_model->unidad(),
		);
        $this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/seguimientos/ingreso_seg_fin',$data);
		$this->load->view('layouts/footer');
	}


	public function ing_pro_fin($id_proyecto){
		//$descrip_compo_proy=$descrip_compo_proy;
		$id_proyecto=$id_proyecto;
		$inicio=$this->segui_actividad_model->inicio_proyect($id_proyecto);
		$anio_inicio=$this->segui_actividad_model->anio_inicio_proyect($inicio);
		$fin=$this->segui_actividad_model->fin_proyect($id_proyecto);
		$anio_fin=$this->segui_actividad_model->anio_fin_proyect($fin);

		$data=array(
			'id_proyecto'=>$id_proyecto,
			'proyecto'=>$this->segui_actividad_model->nombreProy($id_proyecto),
			'gestion1'=>$this->segui_actividad_model->proy_gestion($id_proyecto),
			'inicio'=>$anio_inicio,
			'fin'=>$anio_fin,


			//'inicio'=>$this->segui_actividad_model->inicio_proyect($id_proyecto),
			//'unidad_metrica'=>$this->segui_actividad_model->unidad(),
		);
    $this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/seguimientos/ingreso_programacion_financiera',$data);
		$this->load->view('layouts/footer');
	}
	public function indexCopia(){
	    $id=array();
	    $id=$this->session->userdata('id');
		$data['results'] =  $this->segui_actividad_model->index($id);
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec');
		$this->load->view('admin/proyectos/segui_proy_actividad',$data);
		$this->load->view('layouts/footer');
	}
	
	public function ingreso1($id_tramo,$id_proyecto,$id_componente_tramo){
		$id_tramo=$id_tramo;
		$id_proyecto=$id_proyecto;
		$id_componente_tramo=$id_componente_tramo;
	    $data=array(
			'id_proyecto'=>$id_proyecto,
			'id_tramo'=>$id_tramo,
			'id_componente_tramo'=>$id_componente_tramo,
			'proyecto'=>$this->segui_actividad_model->dato($id_proyecto),
			'unidad_metrica'=>$this->segui_actividad_model->unidad(),
		);
		$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/seguimientos/ingreso_segui_finan',$data);
		$this->load->view('layouts/footer');

	}

	public function store_fisico(){
		$descrip_compo_proy=$this->input->post('descrip_compo_proy');
		$id_proyecto=$this->input->post('id_proyecto');
		$mes=$this->input->post('mes');
		$gestion=$this->input->post('gestion');
		$monto_ejecutado=$this->input->post("monto_actividad");
		$monto_ejecutado = str_replace(',', '', $monto_ejecutado);
		$this->form_validation->set_rules("mes", "Mes de seguimiento del Proyecto","trim|required");
		$this->form_validation->set_rules("gestion", "Gestion de seguimiento del Proyecto","trim|required");
		$this->form_validation->set_rules("monto_actividad", "Monto de seguimiento del Proyecto","trim|required");

		$this->form_validation->set_message("required", "El campo %s es requerido");
		$this->form_validation->set_message("numeric", "El campo %s es numerico");
		$this->form_validation->set_message("decimal", "El campo %s es numerico");
		$this->form_validation->set_message("is_natural", "El campo %s debe contener solo numeros positivos mayores a cero");
		if ($this->form_validation->run()){
					$data = array(
						'id_proyecto'=>$id_proyecto,
						'mes'=>$mes,
						'gestion'=>$gestion,
						'monto_ejecutado'=>$monto_ejecutado,
						'descrip_compo_proy'=>$descrip_compo_proy,

						);
						$this->segui_actividad_model->insert_financiero($data);
						$descrip_compo_proy=$descrip_compo_proy;
						$this->ing_seg_fin($id_proyecto,$descrip_compo_proy,$mes,$gestion);
				}else{						
						$this->ing_seg_fin($id_proyecto,$descrip_compo_proy,$mes,$gestion);
		}
	}
			
	public function store_fisico1(){
				$id_proyecto=$this->input->post('id_proyecto');
				$mes=$this->input->post('mes');
				$gestion=$this->input->post('anio');
				$porcentaje=$this->input->post('porcentaje');
				$resultado_mes=$this->input->post('resultado_mes');
				$problemas_mes=$this->input->post('problemas_mes');
				$acciones_solucion=$this->input->post('acciones_solucion');

				$this->form_validation->set_rules("mes", "Mes de seguimiento del Proyecto","trim|required");
				$this->form_validation->set_rules("anio", "Año de seguimiento del Proyecto","trim|required");
				$this->form_validation->set_rules("porcentaje", "porcentaje de seguimiento fisico del Proyecto","trim|required");
				$this->form_validation->set_rules("resultado_mes", "Resultado del seguimiento en el mes","trim|required");
				$this->form_validation->set_rules("problemas_mes", "Problemas presentados en el seguimiento del Proyecto en el mes","trim|required");
				$this->form_validation->set_rules("acciones_solucion", "Acciones que se tomaron para solucionar el problema en el mes","trim|required");

				$this->form_validation->set_message("required", "El campo %s es requerido");
				$this->form_validation->set_message("numeric", "El campo %s es numerico");
				$this->form_validation->set_message("decimal", "El campo %s es numerico");
				$this->form_validation->set_message("is_natural", "El campo %s debe contener solo numeros positivos mayores a cero");
				if ($this->form_validation->run()){
							$data = array(
								'id_proyecto'=>$id_proyecto,
								'mes'=>$mes,
								'gestion'=>$gestion,
								'porcentaje'=>$porcentaje,
								'resultado_mes'=>$resultado_mes,
								'problemas_mes'=>$problemas_mes,
								'acciones_solucion'=>$acciones_solucion,
							);
								//subir archivo
								if (isset($_FILES['imagen'])){
									$id = $this->segui_actividad_model->insert_fisico($data);
									$carpeta 	= "imagenes/seguifisico/";
									$ext = (explode("/",$_FILES['imagen']['type']));
									$imagen = $id.".".$ext[1];
									move_uploaded_file($_FILES['imagen']['tmp_name'], "$carpeta$imagen");
									$data['ruta']="imagenes/seguifisico/".$imagen;
									$this->segui_actividad_model->updateSeguiFisico($id,$data);
									redirect(base_url()."proyecto/seguir_fisico_controller/ingreso2/$id_proyecto");
								}else{
									//echo '-------------error no existe imagen-----------';
									$this->ingreso2($id_proyecto);
								}
						}else{
									//echo '-------------error repetidos-----------';
									$this->ingreso2($id_proyecto);
						}
			}

					public function ing_seg_fis($id_proyecto)
					{
						//$descrip_compo_proy=$descrip_compo_proy;
						$id_proyecto=$id_proyecto;
						$inicio=$this->segui_actividad_model->inicio_proyect($id_proyecto);
						$anio_inicio=$this->segui_actividad_model->anio_inicio_proyect($inicio);
						$fin=$this->segui_actividad_model->fin_proyect($id_proyecto);
						$anio_fin=$this->segui_actividad_model->anio_fin_proyect($fin);

						$data=array(
							'id_proyecto'=>$id_proyecto,
							'proyecto'=>$this->segui_actividad_model->nombreProy($id_proyecto),
							'gestion1'=>$this->segui_actividad_model->proy_gestion($id_proyecto),
							'inicio'=>$anio_inicio,
							'fin'=>$anio_fin,


							//'inicio'=>$this->segui_actividad_model->inicio_proyect($id_proyecto),
							//'unidad_metrica'=>$this->segui_actividad_model->unidad(),
						);
				    $this->load->view('layouts/header');
						$this->load->view('layouts/menugestor');
						$this->load->view('admin/seguimientos/ingreso_seguimiento_fisico',$data);
						$this->load->view('layouts/footer');
					}
			public function store_gestion()
			{
				//$descrip_compo_proy=$this->input->post('descrip_compo_proy');
				$id_proyecto=$this->input->post('id_proyecto');
				//$mes=$this->input->post('mes');
				$gestion=$this->input->post('gestion');
				$monto_proyectado=$this->input->post('monto_proyectado');
				//$id_proyecto=$this->proyectos_model->current_num();

				/*echo $area.' '.$id_subarea.' '.$tipo_fase.' '.$nombre_proyecto.' '.$costo_proyecto.' '.$tipo_proyecto.' '.$modalidad.' '.$fecha_inicio.' '.$fecha_conclusion.' '.$duracion_anos.' '.$descripcion_proyecto.' '.$objetivo_proyecto.' '.$id_proyecto;*/
				//$this->form_validation->set_rules("mes", "Mes de seguimiento del Proyecto","trim|required");
				$this->form_validation->set_rules("gestion", "Año de seguimiento del Proyecto","trim|required");
				$this->form_validation->set_rules("monto_proyectado", "Monto de proyectado del Proyecto","trim|required");

				$this->form_validation->set_message("required", "El campo %s es requerido");
				$this->form_validation->set_message("numeric", "El campo %s es numerico");
				$this->form_validation->set_message("decimal", "El campo %s es numerico");
				$this->form_validation->set_message("is_natural", "El campo %s debe contener solo numeros positivos mayores a cero");
				if ($this->form_validation->run()){
						$data = array();
						$data['id_proyecto']=$id_proyecto;
						$data['gestion']=$gestion;
						$data['monto_proyectado']=$monto_proyectado;
						//'mes'=>$mes,
						//'gestion'=>$gestion,
						//'monto_proyectado'=>$monto_proyectado,
						$this->segui_actividad_model->insert($data);
						redirect(base_url()."proyecto/seguir_fisico_controller/ing_pro_fin/$id_proyecto");
				}else	{

						echo '-------------error repetidos-----------';
						/// cambiar este metodo
						$this->ing_pro_fin($id_proyecto);
						//redirect(base_url()."proyecto/seguir_fisico_controller/ing_pro_fin/$id_proyecto");
						//$this->index();
				}

			}

			public function edit($id_progFinan,$id_proyecto, $gestion)
			{

			   $data=array(
			        //'metas2'=>$this->segui_actividad_model->pro_ges($id_progFinan),
			        //'metas1'=>$this->segui_actividad_model->proy_gestion($id_proyecto),
					'id_progFinan'=>$id_progFinan,
					'proyecto'=>$this->segui_actividad_model->proy($id_proyecto),
					'gestion'=>$this->segui_actividad_model->proy_gestion($gestion),
					'monto_proyectado'=>$this->segui_actividad_model->monto($monto_proyectado),);
		        $this->load->view('layouts/header');
				$this->load->view('layouts/menugestor');
				$this->load->view('admin/proyectos/registro_gestion_edit',$data);
				$this->load->view('layouts/footer');

			}
			public function delete($id_progFinan,$id_proyecto)
			{
				 $id_progFinan=$id_progFinan;
				 $id_proyecto=$id_proyecto;
				 	$this->segui_actividad_model->delete($id_progFinan);
				 	redirect(base_url()."proyecto/Seguir_fisico_controller/ing_pro_fin/$id_proyecto");
			}
			public function delete1($id_segui_finan,$id_proyecto,$descrip_compo_proy,$mes,$gestion)
			{
				 $id_segui_finan=$id_segui_finan;
				 $id_proyecto=$id_proyecto;
				 $descrip_compo_proy=$descrip_compo_proy;
				 $this->segui_actividad_model->delete1($id_segui_finan);
				 $this->ing_seg_fin($id_proyecto,$descrip_compo_proy,$mes,$gestion);
				 //redirect(base_url()."proyecto/Seguir_fisico_controller/ing_seg_fin/$id_proyecto/$descrip_compo_proy");
			}
			public function deleteFisico($id_seguimiento_fisico,$id_proyecto)
			{
				 $id_seguimiento_fisico=$id_seguimiento_fisico;
				 $id_proyecto=$id_proyecto;
				 $this->segui_actividad_model->deleteFisico($id_seguimiento_fisico);
				 redirect(base_url()."proyecto/Seguir_fisico_controller/ingreso2/$id_proyecto");
			}


}
