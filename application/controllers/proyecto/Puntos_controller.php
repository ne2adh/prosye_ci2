<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Puntos_controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
        $this->load->library('form_validation');
    }
	public function index()
	{
		$id=array();
		$id=$this->session->userdata('id');
        $data['results'] =  $this->proyectos_model->get_proyectos_mios($id);
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec');
		$this->load->view('admin/proyectos/mis_proy_puntos',$data);
		$this->load->view('layouts/footer');
	}

	public function ingresoPuntos($id_proyecto){
		if($id_proyecto>=100000){
			$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
			//recuperar tramos
			$idProyecto =$data_proyecto->id_proyecto;
		}
		else{
			$data_proyecto = $this->convenio_model->traerProy($id_proyecto);
			$idProyecto = $id_proyecto;
		}

		$data=array(
					'punto'=> $this->puntos_model->puntos($id_proyecto),
					'id_proyecto'=> $id_proyecto,
				  'proyecto'=> $data_proyecto
			);
		$this->load->view('layouts/header');
		$tramos = array("tramos" => $this->tramo_model->index($idProyecto));
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/proyectos/puntos',$data);
		$this->load->view('layouts/footer');
	}

	//funcion validadora
	public function validaDecimal($valor) {
		if (preg_match('/\d{6}\.\d{2}/', $valor)) {
				return TRUE;
			}
			else
			{
				return FALSE;
			}
	}
	public function validaDecimale($valor) {
	  if (preg_match('/\d{7}\.\d{2}/', $valor)) {		  
		    return TRUE;
		}
		else{
			
		  	return FALSE;
		}
	}

	public function store(){
	//print_r ($this->ToLL(7201652.67, 436512.99,21));

	$id_coordenada=$this->input->post("id_coordenada");
	$id_proyecto=$this->input->post("id_proyecto");
	$latitud=$this->input->post("latitud");
	$longitud=$this->input->post("longitud");
	$norte=$this->input->post("utm");
	$este=$this->input->post("utme");

	//$this->form_validation->set_rules("latitud", "Latitud del Proyecto","trim|decimal");
	//$this->form_validation->set_rules("longitud", "Longitud del Proyecto","trim|decimal");
	$this->form_validation->set_rules("utm", "Coordenada Este del Proyecto","required|trim|decimal|callback_validaDecimal");
	$this->form_validation->set_rules("utme", "Coordenada Norte del Proyecto","required|trim|decimal|callback_validaDecimale");

	$this->form_validation->set_message("required", "El campo %s es obligatorio");
	$this->form_validation->set_message("decimal", "El campo %s debe ser decimal");
	$this->form_validation->set_message("validaDecimal", "Introduce 6 digitos, el punto decimal y dos decimales");
	$this->form_validation->set_message("validaDecimale", "Introduce 7 digitos, el punto decimal y dos decimales");

	if ($this->form_validation->run()){
			//$convenio=$this->input->post("convenio");
			$data=array();
			$data['id_coordenada'] = $this->input->post("id_coordenada");
			$data['id_proyecto'] = $this->input->post("id_proyecto");
			$data['latitud'] = $this->input->post("latitud");
			$data['longitud'] = $this->input->post("longitud");
			$data['norte']= $this->input->post("utm");
			$data['este']= $this->input->post("utme");

			$resp=0;
			//$resp=$this->puntos_model->compara($id_proyecto, $latitud, $longitud, $utm);
			if ($resp!=1){

				$data1['id_proyecto']=$id_proyecto;
				$data1['pant']=5;

				$this->proyectos_model->update_pant($data1,$id_proyecto);
				$this->puntos_model->insertPuntos($data);
				redirect(base_url()."proyecto/puntos_controller/ingresoPuntos/$id_proyecto");
				//redirect(base_url()."proyecto/municipio_controller/ingreso/$id_proyecto");
			} else {

				echo '-------------puntos repetidos-----------';

				$this->ingresoPuntos($id_proyecto);
			}
	} else {


			$this->ingresoPuntos($id_proyecto);
	}
}

	public function delete($id,$id_proyecto)
	{
		 $id=$id;
		 $id_proyecto=$id_proyecto;
		 	$this->puntos_model->delete($id,$id_proyecto);
		 	redirect(base_url()."proyecto/puntos_controller/ingresoPuntos/$id_proyecto");
	}
	public function edit($id_coordenada,$id_proyecto)
	{
		if($id_proyecto>=100000){
			$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
			//recuperar tramos
			$idProyecto =$data_proyecto->id_proyecto;
		}
		else{
			$data_proyecto = $this->convenio_model->traerProy($id_proyecto);
			$idProyecto = $id_proyecto;
		}
		$data=array(
					'id_coordenada'=>$id_coordenada,
					'puntos'=> $this->puntos_model->traerPunto($id_coordenada,$id_proyecto),
					'punto'=> $this->puntos_model->puntos($id_proyecto),
					'id_proyecto'=> $id_proyecto,
				  'proyecto'=> $data_proyecto,
		);
		$tramos = array("tramos" => $this->tramo_model->index($idProyecto));
	  $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/proyectos/puntosEdit',$data);
		$this->load->view('layouts/footer');
	}
	public function update()
	{
		$id_coordenada=$this->input->post("id_coordenada");
		$id_proyecto=$this->input->post("id_proyecto");
		$latitud=$this->input->post("latitud");
		$longitud=$this->input->post("longitud");
		$norte=$this->input->post("utm");
		$este=$this->input->post("utme");

		$this->form_validation->set_rules("latitud", "Latitud del Proyecto","trim|decimal");
		$this->form_validation->set_rules("longitud", "Longitud del Proyecto","trim|decimal");

		$this->form_validation->set_message("requiered", "El campo %s es negativo y decimal");
		$this->form_validation->set_message("decimal", "El campo %s es negativo y decimal");
		//$this->form_validation->set_message("less_than", "El campo %s debe contener un numero entre -17.993320 y -17.9733200");
		//$this->form_validation->set_message("greater_than", "El campo %s debe contener un numero entre -67.9999999 y -67.1000015 ");
		if ($this->form_validation->run())
        {
				//$convenio=$this->input->post("convenio");
				$data=array();
				$data['id_proyecto'] = $this->input->post("id_proyecto");
				$data['latitud'] = $this->input->post("latitud");
				$data['longitud'] = $this->input->post("longitud");
				$data['norte']=$this->input->post("utm");
				$data['este']=$this->input->post("utme");
				$data1['id_proyecto']=$id_proyecto;
				$data['norte'] = $this->input->post("utm");
				$data['este'] = $this->input->post("utme");
				$this->puntos_model->updatePuntos($id_coordenada,$data);
				redirect(base_url()."proyecto/puntos_controller/ingresoPuntos/$id_proyecto");
				//redirect(base_url()."proyecto/municipio_controller/ingreso/$id_proyecto");
		} else {
				$this->ingresoPuntos($id_proyecto);
		}
	}



}
