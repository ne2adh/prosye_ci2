<?php
if(!defined('BASEPATH'))  exit('No direct script access allowed');

class Componente_controller extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('date','url');


	}
	public function index()
	{        
	   $id=array();
	   $id_proyecto=$this->input->post('id_proyecto');
	   //$id_tramo=$this->input->post('id_tramo');
		$data['results'] =  $this->componente_model->index($id);    
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec');
		$this->load->view('admin/proyectos/mis_proy_componente',$data);
		$this->load->view('layouts/footer');
	}
	
	public function ingreso()
	{
		$id_proyecto=$this->input->post('id_proyecto');
		$id_tramo=$this->input->post('id_tramo');
		$id_tipo=$this->input->post('id_tipo');
		$data=array();
		$data = array( 
		'actividad'=> $this->componente_model->getComponenteProy($id_proyecto),
		'tramo_compo'=> $this->componente_model->getComponente($id_tramo),
		'componente'=> $this->componente_model->getCompoActividad($id_tramo),
		'id_tramo'=>$id_tramo,
		'id_proyecto'=>$id_proyecto,
		'id_tipo'=>$id_tipo,
		'tipo'=>$this->componente_model->getTipo($id_proyecto),
		'proyecto'=> $this->proyectos_model->getProyecto($id_proyecto),
		);
        
	    $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec');
		$this->load->view('admin/proyectos/componente_view',$data);
		$this->load->view('layouts/footer');


	}
	
	public function ingresoComponente($id_tramo,$id_proyecto)
	{
		$id_tramo=$id_tramo;
		$id_proyecto=$id_proyecto;
		$data = array( 
		'id_tramo'=> $id_tramo,
		'proyecto'=>$this->proyectos_model-> getProyecto($id_proyecto),
		'componente'=> $this->componente_model->getCompoActividad($id_tramo),
		'tramo_compo'=> $this->componente_model->getComponente($id_tramo),
		'actividad'=> $this->componente_model->getComponenteProy($id_proyecto),
		'tipo'=> $this->componente_model->getTipo($id_proyecto),
		);
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec');
		$this->load->view('admin/proyectos/componente_view',$data);
		$this->load->view('layouts/footer');


	}


	
	public function store()
	{	
		$id_tramo=$this->input->post('id_tramo');
		$id_proyecto=$this->input->post('id_proyecto');
		$data= array();
		$data['id_tramo']=$id_tramo;
		$data['id_proyecto']=$id_proyecto;
		$data['descripcion_componente']=$this->input->post("descripcion");
		$data['monto']=$this->input->post("monto");
		$data['descrip_componente_proy']=$this->input->post("descrip_compo_proy");
		$this->componente_model->inser($data);
		redirect(base_url()."proyecto/componente_controller/ingresoComponente/$id_tramo/$id_proyecto");
			
	}
	public function delete($id_componente_tramo,$id_proyecto,$id_tramo)
	{
		 $id=$id_componente_tramo;
		 $id_proyecto=$id_proyecto;
		 $id_tramod=$id_tramo;
		 
		 	$this->componente_model->delete($id);
		 	redirect(base_url()."proyecto/componente_controller/ingreso/$id_tramo/$id_proyecto");

		 
	}

	
	public function edit($id_componente_tramo,$id_proyecto,$id_tramo)
	{
		$id_componente_tramo=$id_componente_tramo;
		$id_tramo=$id_tramo;
		$id_proyecto=$id_proyecto;
	   $data=array(
	         'tramo_compo'=>$this->componente_model->tramo($id_tramo),
	         'compo'=>$this->componente_model->compo($id_componente_tramo),
	         'componente'=>$this->componente_model->componente($id_componente_tramo),
	         'id_componente_tramo'=>$id_componente_tramo, 
	         'id_proyecto'=>$id_proyecto,
	         'id_tramo'=>$id_tramo,
		);
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/proyectos/registro_componente_edit',$data);
		$this->load->view('layouts/footer');

	}

	public function update()
	{	
		$id_componente_tramo=$this->input->post("id_componente_tramo");
		$id_tramo=$this->input->post("id_tramo");
		$id_proyecto=$this->input->post("id_proyecto");
		$data= array();
		$data['id_tramo']=$id_tramo;
		$data['id_proyecto']=$id_proyecto;
		$data['descripcion']=$this->input->post("descripcion");
		$data['monto']=$this->input->post("monto");
		$data['fecha_inicio']=$this->input->post("fecha_inicio");
		$data['fecha_fin']=$this->input->post("fecha_fin");
		$data['tiempo_evaluacion']=$this->input->post("tiempo_evaluacion");
		$data['tiempo_ejecucion']=$this->input->post("tiempo_ejecucion");
		$this->componente_model->update($id_componente_tramo,$data);
		redirect(base_url()."proyecto/componente_controller/ingreso/$id_tramo/$id_proyecto");
			
	}




}	