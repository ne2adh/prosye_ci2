<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mis_proyectos extends CI_Controller{
	
	private $permisos;
	public function __construct(){
		parent::__construct();
        //$this->permisos=$this->backend_lib->control();
        $this->load->library('form_validation','session');
        $this->load->helper(array('url', 'form'));
        $this->load->helper('form','date');
	}

	public function index(){
		$id=array();
		//$id=$_GET['id'];
        $data['results'] =  $this->proyectos_model->get_proyectos_espe($id);
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec');
		$this->load->view('admin/proyectos/mis_proy',$data);
		$this->load->view('layouts/footer');

	}

	public function propios(){
		$id=array();
		//$id=$_GET['id'];||
		$id=$this->session->userdata('id');
		$data['results'] =  $this->proyectos_model->get_proyectos_mios($id);
        //$data['control'] =  $this->proyectos_model->get_pant();
        $this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/proyectos/mios',$data);
		$this->load->view('layouts/footer');
	}

	public function revisionProyectos(){
		$id=array();		
		$id=$this->session->userdata('id');		
		//recuperar id_secretaria de usuario
		$id_area = $this->usuarios_model->getId_area($id);
		$id_rol = $this->usuarios_model->getId_rol($id);
		
		if($id_rol == 19 || $id_rol == 22){//usuario planificacion
			//recuperar lista de proyectos segun area
			$data['results'] =  $this->proyectos_model->getProyectos();
		}
		else{
			//recuperar lista de proyectos segun area
			$data['results'] =  $this->proyectos_model->getProyectos_area($id_area);
		}
		
        $this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/proyectos/revisionProyectos',$data);
		$this->load->view('layouts/footer');
	}

	public function listaRevision(){
		//Listar proyectos para revision por el Inmediato superior
		/*
			1. Recuperar el id del usuario
			2. 
		*/		
		$this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		//	$this->load->view('admin/proyectos/mios',$data);
		$this->load->view('layouts/footer');
	}

	public function menu($id_proyecto){
		$id=$id_proyecto;
		$proyecto=$this->proyectos_model->getProyecto($id_proyecto);
		/*
		Buscar si el proyecto tiene tramos -> codigo_tramo = 1
		1. si tiene tramos
		2. recuperar los tramos
		3. enviar a aside_proyec para generar las entradas dinamicamente
		*/
		$codigo_tramo = $proyecto->codigo_tramo;
		$tramos = array("tramos" => $this->tramo_model->index($id));

        $data=$this->proyectos_model->get_ruta($id);
        //echo ($data);
        switch ($data) {
			case '1':
					//convenio
					$data = array('proyecto'=>$this->convenio_model->traerProy($id_proyecto),
								'id_proyecto'=>$id_proyecto,
								);
					if ($proyecto->convenio=='SI'){
					$this->load->view('layouts/header');
					$this->load->view('layouts/aside_proyec', $tramos);
					$this->load->view('admin/proyectos/convenio_view',$data);
					$this->load->view('layouts/footer');
					}
					else{
						redirect(base_url()."proyecto/metas_controller/ingresoMetas/$id_proyecto");

					}
					break;


			case '2':
				//metas
        		$data=array(

							'metas1'=>$this->metas_model->metas($id_proyecto),
							'id_proyecto'=>$id_proyecto,
							'proyecto'=>$this->metas_model->proy($id_proyecto),
							'unidad'=>$this->metas_model->unidad_proy(),
							'metas'=>$this->metas_model->metas_proy(),
							);

				$this->load->view('layouts/header');
				$this->load->view('layouts/aside_proyec', $tramos);
				$this->load->view('admin/proyectos/registro_metas',$data);
				$this->load->view('layouts/footer');
        		break;

			case '3':
				//municipio
        		$data = array(
							'proyecto'=> $this->municipio_model->proyecto($id_proyecto),
							'd_municipio'=> $this->municipio_model->det_municipio($id_proyecto),
							'provincias'=>$this->municipio_model->provincia(),
							);
				$this->load->view('layouts/header');
				$this->load->view('layouts/aside_proyec', $tramos);
				$this->load->view('admin/proyectos/registro_municipio_view',$data);
				$this->load->view('layouts/footer');
        		break;
			case '4':
				//puntos
        		$data=array(
							'punto'=> $this->puntos_model->puntos($id_proyecto),
						    'id_proyecto'=> $id_proyecto,
						    'proyecto'=>$this->convenio_model->traerProy($id_proyecto),
									);
			    $this->load->view('layouts/header');
				$this->load->view('layouts/aside_proyec', $tramos);
				$this->load->view('admin/proyectos/puntos',$data);
				$this->load->view('layouts/footer');
        		break;
			case '5':
					//componente
        			//$id_proyecto=$this->input->post('id_proyecto');
					$data = array(
							'proyecto'=>$this->componente_model->getProyecto($id_proyecto),
							'componente'=>$this->componente_model->getComponenteProy($id_proyecto),
							'conven'=>$this->componente_model->getConvenioProy($id_proyecto),
							'sw'=>0,
							);
			        $this->load->view('layouts/header');
					$this->load->view('layouts/aside_proyec', $tramos);
					$this->load->view('admin/proyectos/componente_proyecto_view',$data);
					$this->load->view('layouts/footer');
        		break;
			case '6':
					//indicador
	        			$data=array('indicador'=>$this->indicador_model->indicador($id_proyecto),
						'id_proyecto'=>$id_proyecto,
						'proyecto'=>$this->indicador_model->traerProy($id_proyecto),
					    );

			        $this->load->view('layouts/header');
					$this->load->view('layouts/aside_proyec', $tramos);
					$this->load->view('admin/proyectos/registro_indicador',$data);
					$this->load->view('layouts/footer');
	        		break;
        }
	}

    public function esperaSisin(){
    	$id=$this->session->userdata('id');
        $date1['results'] = $this->proyectos_model->miosSisin($id);
        $this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/proyectos/miosSinSisin',$date1);
		$this->load->view('layouts/footer');

	}

    public function conSisin(){
    	$id=$this->session->userdata('id');
        $date1['results'] = $this->proyectos_model->miosConSisin($id);
        $this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/proyectos/miosConSisin',$date1);
		$this->load->view('layouts/footer');
	}

    public function ingresados(){
    	//$id=$this->session->userdata('id');
        $date1['results'] = $this->proyectos_model->ingresoSisin();
        $this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/proyectos/mios',$date1);
		$this->load->view('layouts/footer');
    }

    public function actualizarAlta($id_proyecto){

		$data=$this->proyectos_model->get_Sisin($id_proyecto);
		$data3=$this->proyectos_model->get_ruta($id_proyecto);
		//echo $data,'    ',$data3;

		if ( ($data=='1') && ($data3=='6') )
		{//echo $data,$data3;
				$id_proyecto=$id_proyecto;
				$id=$this->session->userdata('id');
				$date1['results'] = $this->proyectos_model->miosSisin($id);
				$date=$this->proyectos_model->update_alta($id_proyecto);
				$id_proyecto=$date->id_proyecto;
				$id_secretaria=$date->id_secretaria;
				$id_area=$date->id_area;
				$id_subarea=$date->id_subarea;
				$fase=$date->fase;
				$nombre_proyecto=$date->nombre_proyecto;
				$costo_proyecto=$date->costo_proyecto;
				$tipo_proyecto=$date->tipo_proyecto;
				$modalidad='';
				$convenio=$date->convenio;
				$fecha_inicio=$date->fecha_inicio;
				$fecha_conclusion=$date->fecha_conclusion;
				$duracion_anos=$date->duracion_anos;
				$descripcion_proyecto=$date->descripcion_proyecto;
				$objetivo_proyecto=$date->objetivo_proyecto;
				$estado=$date->estado;
				$alta=-1;
				//echo $id_proyecto,'  ',$id_secretaria,'  ',$id_area,'  ',$id_subarea,'  ',$fase,'  ',$nombre_proyecto,'  ',$costo_proyecto,'  ',$tipo_proyecto,'  ',$convenio,'  ',$fecha_inicio,'  ',$fecha_conclusion,'  ',$duracion_anos,'  ',$descripcion_proyecto,'  ',$objetivo_proyecto,'  ',$estado,'  ',$alta;

				$data1 = array(
								'id_proyecto'=>$id_proyecto,
								'id_secretaria'=>$id_secretaria,
								'id_area'=>$id_area,
								'id_subarea'=>$id_subarea,
								'fase'=>$fase,
								'nombre_proyecto'=>$nombre_proyecto,
								'costo_proyecto'=>$costo_proyecto,
								'tipo_proyecto'=>$tipo_proyecto,
								'modalidad'=>'',
								'convenio'=>$convenio,
								'fecha_inicio'=>$fecha_inicio,
								'fecha_conclusion'=>$fecha_conclusion,
								'duracion_anos'=>$duracion_anos,
								'descripcion_proyecto'=>$descripcion_proyecto,
								'objetivo_proyecto'=>$objetivo_proyecto,
								'estado'=>$estado,
								'alta'=>$alta,
								);

				$date2=$this->proyectos_model->update_alta1($id_proyecto,$data1);
				redirect(base_url()."proyecto/mis_proyectos/propios");
				//echo 'el codigo'.$data;
		}
		else{
		 	redirect(base_url()."proyecto/mis_proyectos/propios");
		}
	}

	public function todos()
	{
		$data =array('proyectos'=>$this->proyectos_model->getProyectos());
		$this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/proyectos/list',$data);
		$this->load->view('layouts/footer');

	}

	public function area_controller()
	{
		    $data= array(
				'areas'=>$this->proyectos_model->get_area(),
				'usuario'=> $this->proyectos_model->get_proyectos_espe_convenio(),
			);
        	$this->load->view('layouts/header');
			$this->load->view('layouts/menugestor');
			$this->load->view('admin/proyectos/proyecto_nuevo',$data);
			$this->load->view('layouts/footer');
	}

	public function get_subarea()
	{
		$secre=$this->input->post('cod');
		if($secre)
		{
			$subareas=$this->proyectos_model->get_subarea_model($secre);
			echo '<option value="" >Area</option>';
			foreach($subareas as $subarea):?>
				 	<option value="<?php echo $subarea->cod;?>"><?php echo $subarea->nombre;?></option>';
			<?php endforeach;
		}
	}

	public function get_subarea3()
	{
		if($this->input->post('cod'))
		{
			echo $this->proyectos_model->get_subarea_model3($this->input->post('cod'));
		}
	}
	public function tiempo()
	{
		$fechaInicio=$this->input->POST('fecha_inicio');
		$fechaFin=$this->input->POST('fecha_conclusion');

	//$fechaInicio=$_POST['fecha1'];
	//$fechaFin=$_POST['fecha2'];
	$r=$this->proyectos_model->calculaTiempo($fechaInicio,$fechaFin);
	echo ($r);

	}

	public function store(){

		//obtener datos del formulario para la tabla proyectos
		$f1=$this->input->post('fecha_inicio');
		$f2=$this->input->post('fecha_conclusion');
		if(isset($f1) && $f1!=""){
			$vfini = explode("-", $f1);
			$f1 = $vfini[0].'-'.$vfini[1].'-01';
		}

		if(isset($f2) && $f2!=""){
			$vffin = explode("-", $f2);
			$dia_fin = cal_days_in_month(CAL_GREGORIAN, $vffin[1], $vffin[0]); // 31
			$f2 = $vffin[0].'-'.$vffin[1].'-'.$dia_fin;
		}

		$rol_id=$this->input->post("rol_id");
		$id_proyecto=$this->proyectos_model->current_num();
		$id_secretaria=$this->input->post("area");
		$area=$this->input->post("subarea");
		$id_subarea=$this->input->post("sub_area3");
		$tipo_fase=$this->input->post("tipo_fase");
		$nombre_proyecto=$this->input->post("nombre_proyecto");
		$costo_proyecto=$this->input->post("costo_proyecto");
		$tipo_proyecto=$this->input->post("tipo_proyecto");
		$modalidad=$this->input->post("modalidad");
		$convenio=$this->input->post("convenio");
		$fecha_inicio=$this->input->post("fecha_inicio");
		$fecha_conclusion=$this->input->post("fecha_conclusion");
		$duracion_anos=$this->proyectos_model->tiempo($f1,$f2);
		$descripcion_proyecto=$this->input->post("descripcion_proyecto");
		$objetivo_proyecto=$this->input->post("objetivo_proyecto");
		$codigo_tramo=$this->input->post("codigo_tramo");
		$numero_tramo=$this->input->post("numero_tramo");
		$costo_proyecto=str_replace(',','',$costo_proyecto);
		//validar datos
		$this->form_validation->set_rules("nombre_proyecto", "Nombre del Proyecto","trim|required|is_unique[proyectos.nombre_proyecto]");
		$this->form_validation->set_rules("area", "Secretaria","required");
		$this->form_validation->set_rules("subarea", "Area","required");
		$this->form_validation->set_rules("modalidad", "Modalidad","trim|required");
		$this->form_validation->set_rules("tipo_fase", "Fase","required");
		$this->form_validation->set_rules("costo_proyecto", "Costo del Proyecto","trim|required");
		$this->form_validation->set_rules("tipo_proyecto", "Tipo del Proyecto","required");
		$this->form_validation->set_rules("convenio", "Convenio del Proyecto","required");
		$this->form_validation->set_rules("fecha_inicio", "Fecha de Inicio","required");
		$this->form_validation->set_rules("fecha_conclusion", "Fecha de Conclusion","required|callback_fecha_dif");
		$this->form_validation->set_rules("descripcion_proyecto", "Descripcion del Proyecto","trim|required");
		$this->form_validation->set_rules("objetivo_proyecto", "Objetivo del Proyecto","trim|required");
		$this->form_validation->set_message("is_unique", "El campo %s esta duplicado");
		$this->form_validation->set_message("required", "El campo %s es requerido");
		$this->form_validation->set_message("numeric", "El campo %s es numerico");
		$this->form_validation->set_message("decimal", "El campo %s es decimal");
		$this->form_validation->set_message("is_natural", "El campo %s debe contener solo numeros positivos mayores a cero");
		$this->form_validation->set_message("fecha_dif", "El campo %s debe ser mayor a la Fecha de incio" );

		//guardar a la tabla proyectos
		if ($this->form_validation->run()){
			$data = array(
				'id_proyecto'=>$id_proyecto,
				'id_secretaria'=>$id_secretaria,
				'id_area'=>$area,
				'id_subarea'=>$id_subarea,
				'fase'=>$tipo_fase,
				'nombre_proyecto'=>$nombre_proyecto,
				'costo_proyecto'=>$costo_proyecto,
				'tipo_proyecto'=>$tipo_proyecto,
				'modalidad'=>$modalidad,
				'convenio'=>$convenio,
				'fecha_inicio'=>$f1,
				'fecha_conclusion'=>$f2,
				'duracion_anos'=>$duracion_anos,
				'descripcion_proyecto'=>$descripcion_proyecto,
				'objetivo_proyecto'=>$objetivo_proyecto,
				'codigo_tramo'=>$codigo_tramo,
				'estado'=>'1'
			);
			$id=$this->session->userdata('id');
			$data2=array(
				'id_usuario'=>$id,
				'rol_id'=>$rol_id,
				'estado'=>1,
				'id_proyecto' =>$id_proyecto,
			);
			if($this->proyectos_model->insert($data)){
				if($this->proyectos_model->insert_asig($data2)){
					$pant=1;
					$ruta1='proyecto/metas_controller/ingresoMetas';
					$data3=array(
						'id_proyecto'=>$id_proyecto,
						'pant'=>$pant,
					);
					$this->proyectos_model->insert_pant($data3);
					//si tiene tramos
					if ($codigo_tramo>0){
						for ($i=0; $i < $numero_tramo; $i++) {
							$data_tramo = array(
								'id_proyecto'=>$id_proyecto,
								'nombre_tramo'=>"PAQUETE". ($i+1),
								'nombre_proyecto'=>$nombre_proyecto,
							);
							//inserta tramos
							$this->tramo_model->insert_tramo($data_tramo);
						}
					}

					redirect(base_url()."proyecto/convenio_controller/IngresoConvenio/$id_proyecto/$convenio");
					$control=1;
				}
			}


	  }else{
			$this->area_controller();
		}
		//redirecciones
		//print_r($data_tramo);
	}


	//funcion para comparar fechas
	private function compararFechas($primera, $segunda)
	{
		$valoresPrimera = explode ("-", $primera);
		$valoresSegunda = explode ("-", $segunda);

		$diaPrimera    = $valoresPrimera[2];
		$mesPrimera  = $valoresPrimera[1];
		$anyoPrimera   = $valoresPrimera[0];

		$diaSegunda   = $valoresSegunda[2];
		$mesSegunda = $valoresSegunda[1];
		$anyoSegunda  = $valoresSegunda[0];

		$diasPrimeraJuliano = gregoriantojd($mesPrimera, $diaPrimera, $anyoPrimera);
		$diasSegundaJuliano = gregoriantojd($mesSegunda, $diaSegunda, $anyoSegunda);

		if(!checkdate($mesPrimera, $diaPrimera, $anyoPrimera)){
			echo "La fecha ".$primera." no es v&aacute;lida";
			return 0;
		}elseif(!checkdate($mesSegunda, $diaSegunda, $anyoSegunda)){
			echo "La fecha ".$segunda." no es v&aacute;lida";
			return 0;
		}else{
			return  $diasPrimeraJuliano - $diasSegundaJuliano;
		}

	}

	public function fecha_dif(){
		if(($this->input->post('fecha_inicio')!='') && ($this->input->post('fecha_conclusion')!=''))
			{
				$fecha1='';
				$fecha2='';
				//fecha inicial
				$fecha1= $this->input->post('fecha_inicio');
				$vfini = explode("-", $fecha1);
				$fecha1 = $vfini[0].'-'.$vfini[1].'-01';

				//fecha final
				$fecha2= $this->input->post('fecha_conclusion');
				$vffin = explode("-", $fecha2);
				$dia_fin = cal_days_in_month(CAL_GREGORIAN, $vffin[1], $vffin[0]);
				$fecha2 = $vffin[0].'-'.$vffin[1].'-'.$dia_fin;
				if ($this->compararFechas($fecha2, $fecha1)>0){
					return TRUE;
				}else{
					return FALSE;
				}
		    }
	}

	public function edit(){
		//$data['area']=$this->proyectos_model->get_area();
		//$data = array(
		//'proyecto'=> $this->proyectos_model->getProyecto($id_proyecto),
		//);
		//$data=$_GET['result'];
		//print_r($data);

		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/proyectos/edit');
		$this->load->view('layouts/footer');
	}

	public function update($id_proyecto){
		$id_proyecto=$this->input->post("id_proyecto");
		$area=$this->input->post("area");
		$id_subarea=$this->input->post("sub_area");
		$tipo_fase=$this->input->post("tipo_fase");
		$nombre_proyecto=$this->input->post("nombre_proyecto");
		$costo_proyecto=$this->input->post("costo_proyecto");
		$tipo_proyecto=$this->input->post("tipo_proyecto");
		$modalidad=$this->input->post("modalidad");
		$fecha_inicio=$this->input->post("fecha_inicio");
		$fecha_conclusion=$this->input->post("fecha_conclusion");
		$duracion_anos=$this->input->post("duracion_anos");
		$descripcion_proyecto=$this->input->post("descripcion_proyecto");
		$objetivo_proyecto=$this->input->post("objetivo_proyecto");
		$data = array(
			'id_area'=>$area,
			'id_subarea'=>$id_subarea,
			'fase'=>$tipo_fase,
			'nombre_proyecto'=>$nombre_proyecto,
			'costo_proyecto'=>$costo_proyecto,
			'tipo_proyecto'=>$tipo_proyecto,
			'modalidad'=>$modalidad,
			'fecha_inicio'=>$fecha_inicio,
			'fecha_conclusion'=>$fecha_conclusion,
			'duracion_anos'=>$duracion_anos,
			'descripcion_proyecto'=>$descripcion_proyecto,
			'objetivo_proyecto'=>$objetivo_proyecto,

			);
			if($this->proyectos_model->update($id_proyecto,$data))
			{
				redirect(base_url()."proyecto/mis_proyectos");
			}
			else
			{
				redirect(base_url()."proyecto/mis_proyectos/edit".$id_proyecto);
			}
	}

	public function con_convenio()
	{

		$id='Si';

		$data=array(
			'results'=>$this->proyectos_model->getConvenio($id),

		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/reportes/con_convenio',$data);
		$this->load->view('layouts/footer');
	}
	public function sin_convenio(){
		$data= array();
		$id='no';
		$data['results'] =  $this->proyectos_model->getSinconvenio($id);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/reportes/sin_convenio',$data);
		$this->load->view('layouts/footer');
	}


	public function por_provincia()
	{
		//data= array();
		//$provinci=$this->input->post("buscador");
		//echo $provinci;
		$id_provincia=$this->input->post("buscador");
		$data['provincias']=$this->municipio_model->provincia();
		//$provincia=$this->input->post("provincias");
		if ($this->input->post("buscar"))
		{
			$data['provincia']=$this->municipio_model->selec_provincia($id_provincia);
		}
		else
		{
			$data['provincia']=$this->municipio_model->por_provincia_proy();
		}
		$this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/reportes/por_provincia',$data);
		$this->load->view('layouts/footer');
	}

	public function por_municipio()
	{
		//data= array();
		//$provinci=$this->input->post("buscador");
		//echo $provinci;
		$id_municipio=$this->input->post("buscador");
		$data['municipios']=$this->municipio_model->municipio();
		//$provincia=$this->input->post("provincias");
		if ($this->input->post("buscar"))
		{
			$data['municipio']=$this->municipio_model->selec_municipio($id_municipio);
		}
		else
		{
			$data['municipio']=$this->municipio_model->por_municipio_proy();
		}
		$this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/reportes/por_municipio',$data);
		$this->load->view('layouts/footer');
	}

	public function por_fecha(){
		$fecha_inicio=$this->input->post("fecha_inicio");
		$fecha_conclusion=$this->input->post("fecha_conclusion");
		if ($this->input->post("buscar"))
		{
			//$data['proyectos']=$this->proyectos_model->porFecha($fecha_inicio,$fecha_conclusion);
			$proyecto=$this->proyectos_model->porFecha($fecha_inicio,$fecha_conclusion);
		}
		else
		{
			//$data['proyectos']=$this->proyectos_model->getProyectos();
			$proyecto=$this->proyectos_model->getProyectos();
		}
		$data=array(
			'proyectos'=>$proyecto,

		);

		$this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/reportes/por_fecha',$data);
		$this->load->view('layouts/footer');
	}

	public function sisin(){
		//$data['results'] =  $this->proyectos_model->getSinconvenio($id);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/reportes/sisin');
		$this->load->view('layouts/footer');
	}

	/*
	  Funciones	para la revision de proyectos
	*/

	public function revisionProyecto_detalle($id_proyecto){
		  /*
        Recuperar detalle del proyecto
        */
        $detalle_proyecto = $this->proyectos_model->getDetalleProyecto($id_proyecto);
        $detalle_metas = $this->metas_model->metas($id_proyecto);
        $detalle_componente = $this->componente_model->getComponenteProy($id_proyecto);
        $detalle_convenio = $this->convenio_model->getConvenioProy($id_proyecto);
        $detalle_indicador = $this->indicador_model->indicador($id_proyecto);
        $detalle_municipio = $this->municipio_model->det_municipio($id_proyecto);
        $detalle_puntos = $this->puntos_model->puntos($id_proyecto);
        $data = array("detalle_proyecto" => $detalle_proyecto,
                      "metas" => $detalle_metas,
                      "componentes" => $detalle_componente,
                      "convenios" => $detalle_convenio,
                      "indicador" => $detalle_indicador,
                      "municipio" => $detalle_municipio,
                      "puntos" => $detalle_puntos
                    );             
        $this->load->view('layouts/header');
        $this->load->view('layouts/menugestor');
        $this->load->view('admin/proyectos/verRevisionProyecto', $data);
        $this->load->view('layouts/footer');
	}

	/*
	 Cambiar alta a 0 el proyecto vuelve a a revision al funcionario de registro
	*/
	public function rechazarProyecto($id_proyecto){		
		$date2=$this->proyectos_model->updateAlta($id_proyecto, "0");
		redirect(base_url()."proyecto/mis_proyectos/revisionProyectos");
	}

	/*
	 Cambiar alta a 1 el proyecto avanza a registro de SISIN
	*/
	public function aceptarProyecto($id_proyecto){
		$date2=$this->proyectos_model->updateAlta($id_proyecto, "1");
		redirect(base_url()."proyecto/mis_proyectos/revisionProyectos");
	}

}
