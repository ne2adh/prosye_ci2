<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tramos_controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('date','url');
		$this->load->helper(array('form', 'url'));
		$this->request = json_decode(file_get_contents('php://input'));
	}

	public function inicio( $id_tramo ){
		/* registrar detalle del tramo */
		$data_tramo = $this->tramo_model->getTramo($id_tramo);		
		$id_proyecto = $data_tramo->id_proyecto;		

		$tramos = array( "tramos" => $this->tramo_model->index($id_proyecto) );
		
		$proyectos = $this->proyectos_model->getProyecto($id_proyecto);
		
		//recuperar datos del convenio		
		$data_convenio = $this->convenio_model->traer_convenio($id_tramo);
				
		$data=array(
			"proyectos" => $proyectos,
			"tramo" => $data_tramo,
			"convenio" => $data_convenio,
			"id_tipo" => 0,
			"cont" => 0
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/tramos/index',$data);
		$this->load->view('layouts/footer');
	}

	public function index(){
		$id=array();
		//$id=$_GET['id'];
        $data['results'] =  $this->tramo_model->getTipo($id);
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/proyectos/mis_proy_tramo',$data);
		$this->load->view('layouts/footer');
	}
	
	public function ingresoTramo($id_tipo,$cont){
		$numero=$this->tramo_model->num_tipo($id_tipo);
		$id_tipo=$id_tipo;
		//print_r(intval($numero));
		//print_r($cont);
		//print_r($numero);

		$cont=$cont;
		//$cont=intval($cont);
		$data=array(
			'cont'=>$cont,
			'num_tramos'=>$this->tramo_model->num_tramo($id_tipo),
			'proyectos'=>$this->tramo_model->proyecto($id_tipo),
			'cont'=>$cont,
			'id_tipo'=>$id_tipo,
			
		);
		if ($cont<$numero) {
		   	$this->load->view('layouts/header');
			$this->load->view('layouts/aside');
			$this->load->view('admin/proyectos/registro_tramos',$data);
			$this->load->view('layouts/footer');
		   } else
		    {
		    $cont=0;
		  	redirect(base_url()."proyecto/puntos_tramo_controller/traerTramo/$id_tipo");
		   }

	}
	//public function nuevo()
	//{
	//	$id_proyecto=$this->input->post("id_proyecto");
	//	redirect(base_url()."proyecto/numero_tramo_controller/ingresoNumTramo/$id_proyecto");
	//}


	public function store($cont){

		$logo = $this->request->logo;

		$id_tramo = $this->request->post("id_tramo");
        $nombre_tramo = $this->request->post("nombre_tramo");
        $porcentaje = $this->request->post("porcentaje");
        $costo_proyecto = $this->request->post("costo_proyecto");
        $fecha_inicio = $this->request->post("fecha_inicio");
        $fecha_fin = $this->request->post("fecha_fin");
        $tiempo_evaluacion = $this->request->post("tiempo_evaluacion");
        $tiempo_inspeccion = $this->request->post("tiempo_inspeccion");
        $convenio = $this->request->post("convenio");
        $id_tipo = $this->request->post("id_tipo");
        $objetivo = $this->request->post("objetivo");
        $descripcion = $this->request->post("descripcion");
        $meta = $this->request->post("meta");
        $id_proyecto = $this->request->post("id_proyecto");
		$nombre_proyecto = $this->request->post("nombre_proyecto");
		$data = array(
			"id_tramo" => $id_tramo,
			"nombre_tramo" => $nombre_tramo,
			"porcentaje" => $porcentaje,
			"costo_proyecto" => $costo_proyecto,
			"fecha_inicio" => $fecha_inicio,
			"fecha_fin" => $fecha_fin,
			"tiempo_evaluacion" => $tiempo_evaluacion,
			"tiempo_inspeccion" => $tiempo_inspeccion,
			"convenio" => $convenio,
			"id_tipo" => $id_tipo,
			"objetivo" => $objetivo,
			"descripcion" => $descripcion,
			"meta" => $meta,
			"id_proyecto" => $id_proyecto,
			"nombre_proyecto" => $nombre_proyecto
		);

		return $this->tramo_model->update($id_tramo, $data);
	}

	public function storeTramo(){
		$id_tramo = $this->request->id_tramo;
        $nombre_tramo = $this->request->nombre_tramo;
        $porcentaje = $this->request->porcentaje;
        $costo_proyecto = $this->request->costo_proyecto;
        $fecha_inicio = $this->request->fecha_inicio;
        $fecha_conclusion = $this->request->fecha_conclusion;
        $tiempo_evaluacion = $this->request->tiempo_evaluacion;
        $tiempo_inspeccion = $this->request->tiempo_inspeccion;
        $convenio = $this->request->convenio;
        $id_tipo = $this->request->id_tipo;
        $objetivo_proyecto = $this->request->objetivo_proyecto;
        $descripcion_proyecto = $this->request->descripcion_proyecto;
        $meta = $this->request->meta;
        $id_proyecto = $this->request->id_proyecto;
		$nombre_proyecto = $this->request->nombre_proyecto;
		$data = array(			
			"nombre_tramo" => $nombre_tramo,
			"porcentaje" => $porcentaje,
			"costo_proyecto" => $costo_proyecto,
			"fecha_inicio" => $fecha_inicio,
			"fecha_conclusion" => $fecha_conclusion,
			"tiempo_evaluacion" => $tiempo_evaluacion,
			"tiempo_inspeccion" => $tiempo_inspeccion,
			"convenio" => $convenio,
			"id_tipo" => $id_tipo,
			"objetivo_proyecto" => $objetivo_proyecto,
			"descripcion_proyecto" => $descripcion_proyecto,
			"meta" => $meta,
			"id_proyecto" => $id_proyecto,
			"nombre_proyecto" => $nombre_proyecto
		);		
		return $this->tramo_model->update($id_tramo, $data);
	}

	public function updateEstado(){
		$id_tramo = $this->request->id_tramo;        
		$convenio = $this->request->convenio;
		return $this->tramo_model->updateEstado($id_tramo, $convenio);
	}
}
