<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Proyectos_model");


	}
	public function index()
	{

		//$resultado = array();
		//$data = array();
		//$resultado = $this->Proyectos_model->traer_proyectos();
		//$data['resultado'] = $resultado;
		$data =array('proyectos'=>$this->Proyectos_model->getProyectos(),);


		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/proyectos/list',$data);
		$this->load->view('layouts/footer');
	
	}
	public function registrar_nuevo()
	{
		
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/proyectos/proyecto_nuevo');
		$this->load->view('layouts/footer');

	}
	

}
