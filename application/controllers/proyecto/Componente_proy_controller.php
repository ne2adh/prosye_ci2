<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Componente_proy_controller extends CI_Controller
{
	public function __construct(){
		parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('date','url','number');
        $this->load->helper(array('form', 'url'));


	}

	public function index(){
		$id_proyecto=$this->input->post('id_proyecto');
		$proyecto=$this->componente_model->getProyecto($id_proyecto);

		$data = array(
			'proyecto'=>$this->componente_model->getProyecto($id_proyecto),
			'componente'=>$this->componente_model->getComponenteProy($id_proyecto),
			'conven'=>$this->componente_model->getConvenioProy($id_proyecto),
			'total_participa'=>$this->participantes_model->totalParticipante($id_proyecto),
		);
		$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));
		if ($proyecto->convenio=='SI'){
			$this->load->view('layouts/header');
			$this->load->view('layouts/aside_proyec', $tramos);
			$this->load->view('admin/proyectos/componente_proyecto_view',$data);
			$this->load->view('layouts/footer');
		}else{
			$this->load->view('layouts/header');
			$this->load->view('layouts/aside_proyec', $tramos);
			$this->load->view('admin/proyectos/componente_proyecto_view_sc',$data);
			$this->load->view('layouts/footer');
		}
	}

	public function mios(){
	    $id=array();
		$id=$this->session->userdata('id');
        $data['results'] =  $this->municipio_model->getProyecto($id);
	    $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec');
		$this->load->view('admin/proyectos/mis_proy_actividades',$data);
		$this->load->view('layouts/footer');
	}

	public function store_proy(){

		$id_proyecto=$this->input->post("id_proyecto");
				
		//$id_proyecto=$id_proyecto;
		$descrip_compo_proy=$this->input->post("descripcion");
		$monto_comp_proy=$this->input->post("monto");
		$monto_comp_proy=str_replace(',','',$monto_comp_proy);
		$tiempo_actividad=$this->input->post("tiempo_actividad");
		$modalidad=$this->input->post("modalidad");
		$fecha_inicio=$this->input->post("fecha_inicio");
		$fecha_conclusion=$this->input->post("fecha_conclusion");

		$this->form_validation->set_rules("descripcion", "Descripcion del componente del Proyecto","required");
		$this->form_validation->set_rules("monto", "Monto del componente del Proyecto","trim|required");
		$this->form_validation->set_rules("tiempo_actividad", "El monto del componente del Proyecto","trim|required|is_natural_no_zero|is_natural");
		$this->form_validation->set_rules("modalidad", "Descripcion del componente del Proyecto","required");
		$this->form_validation->set_rules("fecha_inicio", "Fecha inicio del componente del Proyecto","required");
		//$this->form_validation->set_rules("tiempo_actividad", "Descripcion del componente del Proyecto","required");

		$this->form_validation->set_message("required", "El campo %s es requerido");
		$this->form_validation->set_message("is_natural_no_zero", "El campo %s tiene que contener un valor mayor a 0");
		$this->form_validation->set_message("is_natural", "El campo %s tiene que contener solo numeros positivos");
		$this->form_validation->set_message("decimal", "El campo %s tiene que contener un numero decimal");
		$this->form_validation->set_message("greater_than", "El campo %s tiene que contener un numero mayor a cero");
		if ($this->form_validation->run()){
					$data= array(
					'id_proyecto'=>$id_proyecto,
					'descrip_compo_proy'=>$descrip_compo_proy,
					'monto_comp_proy'=>$monto_comp_proy,
					'tiempo_actividad'=>$tiempo_actividad,
					'modalidad'=>$modalidad,
					'fecha_inicio'=>$fecha_inicio,
					'fecha_conclusion'=>$fecha_conclusion,
					);
					$data1['id_proyecto']=$id_proyecto;
					$data1['pant']=6;
					$this->proyectos_model->update_pant($data1,$id_proyecto);
					$resp=0;
					$resp=$this->componente_model->compara($descrip_compo_proy,$id_proyecto);
					if ($resp!=1){
								$this->componente_model->insert_proy($data);
									redirect(base_url()."proyecto/componente_proy_controller/nuevo/$id_proyecto");
					}

					else{
					 		//echo ('---------------------------Componente Repetido---------------------------');
							$this->ingresoErr($id_proyecto,1);
					}
				}
				else{
						$this->nuevo($id_proyecto);
				}

	}
	public function nuevo($id_proyecto){
		$id_proyecto=$id_proyecto;

		//si el $id_proyecto es mayor a 100000 se esta asignando un convenio a un tramo
		//recuperar la informacion del tramo		

		if($id_proyecto >= 100000){
			//recuperar datos del tramo
			$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
			$idProyecto = $data_proyecto->id_proyecto;
		}
		else{ 
			$data_proyecto = $this->componente_model->getProyecto($id_proyecto);
			$idProyecto = $id_proyecto;
		}
		$data = array(
			'proyecto' => $data_proyecto,
			'componente'=> $this->componente_model->getComponenteProy($id_proyecto),
			'conven'=> $this->componente_model->getConvenioProy($id_proyecto),
			'total_participa'=> $this->participantes_model->totalParticipante($id_proyecto),
		);
		$tramos = array("tramos" => $this->tramo_model->index($idProyecto));		
		if (trim($data_proyecto->convenio)=='SI'){
			//Si el Proyecto tiene convenio y no existe registre de convenio debe ir a
			//la pantalla de registro de convenio
			$data_convenio = $this->convenio_model->getConvenioProy($id_proyecto);
			//verificar si existe convenio			
			if(count($data_convenio)==0){
				//el usuario debe registrar el convenio				
				redirect( base_url()."proyecto/convenio_controller/get_convenio/".$id_proyecto);
			}
			//var_dump($proyecto);
			$this->load->view('layouts/header');
			$this->load->view('layouts/aside_proyec', $tramos);
			$this->load->view('admin/proyectos/componente_proyecto_view',$data);
			$this->load->view('layouts/footer');
		}else{
			$this->load->view('layouts/header');
			$this->load->view('layouts/aside_proyec', $tramos);
			$this->load->view('admin/proyectos/componente_proyecto_view_sc',$data);
			$this->load->view('layouts/footer');
		}
	}

	public function edit($id_componente_proy,$id_proyecto){
		$id_proyecto=$id_proyecto;
		if($id_proyecto >= 100000){
			//recuperar datos del tramo
			$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
		}
		else{
			$data_proyecto = $this->componente_model->getProyecto($id_proyecto);
		}
		$data = array(
			'id_proyecto'=>$id_proyecto,
			'id_componente_proy'=>$id_componente_proy,
			'compo'=>$this->componente_model->getComponente_edit($id_componente_proy),
			'proyecto'=>$data_proyecto,
			'componente'=>$this->componente_model->getComponenteProy($id_proyecto),
			'conven'=>$this->componente_model->getConvenioProy($id_proyecto),
		);		
		$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));	
		if ($data_proyecto->convenio=='SI'){
			$this->load->view('layouts/header');
			$this->load->view('layouts/aside_proyec', $tramos);
			$this->load->view('admin/proyectos/componente_proyecto_view_edit',$data);
			$this->load->view('layouts/footer');
		}else{
			$this->load->view('layouts/header');
			$this->load->view('layouts/aside_proyec', $tramos);
			$this->load->view('admin/proyectos/componente_proyecto_view_sc_edit',$data);
			$this->load->view('layouts/footer');
		}

	}


	public function ingresoErr( $id_proyecto, $existe_error ){
		$id_proyecto=$id_proyecto;
		$proyecto=$this->componente_model->getProyecto($id_proyecto);
		$data = array(
		'proyecto'=>$this->componente_model->getProyecto($id_proyecto),
		'componente'=>$this->componente_model->getComponenteProy($id_proyecto),
		'conven'=>$this->componente_model->getConvenioProy($id_proyecto),
		'total_participa'=>$this->participantes_model->totalParticipante($id_proyecto),
		'error' => $existe_error
		);
		$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));	
		if ($proyecto->convenio=='SI'){
			$this->load->view('layouts/header');
			$this->load->view('layouts/aside_proyec', $tramos);
			$this->load->view('admin/proyectos/componente_proyecto_view',$data);
			$this->load->view('layouts/footer');
		}else{
			$this->load->view('layouts/header');
			$this->load->view('layouts/aside_proyec', $tramos);
			$this->load->view('admin/proyectos/componente_proyecto_view_sc',$data);
			$this->load->view('layouts/footer');
		}
	}

	public function delete($id,$id_proyecto){
		 $id=$id;
		 $id_proyecto=$id_proyecto;

		 	$this->componente_model->delete($id);
		 	redirect(base_url()."proyecto/componente_proy_controller/nuevo/$id_proyecto");
	}

	

public function update(){

		$id_componente_proy=$this->input->post("id_componente_proy");
		$id_proyecto=$this->input->post("id_proyecto");
		//$id_proyecto=$id_proyecto;
		$descrip_compo_proy=$this->input->post("descripcion");		
		$monto_comp_proy=$this->input->post("monto");
		$tiempo_actividad=$this->input->post("tiempo_actividad");
		$modalidad=$this->input->post("modalidad");		
		$monto_comp_proy = str_replace(',','', $monto_comp_proy);		
		$fecha_inicio=$this->input->post('fecha_inicio');
		$fecha_conclusion=$this->input->post('fecha_conclusion');

		$this->form_validation->set_rules("descripcion", "Descripcion del componente del Proyecto","required");
		$this->form_validation->set_rules("monto", "El monto del componente del Proyecto","trim|required");
		$this->form_validation->set_rules("tiempo_actividad", "El monto del componente del Proyecto","trim|required|is_natural_no_zero|is_natural");
		$this->form_validation->set_rules("modalidad", "Descripcion del componente del Proyecto","required");

		$this->form_validation->set_message("required", "El campo %s es requerido");
		$this->form_validation->set_message("is_natural_no_zero", "El campo %s tiene que contener un valor mayor a 0");
		$this->form_validation->set_message("is_natural", "El campo %s tiene que contener solo numeros positivos");
		$this->form_validation->set_message("decimal", "El campo %s tiene que contener un numero decimal");
		$this->form_validation->set_message("greater_than", "El campo %s tiene que contener un numero mayor a cero");
		
		if ($this->form_validation->run()){
					$data= array(
					'id_proyecto'=>$id_proyecto,
					'descrip_compo_proy'=>$descrip_compo_proy,
					'monto_comp_proy'=>$monto_comp_proy,
					'tiempo_actividad'=>$tiempo_actividad,
					'modalidad'=>$modalidad,
					'fecha_inicio'=>$fecha_inicio,
					'fecha_conclusion'=>$fecha_conclusion,
					);
					//$resp=0;
					//$resp=$this->componente_model->compara($descrip_compo_proy,$id_proyecto);
					//if ($resp!=1){
					$this->componente_model->update_componente($id_componente_proy,$data);
					redirect(base_url()."proyecto/componente_proy_controller/nuevo/$id_proyecto");
						//		}
					 //else   {
					 	//	echo ('---------------------------Componente Repetido---------------------------');
						//	$this->nuevo($id_proyecto);
						//	}
		}
		else {			
			$this->nuevo($id_proyecto);
		}
	}
}
