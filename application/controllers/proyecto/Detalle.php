<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detalle extends CI_Controller{
    public function __construct(){
		parent::__construct();
        //$this->permisos=$this->backend_lib->control();
        $this->load->library('form_validation','session');
        $this->load->helper(array('url', 'form'));
        $this->load->helper('form','date');
    }

    public function proyecto($id_proyecto){
        /*
        Recuperar detalle del proyecto
        */
        $detalle_proyecto = $this->proyectos_model->getDetalleProyecto($id_proyecto);
        /*
        Recuperar metas
                  convenio
                  componentes
                  indicadores
                  municipios
                  puntos
            en diferentes variables para la presentacion de la hoja de impresion -> registro del proyecto
        */
        $detalle_metas = $this->metas_model->metas($id_proyecto);
        $detalle_componente = $this->componente_model->getComponenteProy($id_proyecto);
        $detalle_convenio = $this->convenio_model->getConvenioProy($id_proyecto);
        $detalle_indicador = $this->indicador_model->indicador($id_proyecto);
        $detalle_municipio = $this->municipio_model->det_municipio($id_proyecto);
        $detalle_puntos = $this->puntos_model->puntos($id_proyecto);
        $data = array("detalle_proyecto" => $detalle_proyecto,
                      "metas" => $detalle_metas,
                      "componentes" => $detalle_componente,
                      "convenios" => $detalle_convenio,
                      "indicador" => $detalle_indicador,
                      "municipio" => $detalle_municipio,
                      "puntos" => $detalle_puntos
                    );             
        $this->load->view('layouts/header');
        $this->load->view('layouts/menugestor');
        $this->load->view('admin/proyectos/verDetalleProyectoSisin', $data);
        $this->load->view('layouts/footer');
    }

    public function detalle($id_proyecto){
        /*
            Recuperar detalle del proyecto
        */
        $detalle_proyecto = $this->proyectos_model->getDetalleProyecto($id_proyecto);
        $detalle_metas = $this->metas_model->metas($id_proyecto);
        $detalle_componente = $this->componente_model->getComponenteProy($id_proyecto);
        $detalle_convenio = $this->convenio_model->getConvenioProy($id_proyecto);
        $detalle_indicador = $this->indicador_model->indicador($id_proyecto);
        $detalle_municipio = $this->municipio_model->det_municipio($id_proyecto);
        $detalle_puntos = $this->puntos_model->puntos($id_proyecto);
        $data = array("detalle_proyecto" => $detalle_proyecto,
                      "metas" => $detalle_metas,
                      "componentes" => $detalle_componente,
                      "convenios" => $detalle_convenio,
                      "indicador" => $detalle_indicador,
                      "municipio" => $detalle_municipio,
                      "puntos" => $detalle_puntos
                    );             
        $this->load->view('layouts/header');
        $this->load->view('layouts/menugestor');
        $this->load->view('admin/proyectos/verDetalleProyecto', $data);
        $this->load->view('layouts/footer');
    }
}
