<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gmaps extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index($id_proyecto){

		$data=array(
					'id_proyecto'=> $id_proyecto
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec');
		$this->load->view('map_view', $data);
		$this->load->view('layouts/footer');
	}

	public function listarPuntos() {
    $id = $this->input->get("id_proyecto");

    $resultado = array();
    $listado = $this->puntos_model->puntos($id);
    if(!empty($listado)){
        foreach ($listado as $fila){
						$data['lat'] = (float)$fila->latitud;
						$data['lng'] = (float)$fila->longitud;
            array_push($resultado, $data);
        }
    }
    echo json_encode($resultado);
  }


}
?>
