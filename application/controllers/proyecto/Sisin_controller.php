<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Sisin_controller extends CI_Controller 
{
	private $aux_sub_sector;
	public function __construct()
	{
		parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('date','url');


	}
	
	public function index()
	{
		        
	   //$id=array();
		$data['results'] =  $this->proyectos_model->ingresoSisin();    
        $this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/proyectos/mis_proy_sisin',$data);
		$this->load->view('layouts/footer');

	}

	public function conSisin(){		        
	   //$id=array();
		$data['results'] =  $this->sisin_model->conSisin();    
        $this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/proyectos/mis_proy_sisin',$data);
		$this->load->view('layouts/footer');
	}



	public function ingresoSisin($id_proyecto){	
		$id_proyecto=$id_proyecto;		
		/*
		Buscar si el proyecto tiene SISIN, recuperar los datos
		de sisin
		*/

		$data_sisin = $this->sisin_model->sisin($id_proyecto);		
		$data = array( 
			'proyecto'=> $this->sisin_model->getProyecto($id_proyecto),
			'sectores'=> $this->sisin_model->getSector(),
			'sisin' => $data_sisin
		);

        //$data = $id_proyecto;
	    $this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/proyectos/sisin_view',$data);
		$this->load->view('layouts/footer');
	}

	public function rechazado($id_proyecto){	

		$id_proyecto=$id_proyecto;		        
        $date=$this->proyectos_model->update_alta($id_proyecto);
		$id_proyecto=$date->id_proyecto;
		$id_secretaria=$date->id_secretaria;
		$id_area=$date->id_area;
		$id_subarea=$date->id_subarea;
		$fase=$date->fase;
		$nombre_proyecto=$date->nombre_proyecto;
		$costo_proyecto=$date->costo_proyecto;
		$tipo_proyecto=$date->tipo_proyecto;
		$modalidad='';
		$convenio=$date->convenio;
		$fecha_inicio=$date->fecha_inicio;
		$fecha_conclusion=$date->fecha_conclusion;
		$duracion_anos=$date->duracion_anos;
		$descripcion_proyecto=$date->descripcion_proyecto;
		$objetivo_proyecto=$date->objetivo_proyecto;
		$estado=$date->estado;
		$alta=0;
		$sisin=0;
		$data1 = array(
						'id_proyecto'=>$id_proyecto,
						'id_secretaria'=>$id_secretaria,
						'id_area'=>$id_area,
						'id_subarea'=>$id_subarea,
						'fase'=>$fase,
						'nombre_proyecto'=>$nombre_proyecto,
						'costo_proyecto'=>$costo_proyecto,
						'tipo_proyecto'=>$tipo_proyecto,
						'modalidad'=>'',
						'convenio'=>$convenio,
						'fecha_inicio'=>$fecha_inicio,
						'fecha_conclusion'=>$fecha_conclusion,
						'duracion_anos'=>$duracion_anos,
						'descripcion_proyecto'=>$descripcion_proyecto,
						'objetivo_proyecto'=>$objetivo_proyecto,
						'estado'=>$estado,
						'alta'=>$alta,
						'sisin'=>$sisin,
						);
		$date2=$this->proyectos_model->update_alta1($id_proyecto,$data1);
		$data['results'] =  $this->proyectos_model->ingresoSisin();    
        $this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/proyectos/mis_proy_sisin',$data);
		$this->load->view('layouts/footer');
	}
	
	public function verificarDenominacion($denominacion){
		
		$id_sub_sector = $this->aux_sub_sector;
		if($id_sub_sector!=''){
			$sub_sectores = $this->sisin_model->get_sub_denominacion($id_sub_sector);			
			if( $denominacion=='' && count($sub_sectores)!=0  ){
				return false;
			}
			else{
				return true;
			}
		}
		return false;
	}

	public function store(){
		/*$sql="SELECT max(id_proyecto) as id_proyecto from proyectos";
		$consulta= $this->db->query($sql);
		$id_proyecto=$consulta->row_array()['id_proyecto'];*/
		$id_proyecto=$this->input->post("id_proyecto");
		$codigo_sisin=$this->input->post("codigo_sisin");
		$fecha_inicio=$this->input->post("fecha_inicio");
		$fecha_conclusion=$this->input->post("fecha_conclusion");
		$tipologia=$this->input->post("tipologia");
		$area_influencia=$this->input->post("area_influencia");
		$tipo_inversion=$this->input->post("tipo_inversion");
		$sector=$this->input->post("sector");
		$sub_sector=$this->input->post("sub_sector");
		$this->aux_sub_sector = $sub_sector;

		$denominacion=$this->input->post("denominacion");

		$this->form_validation->set_rules("id_proyecto", "proyecto","is_unique[sisin.id_proyecto]");
		$this->form_validation->set_rules("codigo_sisin", "Codigo Sisin","required|is_unique[sisin.codigo_sisin]");
		$this->form_validation->set_rules("tipologia", "Tipologia","required");
		$this->form_validation->set_rules("area_influencia", "Área de Influencia","required");
		$this->form_validation->set_rules("tipo_inversion", "Tipo de Inversion","required");
		$this->form_validation->set_rules("sector", "Sector","required");
		$this->form_validation->set_rules("sub_sector", "Sub-Sector","required");
		$this->form_validation->set_rules("denominacion", "Denominacion","callback_verificarDenominacion");

		$this->form_validation->set_message("required", "El campo %s es requerido");
		$this->form_validation->set_message("is_unique", "El  %s ya existe");
		$this->form_validation->set_message("verificarDenominacion", "El campo %s es requerido");
		if ($this->form_validation->run()){
			$data = array(
			'id_proyecto'=>$id_proyecto,
			'codigo_sisin'=>$codigo_sisin,
			'fecha_inicio'=>$fecha_inicio,
			'fecha_conclusion'=>$fecha_conclusion,
			'tipologia'=>$tipologia,
			'area_influencia'=>$area_influencia,
			'tipo_inversion'=>$tipo_inversion,
			'id_sector'=>$sector,
			'id_sub_sector'=>$sub_sector,
			'id_denominacion'=>$denominacion,
			);

			$this->sisin_model->insert($data);

			//$id_proyecto=$id_proyecto;
			$date=$this->sisin_model->update_sisin($id_proyecto);
			$id_proyecto=$date->id_proyecto;
			$id_secretaria=$date->id_secretaria;
			$id_area=$date->id_area;
			$id_subarea=$date->id_subarea;
			$fase=$date->fase;
			$nombre_proyecto=$date->nombre_proyecto;
			$costo_proyecto=$date->costo_proyecto;
			$tipo_proyecto=$date->tipo_proyecto;
			$modalidad='';
			$convenio=$date->convenio;
			$fecha_inicio=$date->fecha_inicio;
			$fecha_conclusion=$date->fecha_conclusion;
			$duracion_anos=$date->duracion_anos;
			$descripcion_proyecto=$date->descripcion_proyecto;
			$objetivo_proyecto=$date->objetivo_proyecto;
			$estado=$date->estado;
			$alta=$date->alta;
			$sisin=1;
			$data1 = array(
							'id_proyecto'=>$id_proyecto,
							'id_secretaria'=>$id_secretaria,
							'id_area'=>$id_area,
							'id_subarea'=>$id_subarea,
							'fase'=>$fase,
							'nombre_proyecto'=>$nombre_proyecto,
							'costo_proyecto'=>$costo_proyecto,
							'tipo_proyecto'=>$tipo_proyecto,
							'modalidad'=>'',
							'convenio'=>$convenio,
							'fecha_inicio'=>$fecha_inicio,
							'fecha_conclusion'=>$fecha_conclusion,
							'duracion_anos'=>$duracion_anos,
							'descripcion_proyecto'=>$descripcion_proyecto,
							'objetivo_proyecto'=>$objetivo_proyecto,
							'estado'=>$estado,
							'alta'=>$alta,
							'sisin'=>$sisin,
							);
			$date2=$this->sisin_model->update_sisin1($id_proyecto,$data1);
			redirect(base_url()."proyecto/sisin_controller/");
		}	
		else{
				$this->ingresoSisin($id_proyecto);
		}
	}

	public function updateSisin($id_sisin){
		$id_proyecto=$this->input->post("id_proyecto");
		$codigo_sisin=$this->input->post("codigo_sisin");
		$fecha_inicio=$this->input->post("fecha_inicio");
		$fecha_conclusion=$this->input->post("fecha_conclusion");
		$tipologia=$this->input->post("tipologia");
		$area_influencia=$this->input->post("area_influencia");
		$tipo_inversion=$this->input->post("tipo_inversion");
		$sector=$this->input->post("sector");
		$sub_sector=$this->input->post("sub_sector");
		$this->aux_sub_sector = $sub_sector;
		$denominacion=$this->input->post("denominacion");

		//$this->form_validation->set_rules("id_proyecto", "proyecto","is_unique[sisin.id_proyecto]");
		$this->form_validation->set_rules("codigo_sisin", "Codigo Sisin","required");
		$this->form_validation->set_rules("tipologia", "Tipologia","required");
		$this->form_validation->set_rules("area_influencia", "Área de Influencia","required");
		$this->form_validation->set_rules("tipo_inversion", "Tipo de Inversion","required");
		$this->form_validation->set_rules("sector", "Sector","required");
		$this->form_validation->set_rules("sub_sector", "Sub-Sector","required");
		$this->form_validation->set_rules("denominacion", "Denominacion","callback_verificarDenominacion");

		$this->form_validation->set_message("required", "El campo %s es requerido");
		$this->form_validation->set_message("verificarDenominacion", "El campo %s es requerido");
		//$this->form_validation->set_message("is_unique", "El  %s ya existe");
		if ($this->form_validation->run()){
			$data = array(
				'id_proyecto'=>$id_proyecto,
				'codigo_sisin'=>$codigo_sisin,
				'fecha_inicio'=>$fecha_inicio,
				'fecha_conclusion'=>$fecha_conclusion,
				'tipologia'=>$tipologia,
				'area_influencia'=>$area_influencia,
				'tipo_inversion'=>$tipo_inversion,
				'id_sector'=>$sector,
				'id_sub_sector'=>$sub_sector,
				'id_denominacion'=>$denominacion,
			);
			//redirect(base_url()."proyecto/sisin_controller/conSisin");
			//$this->ingresoSisin($id_proyecto);
			//formulario todo correcto actualizar!!!!!!!!!!!!!
			$this->sisin_model->update($data, $id_proyecto);
		}
		//else{
			$this->ingresoSisin($id_proyecto);
		//}
		
	}

	public function get_sub_sector(){
		$sect=$this->input->post('cod');
		if($sect){
			$sub_sectores=$this->sisin_model->get_sub_sector($sect);
			echo '<option value="">Sub_Sector</option>';
			foreach($sub_sectores as $sub_sector){
				echo '<option value="'.$sub_sector->id_clasificacion_sectorial.'">'.$sub_sector->denominacion.'</option>';
			}
		}
	}

	public function get_denominacion(){
		$sect=$this->input->post('cod');		
		if($sect){
			$denominaciones=$this->sisin_model->get_sub_denominacion($sect);
			echo '<option value="" >Denominacion</option>';
			if($denominaciones!=NULL){				
				foreach($denominaciones as $denominacion):?>
				 	<option value="<?php echo $denominacion->id_clasificacion_sectorial;?>"><?php echo $denominacion->denominacion;?></option>
				<?php endforeach;
			}
			
		}	
	}

}	