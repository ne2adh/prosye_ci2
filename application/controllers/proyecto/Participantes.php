<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Participantes extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('date','url');


	}
	public function escoge($monto){
		
		$id_proyecto=$this->input->post("id_proyecto");
		$id_convenio=$this->input->post("id_convenio");
		$id_componente_proy=$this->input->post("id_componente_proy");
		
	    //$id_tipo=$this->input->post("id_tipo");
	    //$cont=$this->input->post("cont");
	    //$id_tramo=$this->input->post("id_tramo");
		$finan=$this->input->post("finan");
		$this->form_validation->set_rules("finan", "Participante de financiamiento en el Proyecto","required");
		$this->form_validation->set_message("required", "El campo %s es requerido");
		if ($this->form_validation->run())
                {
				$data=array('convenio'=>$this->participantes_model->index($id_convenio),
	    			'id_proyecto'=>$id_proyecto,
					'id_convenio'=>$id_convenio,
					'id_componente_proy'=>$id_componente_proy,
	    			'proyecto'=>$this->participantes_model->proyecto($id_proyecto),
	    			'municipio'=>$this->participantes_model->municipio($id_proyecto),
	    			'participa'=>$this->participantes_model->participante($id_proyecto,$id_convenio,$id_componente_proy),
	    			'ente'=>$this->participantes_model->ente(),
					'ente1'=>$this->participantes_model->ente1($id_proyecto,$id_componente_proy,$id_convenio),	
					'monto'=>$monto,

				);
		$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));
		if ($finan==='ente'){ 
			$this->load->view('layouts/header');
			$this->load->view('layouts/aside_proyec', $tramos);
			$this->load->view('admin/proyectos/reg_participante_ente_view',$data);
			$this->load->view('layouts/footer');
		};
		if ($finan==='municipio'){
			$this->load->view('layouts/header');
			$this->load->view('layouts/aside_proyec', $tramos);
			$this->load->view('admin/proyectos/reg_participante_muni_view',$data);
			$this->load->view('layouts/footer');	
		};
	 }else{
		$this->ingreso($id_componente_proy,$id_proyecto,$id_convenio);	
	 };

	}    
	public function ingreso($id_componente_proy,$id_proyecto,$id_convenio)
	{
		$data=array();        
	    $data=array('convenio'=>$this->participantes_model->index($id_convenio),
	    			'id_proyecto'=>$id_proyecto,
					'id_convenio'=>$id_convenio,
					'id_componente_proy'=>$id_componente_proy,
					'proyecto'=>$this->participantes_model->proyecto($id_proyecto),
					'componente'=>$this->participantes_model->componente($id_componente_proy),
	    			'municipio'=>$this->participantes_model->municipio($id_proyecto),
	    			'participa'=>$this->participantes_model->participante($id_proyecto,$id_convenio,$id_componente_proy),
	    			'ente'=>$this->participantes_model->ente(),
	    			'ente1'=>$this->participantes_model->ente1($id_proyecto,$id_componente_proy,$id_convenio),
	    			//'suma'=>$this->participantes_model->sumar($id_proyecto,$id_componente_proy,$id_convenio),
					'total_participa'=>$this->participantes_model->totalParticipante($id_proyecto,$id_convenio),
		);
		$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/proyectos/registro_participantes_view',$data);
		$this->load->view('layouts/footer');

	}
	public function ingreso_sc($id_convenio,$id_proyecto)
	{
		$data=array();        
	    $data=array('convenio'=>$this->participantes_model->index($id_convenio),
	    			'id_proyecto'=>$id_proyecto,
	    			'id_convenio'=>$id_convenio,
	    			//'id_tipo'=>$id_tipo,
	    			//'cont'=>$cont,
	    			//'id_tramo'=>$id_tramo,
	    			'municipio'=>$this->participantes_model->municipio($id_proyecto),
	    			'participa'=>$this->participantes_model->participante($id_convenio),
	    			'ente'=>$this->participantes_model->ente(),	
	    			//'tramo'=>$this->convenio_model->getTramo($id_tramo),
	    			//$this->participantes_model->insert($data);
	    			redirect(base_url()."proyecto/participantes/store_sc/$id_proyecto"),

		);
		$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/proyectos/registro_participantes_view_sc',$data);
		$this->load->view('layouts/footer');

	}

	public function nuevo()
	{
		$id_proyecto=$this->input->post("id_proyecto");
		redirect(base_url()."proyecto/convenio_controller/ingresoConvenio/$id_proyecto");
	}

	

	
	public function store()
	{


		$id_participa=$this->participantes_model->current_part();
		$id_proyecto=$this->input->post("id_proyecto");
	    $id_convenio=$this->input->post("id_convenio");
	    $id_componente_proy=$this->input->post("id_componente_proy");
		$monto_comp=$this->input->post("costo");
		$id_participa=$id_participa;
		$id_proyecto=$id_proyecto;
		$monto_municipio=0;
		$id_municipio=0;
		$id_convenio=$id_convenio;
		$ente_finan=$this->input->post("ente_finan");
		$participacion=$this->input->post("participacion");
		//$id_componente_proy=$id_componente_proy;
		
		$this->form_validation->set_rules("ente_finan", "Ente de financiamiento del Proyecto","required");
		$this->form_validation->set_rules("participacion", "Monto de Participacion en el Proyecto","required");

		$this->form_validation->set_message("required", "El campo %s es requerido");
		echo $id_proyecto,$id_participa;
		if ($this->form_validation->run())
                {
					$data = array(
								'id_participa'=>$id_participa,
								'id_proyecto'=>$id_proyecto,
								'monto_municipio'=>0,
								'id_municipio'=>0,
								'id_convenio'=>$id_convenio,
								'id_ente_finan'=>$ente_finan,
								'monto_ente'=>$participacion,
								'id_componente_proy'=>$id_componente_proy,
								);
								$this->participantes_model->insert($data);
								redirect(base_url()."proyecto/participantes/ingreso/$id_componente_proy/$id_proyecto/$id_convenio");
									
				}
				else
					{
						$this->escoge($monto_comp);	
					};	
				
	}			
	public function store1()

	{
		$id_participa=$this->participantes_model->current_part();
		$id_proyecto=$this->input->post("id_proyecto");
		$id_convenio=$this->input->post("id_convenio");
		$id_componente_proy=$this->input->post("id_componente_proy");
	    $data['id_participa']=$id_participa;
		//$data['id_tramo']=$id_tramo;
		$data['id_proyecto']=$id_proyecto;
		$data['monto_municipio']=$this->input->post("participacion_muni");
		$data['id_municipio']=$this->input->post("municipio_finan");
		$data['id_convenio']=$id_convenio;
		$data['id_ente_finan']=0;
		$data['monto_ente']=0;
		$data['id_componente_proy']=$id_componente_proy;
		$this->participantes_model->insert($data);
		redirect(base_url()."proyecto/participantes/ingreso/$id_componente_proy/$id_proyecto/$id_convenio");
			
	}
	
	public function store_sc($id_convenio,$id_proyecto)

	{
		$id_participa=$this->participantes_model->current_part();
		$id_proyecto=$id_proyecto;
	    $id_convenio=$id_convenio;
	    $monto=$this->participantes_model->monto($id_proyecto);
		$data['id_participa']=$id_participa;
		//$data['id_tramo']=$id_tramo;
		$data['id_proyecto']=$id_proyecto;
		$data['monto_municipio']=0;
		$data['id_municipio']=0;
		$data['id_convenio']=$id_convenio;
		$data['id_ente_finan']=1;
		$data['monto_ente']=$monto->costo_proyecto;
		$this->participantes_model->insert($data);
		/*$datas['id_tramo']=$id_tramo;
		$datas['id_proyecto']=$id_proyecto;
		$datas['porcentaje_beneficio']=$this->input->post("porcentaje_beneficio");
		$datas['porcentaje_participacion']=$this->input->post("porcentaje_participacion");
		$datas['por_incidencia_proy']=$this->input->post("por_incidencia_proy");
		$datas['cant_empleo']=$this->input->post("cant_empleo");
		$datas['poblacion_beneficiada']=$this->input->post("poblacion_beneficiada");
		$this->participantes_model->insert_detalle($datas);*/
		redirect(base_url()."proyecto/numero_tramo_controller/ingresoNumTramo/$id_proyecto");
			
	}
	public function delete($id_participa,$id_componente_proy,$id_proyecto,$id_convenio)
	{
		 $id_participa=$id_participa;
		 $id_componente_proy=$id_componente_proy;
		 $id_proyecto=$id_proyecto;
		 $id_convenio=$id_convenio;
		 $this->participantes_model->delete($id_participa,$id_componente_proy,$id_proyecto,$id_convenio);
		 redirect(base_url()."proyecto/participantes/ingreso/$id_componente_proy/$id_proyecto/$id_convenio");

		 
	}
	public function editMunicipio($id_participa,$id_componente_proy,$id_proyecto,$id_convenio)
	{
		
				$data=array(	
					//'monto'=>$monto,
					'convenio'=>$this->participantes_model->index($id_convenio),
	    			'id_proyecto'=>$id_proyecto,
					'id_convenio'=>$id_convenio,
					'id_componente_proy'=>$id_componente_proy,
					'id_participa'=>$id_participa,
					'proyecto'=>$this->participantes_model->proyecto($id_proyecto),
					'componente'=>$this->participantes_model->componente($id_componente_proy),
	    			'municipio'=>$this->participantes_model->municipio($id_proyecto),
	    			'participa'=>$this->participantes_model->participante($id_proyecto,$id_convenio,$id_componente_proy),
	    			'participante'=>$this->participantes_model->participa($id_participa,$id_componente_proy,$id_proyecto,$id_convenio),
	    			'ente'=>$this->participantes_model->ente(),
	    			'ente1'=>$this->participantes_model->ente1($id_proyecto,$id_componente_proy,$id_convenio),

				);
		$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));	
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/proyectos/reg_participante_muni_view_edit',$data);
		$this->load->view('layouts/footer');
	}  
	public function editEnte($id_participa,$id_componente_proy,$id_proyecto,$id_convenio)
	{
		
		
             
				$data=array(	
					//'monto'=>$monto,
					'convenio'=>$this->participantes_model->index($id_convenio),
	    			'id_proyecto'=>$id_proyecto,
					'id_convenio'=>$id_convenio,
					'id_componente_proy'=>$id_componente_proy,
					'id_participa'=>$id_participa,
					'proyecto'=>$this->participantes_model->proyecto($id_proyecto),
					'componente'=>$this->participantes_model->componente($id_componente_proy),
	    			'municipio'=>$this->participantes_model->municipio($id_proyecto),
	    			'participa'=>$this->participantes_model->participante($id_proyecto,$id_convenio,$id_componente_proy),
	    			'participanteFinan'=>$this->participantes_model->participafinan($id_participa,$id_componente_proy,$id_proyecto,$id_convenio),
	    			'ente'=>$this->participantes_model->ente(),
	    			'ente1'=>$this->participantes_model->ente1($id_proyecto,$id_componente_proy,$id_convenio),

				);
		$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/proyectos/reg_participante_ente_view_edit',$data);
		$this->load->view('layouts/footer');
		

	}  
	public function update()
	{

		$id_participa=$this->input->post("id_participa");
		$id_proyecto=$this->input->post("id_proyecto");
		$id_convenio=$this->input->post("id_convenio");
		$id_componente_proy=$this->input->post("id_componente_proy");
	    $data['id_participa']=$id_participa;
		//$data['id_tramo']=$id_tramo;
		$data['id_proyecto']=$id_proyecto;
		$data['monto_municipio']=$this->input->post("participacion_muni");
		$data['id_municipio']=$this->input->post("municipio_finan");
		$data['id_convenio']=$id_convenio;
		$data['id_ente_finan']=0;
		$data['monto_ente']=0;
		$data['id_componente_proy']=$id_componente_proy;
		$this->participantes_model->updateMuni($id_participa,$data);
		redirect(base_url()."proyecto/participantes/ingreso/$id_componente_proy/$id_proyecto/$id_convenio");
				
	}	
	public function updateEnte()
	{

		$id_participa=$this->input->post("id_participa");
		$id_proyecto=$this->input->post("id_proyecto");
		$id_convenio=$this->input->post("id_convenio");
		$id_componente_proy=$this->input->post("id_componente_proy");
	    $data['id_participa']=$id_participa;
		//$data['id_tramo']=$id_tramo;
		$data['id_proyecto']=$id_proyecto;
		$data['monto_municipio']=0;
		$data['id_municipio']=0;
		$data['id_convenio']=$id_convenio;
		$data['id_ente_finan']=$this->input->post("ente_finan");
		$data['monto_ente']=$this->input->post("participacion");
		$data['id_componente_proy']=$id_componente_proy;
		$this->participantes_model->updateMuni($id_participa,$data);
		redirect(base_url()."proyecto/participantes/ingreso/$id_componente_proy/$id_proyecto/$id_convenio");
				
	}			  		  
}	