<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indicador_controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

        $this->load->library('form_validation','from_helper');
        $this->load->helper('date','url');


	}

	public function index()
	{
		$id=array();
		$id=$this->session->userdata('id');
        $data['results'] =  $this->proyectos_model->get_proyectos_mios($id);
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec');
		$this->load->view('admin/proyectos/mis_proy_indicadores',$data);
		$this->load->view('layouts/footer');
	}
	public function nuevo()
	{
		$id_proyecto=$this->input->post("id_proyecto");
		$dato=$this->input->post("resp");
		redirect(base_url()."proyecto/indicador_controller/ingresoIndicador/$id_proyecto/$dato");
	}
	public function escoge($id_proyecto)
	{
		if($id_proyecto >= 100000){
			//recuperar datos del tramo
			$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
			$idProyecto = $data_proyecto->id_tramo;
		}
		else{
			$data_proyecto = $this->componente_model->getProyecto($id_proyecto);
			$idProyecto = $id_proyecto;
		}

		$data=array('indicador'=>$this->indicador_model->indicador($idProyecto),
					'id_proyecto'=>$id_proyecto,
					'proyecto'=>$data_proyecto,
					);
		$tramos = array("tramos" => $this->tramo_model->index($idProyecto));
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/proyectos/escoge_indicador',$data);
		$this->load->view('layouts/footer');

	}
	public function ingresoIndicador($id_proyecto){
		$data1['id_proyecto']=$id_proyecto;
		$data1['pant']=6;
		//$dato=$dato;
		$this->proyectos_model->update_pant($data1,$id_proyecto);
		if($id_proyecto >= 100000){
			//recuperar datos del tramo
			$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
			$idProyecto = $data_proyecto->id_tramo;
		}
		else{
			$data_proyecto = $this->componente_model->getProyecto($id_proyecto);
			$idProyecto = $id_proyecto;
		}
		$data=array('indicador'=>$this->indicador_model->indicador($id_proyecto),
					'id_proyecto'=>$id_proyecto,
					//'dato'=>$dato,
					'proyecto'=>$data_proyecto,
				    );
		$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));
    	$this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/proyectos/registro_indicador',$data);
		$this->load->view('layouts/footer');
	}

	public function ingresoErr($id_proyecto, $existe_error){
		$data1['id_proyecto']=$id_proyecto;
		$data1['pant']=6;
		//$dato=$dato;
		$this->proyectos_model->update_pant($data1,$id_proyecto);
		$data=array('indicador'=>$this->indicador_model->indicador($id_proyecto),
					'id_proyecto'=>$id_proyecto,
					//'dato'=>$dato,
					'proyecto'=>$this->indicador_model->traerProy($id_proyecto),
					'error' => $existe_error
		    );
		$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));
    	$this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/proyectos/registro_indicador',$data);
		$this->load->view('layouts/footer');
	}

	public function store(){
		$id_proyecto=$this->input->post("id_proyecto");
		$nombre_indicador=$this->input->post("nombre");
		$valor_indicador=$this->input->post("valor");
		$tipo=$this->post("tipo");
		//$dato=$this->input->post("dato");
		$valor_indicador = str_replace(',','',$valor_indicador);
		$this->form_validation->set_rules("nombre", "Indicador Económico","required");
		$this->form_validation->set_rules("valor", "Valor del Indicador","trim|required");
		$this->form_validation->set_rules("tipo", "tipo del Indicador","trim|required");

		$this->form_validation->set_message("required", "El campo %s es requerido");
		$this->form_validation->set_message("is_natural_no_zero", "El campo %s debe contener solo numeros positivos mayores a cero");
		$this->form_validation->set_message("less_than", "El campo %s es numerico y menor o igual a 100");

		if ($this->form_validation->run())
        {

				$data = array(
								'id_proyecto'=>$id_proyecto,
								'nombre_indicador'=>$nombre_indicador,
								'valor_indicador'=>$valor_indicador,
								'tipo'=>$tipo,
								'valor_indicador_porc'=>0,
							 );

						$resp=0;
						//echo $nombre_indicador;
						$resp=$this->indicador_model->compara($nombre_indicador,$id_proyecto);
						//echo $resp;
						if ($resp!=1)
									{
									//$data1['id_proyecto']=$id_proyecto;
									//$data1['pant']=6;
									//$this->proyectos_model->update_pant($data1,$id_proyecto);
									$this->indicador_model->insert($data);
									redirect(base_url()."proyecto/indicador_controller/ingresoIndicador/$id_proyecto");
									}
								else{
									//echo ('---------------------------Indicador Repetido---------------------------');
									//-----------------------------//
									$this->ingresoErr($id_proyecto, 1);}
									//$this->ingresoIndicador($id_proyecto,$dato);}
		}else{
				$this->ingresoIndicador($id_proyecto);
			}


	}
	public function store_porc()
	{
		$id_proyecto=$this->input->post("id_proyecto");
		$nombre_indicador=$this->input->post("indicador");
		$valor_indicador_porc=$this->input->post("valor_porc");
		$tipo=$this->input->post("tipo");
		//$dato=$this->input->post("dato");
		$valor_indicador_porc = str_replace(',', '', $valor_indicador_porc);

		$this->form_validation->set_rules("indicador", "Indicador Económico","required");
		$this->form_validation->set_rules("valor_porc", "Valor del Indicador","trim|required");

		$this->form_validation->set_message("required", "El campo %s es requerido");
		$this->form_validation->set_message("is_natural_no_zero", "El campo %s debe contener solo numeros positivos mayores a cero");
		$this->form_validation->set_message("less_than", "El campo %s es numerico y menor o igual a 100");

		if ($this->form_validation->run())
        {

				$data = array(
								'id_proyecto'=>$id_proyecto,
								'nombre_indicador'=>$nombre_indicador,
								'valor_indicador'=>0,
								'valor_indicador_porc'=>$valor_indicador_porc,
								'tipo'=>$tipo,
							 );

						$resp=0;
						//echo $nombre_indicador;
						$resp=$this->indicador_model->compara($nombre_indicador,$id_proyecto);
						//echo $resp;
						if ($resp!=1)
									{
									$data1['id_proyecto']=$id_proyecto;
									$data1['pant']=6;
									$this->proyectos_model->update_pant($data1,$id_proyecto);
									$this->indicador_model->insert($data);
									redirect(base_url()."proyecto/indicador_controller/ingresoIndicador/$id_proyecto");

									}
								else{

									//echo ('---------------------------Indicador Repetido---------------------------');

									//-----------------------------//
									$this->ingresoErr($id_proyecto, 1);}

									//$this->ingresoIndicador($id_proyecto,$dato);}

		}else{
				$this->ingresoIndicador($id_proyecto);
			}
	}

	public function delete($id,$id_proyecto)
	{
		 $id=$id;
		 $id_proyecto=$id_proyecto;
		 $this->indicador_model->delete($id);
		 redirect(base_url()."proyecto/indicador_controller/ingresoIndicador/$id_proyecto");
	}


	public function ingresoEdit($id,$id_proyecto,$dato){
		if($id_proyecto >= 100000){
			//recuperar datos del tramo
			$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
			$idProyecto = $data_proyecto->id_tramo;
		}
		else{
			$data_proyecto = $this->componente_model->getProyecto($id_proyecto);
			$idProyecto = $id_proyecto;
		}

		$data=array('indicadorEdit'=>$this->indicador_model->indicadorEdit($id,$idProyecto),
					'indicador'=>$this->indicador_model->indicador($idProyecto),
					'id_proyecto'=>$id_proyecto,
					'id'=>$id,
					'dato'=>$dato,
					'proyecto'=>$data_proyecto,
				    );
		$tramos = array("tramos" => $this->tramo_model->index($idProyecto));
    	$this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/proyectos/registro_indicador_edit',$data);
		$this->load->view('layouts/footer');

	}

	public function update(){
			$id_proyecto=$this->input->post("id_proyecto");
			$nombre_indicador=$this->input->post("indicador");
			$valor_indicador_porc=$this->input->post("valor_porc");
		//	$dato=$this->input->post("dato");
			$id=$this->input->post("id");
			$tipo=$this->input->post("tipo");
			$valor_indicador_porc = str_replace(',','',$valor_indicador_porc);
		$this->form_validation->set_rules("indicador", "Indicador Económico","required");
		$this->form_validation->set_rules("valor_porc", "Valor del Indicador","trim|required");

		$this->form_validation->set_message("required", "El campo %s es requerido");
		$this->form_validation->set_message("is_natural_no_zero", "El campo %s debe contener solo numeros positivos mayores a cero");
		$this->form_validation->set_message("less_than", "El campo %s es numerico y menor o igual a 100");

			if ($this->form_validation->run())
        	{

				$data = array(
								'id_proyecto'=>$id_proyecto,
								'nombre_indicador'=>$nombre_indicador,
								'valor_indicador'=>0,
								'valor_indicador_porc'=>$valor_indicador_porc,
								'tipo'=>$tipo,
							 );
							 $this->indicador_model->update($id,$data);
							redirect(base_url()."proyecto/indicador_controller/ingresoIndicador/$id_proyecto");
			}
}

	 public function saveIndicador(){
		if( isset( $_GET['indicador'] ) ){
			$indicador = $this->input->get('indicador');
			$indicador2 = $this->input->get('indicador2');
			$data = array();
			$data['abreviatura']=$indicador;
			$data['descripcion']=$indicador2;
			//print_r($data);
			$this->indicador_model->insertNombre_indicador($data);
			$mensaje = "Registro Correcto";
			echo json_encode(array(
				 'status' => 201, // success or not?
				 'mensaje' => $mensaje
			));
		}else{
			$mensaje = "No existe descripcion";
			echo json_encode(array(
				'status' => 500, // success or not?
				'mensaje' => $mensaje
			));
		}

	}

	public function listarIndicadores(){
	      $resultado = array();
		  $consulta = $this->indicador_model->index();		  
	      if(!empty($consulta)){
	          foreach ($consulta as $fila){
	              $data['id_indicador'] = $fila['id_indicador'];
	              $data['abreviatura'] = $fila['abreviatura'];
				  $data['descripcion'] = $fila['descripcion'];
	              array_push($resultado, $data);
	          }
	      }
		  echo json_encode($resultado);
		  die;
	  }

}
