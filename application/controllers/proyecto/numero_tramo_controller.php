<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Numero_tramo_controller extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
       
        $this->load->library('form_validation');
        $this->load->helper('date','url');


	}
	public function index()
	{
		$id=array();
		//$id=$_GET['id'];
        $data['results'] =  $this->proyectos_model->get_proyectos_espe($id);    
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec');
		$this->load->view('admin/proyectos/mis_proy_num_tramo',$data);
		$this->load->view('layouts/footer');
	}
	
	public function nuevo()
	{
		$id_proyecto=$this->input->post("id_proyecto");
		redirect(base_url()."proyecto/numero_tramo_controller/ingresoNumTramo/$id_proyecto");
	}
	
	public function menu($id_proyecto)
	{
		$id=$id_proyecto;
		$proyecto=$this->tramo_model->index($id_proyecto);
        $data=$this->proyectos_model->get_ruta($id);  
        //echo ($data); 
        switch ($data) {
			case '1':
		}

	}

	public function ingresoNumTramo($id_proyecto)
	{
		$data = array( 
		'proyecto'=> $this->tramo_model->index($id_proyecto),
		
		);    
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec1');
		$this->load->view('admin/proyectos/intro_num_tramos',$data);
		$this->load->view('layouts/footer');
	}
	
	public function ingresoTramo($id_proyecto)
	{
		$cont=$this->input->post('cont');
		$data = array( 
		'proyecto'=> $this->tramo_model->index($id_proyecto),
		'cont'=>$cont,
		);
		$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/proyectos/registro_tramos',$data);
		$this->load->view('layouts/footer');
		

	}
	public function numTramo($id_proyecto){
		//$cont=0;
		$data = array( 
		'proyecto'=> $this->tramo_model->index($id_proyecto),
		//'cont'=>$cont,
		);
		$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/proyectos/intro_num_tramos',$data);
		$this->load->view('layouts/footer');
	}
	public function verifica()
	{
		$cont=0;
		$id_proyecto=$this->input->post('id_proyecto');
		$res=$this->input->post('respuesta');
		$data = array( 
		'proyecto'=> $this->tramo_model->index($id_proyecto),
		'cont'=>$cont,
		'res'=>$res,
		);
		$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));
		if ($res=='si'){ 
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/proyectos/verifica_num_tramos',$data);
		$this->load->view('layouts/footer');
		}else {
			/*if ($res=='No'){ 
	       $this->tramo_model->insert_tipo($data);
	       redirect(base_url()."proyecto/numero_tramo_controller/storeww/$id_tipo/$cont");
		}*/
		}
	}
	public function store()
	{
		
		$id_tipo=$this->tramo_model->current_num_tipo();
		$cont=$this->input->post("cont");

		$data=array();
		$data['id_tipo']=$id_tipo;
		$data['id_proyecto']=$this->input->post("id_proyecto");
		//$data['id_proyecto']=$this->input->post("id_proyecto");
		$data['tipo_fase']=$this->input->post("tipo_fase");
		$data['costo_fase']=$this->input->post("costo_fase");
		$data['fecha_inicio']=$this->input->post("fecha_inicio");
		$data['fecha_conclusion']=$this->input->post("fecha_conclusion");
		$data['numero_tramos']=$this->input->post("numero_tramos");
		
		$this->tramo_model->insert_tipo($data);
		redirect(base_url()."proyecto/tramos_controller/ingresoTramo/$id_tipo/$cont");
		
		
	}
	public function storeST($cont)
	{
		
		$id_tipo=$this->tramo_model->current_num_tipo();
		$cont=$cont;
		
		$data=array();
		$data['id_tipo']=$id_tipo;
		$data['id_proyecto']=$this->input->post("id_proyecto");
		$data['id_proyecto']=$this->input->post("id_proyecto");
		$data['tipo_fase']=$this->input->post("tipo_fase");
		$data['costo_fase']=$this->input->post("costo_fase");
		$data['fecha_inicio']=$this->input->post("fecha_inicio");
		$data['fecha_conclusion']=$this->input->post("fecha_conclusion");
		$data['numero_tramos']=$this->input->post("numero_tramos");
		
		$this->tramo_model->insert_tipo($data);
		redirect(base_url()."proyecto/tramos_controller/ingresoTramoST/$id_tipo/$cont");
		
		
	}

}
