<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sye_controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

        $this->load->library('form_validation','from_helper');
        $this->load->helper('date','url');


	}
	public function index()
	{
		$id=array();
		$data['results'] =  $this->proyectos_model->getProyectos($id);
        $this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/proyectos/mis_proy_sye',$data);
		$this->load->view('layouts/footer');
	}

	public function ingreso($id_proyecto)
	{

		if($id_proyecto>=100000){
			//si es tramo
			$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
			$idProyecto = $data_proyecto->id_proyecto;
			//$id_proy=$data_proyecto->$id_proyecto;
			}
		else{
			$data_proyecto = $this->metas_model->proy($id_proyecto);
			$idProyecto = $id_proyecto;
			//$id_proy=$data_proyecto->$id_proyecto;

		}
		//print_r($idProyecto);
		$mes="";
		$num=getdate();
		switch ($num['mon']) {
				case 1:
						$mes= "ENERO";
						break;
				case 2:
						$mes= "FEBRERO";
						break;
				case 3:
						$mes= "MARZO";
						break;
				case 4:
						$mes= "ABRIL";
						break;
				case 5:
						$mes= "MAYO";
						break;
				case 6:
						$mes= "JUNIO";
						break;
				case 7:
						$mes= "JULIO";
						break;
				case 8:
						$mes= "AGOSTO";
						break;
				case 9:
						$mes= "SEPTIEMBRE";
						break;
				case 10:
						$mes= "OCTUBRE";
						break;
				case 11:
						$mes= "NOVIEMBRE";
						break;
				case 12:
						$mes= "DICIEMBRE";
						break;


		}

		$id=array();
		$tipo_componentes=array();
		$reporte_cos = array();
		$tipo_componentes=$this->segui_actividad_model->traer_componente($id_proyecto);
		for ($i=0; $i < count($tipo_componentes) ; $i++) {
			$tcp= $tipo_componentes[$i]->descrip_compo_proy;
			array_push($reporte_cos, $this->segui_actividad_model->reporte_costo($id_proyecto, $mes, $tcp) ) ;
			$gestion=date('Y');
			$data_proyecto = $this->tramo_model->getTramo($id_proyecto);

		}
		//print_r($id_proy);			//print_r($idProyecto);
	  //print_r($reporte_cos);
		$data=array(
						'id_proyecto'=>$idProyecto,
						'proyectod'=>$data_proyecto,
						'proyec'=>$id_proyecto,
						'results'=>$this->metas_model->proy($id_proyecto),
						//'proyecto'=>$this->segui_actividad_model->proytramo($idProyecto),
						//'tramo'=>$this->segui_actividad_model->proytramo($id_proy),
						'res'=>$this->metas_model->area($idProyecto),
						'resp'=>$this->metas_model->subarea($idProyecto),
						'municipio'=>$this->municipio_model->muni($id_proyecto),
						'prov'=>$this->municipio_model->prov($id_proyecto),
					  'sisin'=>$this->sisin_model->sisin($idProyecto),
					  'compo'=>$this->componente_model->getComponenteProy($id_proyecto),
						'reporte_cos'=>$reporte_cos,
						'reporte_fis'=>$this->segui_actividad_model->reporte_fisico($id_proyecto, $mes),
						'total'=>$this->municipio_model->total($id_proyecto),
						'total_m'=>$this->municipio_model->total_mes($id_proyecto,$mes),
						'monto_proyect'=>$this->segui_actividad_model->traer_monto_proyectado($id_proyecto),
						'monto_gestion_fin'=>$this->segui_actividad_model->traer_gestion_financiera($id_proyecto),
						'reporte_cos_historico'=>$this->segui_actividad_model->monto_historico_financiero($id_proyecto, $mes),
						'nombre_empresa'=>$this->segui_actividad_model->nombre_empresa($id_proyecto),
						'tipo_garantia'=>$this->segui_actividad_model->tipo_garantia($id_proyecto),
						'nombre_empresaS'=>$this->segui_actividad_model->nombre_empresaS($id_proyecto),
						'tipo_garantiaS'=>$this->segui_actividad_model->tipo_garantiaS($id_proyecto),
						'componente'=>$this->segui_actividad_model->componente($id_proyecto),
						'traer_componente'=>$this->segui_actividad_model->traer_componente($id_proyecto),
						'empresa_ejecutora'=>$this->segui_actividad_model->empresa_ejecutora($id_proyecto),
						'segui_fisico'=>$this->segui_actividad_model->segui_fisico($id_proyecto,$mes,$gestion),
						'sisinfecha'=>$this->segui_actividad_model->sisinfecha($idProyecto),

			);
			//print_r($data);
			//$tramo=$this->segui_actividad_model->proytramo($id_proyecto);
			//print_r($id_proyecto);
    $this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/reportes/sye_view',$data);
		$this->load->view('layouts/footer');
	}

}
