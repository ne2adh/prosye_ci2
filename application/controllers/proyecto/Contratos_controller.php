<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contratos_controller extends CI_Controller
{
	private $monto_proyecto;
	public function __construct(){
		parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('date','url');
	}

	public function index(){
	  $id=array();
	  $id=$this->session->userdata('id');
		$data['results'] =  $this->segui_actividad_model->index($id);
    	$this->load->view('layouts/header');
		//$this->load->view('layouts/aside_proyec');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/contratos/lista_proy_contra',$data);
		$this->load->view('layouts/footer');
	}

	public function index1(){
	  $id=array();
	  $id=$this->session->userdata('id');
		$data['results'] =  $this->segui_actividad_model->index1($id);
    $this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/contratos/lista_proy_garantia',$data);
		$this->load->view('layouts/footer');
	}

	public function index2(){
	  $id=array();
	  $id=$this->session->userdata('id');
		$data['results'] =  $this->segui_actividad_model->index1($id);
    $this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/contratos/lista_compo_poy_mod',$data);
		$this->load->view('layouts/footer');
	}

	public function ingreso($id_proyecto){

		if($id_proyecto>=100000){
			//si es tramo
			$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
			$idProyecto = $data_proyecto->id_proyecto;
		}
		else{
			$data_proyecto = $this->metas_model->proy($id_proyecto);
			$idProyecto = $id_proyecto;
		}

		$data=array(
			'id_proyecto'=>$idProyecto,
			'proyectod'=> $data_proyecto,
			'compo'=> $this->segui_actividad_model->compotramo($id_proyecto),
			'proyecto'=>$this->segui_actividad_model->compo($id_proyecto),
			'nombre'=>$this->segui_actividad_model->nombreProy($idProyecto),
			//'inicio'=>$anio_inicio,
			//'fin'=>$anio_fin,
		);
		//$compo=$this->segui_actividad_model->compotramo($id_proyecto);
		$tramos = array("tramos" => $this->tramo_model->index($idProyecto));
		//print_r($compo);

    	$this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/contratos/lista_compo_proy',$data);
		$this->load->view('layouts/footer');
	}

	public function ingreso1($id_proyecto){
		if($id_proyecto>=100000){
			//si es tramo
			$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
			$idProyecto = $data_proyecto->id_proyecto;
		}
		else{
			$data_proyecto = $this->metas_model->proy($id_proyecto);
			$idProyecto = $id_proyecto;
		}
		$inicio=$this->segui_actividad_model->inicio_proyect($id_proyecto);
		$anio_inicio=$this->segui_actividad_model->anio_inicio_proyect($inicio);
		$fin=$this->segui_actividad_model->fin_proyect($id_proyecto);
		$anio_fin=$this->segui_actividad_model->anio_fin_proyect($fin);
		$contrato=$this->segui_actividad_model->indexp($id_proyecto);

		//$id_proyecto=$idProyecto;
		$data=array(
			'proyectod'=> $data_proyecto,
			'id_proyecto'=>$idProyecto,
			'compo'=> $this->segui_actividad_model->compotramo($id_proyecto),
			'proyecto'=>$this->segui_actividad_model->compo($id_proyecto),
			'nombre'=>$this->segui_actividad_model->nombreProy($idProyecto),
			'inicio'=>$anio_inicio,
			'fin'=>$anio_fin,
			'contrato'=>$this->segui_actividad_model->indexp($id_proyecto),
		);
		$c=$this->segui_actividad_model->indexp($id_proyecto);
		//print_r($c);
		$tramos = array("tramos" => $this->tramo_model->index($idProyecto));
    	$this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/contratos/lista_compo_garantia',$data);
		$this->load->view('layouts/footer');

	}

	public function ingreso2($id_proyecto){
		$inicio=$this->segui_actividad_model->inicio_proyect($id_proyecto);
		$anio_inicio=$this->segui_actividad_model->anio_inicio_proyect($inicio);
		$fin=$this->segui_actividad_model->fin_proyect($id_proyecto);
		$anio_fin=$this->segui_actividad_model->anio_fin_proyect($fin);
		$contrato=$this->segui_actividad_model->indexp($id_proyecto);

		$id_proyecto=$id_proyecto;
		$data=array(
			'id_proyecto'=>$id_proyecto,
			'proyecto'=>$this->segui_actividad_model->compo($id_proyecto),
			'nombre'=>$this->segui_actividad_model->nombreProy($id_proyecto),
			'inicio'=>$anio_inicio,
			'fin'=>$anio_fin,
			'contrato'=>$this->segui_actividad_model->indexp($id_proyecto),
		);
    $this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/contratos/lista_compo_mod',$data);
		$this->load->view('layouts/footer');

	}
	public function ing_cont_emp($id_proyecto,$descrip_compo_proy){
		$descrip_compo_proy= str_replace("%20", " ", $descrip_compo_proy);

		$inicio=$this->segui_actividad_model->inicio_proyect($id_proyecto);
		$anio_inicio=$this->segui_actividad_model->anio_inicio_proyect($inicio);
		$fin=$this->segui_actividad_model->fin_proyect($id_proyecto);
		$anio_fin=$this->segui_actividad_model->anio_fin_proyect($fin);
		$contrato = $this->segui_actividad_model->contratoG($id_proyecto,$descrip_compo_proy);
		$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
		$data=array(
			'proyectod'=> $data_proyecto,
			'id_proyecto'=>$id_proyecto,
			'descrip_compo_proy'=>$descrip_compo_proy,
			'proyecto'=>$this->segui_actividad_model->actividad($id_proyecto,$descrip_compo_proy),
			'contrato'=> $contrato,
			//'monto_ejecutado_mes'=>$this->segui_actividad_model->traer_mes_monto($id_proyecto,$mes,$descrip_compo_proy),
			//'monto_ejecutado_gestion'=>$this->segui_actividad_model->costo_ejecutado($id_proyecto,$descrip_compo_proy),
			'inicio'=>$anio_inicio,
			'fin'=>$anio_fin,
			//'inicio'=>$this->segui_actividad_model->inicio_proyect($id_proyecto),
			//'unidad_metrica'=>$this->segui_actividad_model->unidad(),
		);
    	$this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/contratos/ing_cont_emp',$data);
		$this->load->view('layouts/footer');
	}

	public function ingresoErr($id_proyecto, $descrip_compo_proy, $existe_error){
		$descrip_compo_proy= str_replace("%20", " ", $descrip_compo_proy);

		$inicio=$this->segui_actividad_model->inicio_proyect($id_proyecto);
		$anio_inicio=$this->segui_actividad_model->anio_inicio_proyect($inicio);
		$fin=$this->segui_actividad_model->fin_proyect($id_proyecto);
		$anio_fin=$this->segui_actividad_model->anio_fin_proyect($fin);
		$contrato = $this->segui_actividad_model->contratoG($id_proyecto,$descrip_compo_proy);
		$data_proyecto = $this->tramo_model->getTramo($id_proyecto);

		$data=array(

			'proyectod'=> $data_proyecto,
			'id_proyecto'=>$id_proyecto,
			'descrip_compo_proy'=>$descrip_compo_proy,
			'proyecto'=>$this->segui_actividad_model->actividad($id_proyecto,$descrip_compo_proy),
			'contrato'=> $contrato,
			//'monto_ejecutado_mes'=>$this->segui_actividad_model->traer_mes_monto($id_proyecto,$mes,$descrip_compo_proy),
			//'monto_ejecutado_gestion'=>$this->segui_actividad_model->costo_ejecutado($id_proyecto,$descrip_compo_proy),
			'inicio'=>$anio_inicio,
			'fin'=>$anio_fin,
			'error' => $existe_error
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/contratos/ing_cont_emp',$data);
		$this->load->view('layouts/footer');
	}

		public function ing_garantia_compo($id_proyecto,$descrip_compo_proy){
			$descrip_compo_proy=str_replace("%20", " ", $descrip_compo_proy);
			$id_proyecto=$id_proyecto;
			$inicio=$this->segui_actividad_model->inicio_proyect($id_proyecto);
			$contrato=$this->segui_actividad_model->contratoG($id_proyecto,$descrip_compo_proy);
			$anio_inicio=$this->segui_actividad_model->anio_inicio_proyect($inicio);
			$fin=$this->segui_actividad_model->fin_proyect($id_proyecto);
			$anio_fin=$this->segui_actividad_model->anio_fin_proyect($fin);
			$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
			//var_dump($this->segui_actividad_model->garantiaC($contrato[0]->id_contrato,$descrip_compo_proy));
			$data=array(
				'proyectod'=> $data_proyecto,
				'id_proyecto'=>$id_proyecto,
				'descrip_compo_proy'=>$descrip_compo_proy,
				'proyecto'=>$this->segui_actividad_model->actividad($id_proyecto,$descrip_compo_proy),
				'contrato'=>$this->segui_actividad_model->contratoG($id_proyecto,$descrip_compo_proy),
				'garantia'=>$this->segui_actividad_model->garantiaC($contrato[0]->id_contrato,$descrip_compo_proy),
				'inicio'=>$anio_inicio,
				'fin'=>$anio_fin,
			);

			$garantia = $this->segui_actividad_model->garantiaC($contrato[0]->id_contrato,$descrip_compo_proy);

			
	     	$this->load->view('layouts/header');
			$this->load->view('layouts/menugestor');
			$this->load->view('admin/contratos/ing_garantia_compo',$data);
			$this->load->view('layouts/footer');
		}

	public function ingresoErrGarantia($id_proyecto, $descrip_compo_proy, $existe_error){
	$descrip_compo_proy= str_replace("%20", " ", $descrip_compo_proy);

	$inicio=$this->segui_actividad_model->inicio_proyect($id_proyecto);
	$anio_inicio=$this->segui_actividad_model->anio_inicio_proyect($inicio);
	$fin=$this->segui_actividad_model->fin_proyect($id_proyecto);
	$anio_fin=$this->segui_actividad_model->anio_fin_proyect($fin);
	$contrato = $this->segui_actividad_model->contratoG($id_proyecto,$descrip_compo_proy);
	$data_proyecto = $this->tramo_model->getTramo($id_proyecto);


			$data=array(
				'proyectod'=> $data_proyecto,
				'id_proyecto'=>$id_proyecto,
				'descrip_compo_proy'=>$descrip_compo_proy,
				'proyecto'=>$this->segui_actividad_model->actividad($id_proyecto,$descrip_compo_proy),
				'contrato'=>$this->segui_actividad_model->contratoG($id_proyecto,$descrip_compo_proy),
				'garantia'=>$this->segui_actividad_model->garantiaC($contrato[0]->id_contrato,$descrip_compo_proy),
				'inicio'=>$anio_inicio,
				'fin'=>$anio_fin,
				'error' => $existe_error
	);

	$this->load->view('layouts/header');
	$this->load->view('layouts/menugestor');
	$this->load->view('admin/contratos/ing_garantia_compo',$data);
	$this->load->view('layouts/footer');
}
		public function ing_cont_mod($id_proyecto,$descrip_compo_proy){
				$descrip_compo_proy=$descrip_compo_proy;
				$id_proyecto=$id_proyecto;
				$inicio=$this->segui_actividad_model->inicio_proyect($id_proyecto);
				$anio_inicio=$this->segui_actividad_model->anio_inicio_proyect($inicio);
				$fin=$this->segui_actividad_model->fin_proyect($id_proyecto);
				$anio_fin=$this->segui_actividad_model->anio_fin_proyect($fin);
				$contrato= $this->segui_actividad_model->contratoG($id_proyecto,$descrip_compo_proy);


				$data=array(
					'id_proyecto'=>$id_proyecto,
					'descrip_compo_proy'=>$descrip_compo_proy,
					'proyecto'=>$this->segui_actividad_model->actividad($id_proyecto,$descrip_compo_proy),
					'contrato'=>$contrato,
					'modificacion'=>$this->segui_actividad_model->modificacion($contrato[0]->id_contrato,$descrip_compo_proy),
					//'monto_ejecutado_mes'=>$this->segui_actividad_model->traer_mes_monto($id_proyecto,$mes,$descrip_compo_proy),
					//'monto_ejecutado_gestion'=>$this->segui_actividad_model->costo_ejecutado($id_proyecto,$descrip_compo_proy),
					'inicio'=>$anio_inicio,
					'fin'=>$anio_fin,
					//'inicio'=>$this->segui_actividad_model->inicio_proyect($id_proyecto),
					//'unidad_metrica'=>$this->segui_actividad_model->unidad(),
				);
				$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));
		        $this->load->view('layouts/header');
				$this->load->view('layouts/aside_proyec', $tramos);
				$this->load->view('admin/contratos/ing_cont_mod',$data);
				$this->load->view('layouts/footer');
		}
		
	function validarMonto($costo_adjudicado){		
		if(floatval($costo_adjudicado) <= floatval($this->monto_proyecto))
			return TRUE;
		return FALSE;
		//return (intval($costo_adjudicado) > intval($this->monto_proyecto));
	}

	public function store(){
			$descrip_compo_proy=$this->input->post('descrip_compo_proy');
			$id_proyecto=$this->input->post('id_proyecto');
			$nombre_empresa=$this->input->post('nombre_empresa');
			$costo_adj=$this->input->post('costo_adj');
			$plazo_contra=$this->input->post("plazo_contra");
			$fecha_ini=$this->input->post('fecha_ini');
			$fecha_con=$this->input->post('fecha_con');
			$adm=$this->input->post('adm');
			$monto_proy=$this->input->post('m_proy');
			$id_contrato=$this->input->post('id_contrato');
			$costo_adj=str_replace(',','',$costo_adj);
			//print_r($id_contrato);
			if($adm){
				$adm_valor=1;
			}else{
				$adm_valor=0;
			}
			$this->monto_proyecto = $monto_proy;
			/*echo $area.' '.$id_subarea.' '.$tipo_fase.' '.$nombre_proyecto.' '.$costo_proyecto.' '.$tipo_proyecto.' '.$modalidad.' '.$fecha_inicio.' '.$fecha_conclusion.' '.$duracion_anos.' '.$descripcion_proyecto.' '.$objetivo_proyecto.' '.$id_proyecto;*/
			$this->form_validation->set_rules("nombre_empresa", "Nombre de la empresa","trim|required");
			$this->form_validation->set_rules("costo_adj", "Costo adjudicado","trim|required|callback_validarMonto");
			$this->form_validation->set_rules("plazo_contra", "Plazo contractual del Proyecto","trim|required");
			$this->form_validation->set_rules("fecha_ini", "Fecha inicio de contrato del Proyecto","trim|required");
			$this->form_validation->set_rules("fecha_con", "Fecha conclucion de contrato del Proyecto","trim|required");

			$this->form_validation->set_message("required", "El campo %s es requerido");
			$this->form_validation->set_message("numeric", "El campo %s es numerico");
			$this->form_validation->set_message("decimal", "El campo %s es numerico");
			$this->form_validation->set_message("is_natural", "El campo %s debe contener solo numeros positivos mayores a cero");
			$this->form_validation->set_message("validarMonto", "El costo adjudicado es mayor al monto del componente.");
			if ($this->form_validation->run()){
						$data = array(
							'nombre_empresa'=>$nombre_empresa,
							'costo_adj'=>$costo_adj,
							'plazo_contra'=>$plazo_contra,
							'fecha_ini'=>$fecha_ini,
							'fecha_con'=>$fecha_con,
							'id_proyecto'=>$id_proyecto,
							'descrip_compo_proy'=>$descrip_compo_proy,
							'estado'=>1,
							'adm'=>$adm_valor,
							'id_contrato'=>$id_contrato,
							);
						$resp=0;
						$resp=$this->contrato_model->compara($id_contrato,$id_proyecto);
						if ($resp!=1){
							//echo $resp;
									$this->segui_actividad_model->insert_contrato($data);
									$this->ing_cont_emp($id_proyecto,$descrip_compo_proy);
						}
						else{
						 	//echo ('---------ingreso error-----------------');
						 	$this->ingresoErr($id_proyecto,$descrip_compo_proy, 1);
						}
					}else{							
							$this->ing_cont_emp($id_proyecto,$descrip_compo_proy);
							//$this->ing_seg_fin($id_proyecto,$descrip_compo_proy);
					}
				}
	public function store_garantia(){
		$descrip_compo_proy=$this->input->post('descrip_compo_proy');
		$id_proyecto=$this->input->post('id_proyecto');
		$tipo=$this->input->post('tipo');
		$monto=$this->input->post('monto');
		$entidad_financiera=$this->input->post("entidad_financiera");
		$id_contrato=$this->input->post('id_contrato');
		$monto=str_replace(',','',$monto);
		/*echo $area.' '.$id_subarea.' '.$tipo_fase.' '.$nombre_proyecto.' '.$costo_proyecto.' '.$tipo_proyecto.' '.$modalidad.' '.$fecha_inicio.' '.$fecha_conclusion.' '.$duracion_anos.' '.$descripcion_proyecto.' '.$objetivo_proyecto.' '.$id_proyecto;*/
		$this->form_validation->set_rules("tipo", "tipo de garantia de la empresa","trim|required");
		$this->form_validation->set_rules("monto", "Monto de la garantia","trim|required");
		$this->form_validation->set_rules("entidad_financiera", "Entidad financiera de la garantia","trim|required");

		$this->form_validation->set_message("required", "El campo %s es requerido");
		$this->form_validation->set_message("numeric", "El campo %s es numerico");
		$this->form_validation->set_message("decimal", "El campo %s es decimal");
		$this->form_validation->set_message("is_natural", "El campo %s debe contener solo numeros positivos mayores a cero");
		if ($this->form_validation->run()){
					$data = array(
						'tipo'=>$tipo,
						'monto'=>$monto,
						'descrip_compo_proy'=>$descrip_compo_proy,
						'entidad_financiera'=>$entidad_financiera,
						'id_contrato'=>$id_contrato,
						'id_proyecto'=>$id_proyecto,
						);

			$resp=0;
			$resp=$this->contrato_model->comparaGarantia($id_contrato,$id_proyecto);
			if ($resp!=1){

				$this->segui_actividad_model->insert_garantia($data);
				redirect(base_url()."proyecto/contratos_controller/ing_garantia_compo/".trim($id_proyecto)."/".$descrip_compo_proy);
			}else{
						echo '--------algo salio mal------------';
						$this->ingresoErrGarantia($id_proyecto,$descrip_compo_proy, 1);

					}
		}else{
			echo '-------------error repetidos-----------';
			$this->ing_garantia_compo($id_proyecto,$descrip_compo_proy);

		}
	}
	public function store_mod(){
		$descrip_compo_proy=$this->input->post('descrip_compo_proy');
		$id_proyecto=$this->input->post('id_proyecto');
		$fecha_modif=$this->input->post('fecha_modif');
		$motivo=$this->input->post('motivo');
		$fecha_ini=$this->input->post("fecha_ini");
		$fecha_fin=$this->input->post("fecha_fin");
		$monto_incremento=$this->input->post('monto_incremento');
		$id_contrato=$this->input->post('id_contrato');
		/*echo $area.' '.$id_subarea.' '.$tipo_fase.' '.$nombre_proyecto.' '.$costo_proyecto.' '.$tipo_proyecto.' '.$modalidad.' '.$fecha_inicio.' '.$fecha_conclusion.' '.$duracion_anos.' '.$descripcion_proyecto.' '.$objetivo_proyecto.' '.$id_proyecto;*/
		$this->form_validation->set_rules("motivo", "Motivo de la modificacion del contrato","trim|required");
		$this->form_validation->set_rules("fecha_ini","Fecha inicio segun modificacion de contrato","trim|required");
		$this->form_validation->set_rules("fecha_fin","Fecha conclucion segun modificacion de contrato","trim|required");
		$this->form_validation->set_rules("monto_incremento", "Monto de incremento al contrato","trim|required");

		$this->form_validation->set_message("required", "El campo %s es requerido");
		$this->form_validation->set_message("numeric", "El campo %s es numerico");
		$this->form_validation->set_message("decimal", "El campo %s es numerico");
		$this->form_validation->set_message("is_natural", "El campo %s debe contener solo numeros positivos mayores a cero");
		if ($this->form_validation->run()){
					$data = array(
						'id_contrato'=>$id_contrato,
						'fecha_modif'=>$fecha_modif,
						'motivo'=>$motivo,
						'fecha_ini'=>$fecha_ini,
						'fecha_fin'=>$fecha_fin,
						'monto_incremento'=>$monto_incremento,
						'descrip_compo_proy'=>$descrip_compo_proy,
						'id_contrato'=>$id_contrato,
						);
						$this->segui_actividad_model->insert_mod_cont($data);
						$descrip_compo_proy=$descrip_compo_proy;
						//echo $mes;
						$this->ing_cont_mod($id_proyecto,$descrip_compo_proy);
		}else{
						echo '-------------error repetidos-----------';
						$this->ing_cont_mod($id_proyecto,$descrip_compo_proy);
						//$this->ing_seg_fin($id_proyecto,$descrip_compo_proy);
					}
			}

	public function delete($id_contrato,$id_proyecto,$descrip_compo_proy){
		 $id_contrato=$id_contrato;
		 $id_proyecto=$id_proyecto;
		 $descrip_compo_proy=$descrip_compo_proy;
		 $this->segui_actividad_model->delete_contrato($id_contrato);
		 redirect(base_url()."proyecto/contratos_controller/ing_cont_emp/$id_proyecto/$descrip_compo_proy");
	}

	public function delete1($id_garantia,$id_proyecto,$descrip_compo_proy){
		 $this->segui_actividad_model->delete_garantia($id_garantia);
		 redirect(base_url()."proyecto/contratos_controller/ing_garantia_compo/$id_proyecto/$descrip_compo_proy");
	}
	public function delete2($id,$id_proyecto,$descrip_compo_proy){
		 $id=$id;
		 $id_proyecto=$id_proyecto;
		 $descrip_compo_proy=$descrip_compo_proy;
		 $this->segui_actividad_model->delete_mod($id);
		 redirect(base_url()."proyecto/contratos_controller/ing_cont_mod/$id_proyecto/$descrip_compo_proy");
	}
public function edit($id_contrato,$descrip_compo_proy,$id_proyecto){
		$descrip_compo_proy= str_replace("%20", " ", $descrip_compo_proy);
		$inicio=$this->segui_actividad_model->inicio_proyect($id_proyecto);
		$anio_inicio=$this->segui_actividad_model->anio_inicio_proyect($inicio);
		$fin=$this->segui_actividad_model->fin_proyect($id_proyecto);
		$anio_fin=$this->segui_actividad_model->anio_fin_proyect($fin);
		$contrato = $this->segui_actividad_model->contratoG($id_proyecto,$descrip_compo_proy);
		$data_proyecto = $this->tramo_model->getTramo($id_proyecto);

		$data=array(
			'proyectod'=> $data_proyecto,
			'id_proyecto'=>$id_proyecto,
			'descrip_compo_proy'=>$descrip_compo_proy,
			'proyecto'=>$this->segui_actividad_model->actividad($id_proyecto,$descrip_compo_proy),
			'contrato'=> $contrato,
			//'monto_ejecutado_mes'=>$this->segui_actividad_model->traer_mes_monto($id_proyecto,$mes,$descrip_compo_proy),
			//'monto_ejecutado_gestion'=>$this->segui_actividad_model->costo_ejecutado($id_proyecto,$descrip_compo_proy),
			'inicio'=>$anio_inicio,
			'fin'=>$anio_fin,
			//'inicio'=>$this->segui_actividad_model->inicio_proyect($id_proyecto),
			//'unidad_metrica'=>$this->segui_actividad_model->unidad(),
		);
		//print_r($data);
    	$this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/contratos/ing_cont_emp_edit',$data);
		$this->load->view('layouts/footer');

	}
public function update(){
			$descrip_compo_proy=$this->input->post('descrip_compo_proy');
			$id_proyecto=$this->input->post('id_proyecto');
			$nombre_empresa=$this->input->post('nombre_empresa');
			$costo_adj=$this->input->post('costo_adj');
			$plazo_contra=$this->input->post("plazo_contra");
			$fecha_ini=$this->input->post('fecha_ini');
			$fecha_con=$this->input->post('fecha_con');
			$adm=$this->input->post('adm');
			$monto_proy = $this->input->post('m_proy');
			$id_contrato=$this->input->post('id_contrato');
			//print_r($id_contrato);
			if($adm){
				$adm_valor=1;
			}else{
				$adm_valor=0;
			}

			$costo_adj=str_replace(',','',$costo_adj);
			$this->monto_proyecto = $monto_proy;
			
			$this->form_validation->set_rules("nombre_empresa", "Nombre de la empresa","trim|required");
			$this->form_validation->set_rules("costo_adj", "Costo adjudicado","trim|required|callback_validarMonto");
			$this->form_validation->set_rules("plazo_contra", "Plazo contractual del Proyecto","trim|required");
			$this->form_validation->set_rules("fecha_ini", "Fecha inicio de contrato del Proyecto","trim|required");
			$this->form_validation->set_rules("fecha_con", "Fecha conclucion de contrato del Proyecto","trim|required");

			$this->form_validation->set_message("required", "El campo %s es requerido");
			$this->form_validation->set_message("numeric", "El campo %s es numerico");
			$this->form_validation->set_message("decimal", "El campo %s es numerico");
			$this->form_validation->set_message("is_natural", "El campo %s debe contener solo numeros positivos mayores a cero");
			$this->form_validation->set_message("validarMonto", "El costo adjudicado es mayor al monto del componente.");

			if ($this->form_validation->run())
	                {
						$data = array(
							'nombre_empresa'=>$nombre_empresa,
							'costo_adj'=>$costo_adj,
							'plazo_contra'=>$plazo_contra,
							'fecha_ini'=>$fecha_ini,
							'fecha_con'=>$fecha_con,
							'id_proyecto'=>$id_proyecto,
							'descrip_compo_proy'=>$descrip_compo_proy,
							'estado'=>1,
							'adm'=>$adm_valor,
							'id_contrato'=>$id_contrato,
							);
							//echo $resp;
							$this->contrato_model->update($id_contrato,$data);
							$this->ing_cont_emp($id_proyecto,$descrip_compo_proy);

					}else{
						//	echo '-------------error en llenado de datos-----------';
							$this->edit($id_proyecto,$descrip_compo_proy, $id_proyecto);
							//$this->ing_seg_fin($id_proyecto,$descrip_compo_proy);
						}
	}
public function editGarantia($id_garantia,$descrip_compo_proy,$id_proyecto){

			$descrip_compo_proy=str_replace("%20", " ", $descrip_compo_proy);
			$id_proyecto=$id_proyecto;
			$inicio=$this->segui_actividad_model->inicio_proyect($id_proyecto);
			$contrato=$this->segui_actividad_model->contratoG($id_proyecto,$descrip_compo_proy);
			$anio_inicio=$this->segui_actividad_model->anio_inicio_proyect($inicio);
			$fin=$this->segui_actividad_model->fin_proyect($id_proyecto);
			$anio_fin=$this->segui_actividad_model->anio_fin_proyect($fin);
			$data_proyecto = $this->tramo_model->getTramo($id_proyecto);

			$data=array(
				'proyectod'=> $data_proyecto,
				'id_proyecto'=>$id_proyecto,
				'descrip_compo_proy'=>$descrip_compo_proy,
				'proyecto'=>$this->segui_actividad_model->actividad($id_proyecto,$descrip_compo_proy),
				'contrato'=>$this->segui_actividad_model->contratoG($id_proyecto,$descrip_compo_proy),
				'garantia'=>$this->segui_actividad_model->garantiaC($contrato[0]->id_contrato,$descrip_compo_proy),
				'inicio'=>$anio_inicio,
				'fin'=>$anio_fin,
			);
		//print_r($data);
    	$this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/contratos/ing_garantia_compo_edit',$data);
		$this->load->view('layouts/footer');

	}

public function updateGarantia(){

		$descrip_compo_proy=$this->input->post('descrip_compo_proy');
		$id_proyecto=$this->input->post('id_proyecto');
		$tipo=$this->input->post('tipo');
		$monto=$this->input->post('monto');
		$entidad_financiera=$this->input->post("entidad_financiera");
		$id_garantia=$this->input->post('id_garantia');

		$monto=str_replace(',','',$monto);
		/*echo $area.' '.$id_subarea.' '.$tipo_fase.' '.$nombre_proyecto.' '.$costo_proyecto.' '.$tipo_proyecto.' '.$modalidad.' '.$fecha_inicio.' '.$fecha_conclusion.' '.$duracion_anos.' '.$descripcion_proyecto.' '.$objetivo_proyecto.' '.$id_proyecto;*/
		$this->form_validation->set_rules("tipo", "tipo de garantia de la empresa","trim|required");
		$this->form_validation->set_rules("monto", "Monto de la garantia","trim|required");
		$this->form_validation->set_rules("entidad_financiera", "Entidad financiera de la garantia","trim|required");

		$this->form_validation->set_message("required", "El campo %s es requerido");
		$this->form_validation->set_message("numeric", "El campo %s es numerico");
		$this->form_validation->set_message("decimal", "El campo %s es decimal");
		$this->form_validation->set_message("is_natural", "El campo %s debe contener solo numeros positivos mayores a cero");
		if ($this->form_validation->run()){
					$data = array(
						'tipo'=>$tipo,
						'monto'=>$monto,
						'descrip_compo_proy'=>$descrip_compo_proy,
						'entidad_financiera'=>$entidad_financiera,
						'id_garantia'=>$id_garantia,
						'id_proyecto'=>$id_proyecto,
						);
						//print_r($data) ;
						//echo 'lego aqui pero nu modifica';
			$this->contrato_model->updateGarantia($id_garantia,$data);
			$this->ing_garantia_compo($id_proyecto,$descrip_compo_proy);

		}else{
				echo '-------------error en llenado de datos-----------';
				$this->ing_garantia_compo($id_proyecto,$descrip_compo_proy);
				//$this->ing_seg_fin($id_proyecto,$descrip_compo_proy);
		}
	}
}
