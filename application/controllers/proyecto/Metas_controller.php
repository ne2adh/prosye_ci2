<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Metas_controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

        $this->load->library('form_validation','from_helper');
        $this->load->helper('date','url');


	}
	public function index()
	{

		$id=array();
		//$id=$_GET['id'];
        $id=$this->session->userdata('id');
        $data['results'] =  $this->proyectos_model->get_proyectos_mios($id);
        //$data['ruta']= $this->proyectos_model->get_ruta($id);
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec');
		$this->load->view('admin/proyectos/mis_proy_metas',$data);
		$this->load->view('layouts/footer');
	}
	public function ingresoMetas($id_proyecto)
	{
		//$id_proyecto=$this->input->post("id_proyecto");
				//$metas=$this->metas_model->metas($id_proyecto);
	   $data=array(
			//'metas3'=>$this->metas_model->metas_proy1($num),
			'metas1'=>$this->metas_model->metas($id_proyecto),
			'id_proyecto'=>$id_proyecto,
			'proyecto'=>$this->metas_model->proy($id_proyecto),
			'unidad'=>$this->metas_model->unidad_proy(),
			'metas'=>$this->metas_model->metas_proy(),
		);
	   //if($metas==0){
		//	redirect(base_url()."proyecto/metas_controller/ingresoMetas/$id_proyectos");
		//} else
		//{
			$this->load->view('layouts/header');
			$this->load->view('layouts/aside_proyec');
			$this->load->view('admin/proyectos/registro_metas',$data);
			$this->load->view('layouts/footer');

		//}

	}

public function listarMetas(){
      $resultado = array();
      $consulta = $this->metas_model->index();
      if(!empty($consulta)){
          foreach ($consulta as $fila){
              $data['id_metas_proy'] = $fila['id_metas_proy'];
              $data['descripcion'] = $fila['descripcion'];
              array_push($resultado, $data);
          }
      }
      echo json_encode($resultado);die;

  }

	public function edit($id,$id_proyecto)
	{

	   $data=array(
	        'metas2'=>$this->metas_model->meta($id),
	        'metas1'=>$this->metas_model->metas($id_proyecto),
			'id'=>$id,
			'proyecto'=>$this->metas_model->proy($id_proyecto),
			'unidad'=>$this->metas_model->unidad_proy(),
			'metas'=>$this->metas_model->metas_proy(),);
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec');
		$this->load->view('admin/proyectos/registro_metas_edit',$data);
		$this->load->view('layouts/footer');

	}
	public function store(){
		$id_proyecto=$this->input->post('id_proyecto');
		$descripcion=$this->input->post('meta');
		$cantidad=$this->input->post('cantidad');
		$unidad=$this->input->post('unidad');
		if(!$this->input->post('observacion')){
			$observacion='NINGUNA';
		} else{
			$observacion=$this->input->post('observacion');
		}

		$cantidad = str_replace(",",'', $cantidad);
		$this->form_validation->set_rules("meta", "Descripcion de la meta","required");
		$this->form_validation->set_rules("cantidad", "Cantidad especifica de la meta","required");
		$this->form_validation->set_rules("observacion", "Observacion adicionales de la meta","trim");
		$this->form_validation->set_rules("unidad", "Unidad de Medicion de la Meta","trim|required");

		$this->form_validation->set_message("required", "El campo %s es requerido");
		$this->form_validation->set_message("is_natural_no_zero", "El campo %s debe contener un numero mayor que 0 ");
		if ($this->form_validation->run())
		{
			$data = array();
			$data['id_proyecto']=$id_proyecto;
			$data['descripcion']=$descripcion;
			$data['cantidad']=$cantidad;
			$data['unidad']=$unidad;
			$data['observacion']=$observacion;
			$this->metas_model->insert($data,$id_proyecto);
			$data1['id_proyecto']=$id_proyecto;
			$data1['pant']=3;
			$this->proyectos_model->update_pant($data1,$id_proyecto);
			redirect(base_url()."proyecto/metas_controller/ingresoMetas/$id_proyecto");
		}else
		{
			$this->ingresoMetas($id_proyecto);
			//redirect(base_url()."proyecto/metas_controller/ingresoMetas/$id_proyecto");
		}


	}

	public function nuevo()
	{
		$id_proyecto=$this->input->post("id_proyecto");
		redirect(base_url()."proyecto/municipio_controller/ingreso/$id_proyecto");

	}
	public function update()
	{
		$id=$this->input->post("id");
		$id_proyecto=$this->input->post('id_proyecto');
		$descripcion=$this->input->post('meta');
		$cantidad=$this->input->post('cantidad');
		$unidad=$this->input->post('unidad');
		if(!$this->input->post('observacion')){
			$observacion='NINGUNA';
		} else{
			$observacion=$this->input->post('observacion');
		}
		$cantidad = str_replace(",",'', $cantidad);

		$this->form_validation->set_rules("meta", "Descripcion de la meta","required");
		$this->form_validation->set_rules("cantidad", "Cantidad especifica de la meta","required");
		$this->form_validation->set_rules("observacion", "Observacion adicionales de la meta","trim");
		$this->form_validation->set_rules("unidad", "Unidad de Medicion de la Meta","trim|required");

		$this->form_validation->set_message("required", "El campo %s es requerido");
		$this->form_validation->set_message("is_natural_no_zero", "El campo %s debe contener un numero mayor que 0 ");
		 if ($this->form_validation->run())
		 {
			$data = array();
			$data['id']=$id;
			$data['id_proyecto']=$id_proyecto;
			$data['descripcion']=$descripcion;
			$data['cantidad']=$cantidad;
			$data['unidad']=$unidad;
			$data['observacion']=$observacion;
			$this->metas_model->update($id,$data);
			redirect(base_url()."proyecto/metas_controller/ingresoMetas/$id_proyecto");
		}
		else{
			$this->edit($id, $id_proyecto);
		}
	}

	public function delete($id,$id_proyecto)
	{
		 $id=$id;
		 $id_proyecto=$id_proyecto;

		 	$this->metas_model->delete($id);
		 	redirect(base_url()."proyecto/metas_controller/ingresoMetas/$id_proyecto");


	}
	public function saveMeta(){
		if( isset( $_GET['meta'] ) ){
			$meta = $this->input->get('meta');
			$data = array();
			$data['descripcion']=$meta;
			$this->metas_model->insertDescripcion($data);
			$mensaje = "Registro Correcto";
			echo json_encode(array(
				 'status' => 201, // success or not?
				 'mensaje' => $mensaje
			));
		}else{
			$mensaje = "No existe descripcion";
			echo json_encode(array(
				'status' => 500, // success or not?
				'mensaje' => $mensaje
			));
		}

	}
	public function editMeta(){
        $id_metas_proy=$this->input->get("id_metas_proy");
        $descripcion=$this->input->get("descripcion");
            $data=array(
            'descripcion'=>$descripcion,
        );
        $this->metas_model->edit($id_metas_proy,$data);
				$mensaje = "Edicion Correcta";
				echo json_encode(array(
					 'status' => 201, // success or not?
					 'mensaje' => $mensaje
				));
	}


}
