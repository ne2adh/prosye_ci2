<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Proyecto_controller extends CI_Controller{

        public function __construct(){
            parent::__construct();
            //$this->permisos=$this->backend_lib->control();
            $this->load->library('form_validation','session');
            $this->load->helper(array('url', 'form'));
            $this->load->helper('form','date');
        }

        //editar proyecto
        public function editar($id_proyecto){
            $areas = $this->proyectos_model->get_area();
            $detalle_proyecto = $this->proyectos_model->getDetalleProyecto($id_proyecto);
            $data = Array(
                "areas"=>$areas,
                "proyecto"=>$detalle_proyecto
            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside_proyec');
            $this->load->view('admin/proyectos/editProyecto', $data);
            $this->load->view('layouts/footer');
        }

        public function editProyecto(){
            //obtener datos del formulario para la tabla proyectos
            $f1=$this->input->post('fecha_inicio');
            $f2=$this->input->post('fecha_conclusion');
            if(isset($f1) && $f1!=""){
                $vfini = explode("-", $f1);
                $f1 = $vfini[0].'-'.$vfini[1].'-01';
            }

            if(isset($f2) && $f2!=""){
                $vffin = explode("-", $f2);
                $dia_fin = cal_days_in_month(CAL_GREGORIAN, $vffin[1], $vffin[0]); // 31
                $f2 = $vffin[0].'-'.$vffin[1].'-'.$dia_fin;
            }
            $id_proyecto = $this->input->post("id_proyecto");
            $nombre_proyecto=$this->input->post("nombre_proyecto");
            $costo_proyecto=$this->input->post("costo_proyecto");
            $descripcion_proyecto=$this->input->post("descripcion_proyecto");
            $objetivo_proyecto=$this->input->post("objetivo_proyecto");
            $duracion_anos=$this->proyectos_model->tiempo($f1,$f2);
            $costo_proyecto=str_replace(',','',$costo_proyecto);

            //validar datos
            $this->form_validation->set_rules("nombre_proyecto", "Nombre del Proyecto","trim|required");
            $this->form_validation->set_rules("costo_proyecto", "Costo del Proyecto","trim|required");
            $this->form_validation->set_rules("fecha_inicio", "Fecha de Inicio","required");
            $this->form_validation->set_rules("fecha_conclusion", "Fecha de Conclusion","required|callback_fecha_dif");
            $this->form_validation->set_rules("descripcion_proyecto", "Descripcion del Proyecto","trim|required");
            $this->form_validation->set_rules("objetivo_proyecto", "Objetivo del Proyecto","trim|required");
            $this->form_validation->set_message("is_unique", "El campo %s esta duplicado");
            $this->form_validation->set_message("required", "El campo %s es requerido");
            $this->form_validation->set_message("numeric", "El campo %s es numerico");
            $this->form_validation->set_message("decimal", "El campo %s es decimal");
            $this->form_validation->set_message("is_natural", "El campo %s debe contener solo numeros positivos mayores a cero");
            $this->form_validation->set_message("fecha_dif", "El campo %s debe ser mayor a la Fecha de incio" );

            //guardar a la tabla proyectos
            if ($this->form_validation->run()){
                $data = array(
                    'nombre_proyecto'=>$nombre_proyecto,
                    'costo_proyecto'=>$costo_proyecto,
                    'fecha_inicio'=>$f1,
                    'fecha_conclusion'=>$f2,
                    'duracion_anos'=>$duracion_anos,
                    'descripcion_proyecto'=>$descripcion_proyecto,
                    'objetivo_proyecto'=>$objetivo_proyecto,
                );
                $this->proyectos_model->editProyecto($id_proyecto,$data);
            }
            $this->editar($id_proyecto);

        }

        private function compararFechas($primera, $segunda)
        {
            $valoresPrimera = explode ("-", $primera);
            $valoresSegunda = explode ("-", $segunda);

            $diaPrimera    = $valoresPrimera[2];
            $mesPrimera  = $valoresPrimera[1];
            $anyoPrimera   = $valoresPrimera[0];

            $diaSegunda   = $valoresSegunda[2];
            $mesSegunda = $valoresSegunda[1];
            $anyoSegunda  = $valoresSegunda[0];

            $diasPrimeraJuliano = gregoriantojd($mesPrimera, $diaPrimera, $anyoPrimera);
            $diasSegundaJuliano = gregoriantojd($mesSegunda, $diaSegunda, $anyoSegunda);

            if(!checkdate($mesPrimera, $diaPrimera, $anyoPrimera)){
                echo "La fecha ".$primera." no es v&aacute;lida";
                return 0;
            }elseif(!checkdate($mesSegunda, $diaSegunda, $anyoSegunda)){
                echo "La fecha ".$segunda." no es v&aacute;lida";
                return 0;
            }else{
                return  $diasPrimeraJuliano - $diasSegundaJuliano;
            }

        }

        public function fecha_dif(){
            if(($this->input->post('fecha_inicio')!='') && ($this->input->post('fecha_conclusion')!=''))
                {
                    $fecha1='';
                    $fecha2='';
                    //fecha inicial
                    $fecha1= $this->input->post('fecha_inicio');
                    $vfini = explode("-", $fecha1);
                    $fecha1 = $vfini[0].'-'.$vfini[1].'-01';

                    //fecha final
                    $fecha2= $this->input->post('fecha_conclusion');
                    $vffin = explode("-", $fecha2);
                    $dia_fin = cal_days_in_month(CAL_GREGORIAN, $vffin[1], $vffin[0]);
                    $fecha2 = $vffin[0].'-'.$vffin[1].'-'.$dia_fin;
                    if ($this->compararFechas($fecha2, $fecha1)>0){
                        return TRUE;
                    }else{
                        return FALSE;
                    }
                }
        }

    }
?>
