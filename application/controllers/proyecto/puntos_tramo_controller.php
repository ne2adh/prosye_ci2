<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Puntos_tramo_controller extends CI_Controller 
{
	public function __construct(){
		parent::__construct();
        $this->load->library('form_validation');
	}
	
	public function index(){
		$id=array();
		$id=$this->session->userdata('id');
        $data['results'] =  $this->proyectos_model->get_proyectos_espe($id);    
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec');
		$this->load->view('admin/proyectos/mis_proy_puntos',$data);
		$this->load->view('layouts/footer');
	}

	public function traerTramo($id_tipo){
		//$id_proyecto=$this->input->post('id_proyecto');
		$data=array(
		'id_tipo'=> $id_tipo,
		'tramos'=>$this->puntos_model->getTramo($id_tipo),
	    //'id_proyecto'=> $id_proyecto,
	    //'proyecto'=>$this->convenio_model->traerProy($id_proyecto),
		);
	    $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec');
		$this->load->view('admin/proyectos/mis_proy_puntos_tramo',$data);
		$this->load->view('layouts/footer');
	}

	public function traerTramo1($id_proyecto){
		$id_proyecto=$this->input->post('id_proyecto');
		$data=array(
		//'id_tipo'=> $id_tipo,
		'tramos'=>$this->puntos_model->getTramo1($id_proyecto),
	    //'id_proyecto'=> $id_proyecto,
	    //'proyecto'=>$this->convenio_model->traerProy($id_proyecto),
		);
		$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));
	    $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/proyectos/mis_proy_puntos_tramo',$data);
		$this->load->view('layouts/footer');
	}

	public function ingresoPuntosTramo($id_proyecto,$id_tramo,$id_tipo){
		$data=array(
		'tramo'=>$this->puntos_model->nombTramo($id_tramo),
		'punto'=> $this->puntos_model->puntosProy($id_proyecto,$id_tramo,$id_tipo),
	    'id_proyecto'=> $id_proyecto,
	    'proyecto'=>$this->convenio_model->traerProy($id_proyecto),
	    'id_tipo'=>$id_tipo,
		);
		$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));
	    $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/proyectos/puntos1',$data);
		$this->load->view('layouts/footer');
	}

	public function store(){
		$id_proyecto=$this->input->post("id_proyecto");
		$id_tramo=$this->input->post("id_tramo");
		$id_tipo=$this->input->post("id_tipo");
		$data=array(
			'id_proyecto'=>$this->input->post("id_proyecto"),
			'id_tramo'=>$this->input->post("id_tramo"),
			'id_tipo'=>$this->input->post("id_tipo"),
			'latitud'=>$this->input->post("latitud"),
			'longitud'=>$this->input->post("longitud"),
		);
		$this->puntos_model->insertPuntosTramo($data);
		redirect(base_url()."proyecto/puntos_tramo_controller/ingresoPuntosTramo/$id_proyecto/$id_tramo/$id_tipo");
	}
}
