<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Procesar_controller extends CI_Controller
{
	private $latitud;
	private $longitud;

	public function __construct(){
			parent::__construct();
			$this->load->library('form_validation');
        	$this->load->helper(array('url', 'form'));
        	$this->load->helper('form','date');
			}

  
    public function convertir($UTM){
				$parts = explode(" ", $UTM);
				$Zone = intval($parts[0]);
				$Letter = $parts[1];
				$Easting = floatval($parts[2]);
				$Northing = floatval($parts[3]);
				$Hem='';
				if ($Letter>'M'){
					$Hem='N';
				}
				else{
					$Hem='S';
				}
				$north;
				if ($Hem == 'S'){
					$north = $Northing - 10000000;
				}
				else{
					$north = $Northing;
				}
				$latitude = ($north/6366197.724/0.9996+(1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)-0.006739496742*sin($north/6366197.724/0.9996)*cos($north/6366197.724/0.9996)*(atan(cos(atan(( exp(($Easting - 500000) / (0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*(1-0.006739496742*pow(($Easting - 500000) / (0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2)/3))-exp(-($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*( 1 -  0.006739496742*pow(($Easting - 500000) / (0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2)/3)))/2/cos(($north-0.9996*6399593.625*($north/6366197.724/0.9996-0.006739496742*3/4*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+pow(0.006739496742*3/4,2)*5/3*(3*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996 )/2)+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2))/4-pow(0.006739496742*3/4,3)*35/27*(5*(3*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2))/4+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2)*pow(cos($north/6366197.724/0.9996),2))/3))/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*(1-0.006739496742*pow(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2))+$north/6366197.724/0.9996)))*tan(($north-0.9996*6399593.625*($north/6366197.724/0.9996 - 0.006739496742*3/4*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+pow(0.006739496742*3/4,2)*5/3*(3*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+sin(2*$north/6366197.724/0.9996 )*pow(cos($north/6366197.724/0.9996),2))/4-pow(0.006739496742*3/4,3)*35/27*(5*(3*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2))/4+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2)*pow(cos($north/6366197.724/0.9996),2))/3))/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*(1-0.006739496742*pow(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2))+$north/6366197.724/0.9996))-$north/6366197.724/0.9996)*3/2)*(atan(cos(atan((exp(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*(1-0.006739496742*pow(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2)/3))-exp(-($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*(1-0.006739496742*pow(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2)/3)))/2/cos(($north-0.9996*6399593.625*($north/6366197.724/0.9996-0.006739496742*3/4*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+pow(0.006739496742*3/4,2)*5/3*(3*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2))/4-pow(0.006739496742*3/4,3)*35/27*(5*(3*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2))/4+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2)*pow(cos($north/6366197.724/0.9996),2))/3))/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*(1-0.006739496742*pow(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2))+$north/6366197.724/0.9996)))*tan(($north-0.9996*6399593.625*($north/6366197.724/0.9996-0.006739496742*3/4*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+pow(0.006739496742*3/4,2)*5/3*(3*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2))/4-pow(0.006739496742*3/4,3)*35/27*(5*(3*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2))/4+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2)*pow(cos($north/6366197.724/0.9996),2))/3))/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*(1-0.006739496742*pow(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2))+$north/6366197.724/0.9996))-$north/6366197.724/0.9996))*180/pi();
				$latitude=round($latitude*10000000);
				$latitude=$latitude/10000000;
				$longitude =atan((exp(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*(1-0.006739496742*pow(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2)/3))-exp(-($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*(1-0.006739496742*pow(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2)/3)))/2/cos(($north-0.9996*6399593.625*($north/6366197.724/0.9996-0.006739496742*3/4*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+pow(0.006739496742*3/4,2)*5/3*(3*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2))/4-pow(0.006739496742*3/4,3)*35/27*(5*(3*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2))/4+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2)*pow(cos($north/6366197.724/0.9996),2))/3)) / (0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*(1-0.006739496742*pow(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2))+$north/6366197.724/0.9996))*180/pi()+$Zone*6-183;
				$longitude=round($longitude*10000000);
				$longitude=$longitude/10000000;
				$this->latitud = $latitude;
				$this->longitud = $longitude;         
			}

	public function excel(){
		$id_proyecto=$this->input->post('id_proyecto');

		if (substr($_FILES['excel']['name'],-3)=="csv")
		{
			$fecha		= date("Y-m-d");
			$carpeta 	= "subir_archivo_excel/archivo excel/";
			$excel  	= $fecha."-".$_FILES['excel']['name'];

			move_uploaded_file($_FILES['excel']['tmp_name'], "$carpeta$excel");
			$row = 1;
			$fp = fopen ("$carpeta$excel","r");

				while ($data = fgetcsv ($fp, 1000, ";")){
					if ($row!=1){
						//echo $data[0];
						$num1=(string)$data[0];
						$num2=(string)$data[1];
						//echo $num2;
						$data[1]=str_replace(",",".",$num1);
						//echo $data[1];
						$data[2]=str_replace(',','.',$num2);
						//echo $data[2];
						$data[0]=(string)$id_proyecto;
						///
						///
						 $cn=mysqli_connect('localhost','root','','dbspac');
						 $utm= "19 J ".$data[1]." ".$data[2];
						 $this->convertir($utm);						
						
						$sql_guardar="INSERT INTO coordenada_inicial (id_proyecto,latitud, longitud, norte,este) VALUES ('$data[0]','$this->latitud','$this->longitud', '$data[1]', '$data[2]')";						
						//$sql = mysqli_query($insertar) or die(mysqli_error());
						mysqli_query($cn,$sql_guardar) or die(mysqli_error());
						if (!$sql_guardar){
							//echo "<div>Hubo un problema al momento de importar porfavor vuelva a intentarlo</div >";
							$mensaje = "Hubo un problema al momento de importar porfavor vuelva a intentarlo";
							echo json_encode(array(
								'status' => 500, // success or not?
								'mensaje' => $mensaje
							));
							exit;
						}
					}
				  $row++;
				}
			    $data1['id_proyecto']=$id_proyecto;
				$data1['pant']=5;
				$this->proyectos_model->update_pant($data1,$id_proyecto);
				fclose ($fp);
				$mensaje = "La importacion de archivo subio satisfactoriamente";
				echo json_encode(array(
					'status' => 201, // success or not?
					'mensaje' => $mensaje
				));
		}
		else{
			fclose ($fp);
			$mensaje = "No es un archivo cvs";
			echo json_encode(array(
				'status' => 401, // success or not?
				'mensaje' => $mensaje
			));
		}
	}


	public function excelProy()
	{
	$id_proyecto=$this->input->post('id_proyecto');
	$id_tramo=$this->input->post('id_tramo');
	$id_tipo=$this->input->post('id_tipo');

		if (substr($_FILES['excel']['name'],-3)=="csv")
		{
			$fecha		= date("Y-m-d");
			$carpeta 	= "subir_archivo_excel/archivo excel/";
			$excel  	= $fecha."-".$_FILES['excel']['name'];


			move_uploaded_file($_FILES['excel']['tmp_name'], "$carpeta$excel");

			$row = 1;

			$fp = fopen ("$carpeta$excel","r");

			//fgetcsv. obtiene los valores que estan en el csv y los extrae.


				while ($data = fgetcsv ($fp, 1000, ";"))
				{
					//si la linea es igual a 1 no guardamos por que serian los titulos de la hoja del excel.
					if ($row!=1)
					{

						//echo $data[0];
						$num1=(string)$data[0];
						$num2=(string)$data[1];
						//echo $num2;
						$data[0]=str_replace(",",".",$num1);
						//echo $data[1];
						$data[1]=str_replace(',','.',$num2);
						//echo $data[2];
						$data[2]=(string)$id_proyecto;
						$data[3]=(string)$id_tramo;
						$data[4]=(string)$id_tipo;
		 				$cn=mysqli_connect('localhost','root','','dbspac');
						$sql_guardar="INSERT INTO coordenadas_proy (latitud,longitud,id_proyecto,id_tramo,id_tipo) VALUES ('$data[0]','$data[1]','$data[2]','$data[3]','$data[4]')";
						//$sql = mysqli_query($insertar) or die(mysqli_error());
						mysqli_query($cn,$sql_guardar) or die(mysqli_error());
						if (!$sql_guardar)
						{
							echo "<div>Hubo un problema al momento de importar porfavor vuelva a intentarlo</div >";
							exit;
						}

					}
				$row++;
				}

				fclose ($fp);

				echo "<div>La importacion de archivo subio satisfactoriamente</div >";?>
				<a href="<?php echo base_url();?>proyecto/puntos_tramo_controller/ingresoPuntosTramo/<?php echo $id_proyecto;?>/<?php echo $id_tramo;?>/<?php echo $id_tipo;?>">Regresar a la ventana</a>; <?php
				//exit;

		}
		else{
			echo "<p>No es un archivo cvs</p>";?>
			<a href="<?php echo base_url();?>proyecto/puntos_tramo_controller/ingresoPuntosTramo/<?php echo $id_proyecto;?>/<?php echo $id_tramo;?>/<?php echo $id_tipo;?>">Regresar a la ventana</a>; <?php
		}
	}
}
