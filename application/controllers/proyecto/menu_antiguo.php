<li>
                        <a href="<?php echo base_url();?>proyecto/mis_proyectos/propios">
                            <i class="fa fa-home"></i> <span>Inicio</span>
                        </a>
                    </li>

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-share-alt"></i> <span>Proyecto</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a class="text-yellow" href="<?php echo base_url();?>proyecto/mis_proyectos/propios/"><i class="fa fa-circle-o text-yellow"></i>En Registro</a></li>
                            <li><a class="text-red" href="<?php echo base_url();?>proyecto/mis_proyectos/esperaSisin/"><i class="fa fa-circle-o text-red"></i>En Espera Sisin</a></li>
                            <li><a class="text-blue" href="<?php echo base_url();?>proyecto/mis_proyectos/conSisin/"><i class="fa fa-circle-o text-blue"></i>Con Sisin</a></li>
                            <li><a class="text-green" href="#"><i class="fa fa-circle-o text-green"></i>Finalizados</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-laptop"></i> <span>Contratacion</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a class="text-yellow" href=""><i class="fa fa-circle-o"></i>Convocatoria</a></li>
                            <li><a class="text-red" href="<?php echo base_url();?>proyecto/contratos_controller"><i class="fa fa-circle-o"></i>Contrato</a></li>
                            <li><a class="text-blue" href="<?php echo base_url();?>proyecto/contratos_controller/index1"><i class="fa fa-circle-o"></i>Garantias</a></li>
                            <li><a class="text-green" href=""><i class="fa fa-circle-o"></i>Seguimiento</a></li>
                            <li><a class="text-red" href="<?php echo base_url();?>proyecto/contratos_controller/index2"><i class="fa fa-circle-o"></i>Modificacion Contrato</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-dashboard"></i> <span>Seguimiento</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">

                            <li><a class="text-yellow" href="<?php echo base_url();?>proyecto/seguir_fisico_controller"><i class="fa fa-circle-o text-yellow"></i>Financiero</a></li>
                            <li><a class="text-red" href="<?php echo base_url();?>proyecto/seguir_fisico_controller/index1"><i class="fa fa-circle-o text-red"></i>Fisico</a></li>
                            <li><a class="text-blue" href="<?php echo base_url();?>proyecto/seguir_controller_convenio"><i class="fa fa-circle-o text-blue"></i> Convenio</a></li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-dashboard"></i> <span>Seguimiento Fisico</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                             <li><a class="text-yellow" href="<?php echo base_url();?>proyecto/seguir_fisico_controller"><i class="fa fa-circle-o text-yellow"></i>Proyectos sin paquetes</a></li>
                             <li><a class="text-yellow" href="<?php echo base_url();?>proyecto/seguir_fisico_controller"><i class="fa fa-circle-o text-yellow"></i>Proyectos con paquetes</a></li>
                            <li><a class="text-yellow" href="<?php echo base_url();?>proyecto/seguir_fisico_controller"><i class="fa fa-circle-o text-yellow"></i>Fisico</a></li>
                            <li><a class="text-red" href=""><i class="fa fa-circle-o text-red"></i>Financiero</a></li>
                            <li><a class="text-blue" href="<?php echo base_url();?>proyecto/seguir_controller_convenio"><i class="fa fa-circle-o text-blue"></i> Convenio</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-dashboard"></i> <span>Seguimiento Economico</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                             <li><a class="text-yellow" href="<?php echo base_url();?>proyecto/seguir_fisico_controller"><i class="fa fa-circle-o text-yellow"></i>Proyectos sin paquetes</a></li>
                             <li><a class="text-yellow" href="<?php echo base_url();?>proyecto/seguir_fisico_controller"><i class="fa fa-circle-o text-yellow"></i>Proyectos con paquetes</a></li>
                            <li><a class="text-yellow" href="<?php echo base_url();?>proyecto/seguir_fisico_controller"><i class="fa fa-circle-o text-yellow"></i>Fisico</a></li>
                            <li><a class="text-red" href=""><i class="fa fa-circle-o text-red"></i>Financiero</a></li>
                            <li><a class="text-blue" href="<?php echo base_url();?>proyecto/seguir_controller_convenio"><i class="fa fa-circle-o text-blue"></i> Convenio</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-file-o"></i> <span>Sisin</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                        </a>
                         <ul class="treeview-menu">
                            <li><a class="text-aqua" href="<?php echo base_url();?>proyecto/sisin_controller/"><i class="fa fa-circle-o"></i>Proyectos Ingresados</a></li>
                            <!--<li><a href="<?php echo base_url();?>proyecto/mis_proyectos/ingresados/"><i class="fa fa-circle-o text-yellow"></i>Proyectos Ingresados</a></li>-->
                            <li><a class="text-orange" href="<?php echo base_url();?>proyecto/sisin_controller/conSisin"><i class="fa fa-circle-o text-blue"></i>Proyectos Con Sisin</a></li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-print"></i> <span>Reportes</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu ">
                            <li><a class="text-purple" href="<?php echo base_url();?>proyecto/mis_proyectos/con_convenio"><i class="fa fa-circle-o"></i> Proyectos con Convenio</a></li>
                            <li><a class="text-muted" href="<?php echo base_url();?>proyecto/mis_proyectos/sin_convenio"><i class="fa fa-circle-o"></i> Proyectos sin Convenio</a></li>
                            <li><a class="text-light-blue" href="<?php echo base_url();?>proyecto/mis_proyectos/por_provincia"><i class="fa fa-circle-o"></i> Proyectos por Provincia</a></li>
                            <li><a class="text-fuchsia" href="<?php echo base_url();?>proyecto/mis_proyectos/por_fecha"><i class="fa fa-circle-o"></i> Proyectos por Fecha</a></li>
                            <li><a class="text-teal" href="<?php echo base_url();?>proyecto/sye_controller"><i class="fa fa-circle-o"></i> SyE</a></li>
                            <li><a class="text-lime" href="<?php echo base_url();?>proyecto/mis_proyectos/por_municipio"><i class="fa fa-circle-o"></i> Municipio</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-power-off"></i> <span>Temporal</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                           <li><a href="<?php echo base_url();?>temporal/avance_controller"><i class="fa fa-circle-o text-red"></i>Avances</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-user"></i> <span>Administracion</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url();?>administrador/seccion"><i class="fa fa-circle-o text-yellow"></i>Secciones y Menus</a></l>
                            <li><a href="<?php echo base_url();?>administrador/rol"><i class="fa fa-circle-o text-green"></i>Roles</a></li>
                            <li><a href="<?php echo base_url();?>administrador/permisos"><i class="fa fa-circle-o text-red"></i>Permisos</a></li>
                            <li><a href="<?php echo base_url();?>administrador/usuarios"><i class="fa fa-circle-o text-blue"></i>Usuarios</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>auth/logout">
                            <i class="fa fa-close"></i> <span>Cerrar</span>
                        </a>
                    </li>