 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-


<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Municipio_controller extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('date','url');
	}

	public function index()
	{
		$id=array();
		$id=$this->session->userdata('id');
        $data['results'] =  $this->municipio_model->getProyecto($id);
	    $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec');
		$this->load->view('admin/proyectos/mis_proy_municipio',$data);
		$this->load->view('layouts/footer');
	}

	public function ingreso($id_proyecto){
		if($id_proyecto>=100000){
			$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
			//recuperar tramos
			$idProyecto =$data_proyecto->id_proyecto;
		}
		else{
			$data_proyecto = $this->metas_model->proy($id_proyecto);
			$idProyecto = $id_proyecto;
		}		
		$data = array(
			'proyecto'=> $data_proyecto,
			'd_municipio'=> $this->municipio_model->det_municipio($id_proyecto),
			'provincias'=>$this->municipio_model->provincia(),
			'municipio'=>$this->municipio_model->municipio(),
		);
		$tramos = array("tramos" => $this->tramo_model->index($idProyecto));	
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/proyectos/registro_municipio_view',$data);
		$this->load->view('layouts/footer');
	}

	public function ingreso_1($id_proyecto)
	{
		if($id_proyecto>=100000){
			$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
			//recuperar tramos
			$idProyecto =$data_proyecto->id_proyecto;
		}
		else{
			$data_proyecto = $this->metas_model->proy($id_proyecto);
			$idProyecto = $id_proyecto;
		}		
		$data = array(
		'proyecto'=> $data_proyecto,
		'd_municipio'=> $this->municipio_model->det_municipio($id_proyecto),
		'provincias'=>$this->municipio_model->provincia(),
		);
        $tramos = array("tramos" => $this->tramo_model->index($idProyecto));	
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/proyectos/registro_municipio_view_edit',$data);
		$this->load->view('layouts/footer');
	}

	public function ingresoErr($id_proyecto, $existe_error){
		$data = array(
			'proyecto'=> $this->municipio_model->proyecto($id_proyecto),
			'd_municipio'=> $this->municipio_model->det_municipio($id_proyecto),
			'provincias'=>$this->municipio_model->provincia(),
			'error' => $existe_error
		);
		$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));	
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/proyectos/registro_municipio_view',$data);
		$this->load->view('layouts/footer');
	}

	public function getMunicipio()
	{
		if($this->input->post('id_provincia'))
		{
			echo $this->municipio_model->get_municipio_model($this->input->post('id_provincia'));
		}
	}

	public function getProvincia()
	{
		if($this->input->post('provincia'))
		{
			echo $this->municipio_model->get_municipio($this->input->post('provincia'));
		}
	}
	public function store(){
		$id_proyecto=$this->input->post("id_proyecto");
		$sum3=$this->input->post("sum3");
		//if ($sum3<300) {
			$id_proyecto=$this->input->post("id_proyecto");
			$id_provincia=$this->input->post("provincia");
			$id_municipio=$this->input->post("municipio");
			$cant_genera_empleo=$this->input->post("cant_genera_empleo");
			$total_pobla_beneficio=$this->input->post("total_pobla_beneficio");
			$por_incidencia_proy=$this->input->post("por_incidencia_proy");

			$por_benef_poblacion=$this->input->post("por_benef_poblacion");
			$por_participacion=$this->input->post("por_participacion");            

			$this->form_validation->set_rules("municipio", "Municipio del Proyecto","required");
			$this->form_validation->set_rules("provincia", "Municipio del Proyecto","required");
			$this->form_validation->set_rules("cant_genera_empleo", "Cantidad de empleo generado en el Proyecto","trim|required|is_natural_no_zero");
			$this->form_validation->set_rules("total_pobla_beneficio", "Poblacion beneficiada en el Proyecto","trim|required|is_natural_no_zero");
			$this->form_validation->set_rules("por_incidencia_proy", "Poblacion beneficiada en el Proyecto","trim|required|decimal");

			$this->form_validation->set_message("required", "El campo %s es requerido");
			$this->form_validation->set_message("is_natural_no_zero", "El campo %s tiene que contener un valor mayor a 0");
			$this->form_validation->set_message("decimal", "El campo %s debe contener un numero decimal.");
			
			if ($this->form_validation->run()){
					$data = array(
						'id_proyecto'=>$id_proyecto,
						'id_municipio'=>$id_municipio,
						'id_proyecto'=>$id_proyecto,
						'por_benef_poblacion'=>$por_benef_poblacion,
						'cant_genera_empleo'=>$cant_genera_empleo,
						'total_pobla_beneficio'=>$total_pobla_beneficio,
						'por_participacion'=>$por_participacion,
						'por_incidencia_proy'=>$por_incidencia_proy,
						);

						$resp=0;
						$resp=$this->municipio_model->compara($id_municipio,$id_proyecto);
						if ($resp!=1){
									$data1['id_proyecto']=$id_proyecto;
									$data1['pant']=4;
									$this->proyectos_model->update_pant($data1,$id_proyecto);
									$this->municipio_model->insert($data);
									redirect(base_url()."proyecto/municipio_controller/ingreso/$id_proyecto");
									}

						 else   {

						 			//echo ('---------Municipio Repetido-----------------');
						 		    $this->ingresoErr($id_proyecto, 1);
								}

				} else {
							$this->ingreso($id_proyecto);
						}

		//redirect(base_url());
	}
	public function nuevo()
	{
		$id_proyecto=$this->input->post("id_proyecto");
		redirect(base_url()."proyecto/puntos_controller/ingresoPuntos/$id_proyecto");
	}

	public function delete($id,$id_proyecto)
	{
		 $id=$id;
		 $id_proyecto=$id_proyecto;
		 	$this->municipio_model->delete($id,$id_proyecto);
		 	redirect(base_url()."proyecto/municipio_controller/ingreso/$id_proyecto");
	}

	public function edit($id,$id_municipio,$id_proyecto){
		$id=$id;
		$id_proyecto=$id_proyecto;
		if($id_proyecto>=100000){
			$data_proyecto = $this->tramo_model->getTramo($id_proyecto);
			//recuperar tramos
			$idProyecto =$data_proyecto->id_proyecto;
		}
		else{
			$data_proyecto = $this->metas_model->proy($id_proyecto);
			$idProyecto = $id_proyecto;
		}		
	  	$data=array(
			'id'=>$id,
				'id_proyecto'=>$id_proyecto,
				'provincias'=>$this->municipio_model->provincia(),
				//'provincias2'=>$this->municipio_model->traer(),
			'proyecto'=> $data_proyecto,
				'd_municipio'=> $this->municipio_model->det_municipio($id_proyecto),
				//'provincias'=$this->municipio_model->provincia(),
				'municipio'=> $this->municipio_model->traer($id),
		);
		$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));
    	$this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/proyectos/registro_municipio_view_1',$data);
		$this->load->view('layouts/footer');
	}

	public function update()
	{
		$id=$this->input->post("id");
		//$id_proyecto=$this->input->post("id_proyecto");
		$sum3=$this->input->post("sum3");
		//if ($sum3<300) {
			$id_proyecto=$this->input->post("id_proyecto");
			$id_provincia=$this->input->post("provincia");
			$id_municipio=$this->input->post("municipio");
			$cant_genera_empleo=$this->input->post("cant_genera_empleo");
			$total_pobla_beneficio=$this->input->post("total_pobla_beneficio");
			$por_incidencia_proy=$this->input->post("por_incidencia_proy");

			//$por_benef_poblacion=$this->input->post("por_benef_poblacion");
			//$por_participacion=$this->input->post("por_participacion");

			$this->form_validation->set_rules("municipio", "Municipio del Proyecto","required");
			$this->form_validation->set_rules("provincia", "Municipio del Proyecto","required");
			$this->form_validation->set_rules("cant_genera_empleo", "Cantidad de empleo generado en el Proyecto","trim|required|is_natural_no_zero");
			$this->form_validation->set_rules("total_pobla_beneficio", "Poblacion beneficiada en el Proyecto","trim|required|is_natural_no_zero");
			$this->form_validation->set_rules("por_incidencia_proy", "Poblacion beneficiada en el Proyecto","trim|required|decimal");

			$this->form_validation->set_message("required", "El campo %s es requerido");
			$this->form_validation->set_message("is_natural_no_zero", "El campo %s tiene que contener un valor mayor a 0");
			$this->form_validation->set_message("decimal", "El campo %s debe contener un numero decimal.");
			if ($this->form_validation->run())
                {
					$data = array(

						'id_proyecto'=>$id_proyecto,
						'id_municipio'=>$id_municipio,
						'id_proyecto'=>$id_proyecto,
						//'por_benef_poblacion'=>$por_benef_poblacion,
						'cant_genera_empleo'=>$cant_genera_empleo,
						'total_pobla_beneficio'=>$total_pobla_beneficio,
						//'por_participacion'=>$por_participacion,
						'por_incidencia_proy'=>$por_incidencia_proy,
						);
						//$resp=0;
						//$resp=$this->municipio_model->compara($id_municipio,$id_proyecto);
						//if ($resp!=1){
									//$data1['id_proyecto']=$id_proyecto;
									//$data1['pant']=4;
									//$this->proyectos_model->update_pant($data1,$id_proyecto);
								$resp=0;
                //aqui el if si es el mismo municipio
                $id_municipio_anterior = $this->municipio_model->traer($id)->id_municipio;
                if($id_municipio_anterior != $id_municipio){
                  $resp=$this->municipio_model->compara($id_municipio,$id_proyecto);
                }
                if ($resp!=1){
									$this->municipio_model->update($id,$data);
									redirect(base_url()."proyecto/municipio_controller/ingreso_1/$id_proyecto");
                }else {
						 		  $this->ingresoErr($id_proyecto, 1);
								}

				} else {
							$this->ingreso($id_proyecto);
						}

	}


}
