<?php
 /*
  ************************************************************
  *** Registro de Usuario
  *** Version: 0.0.11
  *** Fecha: 
  *** Autor: Anterior Funcionario
  *** Descripcion: 
  ******************
  *** REVISIONES ***
  ******************
  ************************************************************
  *** Revision 1
  ************************************************************
  *** Autor: Erwin Saul
  *** Fecha: 03/04/2019
  *** Descripcion: Se implento las funciones para gestion de usuarios
  ***		
  ************************************************************
  *** NOTA
  *** 1. Respetar la autoria del modulo y de las revisiones.
  *** 2. Realizar la descripcion de las revisiones y/o modificaciones
  ***    realizadas.
    *** 3. Documentar el codigo, realizando comentarios en cada
    ***    seccion explicando la funcionalidad y objetivo del mismo.
    *** 4. Todos los comentarios deben realizarse en el lado del
    ***    servidor utilizando:
    ***    <?php
    ***       //comentarios
    ***    ?>
  ************************************************************   
  */
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_controller extends CI_Controller {

	// use UsuarioRule;

	private $request;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('form_ci');
		$this->load->library('form_validation');
		$this->load->library('rules/Usuario_rule');		
		$this->load->library('Utils');
		// $this->load->library('lib_log');
		$this->load->model('usuario');	
		$this->load->model('usuario_model');		
		$this->load->model('registros_usuario');
		$this->load->model('rol_asignado');
		$this->request = json_decode(file_get_contents('php://input'));
	}

	public function registrar_usuario(){
		$this->load->view('usuario/index');
	}

	public function mi_perfil(){
		$id_usuario = $this->session->userdata('id_usuario');
		$this->usuario->setId_usuario($id_usuario);
		$datas['resultado'] = $this->usuario->buscar();
		//mio
		$datas['resultado1'] = $this->usuario_model->buscarusuariotodo($id_usuario);
		//mio
		$this->load->view('usuario/mi_perfil', $datas);
	}
 // ver estado
	public function miperfil()
	{
		$id_usuario = $this->session->userdata('id_usuario');
		$datas['resultado'] = $this->usuario_model->buscarusuariotodo($id_usuario);
		//$this->load->view('usuario/mi_perfil', $datas);

	}
 //todo
	public function index()
	{		
		$this->load->view('usuario/index');
	}

	public function lista()
	{
		$this->load->view('usuario/listaUsuarios');
	}

	public function listaUsuarios()
	{
		$datas = $this->usuario->listar();
		echo json_encode($datas);
	}

	public function listaUsuariosActivos()
	{
		$datas = $this->usuario->listar_activos();
		$r = array();
		foreach($datas as $data){
			 $id_usuario = $data['id_usuario'];
             $ci_persona = $data['ci_persona'];
             $nombre = $data['nombres']." ".$data['ap_paterno']." ".$data['ap_materno'];
             $nombre_rol = "SIN ASIGNAR";
             $id_rol_asignado = -1;
             $v = false;
             $this->rol_asignado->setId_usuario($id_usuario);
             if($this->rol_asignado->verificarAsignacion()){
             	//buscar el Nombre del Rol asignado
             	$data_nombre = $this->rol_asignado->buscarAsignacion();
             	$nombre_rol = $data_nombre['nombre'];
             	$id_rol_asignado = $data_nombre['id_rol_asignado'];
             	$v = true;
             }
             $p = array(
             		"id_usuario" => $id_usuario,
             		"ci_persona" => $ci_persona,
             		"nombre" => $nombre,
             		"nombre_rol" => $nombre_rol,
             		"id_rol_asignado" => $id_rol_asignado,
             		"v"=> $v
             );
             array_push($r, $p);

		}
		echo json_encode($r);
	}

	public function nombreUsuario(){
		$ci = $this->request->ci;
		$this->usuario->setCi_persona($ci);
		$cuenta = $this->usuario->generar_cuenta();
		echo json_encode(array("cuenta"=>"$cuenta"));
	}


	public function passwordUsuario()
	{
		$id_usuario = $this->session->userdata('id_usuario');
		$datas['id_usuario'] = $id_usuario;
		$this->load->view('usuario/passwordUsuario', $datas);	
	}

	public function modificarEstado(){
		$id_usuario = $this->request->id_usuario;
    	$estado = $this->request->estado;
    	$this->usuario->setId_usuario($id_usuario);
    	$mensaje="";	
    	if($estado == 0){ //usuario Inactivo -> Habilitar
    		$this->usuario->setEstado(1);    		
    		$mensaje = "Usuario Habilitado";
    	}
    	else{ //Usuario Activo -> Dar de Baja
    		$this->usuario->setEstado(0);
    		$mensaje = "Usuario Dado de Baja";    		
    	}
    	$data = $this->usuario->modificar_estado();
    	echo json_encode(array("mensaje"=>"$mensaje"));
	}

	public function detalleUsuario()
	{
		$id_usuario = $this->input->get('idUsuario');
		$this->usuario->setId_usuario($id_usuario);
		$datas['resultado'] = $this->usuario->buscar();

		$this->load->view('usuario/detalleUsuario', $datas);	
	}

	public function reset(){
		$id_usuario = $this->request->id_usuario;
		$this->usuario->setId_usuario($id_usuario);
		$data = $this->usuario->buscar();
		$ci_persona = $data['ci_persona'];		
		$this->usuario->setClave($ci_persona);		
		$this->usuario->reset_password();
		echo json_encode(array("mensaje"=>"Password Reiniciado."));
	}


	public function listaLogs(){
		$id_usuario = $this->request->id_usuario;
		$this->registros_usuario->setId_usuario($id_usuario);
		$datas = $this->registros_usuario->listar_logs();
		echo json_encode($datas);
	}

	public function activarCuenta(){		
		$id_usuario = $this->request->id_usuario;
		$nuevo = $this->request->actual;
		$this->usuario->setId_usuario($id_usuario);
		$this->usuario->setClave($nuevo);
		$this->usuario->setGrado("1");
		$this->usuario->activar_cuenta();
		echo json_encode(array("mensaje"=>"Cuenta activada."));
	}


	public function createUsuario()
	{
		$estado = 0; //0 -> Inactivo -- 1-> Activo
    	$fecha_ingreso = $this->request->fecha_ingreso;
    	$clave = $this->request->clave;
    	$cuenta = $this->request->cuenta;
    	$grado = 0;
    	$ci_persona = $this->request->ci_persona; // ci personal

    	$this->usuario->setEstado($estado);
    	$this->usuario->setFecha_ingreso($fecha_ingreso);
		$this->usuario->setCuenta($cuenta);
		$this->usuario->setClave($clave);
		$this->usuario->setGrado($grado);
		$this->usuario->setCi_persona($ci_persona);
		$data = $this->usuario->registrar();
		echo json_encode($data);
	}

	public function asignarRol(){
		$this->load->view('usuario/asignarRol');	
	}

	public function verificarPassword(){
		$password = $this->request->password;
		$id_usuario = $this->request->id_usuario;
		$nuevo = $this->request->actual;
		$this->usuario->setClave($password);
		$this->usuario->setId_usuario($id_usuario);
		if($this->usuario->validarPassword()){
		//password correcto
			$this->usuario->setClave($nuevo);
			$this->usuario->modificar_password();
				//cambio realizado				
				$mensaje = array("error" => "1", "mensaje"=>"Cambios Realizados");
		}
		else{//password Incorrecto
			$mensaje = array("error" => "2", "mensaje"=>"Error en el password actual");
		}
		echo json_encode($mensaje);
	}

	public function editUsuario()
	{
		$usuario = $this->input->post("usuario");

		$this->form_validation->set_rules($this->usuario_rule->apply());
		$this->form_validation->set_rules('usuario[id_usuario]', 'ID', 'trim|required|callback__verificar_id_usuario');

		if ( isset($usuario["password"]) && $usuario["password"] != '')
		{
			$this->form_validation->set_rules('usuario[password]', 'Contraseña', 'trim|required');			
			$this->form_validation->set_rules('usuario[rep_password]', 'Repetir Contraseña', 'trim|required|callback__validar_repetir_password['.$usuario["password"].']');		
			$usuario["password"] = md5($usuario["password"]);
				
		} else
		{
			unset($usuario["password"]);
		}
		

		if ( $this->form_validation->run() )
		{
	
			unset($usuario["rep_password"]);			

			$this->usuario_model->updateById( $usuario, $usuario["id_usuario"]);
			$this->session->set_flashdata('message', [ "success"=>"Se edito el registro" ]);
		} else
		{
			$this->session->set_flashdata('message', [ "error"=>validation_errors() ]);

		}
		redirect('usuario/lista','refresh');

	}

	

	public function desconectar()
	{
		$this->session->sess_destroy();
		//$this->config->load('SystemSupervisor/config');
		//redirect($this->config->item('page_login'),'refresh');
	}

	public function perfil(){
		$this->load->view('usuario/mi_perfil');
	}


	public function getUsuarioAjax()
	{
		$data = array();

		$usuario = $this->input->post("usuario");
		$data["usuario"] = $this->usuario_model->getById($usuario["id_usuario"]);  

		$this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
        ->_display();
		exit;
	}

	public function createAjax()
	{
		$usuario = $this->input->post("usuario");

		$this->form_validation->set_rules($this->usuario_rule->apply());
		$this->form_validation->set_rules('usuario[cuenta]', 'Cuenta', 'callback__verificar_cuenta_repetida');
		$this->form_validation->set_rules('usuario[rep_password]', 'Repetir Contraseña', 'trim|required|callback__validar_repetir_password['.$usuario["password"].']');

		if ( $this->form_validation->run() ) 
		{
			unset($usuario["rep_password"]);
			$usuario["password"] = md5($usuario["password"]);

			$this->usuario_model->insert($usuario);
			$data['result'] = 1;
			$data['message'] = "Se creo el usuario";
			
		} else
		{						
			$data['result'] = 0;
			$data['message'] = validation_errors();
		}

		$this->utils->json($data);
	}

	public function editAjax()
	{
		$usuario = $this->input->post("usuario");

		$this->form_validation->set_rules($this->usuario_rule->apply());
		$this->form_validation->set_rules('usuario[cuenta]', 'Cuenta', 'trim|required|callback__verificar_cuenta_repetida['.$usuario['id_usuario'].']');
		$this->form_validation->set_rules('usuario[id_usuario]', 'ID', 'trim|required|callback__verificar_id_usuario');

		if ( isset($usuario["password"]) && $usuario["password"] != '')
		{
			$this->form_validation->set_rules('usuario[password]', 'Contraseña', 'trim|required');			
			$this->form_validation->set_rules('usuario[rep_password]', 'Repetir Contraseña', 'trim|required|callback__validar_repetir_password['.$usuario["password"].']');		
			$usuario["password"] = md5($usuario["password"]);
				
		} else
		{
			unset($usuario["password"]);
		}
		

		if ( $this->form_validation->run() )
		{
	
			unset($usuario["rep_password"]);			

			$this->usuario_model->updateById( $usuario, $usuario["id_usuario"] );

			$data['result'] = 1;
			$data['message'] = "Se edito el registro";
		} else
		{
			
			$data['result'] = 0;
			$data['message'] = validation_errors();
		}
		$this->utils->json($data);
	}

	public function deleteUsuarioAjax()
	{
		$usuario = $this->input->post("usuario");
		$data = array();

		$this->form_validation->set_rules('usuario[id_usuario]', 'ID', 'trim|required|callback__verificar_id_usuario');

		if ( $this->form_validation->run() )
		{
			$this->usuario_model->deleteById($usuario["id_usuario"]);
			$data["result"] = 1;
		} else
		{
			$data["result"] = 0;
			$data["msg"] = validation_errors();
		}
		
		$this->utils->json($data);
	}

	public function getRelationWithRolAjax()
	{
		$data    = array();
		$usuario = $this->input->post('usuario');
		$rols    = $this->rol_model->getRolByUsuario( $usuario['id_usuario'] );
		$data['rols']   = $rols;
		$data['result'] = 1;
		$this->utils->json($data);	
	}

	public function assignRolToUsuarioAjax()
	{
		$this->load->model('rol_asignado_model');

		$data = array();
		$rols     = $this->input->post('rol');
		$usuario = $this->input->post('usuario');

		if ( $usuario['id_usuario']>0 )
		{
			$this->rol_asignado_model->deleteAllByUsuario( $usuario['id_usuario'] );

			foreach ($rols as $rol) {
				$this->rol_asignado_model->insert( [ 'id_usuario'=>$usuario['id_usuario'] , 'id_rol'=>$rol['id_rol'] ] );
			}
			$data['result'] = 1;
		} else
		{
			$data['result'] = 0;
		}

		$this->utils->json($data);

	}

	public function getRelationWithRutaAjax()
	{
		$data    = array();
		$usuario = $this->input->post('usuario');
		$rutas    = $this->ruta_model->getRutaByUsuario( $usuario['id_usuario'] );
		$data['rutas']  = $rutas;
		$data['result'] = 1;
		$this->utils->json($data);	
	}

	public function assignRutaToUsuarioAjax()
	{
		$this->load->model('ruta_asignado_model');

		$data = array();
		$rutas   = $this->input->post('ruta');
		$usuario = $this->input->post('usuario');

		if ( $usuario['id_usuario']>0 )
		{
			$this->ruta_asignado_model->deleteAllByUsuario( $usuario['id_usuario'] );

			foreach ($rutas as $ruta) {
				$this->ruta_asignado_model->insert( [ 'id_usuario'=>$usuario['id_usuario'] , 'id_ruta'=>$ruta['id_ruta'] ] );
			}
			$data['result'] = 1;
		} else
		{
			$data['result'] = 0;
		}

		$this->utils->json($data);

	}

	public function _verificar_cuenta_repetida($cuenta, $idUsuario=0)
	{
		$this->form_validation->set_message(__FUNCTION__, 'Ya existe la Cuenta');
		if ( $idUsuario==0 )
		{
			$cantidad = $this->usuario_model->count( [ 'cuenta'=>$cuenta ] );
		} else
		{
			$cantidad = $this->usuario_model->count( [ 'cuenta'=>$cuenta, 'id_usuario!='=>$idUsuario ] );
		}
		return ( $cantidad==0 );
	}

	public function _validar_repetir_password($repPassword, $password)
	{
		$this->form_validation->set_message(__FUNCTION__, 'Repetir el mismo password escrito');
		return ($repPassword == $password);		
	}

	public function _verificar_id_usuario($idUsuario)
	{
		$this->form_validation->set_message(__FUNCTION__, 'No existe el identificador del usuario');
		if ($idUsuario > 0)
		{			
			$usuario = $this->usuario_model->getById($idUsuario);			
			return ( isset($usuario) );
		} else
		{
			return FALSE;
		}
	}

}

/* End of file usuario_controller.php */
/* Location: ./application/controllers/usuario_controller.php */