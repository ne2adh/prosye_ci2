<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seguir_controller_convenio extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('date','url');


	}
	
	public function index(){
		        
	    $id=array();
		$data['results'] =  $this->proyectos_model->get_proyectos_espe_convenio();    
        $this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/proyectos/mis_proy_segui_convenio',$data);
		$this->load->view('layouts/footer');

	}

}