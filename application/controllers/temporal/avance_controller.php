<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Avance_controller extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('date','url');


	}
	
	public function index()
	{
		        
	    $id=array();
	    //$id=$this->session->userdata('id');
		$data['results'] =  $this->temporal_model->index();    
        $this->load->view('layouts/header');
		$this->load->view('layouts/menugestor');
		$this->load->view('admin/temporales/segui_proy_view',$data);
		$this->load->view('layouts/footer');

	}

	public function ingreso($id_tramo,$id_proyecto){
		$id_tramo=$id_tramo;
		$id_proyecto=$id_proyecto;
		        
	    $data=array(
			'id_proyecto'=>$id_proyecto,
			'id_tramo'=>$id_tramo,
			'proyecto'=>$this->temporal_model->proy($id_proyecto),
			
		);
		$tramos = array("tramos" => $this->tramo_model->index($id_proyecto));
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside_proyec', $tramos);
		$this->load->view('admin/temporales/temporal_proy_view',$data);
		$this->load->view('layouts/footer');

	}
	public function store()
	{
		$id_tramo=$this->input->post('id_tramo');
		$id_proyecto=$this->input->post('id_proyecto');
		$id_segui=$this->temporal_model->current_num();        
	    $data=array(
			'id_proyecto'=>$id_proyecto,
			'id_tramo'=>$id_tramo,
			'id_segui_temporal'=>$id_segui,
			'anio'=>$this->input->post('gestion'),
			'mes'=>$this->input->post('mes'),
			'monto'=>$this->input->post('monto'),
			'porcent_avance_fisico'=>$this->input->post('por_avance'),

		);    
        $this->temporal_model->insert($data);
		redirect(base_url()."temporal/avance_controller");

	}
}