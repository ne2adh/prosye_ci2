<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("Usuarios_model");
		$this->load->model("Rol_model");
		$this->load->library('session');
		$this->load->helper('url');
	}
	public function index(){
		if($this->session->userdata("login"))
		{
			redirect(base_url()."dashboard");
		}
		else 
		{
			$this->load->view("admin/login");
		}
	}
 
	public function login(){
		$cuenta=$this->input->post("cuenta");
		$clave=$this->input->post("clave");
		$res=$this->Usuarios_model->login($cuenta,md5($clave));
		if (!$res)
		{
			$this->session->set_flashdata("error","El ususario y/o contraseña son incorrectos 
			ingrese los datos nuevamente");
			redirect(base_url());
		}
		else 
		{
			//Buscar Nombre Rol
			$nombre_rol = $this->Rol_model->searchNombreRol($res->rol_id);			
			$data=array
			('id' =>$res->id_usuario,
			 'estado' =>$res->estado,
			 'ci'=>$res->ci_persona,
			 //'perfil'=>$res->id_perfil,
			 'rol'=>$res->rol_id,
			 'nombre_rol'=>$nombre_rol[0]->nombre,
			 'nombre'=>$res->nombres.' '.$res->ap_paterno.' '.$res->ap_materno,
			 'login'=>TRUE,
			);
		
			$this->session->set_userdata($data);
			
			//redirect(base_url()."proyecto/mis_proyectos");
			redirect(base_url()."dashboard");

			/*switch ($res->rol_id) {
				case 19:
					redirect(base_url()."proyecto/mis_proyectos/propios/$res->id_usuario");
					break;
				case 20:
					redirect(base_url()."proyecto/mis_proyectos/sisin/$res->id_usuario");
					break;
				
			}*/
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());

	}


}
