<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro_ente extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
        
        $this->load->library('form_validation','from_helper');
        $this->load->helper('date','url');


	}
	public function index()
	{
		//$id=array();
		//$data=array();
        $data['results']=$this->ente_model->index();    
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/financiamiento/registro',$data);
		$this->load->view('layouts/footer');

	}
	public function add()
	{ 
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/financiamiento/ingreso');
		$this->load->view('layouts/footer');

	}

	public function ingreso()
	{
		$data=array();
		$data['cod_inst']=$this->input->post('cod_inst');
		$data['nombre']=$this->input->post('nombre');
		$data['descripcion']=$this->input->post('descripcion');
		$this->ente_model->store($data);
		redirect(base_url()."proyecto/mis_proyectos/todos");
	}

	


	
	


}
