<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Componente_model extends CI_Model
{
  
		public function __construct()
			{
			parent::__construct();
			}
	    
	    public function index(){
			$this->db->select('*'); 
			$this->db->from('asignacion');
			$this->db->join('usuario','usuario.id_usuario=asignacion.id_usuario');
			$this->db->join('proyectos','proyectos.id_proyecto = asignacion.id_proyecto');
			$this->db->join('tramo','tramo.id_proyecto=proyectos.id_proyecto');
			//$this->db->join('componente_tramo','componente_tramo.id_tramo = tramo.id_tramo');
			//$this->db->order_by("category", "asc");			
			return $this->db->get()->result();
		 }	
		   public function getProyecto($id_proyecto)
		 {
			$this->db->select('*'); 
			$this->db->from('proyectos');
			$this->db->where("id_proyecto",$id_proyecto);
			return $this->db->get()->row();
		 
		 }
		
		public function getComponenteProy($id_proyecto){
			$this->db->select('*'); 
			$this->db->from('componente_proy');
			$this->db->where("id_proyecto",$id_proyecto);
			return $this->db->get()->result();		 
		}

		public function getComponente_edit($id_componente_proy){
			$this->db->select('*'); 
			$this->db->from('componente_proy');
			$this->db->where("id_componente_proy",$id_componente_proy);
			return $this->db->get()->row();
		 
		}
		
		public function getConvenioProy($id_proyecto){
			$this->db->select('id_convenio'); 
			$this->db->from("convenio");
			$this->db->where("id_proyecto",$id_proyecto);
			$this->db->where("estado",1);
			//$query=$this->db->get();
			//$result=$query->row();
			
			//if (isset($result))
			 //{ 
				// return $result->id_convenio;
			 //}
			 return $this->db->get()->row();
			
		 }
		 		
		  public function getTramo($id_tipo)
		 {
			$this->db->select('*'); 
			$this->db->from('proyectos');
			$this->db->join("tramo","tramo.id_proyecto=proyectos.id_proyecto");
			$this->db->where("id_tipo",$id_tipo);
			return $this->db->get()->result();
		 
		 }	
		  
		  public function getComponente($id_tramo)
		 {
			$this->db->select('*'); 
			$this->db->from('tramo');
			$this->db->where("id_tramo",$id_tramo);
			return $this->db->get()->row();
		 
		 }	
		 public function getCompo($id_tramo)
		 {
			$this->db->select('*'); 
			$this->db->from('componente_tramo');
			$this->db->where("id_tramo",$id_tramo);
			return $this->db->get()->result();
		 
		 }
		  public function getCompoActividad($id_tramo)
		 {
			$this->db->select('*'); 
			$this->db->from('componente_tramo');
			$this->db->where("id_tramo",$id_tramo);
			return $this->db->get()->result();
		 
		 }
		
		public function inser($data)
		{
			return $this->db->insert("componente_tramo",$data);
		}
		public function insert_proy($data)
		{
			return $this->db->insert("componente_proy",$data);
		}
		 public function delete($id_componente_proy)
		{
		    $this->db->delete('componente_proy', array('id_componente_proy' => $id_componente_proy));
		}

		public function delete1($id)
		{
		    $this->db->delete("componente_tramo", array('id_componente_tramo' => $id));
		}
		public function tramo($id_tramo)
		{
		 	$this->db->select('*');
		 	$this->db->from('tramo');
		 	$this->db->where('id_tramo',$id_tramo);
		 	return $this->db->get()->row();
		}
		 
		 public function componente($id_componente_tramo)
		 {
		 	$this->db->select('*');
		 	$this->db->from('componente_tramo');
		 	$this->db->where('id_componente_tramo',$id_componente_tramo);
		 	return $this->db->get()->row();
		 }
		  public function compo($id_componente_tramo)
		 {
		 	$this->db->select('*');
		 	$this->db->from('componente_tramo');
		 	$this->db->where('id_componente_tramo',$id_componente_tramo);
		 	return $this->db->get()->result();
		 }

		  public function update($id_componente_tramo,$data)
		 {
			$this->db->where("id_componente_tramo",$id_componente_tramo);
			return $this->db->update("componente_tramo",$data);
			
		 }
		  public function update_componente($id_componente_proy,$data)
		 {
			$this->db->where("id_componente_proy",$id_componente_proy);
			$result = $this->db->update("componente_proy",$data);			
			return $result;
			
		 }
		 public function getTipo($id_proyecto)
		{
		 	$this->db->select('*');
		 	$this->db->from('tipo');
		 	$this->db->where('id_proyecto',$id_proyecto);
		 	return $this->db->get()->row();
		}
		 public function compara($descrip_compo_proy,$id_proyecto)
		 {
			$this->db->select('*');
		 	$this->db->from('componente_proy');
			$this->db->where("descrip_compo_proy",$descrip_compo_proy);
			$this->db->where("id_proyecto",$id_proyecto);
			$query=$this->db->get();
			$result=$query->row();
			 if (isset($result))
			 { 
				 return $resp=1;
			 }
			//if (($this->db->get()->result())>1){return (1);}else{ return (0);} 

		 }
		 
		 


}//fin class

