<?php
class Map_model extends CI_Model {
function   construct()
{
parent::  construct();
}

function get_coordinates($id_proyecto)
{

$return = array();

$this->db->select("latitud,longitud");
$this->db->from("coordenada_inicial");
$this->db->where('id_proyecto',$id_proyecto);
$query = $this->db->get();

if ($query->num_rows()>0) {
foreach ($query->result() as $row) {
array_push($return, $row);
}
}

return $return;

}
}
