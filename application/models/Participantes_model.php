<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Participantes_model extends CI_Model
{
  
		public function __construct()
			{
			parent::__construct();
			}
		
		
		
		
		 public function index($id_convenio)
		 {
		 	$this->db->select('*');
		 	$this->db->from('convenio');
		 	$this->db->where('id_convenio',$id_convenio);

		 	//$this->db->join('municipio','municipio.id_municipio=detalle_municipio.id_municipio');
		 	//$this->db->join('detalle_municipio','detalleMmunicipio.id_proyecto=convenio.id_proyecto');
		 	return $this->db->get()->row();
		 }
		
		 public function current_part()
		 {
			$query = $this->db->query('SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA=DATABASE() AND   TABLE_NAME="participantes" ');
			$resultado= $query->row_array();
			return $resultado['AUTO_INCREMENT'];
		 }
		 
			public function municipio($id_proyecto)
		 {
		 	$this->db->select('*');
		 	$this->db->from('detalle_municipio');
		  	$this->db->join('municipios','municipios.id_municipio=detalle_municipio.id_municipio');
		 	$this->db->where('detalle_municipio.id_proyecto',$id_proyecto);	
		 	return $this->db->get()->result();
		 }	
		  public function ente()
		 {
		 	$this->db->select('*');
		 	$this->db->from('ente_finan');
		 	//$this->db->where('id_proyecto',$id_proyecto);
			//$this->db->join('participantes','participantes.id_ente_finan=ente_finan.id_ente');
		 	//$this->db->join('municipio','municipio.id_municipio=detalle_municipio.id_municipio');
		 	//$this->db->join('detalle_municipio','detalleMmunicipio.id_proyecto=convenio.id_proyecto');
		 	return $this->db->get()->result();
		 }
		  public function ente1($id_proyecto,$id_componente_proy,$id_convenio)
		 {
		 	$this->db->select('*');
		 	$this->db->from('ente_finan');
			$this->db->where('id_proyecto',$id_proyecto);
			$this->db->where('id_componente_proy',$id_componente_proy);
			$this->db->where('id_convenio',$id_convenio); 
			$this->db->join('participantes','participantes.id_ente_finan=ente_finan.id_ente');
		 	//$this->db->join('municipio','municipio.id_municipio=detalle_municipio.id_municipio');
		 	//$this->db->join('detalle_municipio','detalleMmunicipio.id_proyecto=convenio.id_proyecto');
		 	return $this->db->get()->result();
		 }
		
		 public function participante($id_proyecto,$id_convenio,$id_componente_proy)
		 {
		 	$this->db->select('*');
		 	$this->db->from('participantes');
			$this->db->join('municipios','municipios.id_municipio=participantes.id_municipio');
			$this->db->where('id_proyecto',$id_proyecto);
			$this->db->where('id_convenio',$id_convenio);
			$this->db->where('id_componente_proy',$id_componente_proy);
		 	//$this->db->join('detalle_municipio','detalleMmunicipio.id_proyecto=convenio.id_proyecto');
		 	return $this->db->get()->result();
		 }
		 public function totalParticipante($id_proyecto)
		 {
		 	$this->db->select('monto_municipio,monto_ente');
		 	$this->db->from('participantes');
			//$this->db->join('municipios','municipios.id_municipio=participantes.id_municipio');
			$this->db->where('id_proyecto',$id_proyecto);
			//$this->db->where('id_convenio',$id_convenio);
			//$this->db->where('id_componente_proy',$id_componente_proy);
		 	//$this->db->join('detalle_municipio','detalleMmunicipio.id_proyecto=convenio.id_proyecto');
			 $query=$this->db->get();
			 $total=0;



			 //$output='<option value="">Sub-Area</option>';
			 foreach($query->result() as $row)
			 {
				 $total=$total+$row->monto_municipio+$row->monto_ente;
 
			 }


			 return $total;


			 

		 }
		 public function participa($id_participa,$id_componente_proy,$id_proyecto,$id_convenio)
		 {
		 	$this->db->select('*');
		 	$this->db->from('participantes');
			$this->db->join('municipios','municipios.id_municipio=participantes.id_municipio');
			$this->db->where('id_proyecto',$id_proyecto);
			$this->db->where('id_convenio',$id_convenio);
			$this->db->where('id_componente_proy',$id_componente_proy);
			$this->db->where('id_participa',$id_participa);
		 	//$this->db->join('detalle_municipio','detalleMmunicipio.id_proyecto=convenio.id_proyecto');
		 	return $this->db->get()->row();
		 }
		  public function participaFinan($id_participa,$id_componente_proy,$id_proyecto,$id_convenio)
		 {
		 	$this->db->select('*');
		 	$this->db->from('participantes');
			$this->db->join('ente_finan','ente_finan.id_ente=participantes.id_ente_finan');
			$this->db->where('id_proyecto',$id_proyecto);
			$this->db->where('id_convenio',$id_convenio);
			$this->db->where('id_componente_proy',$id_componente_proy);
			$this->db->where('id_participa',$id_participa);
		 	//$this->db->join('detalle_municipio','detalleMmunicipio.id_proyecto=convenio.id_proyecto');
		 	return $this->db->get()->row();
		 }
		  public function proyecto($id_proyecto)
		 {
		 	$this->db->select('*');
		 	$this->db->from('proyectos');
		 	$this->db->where('id_proyecto',$id_proyecto);

		 	//$this->db->join('municipio','municipio.id_municipio=detalle_municipio.id_municipio');
		 	//$this->db->join('detalle_municipio','detalleMmunicipio.id_proyecto=convenio.id_proyecto');
		 	return $this->db->get()->row();
		 }
		 public function componente($id_componente_proy)
		 {
		 	$this->db->select('*');
		 	$this->db->from('componente_proy');
			 $this->db->where('id_componente_proy',$id_componente_proy);
			 return $this->db->get()->row();
		 }
		 

		 public function insert($data)
		 {
			
			return $this->db->insert("participantes",$data);
		 }
		 public function insert_detalle($datas)
		 {
			
			return $this->db->insert("detalle_beneficio",$datas);
		 }
		  public function delete($id_participa,$id_componente_proy,$id_proyecto,$id_convenio)
		{
		    $this->db->where('id_participa',$id_participa);
		    $this->db->where('id_componente_proy',$id_componente_proy);
		    $this->db->where('id_proyecto',$id_proyecto);
		    $this->db->where('id_convenio',$id_convenio);
		    $this->db->delete('participantes', array('id_participa' => $id_participa));
		}
		 
		 public function updateMuni($id_participa,$data)
		 {
			
			$this->db->where("id_participa",$id_participa);
			return $this->db->update("participantes",$data);
			
		 }
		  
		 

}//fin class

