<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sisin_model extends CI_Model
{
  
		public function __construct()
			{
			parent::__construct();
			}
		
		public function get_sector()
		 {
			$data=$this->input->post('sector');
			//console.log($data);
			return $data;			
			
		 }
		
		public function get_sub_sector_model($sector)
		 {
			$this->db->where('relacion',$sector);
			$this->db->order_by('denominacion','ASC');
			$query=$this->db->get('sectores2');

			$output='<option value="">Denominacion</option>';
			foreach($query->result() as $row)
			{
				
				$output.='<option value="'.$row->id_clasificacion_sectorial.'">'.$row->denominacion.'-->'.$row->sigla;'</option>';

			}
			return $output;			
		 }

		 public function getProyecto($id_proyecto)
		 {
			$this->db->where("id_proyecto",$id_proyecto);
			$resultado = $this->db->get("proyectos");
		 	return $resultado->row();
		 
		 }
		 public function sisin($id_proyecto)
		 {
			$this->db->where("id_proyecto",$id_proyecto);
			$resultado = $this->db->get("sisin");
		 	return $resultado->row();
		 
		 }
		 public function conSisin()
		 {
			$this->db->select('*'); 
			$this->db->from('proyectos');
			$this->db->join('sisin','sisin.id_proyecto=proyectos.id_proyecto');
			$this->db->where('proyectos.estado','1');
			$this->db->where('proyectos.alta','1');
			//$this->db->where('id_usuario',$id);
			$this->db->where("sisin.codigo_sisin!=''");
			//$this->db->join('asignacion','asignacion.id_proyecto=proyectos.id_proyecto');
			return $this->db->get()->result();
		 }


		public function getSector(){
			$this->db->select('*'); 
			$this->db->from('sectores1');
			//$this->db->join('asignacion','asignacion.id_proyecto=proyectos.id_proyecto');
			//$this->db->where('id_usuario',$id);
			return $this->db->get()->result();
		}

		public function update_sisin($id_proyecto){
		 	$this->db->select('*'); 
			$this->db->from('proyectos');
			$this->db->where('proyectos.id_proyecto',$id_proyecto);
			return $this->db->get()->row();
		}

		public function update_sisin1($id_proyecto,$data1){
		 	$this->db->where('proyectos.id_proyecto',$id_proyecto);
			return $this->db->update('proyectos',$data1);
			return $this->db->get()->result();
		}
 
		public function insert($data){
			return $this->db->insert("sisin",$data);
		}

		 public function nombres($num)
		 {
			$this->db->select("denominacion"); 
			$this->db->from("sectores2");
			$this->db->where("id_clasificacion_sectorial",$num);
			$query=$this->db->get();
			$result=$query->row();
			 if (isset($result))
			 { 
				 return $result->denominacion;
			 }
			
		 }

		public function nombres1($num){
			$this->db->select("denominacion"); 
			$this->db->from("sectores3");
			$this->db->where("id_clasificacion_sectorial",$num);
			$query=$this->db->get();
			$result=$query->row();
		 	if (isset($result)){ 
				return $result->denominacion;
			}
		}
		
		 public function get_sub_sector($sect)
		 {
			$this->db->where('relacion',$sect);
			$this->db->order_by('denominacion','ASC');
			$sub_sector=$this->db->get('sectores2');
			if($sub_sector->num_rows() > 0)
			{
			return $sub_sector->result();		
			}	
		 }
		 
		 public function get_sub_denominacion($sect)
		 {
			$this->db->where('relacion',$sect);
			$this->db->order_by('denominacion','ASC');
			$sub_sector=$this->db->get('sectores3');
			if($sub_sector->num_rows() > 0)
			{
				return $sub_sector->result();		
			}	
		 }

		 public function update($data, $id_proyecto){
			$this->db->where('id_proyecto', $id_proyecto);
			$this->db->update('sisin', $data);
		}

}//fin class

