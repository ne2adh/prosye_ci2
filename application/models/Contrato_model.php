<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contrato_model extends CI_Model
{
  public function __construct()
    {
    parent::__construct();
    }
public function index($id)
{
$this->db->select('*');
$this->db->from('asignacion');
$this->db->join('usuario','usuario.id_usuario=asignacion.id_usuario');
$this->db->join('proyectos','proyectos.id_proyecto = asignacion.id_proyecto');
$this->db->join('sisin','sisin.id_proyecto = proyectos.id_proyecto');
$this->db->where('proyectos.alta',1);
$this->db->where('proyectos.sisin',1);
return $this->db->get()->result();
}
public function insert($data)
{
 return $this->db->insert("contrato_proy",$data);
}
public function compara($id_contrato,$id_proyecto){
			$this->db->select('*');
		 	$this->db->from('contrato_proy');
			$this->db->where("id_contrato",$id_contrato);
			$this->db->where("id_proyecto",$id_proyecto);
			$query=$this->db->get();
			$result=$query->row();
			 if (isset($result))
			 {
				 return $resp=1;
			 }

		 }
public function update($id_contrato,$data){
			$this->db->where("id_contrato",$id_contrato);
			return $this->db->update("contrato_proy",$data);
		 }

public function comparaGarantia($id_contrato,$id_proyecto){
			$this->db->select('*');
		 	$this->db->from('garantia_proy');
			$this->db->where("id_contrato",$id_contrato);
			$this->db->where("id_proyecto",$id_proyecto);
			$query=$this->db->get();
			$result=$query->row();
			 if (isset($result))
			 {
				 return $resp=1;
			 }

		 }

public function updateGarantia($id_garantia,$data){
			$this->db->where("id_garantia",$id_garantia);
			return $this->db->update("garantia_proy",$data);
		 }
}
