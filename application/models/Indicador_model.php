<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indicador_model extends CI_Model
{

		public function __construct()
			{
			parent::__construct();
			}

			public function index(){
				$this->db->select('*');
				$result = $this->db->get('indicador');
				return $result->result_array();
				}
				

		  public function indicador($id_proyecto)
		 {
			$this->db->select('*');
		 	$this->db->from('indicador_inicial');
			$this->db->where("id_proyecto",$id_proyecto);
			return $this->db->get()->result();

		 }
		  public function indicadorEdit($id,$id_proyecto)
		 {
			$this->db->select('*');
		 	$this->db->from('indicador_inicial');
			$this->db->where("id_proyecto",$id_proyecto);
			$this->db->where("id",$id);
			return $this->db->get()->row();

		 }
		   public function traerProy($id_proyecto)
		 {
			$this->db->select('*');
		 	$this->db->from('proyectos');
			$this->db->where("id_proyecto",$id_proyecto);
			return $this->db->get()->row();

		 }
		  public function compara($nombre_indicador,$id_proyecto)
		 {
			$this->db->select('*');
		 	$this->db->from('indicador_inicial');
			$this->db->where("nombre_indicador",$nombre_indicador);
			$this->db->where("id_proyecto",$id_proyecto);
			$query=$this->db->get();
			$result=$query->row();
			 if (isset($result))
			 {
				 return $resp=1;
			 }
			//if (($this->db->get()->result())>1){return (1);}else{ return (0);}

		 }

		 public function insert($data)
		 {

			return $this->db->insert("indicador_inicial",$data);
		 }

		public function delete($id)
		{
			//$this->db->where('id_proyecto',$id_proyecto);
		  $this->db->delete('indicador_inicial', array('id' => $id));
		}

		public function update($id,$data)
		 {

			$this->db->where("id",$id);
			return $this->db->update("indicador_inicial",$data);

		 }
		public function insertNombre_indicador($data){
		return $this->db->insert("indicador",$data);
		}



}//fin class
