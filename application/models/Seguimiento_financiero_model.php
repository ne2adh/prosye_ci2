<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seguimiento_financiero_model extends CI_Model{
	public function __construct(){
		parent::__construct();
    }
    
    public function buscarSeguimiento($id_proyecto, $mes, $gestion, $descrip_compo_proy ){
        $this->db->select('*');
		$this->db->from('seguimiento_financiero');
        $this->db->where('id_proyecto',$id_proyecto);
        $this->db->where('mes',$mes);
        $this->db->where('gestion',$gestion);
        $this->db->where('descripcion_compo_proy',$descripcion_compo_proy);
        $result = $this->db->get()->row_array(); 
		return count($result)>0;
    }

}//fin class
