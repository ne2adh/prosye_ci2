<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_model extends CI_Model {

	public function getUsuarios(){		
		$this->db->select('usuario.id_usuario, usuario.ci_persona, 
						   usuario.cuenta, personal.nombres, personal.ap_paterno, 
						   personal.ap_materno, rol.nombre, rol.id_rol, 
						   area.nombre AS nombre_area'); 
		$this->db->from('usuario');
		$this->db->join('rol','usuario.rol_id=rol.id_rol');
		$this->db->join('personal','personal.CI = usuario.ci_persona');
		$this->db->join('area','area.cod = usuario.id_area', 'left');
		return $this->db->get()->result_array();	
	}

	public function index(){
		$this->db->select('*');
		$result = $this->db->get('usuario');
		return $result->result_array();
		}

	public function getRoles(){
		$this->db->order_by('nombre','ASC');
		$query=$this->db->get('rol');
		return $query->result();

	}
	public function getPersona(){
		$this->db->order_by('ap_paterno','ASC');
		$this->db->where('activo','1');
		$query=$this->db->get('personal');
		return $query->result();
	}
	public function save($data){
		return $this->db->insert("usuario",$data);
	}


	public function login($cuenta,$clave){
		$this->db->select('*');
		$this->db->from('usuario');
		$this->db->join('personal','personal.ci=usuario.ci_persona');
		$this->db->where("cuenta",$cuenta);
		$this->db->where("clave",$clave);

		//return $this->db->get()->result()
		//$resultados=$this->db->get("usuario");
		$resultados=$this->db->get();
			if ($resultados->num_rows() > 0){
				return $resultados->row();
			}else{
				return false;
			}
	}

	public function personal(){
			$this->db->select('*');
			$result = $this->db->get('personal');
			return $result->result_array();
	}

	public function nombre($CI){
			$this->db->where('CI', $CI);
			$consulta=$this->db->get('personal');
			return $consulta->result();
	}

	public function detalleUsuario($idUsuario){
		$this->db->select("id_usuario, estado, fecha_ingreso, cuenta, grado,
						   ci_persona, rol_id, personal.CI, CI2, expedicion, 
						   nombres, ap_paterno, ap_materno, sexo, fecha_nacimiento, 
						   est_civil, profesion, telefono, asigna_cargo.activo, 
						   afp, registro_nuevo_afp_gador, rentista, fech_registro, 
						   cargo.cod, fecha_ing, fecha_sal, motivo, observacion, 
						   baja, cargo.nombre AS nombre_cargo, item, privilegio, 
						   salario, libre, id_rol, rol.nombre AS nombre_rol,
						   area.nombre AS nombre_area, area.cod AS id_area");
		$this->db->from('usuario');
		$this->db->join('personal', 'usuario.ci_persona=personal.CI');
		$this->db->join('asigna_cargo', 'personal.CI = asigna_cargo.CI');
		$this->db->join('cargo', 'asigna_cargo.rel = cargo.cod');
		$this->db->join('rol', 'usuario.rol_id = rol.id_rol');
		$this->db->join('area','area.cod = usuario.id_area', 'left');
		$this->db->where("id_usuario",$idUsuario);
		$this->db->limit(1);
		return $this->db->get()->result();
	}

	public function verificarPassword($nuevo){
		$this->db->select("clave");
		$this->db->from("usuario");
		$nuevo = md5($nuevo);
		$this->db->where("id_usuario", $this->session->id);
		$result = $this->db->get()->result();		
		return ($nuevo==$result["0"]->clave);

	}

	public function updatePassword($nuevo){
		$nuevo = md5($nuevo);
		$this->db->set("clave", $nuevo);
		$this->db->where("id_usuario", $this->session->id);
		return $this->db->update("usuario");
	}

	public function editUsuario($id_usuario, $data){		
		$this->db->set($data);
		$this->db->where("id_usuario", $id_usuario);
		$data = $this->db->update("usuario"); 		
		return $data;
	}

	public function getId_area($id_usuario){
		$this->db->select("id_area");
		$this->db->from("usuario");		
		$this->db->where("id_usuario", $id_usuario);
		$result = $this->db->get()->result();
		return ($result["0"]->id_area);
	}

	public function getId_rol($id_usuario){
		$this->db->select("rol_id");
		$this->db->from("usuario");		
		$this->db->where("id_usuario", $id_usuario);
		$result = $this->db->get()->result();
		return ($result["0"]->rol_id);
	}
}
