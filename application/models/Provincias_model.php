<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Provincias_model extends CI_Model
    {
        public function __construct(){
            parent::__construct();

        }
        public function listProvincias(){
            $this->db->select('*');
            $this->db->from('provincias');
            return $this->db->get()->result();
        }        
    }
