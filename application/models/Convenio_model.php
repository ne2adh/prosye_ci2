<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Convenio_model extends CI_Model
{
  
		public function __construct()
			{
			parent::__construct();
			}
	    
	    public function index(){
			$this->db->select('*'); 
			$this->db->from('asignacion');
			$this->db->join('usuario','usuario.id_usuario=asignacion.id_usuario');
			$this->db->join('proyectos','proyectos.id_proyecto = asignacion.id_proyecto');
			$this->db->join('tramo','tramo.id_proyecto=proyectos.id_proyecto');
			$this->db->join('componente_tramo','componente_tramo.id_tramo = tramo.id_tramo');
			//$this->db->order_by("category", "asc");
			
			return $this->db->get()->result();

		}	

		public function current_con(){
			$query = $this->db->query('SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA=DATABASE() AND   TABLE_NAME="convenio" ');
			$resultado= $query->row_array();
			return $resultado['AUTO_INCREMENT'];
		}

		public function getConvenioProy($id_proyecto){
			$this->db->select('*'); 
			$this->db->from('convenio');
			$this->db->where("id_proyecto",$id_proyecto);
			return $this->db->get()->result();		 
		}
		 
		 public function getConvenio($convenio)
		 {
			$this->db->select('*'); 
			$this->db->from('convenio');
			$this->db->where('id_convenio',$convenio);
			//$this->db->join('proyectos','proyectos.id_proyecto = asignacion.id_proyecto');
			//$this->db->join('tramo','tramo.id_proyecto=proyectos.id_proyecto');
			//$this->db->join('componente_tramo','componente_tramo.id_tramo = tramo.id_tramo');
			//$this->db->order_by("category", "asc");
			
			return $this->db->get()->row();		 
		 }	

		  public function traerProy($id_proyecto){
			$this->db->select('*'); 
			$this->db->from('proyectos');
			$this->db->where('id_proyecto',$id_proyecto);
			return $this->db->get()->row();		 
		 }

		public function traer_convenio($id_proyecto){
			$this->db->select('id_convenio'); 
			$this->db->from('convenio');
			$this->db->where('id_proyecto',$id_proyecto);
			/*$sql = $this->db->get_compiled_select();
			echo $sql;*/
			$query=$this->db->get();
			//var_dump($query);
			$result=$query->row();			
			if (isset($result)){ 
				 return $result->id_convenio;
			}
			//var_dump($this->db->get()->row());
			//return $this->db->get()->row();
			//verificar si agrega correctamente el convenio en proyecto
			return NULL;
		}
		
		public function traer_ruta($id_proyecto){
			$this->db->select('pant'); 
			$this->db->from('pantalla');
			$this->db->where('id_proyecto',$id_proyecto);
			$query=$this->db->get();
			$result=$query->row();
			 if (isset($result))
			 { 
				 return $result->pant;
			 }
			
			return $this->db->get()->row();		
		 }	
		
		public function insert($data)
		{
			return $this->db->insert("convenio",$data);
		}
		public function update($id_convenio,$data)
		{

			$this->db->where("id_convenio",$id_convenio);
			return $this->db->update("convenio",$data);
		}

}//fin class

