<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rol_model extends CI_Model{
		public function __construct(){
			parent::__construct();
		}

public function index(){
	$this->db->select('*');
	$result = $this->db->get('rol');
	return $result->result_array();
	}

	public function insert($data){
		return $this->db->insert("rol",$data);
	}

	public function dameRol($id_rol){
		$this->db->select('*');
		$this->db->from('rol');
		$this->db->where('id_rol',$id_rol);		
		return $this->db->get()->row();
	}

public function delete($id_rol){
	 $this->db->delete('rol', array('id_rol' => $id_rol));
	}

public function edit($id,$data){
	$this->db->where("id_rol",$id);
	return $this->db->update("rol",$data);
	}

	//Lista menus asignados a un Rol
	public function listRolAsignado($id_rol){
		$this->db->select('*');
 		$this->db->from('rol_menu'); 		
		$this->db->where('rol_menu.id_rol',$id_rol);
		$result_rol_menu = $this->db->get()->result(); //lista de menus asignados a un rol		
		$data=$this->seccion_model->index(); 
		$r = array();
		foreach($data as $item){
			 $id = $item->id;
			 $nombre = $item->nombre;
			 $logo = $item->logo;
			 $prioridad = $item->prioridad;
			 $item_seccion = $this->menu_model->buscar($id);
			 $item_menu = array();
			 foreach($item_seccion as $menu){
				$id_menu = $menu->id;				
				$res = array_filter(
					$result_rol_menu,
					function ($e) use($id_menu) {						
						return $e->id_menu == $id_menu;
					}
				);				
				if(count($res)>0){
					$temp = array (
						"id"=> $menu->id,
						"nombre_menu"=> $menu->nombre_menu,
						"direccion"=> $menu->direccion,
						"tipo_menu"=> $menu->tipo_menu,
						"id_seccion"=> $menu->id_seccion,
						"logo"=> $menu->logo
					);
					array_push($item_menu, $temp);
				}
			 }
			 $temp = array(
				 "id" => $id,
				"nombre" => $nombre,
				"logo" => $logo,
				"prioridad" =>$prioridad,
				"menu" => $item_menu
			 ); 
			 array_push($r, $temp);
		}		
		return $r;
	}

	public function searchNombreRol($id_rol){
		$this->db->select('nombre');
 		$this->db->from('rol'); 		
		$this->db->where('id_rol',$id_rol);
		return $this->db->get()->result();
	}

}//fin class
