<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tramo_model extends CI_Model
{

		public function __construct()
			{
			parent::__construct();
			}

	    public function index($id_proyecto){
			$this->db->select('*');
			$this->db->from('tramo');
			$this->db->where("id_proyecto",$id_proyecto);
			return $this->db->get()->result();
		}

		public function getTramo($id_tramo){
			$this->db->select("*");
			$this->db->from("tramo");
			$this->db->where("id_tramo",$id_tramo);			
			return $this->db->get()->row();
		}

		public function update($id_tramo, $data){
			$this->db->set($data);
			$this->db->where('id_tramo', $id_tramo);
			//$sql = $this->db->get_compiled_select('tramo');			
			$result = $this->db->update('tramo');
			print_r($this->db->last_query()); 
			return $result;
		}

		public function getTipo($id_proyecto){
			$this->db->select('*');
			$this->db->from('asignacion');
			$this->db->join('usuario','usuario.id_usuario=asignacion.id_usuario');
			$this->db->join('proyectos','proyectos.id_proyecto = asignacion.id_proyecto');
			$this->db->join('tipo','tipo.id_proyecto=proyectos.id_proyecto');
			//$this->db->join('componente_tramo','componente_tramo.id_tramo = tramo.id_tramo');
			return $this->db->get()->result();
		 }

		 public function num_tramo($id_tipo){
		 	$this->db->select('*');
			$this->db->from('tipo');
			$this->db->where("id_tipo",$id_tipo);
			return $this->db->get()->row();
		 }

		 public function num_tipo($id_tipo){
		 	$this->db->select("numero_tramos");
			$this->db->from("tipo");
			$this->db->where("id_tipo",$id_tipo);
			$query = $this->db->get();
            $result=$query->row();
			//return $this->db->get()->row();
			if (isset($result))
		        {
		            return $result->numero_tramos;//write your column name
		        }
		        else
		        {
		            return 1;
		        }
		 }
		 public function insert_tipo($data)
		 {
			return $this->db->insert("tipo",$data);
		 }
		 public function insert_tramo($data_tramo)
		 {

		 	return $this->db->insert("tramo",$data_tramo);
		 }

		 public function current_num_tipo()
		 {
			$query = $this->db->query('SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA=DATABASE() AND   TABLE_NAME="tipo" ');
			$resultado= $query->row_array();
			return $resultado['AUTO_INCREMENT'];
		 }
		  public function proyecto($id_tipo)
		 {
		 	$this->db->select('*');
			$this->db->from('proyectos');
			$this->db->join("tipo","tipo.id_proyecto=proyectos.id_proyecto");
			$this->db->where("id_tipo",$id_tipo);
			return $this->db->get()->row();
		 }

		 public function current_num_tramo(){
			$query = $this->db->query('SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA=DATABASE() AND   TABLE_NAME="tramo" ');
			$resultado= $query->row_array();
			return $resultado['AUTO_INCREMENT'];
		 }

		 public function updateEstado($id_tramo, $convenio){
			$data = array("convenio" => $convenio);
			$this->db->set($data);			
			$this->db->where('id_tramo', $id_tramo);
			//$sql = $this->db->get_compiled_select('tramo');			
			$result = $this->db->update('tramo');			
			return $result;
		 }


}//fin class
