<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Temporal_model extends CI_Model
{
  
		public function __construct()
			{
			parent::__construct();
			}
	    
	    public function index()
		{
		$this->db->select('*'); 
		$this->db->from('asignacion');
		$this->db->join('usuario','usuario.id_usuario=asignacion.id_usuario');
		$this->db->join('proyectos','proyectos.id_proyecto = asignacion.id_proyecto');
		$this->db->join('tipo','tipo.id_proyecto=proyectos.id_proyecto');
		$this->db->join('tramo','tramo.id_tipo = tipo.id_tipo');

		//$this->db->where('usuario.id_usuario',$id);
		//$this->db->order_by("category", "asc");
		
		return $this->db->get()->result();

		 }	
		  public function proy($id_proyecto)
		{
		$this->db->select('*'); 
		$this->db->from('proyectos');
		$this->db->join('tramo','tramo.id_proyecto=proyectos.id_proyecto');
		//$this->db->join('componente_tramo','componente_tramo.id_tramo = tramo.id_tramo');
		$this->db->where('proyectos.id_proyecto',$id_proyecto);
		//$this->db->order_by("category", "asc");
		
		return $this->db->get()->row();

	 	}	
	  	
		 public function insert($data)
		 {

		 	return $this->db->insert("temporal_seguimineto",$data);
		 }

		 public function current_num()
		 {
			$query = $this->db->query('SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA=DATABASE() AND   TABLE_NAME="temporal_seguimineto" ');
			$resultado= $query->row_array();
			return $resultado['AUTO_INCREMENT'];
		 }
		 
		  
}