<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Procesar extends CI_Model 
	{
		private $latitud;
		private $longitud;
	
		public function __construct(){
			parent::__construct();
		}

		public function convertir($UTM){
			$parts = explode(" ", $UTM);
            $Zone = intval($parts[0]);
            $Letter = $parts[1].toUpperCase().charAt(0);
            $Easting = parseFloat($parts[2]);
            $Northing = parseFloat($parts[3]);
            $Hem='';
            if ($Letter>'M'){
				$Hem='N';
			}
            else{
				$Hem='S';
			}
            $north;
            if ($Hem == 'S'){
				$north = $Northing - 10000000;
			}
            else{
				$north = $Northing;
			}
            $latitude = ($north/6366197.724/0.9996+(1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)-0.006739496742*sin($north/6366197.724/0.9996)*cos($north/6366197.724/0.9996)*(atan(cos(atan(( exp(($Easting - 500000) / (0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*(1-0.006739496742*pow(($Easting - 500000) / (0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2)/3))-exp(-($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*( 1 -  0.006739496742*pow(($Easting - 500000) / (0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2)/3)))/2/cos(($north-0.9996*6399593.625*($north/6366197.724/0.9996-0.006739496742*3/4*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+pow(0.006739496742*3/4,2)*5/3*(3*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996 )/2)+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2))/4-pow(0.006739496742*3/4,3)*35/27*(5*(3*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2))/4+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2)*pow(cos($north/6366197.724/0.9996),2))/3))/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*(1-0.006739496742*pow(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2))+$north/6366197.724/0.9996)))*tan(($north-0.9996*6399593.625*($north/6366197.724/0.9996 - 0.006739496742*3/4*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+pow(0.006739496742*3/4,2)*5/3*(3*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+sin(2*$north/6366197.724/0.9996 )*pow(cos($north/6366197.724/0.9996),2))/4-pow(0.006739496742*3/4,3)*35/27*(5*(3*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2))/4+sin(2*$north/6366197.724/0.9996)*pow(cos(north/6366197.724/0.9996),2)*pow(cos($north/6366197.724/0.9996),2))/3))/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*(1-0.006739496742*pow(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2))+$north/6366197.724/0.9996))-$north/6366197.724/0.9996)*3/2)*(atan(cos(atan((exp(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*(1-0.006739496742*pow(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2)/3))-exp(-($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*(1-0.006739496742*pow(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2)/3)))/2/cos(($north-0.9996*6399593.625*($north/6366197.724/0.9996-0.006739496742*3/4*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+pow(0.006739496742*3/4,2)*5/3*(3*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2))/4-pow(0.006739496742*3/4,3)*35/27*(5*(3*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2))/4+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2)*pow(cos($north/6366197.724/0.9996),2))/3))/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*(1-0.006739496742*pow(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2))+$north/6366197.724/0.9996)))*tan(($north-0.9996*6399593.625*($north/6366197.724/0.9996-0.006739496742*3/4*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+pow(0.006739496742*3/4,2)*5/3*(3*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2))/4-pow(0.006739496742*3/4,3)*35/27*(5*(3*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2))/4+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2)*pow(cos($north/6366197.724/0.9996),2))/3))/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*(1-0.006739496742*pow(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2))+$north/6366197.724/0.9996))-$north/6366197.724/0.9996))*180/pi;
            $latitude=round($latitude*10000000);
            $latitude=$latitude/10000000;
            $longitude =atan((exp(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*(1-0.006739496742*pow(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2)/3))-exp(-($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*(1-0.006739496742*pow(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2)/3)))/2/cos(($north-0.9996*6399593.625*($north/6366197.724/0.9996-0.006739496742*3/4*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+pow(0.006739496742*3/4,2)*5/3*(3*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2))/4-pow(0.006739496742*3/4,3)*35/27*(5*(3*($north/6366197.724/0.9996+sin(2*$north/6366197.724/0.9996)/2)+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2))/4+sin(2*$north/6366197.724/0.9996)*pow(cos($north/6366197.724/0.9996),2)*pow(cos($north/6366197.724/0.9996),2))/3)) / (0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2))))*(1-0.006739496742*pow(($Easting-500000)/(0.9996*6399593.625/sqrt((1+0.006739496742*pow(cos($north/6366197.724/0.9996),2)))),2)/2*pow(cos($north/6366197.724/0.9996),2))+$north/6366197.724/0.9996))*180/pi+$Zone*6-183;
            $longitude=round($longitude*10000000);
            $longitude=$longitude/10000000;
            $this->latitud = $latitude;
			$this->longitud = $longitude;         
		}
	public function index($id_proyecto)	
	{
	$id_proyecto=$this->input->post('id_proyecto');
	
	if (substr($_FILES['excel']['name'],-3)=="csv")
	{
		$fecha		= date("Y-m-d");
		$carpeta 	= "archivo excel/";
		$excel  	= $fecha."-".$_FILES['excel']['name'];
		 
	
		move_uploaded_file($_FILES['excel']['tmp_name'], "$carpeta$excel");
		
		$row = 1;

		$fp = fopen ("$carpeta$excel","r");

		//fgetcsv. obtiene los valores que estan en el csv y los extrae.
  


		while ($data = fgetcsv ($fp, 1000, ","))
		{
			//si la linea es igual a 1 no guardamos por que serian los titulos de la hoja del excel.
			if ($row!=1)
			{
				/*$num1=(string)$data[0];
				echo $num1;
				$data[0]=str_replace(",",".",$num1);
				echo $data[0];
				$num2=(string)$data[1];
				echo $num2;
				$data[1]=str_replace(',','.',$num2);
				echo $data[1];
 				$data[2]=$id_proyecto;*/							
				
				$utm= "19 J ".$data[1]+" "+$data[2];
				$this->convertir($utm);
				$sql_guardar  ="INSERT INTO coordenada_inicial (id_proyecto,latitud,longitud)"; 
				$sql_guardar ="VALUES ('$id_proyecto','$this->latitud','$this->longitud')";
				echo $sql_guardar;
				mysqli_query($sql_guardar) or die(mysqli_error());

				if (!$sql_guardar)
				{
					echo "<div>Hubo un problema al momento de importar porfavor vuelva a intentarlo</div>";
					exit;
				}
			}

		$row++;
		}

		fclose ($fp);

		echo "<div>La importacion de archivo subio satisfactoriamente</div >";
		
		exit;

	}
	}
}	

