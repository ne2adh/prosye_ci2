<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model{
		public function __construct(){
			parent::__construct();
		}

		public function buscar($id_seccion){
			$this->db->select('*'); 
            $this->db->from('menu');
            $this->db->where('id_seccion',$id_seccion);
			$this->db->order_by("tipo_menu", "asc");
			$resultados=$this->db->get();
			return $resultados->result();
		}

		public function save($data){
			return $this->db->insert("menu",$data);
		}

		public function delete($idMenu){
			$this->db->where('id', $idMenu);
			return $this->db->delete('menu');
		}
		
}//fin class
