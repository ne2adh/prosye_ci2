<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Segui_componente_model extends CI_Model
{  
		public function __construct(){
			parent::__construct();
		}
	    
	    public function index(){
			$this->db->select('*'); 
			$this->db->from('asignacion');
			$this->db->join('usuario','usuario.id_usuario=asignacion.id_usuario');
			$this->db->join('proyectos','proyectos.id_proyecto = asignacion.id_proyecto');
			$this->db->join('tramo','tramo.id_proyecto=proyectos.id_proyecto');
			$this->db->join('componente_tramo','componente_tramo.id_tramo = tramo.id_tramo');
			//$this->db->order_by("category", "asc");
			
			return $this->db->get()->result();

		}	

		//recuperar  la suma de pagos de seguimientos financieros segun gestion y proyecto

		public function pagoAnualProyecto($id_proyecto, $gestion){			
			$sql = "SELECT SUM(monto_ejecutado) as monto_ejecutado FROM seguimiento_financiero WHERE id_proyecto=".$id_proyecto." AND gestion=".$gestion;
			$query = $this->db->query($sql);
			$resultado= $query->result();			
			return $resultado;
		}
}