<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Segui_actividad_model extends CI_Model
{

		public function __construct(){
			parent::__construct();
		}

	    public function index($id){
			$this->db->select('*');
			$this->db->from('asignacion');
			$this->db->join('usuario','usuario.id_usuario=asignacion.id_usuario');
			$this->db->join('proyectos','proyectos.id_proyecto = asignacion.id_proyecto');
			//$this->db->join('tramo','tramo.id_proyecto=proyectos.id_proyecto');
			$this->db->join('sisin','sisin.id_proyecto = proyectos.id_proyecto');
			//$this->db->join('contrato_proy','contrato_proy.id_proyecto = proyectos.id_proyecto');
			$this->db->where('proyectos.alta',1);
			$this->db->where('proyectos.sisin',1);
			$this->db->where('proyectos.estado',1);
			$this->db->group_by("proyectos.id_proyecto", "asc");
			return $this->db->get()->result();
		}

		public function index1($id){
			$this->db->select('*');
			$this->db->from('asignacion');
			$this->db->join('usuario','usuario.id_usuario=asignacion.id_usuario');
			$this->db->join('proyectos','proyectos.id_proyecto = asignacion.id_proyecto');
			//$this->db->join('tramo','tramo.id_proyecto=proyectos.id_proyecto');
			$this->db->join('sisin','sisin.id_proyecto = proyectos.id_proyecto');
			$this->db->join('contrato_proy','contrato_proy.id_proyecto = proyectos.id_proyecto');
			$this->db->where('proyectos.alta',1);
			$this->db->where('proyectos.sisin',1);
			$this->db->where('contrato_proy.estado',1);
			$this->db->group_by("proyectos.id_proyecto", "asc");
			return $this->db->get()->result();
		 }
		 
 		public function indexp($id_proyecto){
			$this->db->select('*');
			$this->db->from('contrato_proy');
			$this->db->where('id_proyecto',$id_proyecto);
			return $this->db->get()->result();
		}

		public function compo($id_proyecto){
			$this->db->select('*');
			$this->db->from('proyectos');
			$this->db->join('componente_proy','proyectos.id_proyecto=componente_proy.id_proyecto');
			$this->db->where('proyectos.id_proyecto',$id_proyecto);
			return $this->db->get()->result();
		}
		public function compotramo($id_proyecto){
			return $this->db->query('
					SELECT * FROM tramo t join componente_proy c WHERE t.id_tramo=c.id_proyecto and c.id_proyecto='.$id_proyecto.'
				')->result();
			}
		public function contratoG($id_proyecto,$descrip_compo_proy)
		{
			return $this->db->query('
			SELECT * FROM contrato_proy where id_proyecto='.$id_proyecto.'
			and descrip_compo_proy="'.$descrip_compo_proy.'"
			')->result();
			/*return $this->db->query('
			SELECT * FROM contrato_proy where id_proyecto=360
			and descrip_compo_proy="SUPERVISION" limit 1
			')->result();*/
		}

		public function nombreProy($id_proyecto)
		{
		$this->db->select('*');
		$this->db->from('proyectos');
		$this->db->where('proyectos.id_proyecto',$id_proyecto);
		return $this->db->get()->row();
		}
		public function proytramo($id_proyecto){

		 	$this->db->select('*');
		 	$this->db->from('proyectos');
		 	$this->db->where('id_proyecto',$id_proyecto);
		 	return $this->db->get()->row();
		}

		public function actividad($id_proyecto,$descrip_compo_proy){
			if($id_proyecto>=100000){
				//si es tramo
				$tabla = 'tramo';
				$id = 'tramo.id_tramo';
				$join ='componente_proy.id_proyecto = tramo.id_tramo';
			}else{
				$tabla = 'proyectos';
				$id = 'proyectos.id_proyecto';
				$join ='componente_proy.id_proyecto = proyectos.id_proyecto';
			}
			$this->db->select('*');
			$this->db->from($tabla);
			$this->db->join('componente_proy',$join);
			$this->db->where($id,$id_proyecto);
			$this->db->where('componente_proy.descrip_compo_proy',$descrip_compo_proy);
			//$this->db->order_by("category", "asc");
			return $this->db->get()->row();
		}

		public function inicio_proyect($id_proyecto){
			if($id_proyecto>=100000){
				//si es tramo
				$tabla = 'tramo';
				$id = 'id_tramo';
			}else{
				$tabla = 'proyectos';
				$id = 'id_proyecto';
			}
		 	$this->db->select('fecha_inicio');
			$query=$this->db->get_where($tabla,[$id=>$id_proyecto]);
			return $query->row()->fecha_inicio;
		}

		public function fin_proyect($id_proyecto){
				if($id_proyecto>=100000){
	 				//si es tramo
	 				$tabla = 'tramo';
	 				$id = 'id_tramo';
	 			}else{
	 				$tabla = 'proyectos';
	 				$id = 'id_proyecto';
	 			}
			 	$this->db->select('fecha_conclusion');
				$query=$this->db->get_where($tabla,[$id=>$id_proyecto]);
				return $query->row()->fecha_conclusion;
		}

		public function anio_inicio_proyect($inicio){
		 	$fecha_partida1=explode("-",$inicio );
			$anio1= $fecha_partida1[0];
			$mes1= $fecha_partida1[1];
			$dia1= $fecha_partida1[2];
			//echo $dia."/".$mes."/".$anio;
			$dura=(intval($anio1));
			return ($dura);
		}

		public function anio_fin_proyect($fin){
		 	$fecha_partida1=explode("-",$fin);
				$anio1= $fecha_partida1[0];
				$mes1= $fecha_partida1[1];
				$dia1= $fecha_partida1[2];
				//echo $dia."/".$mes."/".$anio;
			$dura=(intval($anio1));
			return ($dura);
		}

		public function dato($id_proyecto){
			$this->db->select('*');
			$this->db->from('proyectos');
			$this->db->join('tramo','tramo.id_proyecto=proyectos.id_proyecto');
			$this->db->join('componente_tramo','componente_tramo.id_tramo = tramo.id_tramo');
			$this->db->where('proyectos.id_proyecto',$id_proyecto);
			//$this->db->order_by("category", "asc");
			return $this->db->get()->row();
		}

		public function unidad(){
			$this->db->select('*');
			$this->db->from('unidad_metrica');
			return $this->db->get()->result();
		}
		 
		public function insert($data){
			return $this->db->insert("proyecto_gestion",$data);
		}

		public function insert_financiero($data){
			return $this->db->insert("seguimiento_financiero",$data);
		}

		public function insert_mod_cont($data){
			$this->db->insert("modif_contrato",$data);
			return $this->db->insert_id();
		}

		public function insert_fisico($data){
			$this->db->insert("seguimiento_fisico",$data);
			return $this->db->insert_id();
		}

		public function updateSeguiFisico($id,$data){
		 	$this->db->where('id_seguimiento_fisico',$id);
		 	return $this->db->update('seguimiento_fisico',$data);
		}

		public function updatemodificacion($id,$data){
		 	$this->db->where('id',$id);
		 	return $this->db->update('modif_contrato',$data);
		}
		
		public function traer_fisico($id_proyecto){

			$this->db->select('*');
			$this->db->from('seguimiento_fisico');
			$this->db->where('id_proyecto',$id_proyecto);
			return $this->db->get()->result();
			}
		
		public function proy_gestion($id_proyecto){
			$this->db->select('*');
			$this->db->from('proyecto_gestion');
			$this->db->where('id_proyecto',$id_proyecto);
			return $this->db->get()->result();
		}
		public function proy_gestion2($id_proyecto,$descrip_compo_proy){
			$this->db->select('*');
			$this->db->from('seguimiento_financiero');
			$this->db->where('id_proyecto',$id_proyecto);
			$this->db->where('descrip_compo_proy',$descrip_compo_proy);
			return $this->db->get()->result();
//SELECT sum(monto_ejecutado) as suma FROM seguimiento_financiero WHERE gestion=2020 and descrip_compo_proy='consultoria'
     	}
		 
		public function delete($id_progFinan){
		    $this->db->delete('proyecto_gestion', array('id_progFinan' => $id_progFinan));
		}

		public function delete1($id_segui_finan){
			 $this->db->delete('seguimiento_financiero', array('id_segui_finan' => $id_segui_finan));
	 	}
		 
		public function delete_contrato($id_contrato){
			$this->db->delete('contrato_proy', array('id_contrato' => $id_contrato));
		}
	
		public function delete_garantia($id_garantia){
			$this->db->delete('garantia_proy', array('id_garantia' => $id_garantia));
		}

		public function delete_mod($id){
			$this->db->delete('modif_contrato', array('id' => $id));
		}
	 
		public function deleteFisico($id_seguimiento_fisico)
		{
			$this->db->delete('seguimiento_fisico', array('id_seguimiento_fisico' => $id_seguimiento_fisico));
		}
		
		public function pro_ges($id_progFinan){
			$this->db->select('*');
			$this->db->from('proyecto_gestion');
			$this->db->where('id_progFinan',$id_progFinan);
			return $this->db->get()->row();
		}
		 
	 	public function proy($id_proyecto){
			$this->db->select('*');
			$this->db->from('proyectos');
			$this->db->where('id_proyecto',$id_proyecto);
			return $this->db->get()->row();
		}

		//select monto_proyectado from poryecto_gestion where gestion=2020 and id_proyecto=360
		public function traer_monto_proyectado($id_proyecto){
			$this->db->select('*');
			$this->db->from('proyecto_gestion');
			$this->db->where('id_proyecto', $id_proyecto);
			$this->db->where('gestion', date('Y'));
			return $this->db->get()->result();
		}

		public function traer_gestion_financiera($id_proyecto){
			$this->db->select('sum(monto_ejecutado)as suma');
			$this->db->from('seguimiento_financiero');
			$this->db->where('id_proyecto', $id_proyecto);
			$this->db->where('gestion', date('Y'));
			return $this->db->get()->result();
			//select sum(monto_ejecutado) from seguimiento_financiero where gestion=2020 and id_proyecto=360
		}

		public function costo_ejecutado($id_proyecto,$descrip_compo_proy){
			$this->db->select('sum(monto_ejecutado)as suma');
			$this->db->from('seguimiento_financiero');
			$this->db->where('id_proyecto', $id_proyecto);
			$this->db->where('descrip_compo_proy', $descrip_compo_proy);
			$this->db->group_by('monto_ejecutado');
			return $this->db->get()->result();
		}

		public function reporte_costo($id_proyecto, $mes, $tipo_compo_proy){
			return $this->db->query(
			'select c.id_contrato, c.id_proyecto, c.costo_adj as inicial, c.descrip_compo_proy, c.fecha_con,
			IF( (select m.fecha_fin from modif_contrato m join contrato_proy c USING(id_contrato)
			where c.id_proyecto='.$id_proyecto.' and c.descrip_compo_proy="'.$tipo_compo_proy.'") >0,
			m.fecha_fin, c.fecha_con ) as fechafin,

			if( (select m.fecha_fin from modif_contrato m join contrato_proy c USING(id_contrato)
			where c.id_proyecto='.$id_proyecto.' and c.descrip_compo_proy="'.$tipo_compo_proy.'") >0,
			(SELECT DATEDIFF(m.fecha_fin, c.fecha_con) from modif_contrato m join contrato_proy c USING(id_contrato)
			where c.id_proyecto='.$id_proyecto.' and c.descrip_compo_proy="'.$tipo_compo_proy.'"),
			0) as dias,
			IF((select sum(m.monto_incremento) from modif_contrato m join contrato_proy c using(id_contrato)
			where c.id_proyecto ='.$id_proyecto.' and c.descrip_compo_proy = "'.$tipo_compo_proy.'")  >0,
			(select sum(m.monto_incremento) from modif_contrato m join contrato_proy c using(id_contrato)
			where c.id_proyecto = '.$id_proyecto.' and c.descrip_compo_proy = "'.$tipo_compo_proy.'")  ,
				0) as incremento_costo,
			if ( (select sum(m.monto_incremento) from modif_contrato m join contrato_proy c using(id_contrato)
			where c.id_proyecto = '.$id_proyecto.' and c.descrip_compo_proy = "'.$tipo_compo_proy.'") > 0,
				c.costo_adj +  (select sum(m.monto_incremento) from modif_contrato m join contrato_proy c using(id_contrato)
			where c.id_proyecto = '.$id_proyecto.' and c.descrip_compo_proy = "'.$tipo_compo_proy.'")  ,
				c.costo_adj ) as costo_total_actualizado,
			if ( (select sum(monto_ejecutado) from seguimiento_financiero s
			where id_proyecto=c.id_proyecto and s.descrip_compo_proy = "'.$tipo_compo_proy.'" and s.mes= "'.$mes.'") > 0  , (select sum(monto_ejecutado) from seguimiento_financiero s
			where id_proyecto=c.id_proyecto and s.descrip_compo_proy = "'.$tipo_compo_proy.'" and s.mes= "'.$mes.'") , 0 ) as costo_mes ,

			if((select sum(monto_ejecutado) from seguimiento_financiero s
			where id_proyecto=c.id_proyecto and s.descrip_compo_proy = "'.$tipo_compo_proy.'")>0, (select sum(monto_ejecutado) from seguimiento_financiero s
			where id_proyecto=c.id_proyecto and s.descrip_compo_proy = "'.$tipo_compo_proy.'"), 0)
			as costo_ejecutado,

			IF((select sum(m.monto_incremento) from modif_contrato m
			join contrato_proy c using(id_contrato)
			where c.id_proyecto = '.$id_proyecto.'
			and c.descrip_compo_proy = "'.$tipo_compo_proy.'")>0,
			(c.costo_adj +  (select sum(m.monto_incremento)
			from modif_contrato m join contrato_proy c using(id_contrato)
			where c.id_proyecto = '.$id_proyecto.' and
			c.descrip_compo_proy = "'.$tipo_compo_proy.'"))-
			(select sum(monto_ejecutado) from seguimiento_financiero s
			where id_proyecto=c.id_proyecto and s.descrip_compo_proy = "'.$tipo_compo_proy.'"),
			c.costo_adj - (select sum(monto_ejecutado) from seguimiento_financiero s
			where id_proyecto=c.id_proyecto and s.descrip_compo_proy = "'.$tipo_compo_proy.'"))as costo_por_ejecutar

			from contrato_proy c join seguimiento_financiero s using(id_proyecto) join modif_contrato m
			where id_proyecto = '.$id_proyecto.' and mes = "'.$mes.'" and c.descrip_compo_proy ="'.$tipo_compo_proy.'"
			group by id_contrato, costo_adj;
						'
			)->result();
		}

		public function empresa_ejecutora($id_proyecto){
			return $this->db->query(
			'SELECT c.descrip_compo_proy,c.nombre_empresa,c.costo_adj,c.plazo_contra,c.fecha_ini,c.fecha_con ,
			if(g.tipo is null,"NO ",g.tipo)as tipo,
			if(g.entidad_financiera is null,"CORRESPONDE",g.entidad_financiera)as entidad_financiera
			FROM contrato_proy c left join garantia_proy g using(id_proyecto,id_contrato)
			where c.id_proyecto='.$id_proyecto.''
			)->result();
		}

		public function traer_componente($id_proyecto){
			return $this->db->query(
			'select distinct(descrip_compo_proy)
			from contrato_proy
			where id_proyecto='.$id_proyecto
			)->result();
		}

		public function monto_historico_financiero($id_proyecto){
			$this->db->select('sum(monto_ejecutado)as suma');
			$this->db->from('seguimiento_financiero');
			$this->db->where('id_proyecto', $id_proyecto);
			//$this->db->group_by('monto_ejecutado');
			return $this->db->get()->result();
		}

		public function reporte_fisico($id_proyecto, $mes){
			return $this->db->query('
			select id_proyecto, mes, gestion, porcentaje, resultado_mes, problemas_mes, acciones_solucion,
			(select sum(porcentaje) from seguimiento_fisico where id_proyecto='.$id_proyecto.'
			and gestion='.date('Y').') as suma_gestion, (select sum(porcentaje)
			from seguimiento_fisico as s where id_proyecto='.$id_proyecto.') as suma_historico
			from seguimiento_fisico where id_proyecto='.$id_proyecto.' and gestion='.date('Y').' and mes= "'.$mes.'";'
			)->result();
		}
	
		public function insert_contrato($data){
	 		return $this->db->insert("contrato_proy",$data);
		}
	
		public function insert_garantia($data){
	 		return $this->db->insert("garantia_proy",$data);
		}
	
		public function garantiaC($id_contrato,$descrip_compo_proy){
			$this->db->select('*');
			$this->db->from('garantia_proy');
			$this->db->where('id_contrato',$id_contrato);
			$this->db->where('descrip_compo_proy',$descrip_compo_proy);		
			return $this->db->get()->result();
			//SELECT sum(monto_ejecutado) as suma FROM seguimiento_financiero WHERE gestion=2020 and descrip_compo_proy='consultoria'
		}
	
		public function nombre_empresa($id_proyecto){
			$this->db->select('*');
			$this->db->from('contrato_proy');
			$this->db->where('id_proyecto',$id_proyecto);
			$this->db->where('descrip_compo_proy','CONSULTORIA');
			return $this->db->get()->result();
		}

		public function nombre_empresaS($id_proyecto){
			$this->db->select('*');
			$this->db->from('contrato_proy');
			$this->db->where('id_proyecto',$id_proyecto);
			$this->db->where('descrip_compo_proy','SUPERVISION');
			return $this->db->get()->result();
		}
	
		public function tipo_garantia($id_proyecto){
			$this->db->select('*');
			$this->db->from('garantia_proy');
			$this->db->where('id_proyecto',$id_proyecto);
			$this->db->where('descrip_compo_proy','CONSULTORIA');
			return $this->db->get()->result();
		}
	
		public function tipo_garantiaS($id_proyecto){
			$this->db->select('*');
			$this->db->from('garantia_proy');
			$this->db->where('id_proyecto',$id_proyecto);
			$this->db->where('descrip_compo_proy','SUPERVISION');
			return $this->db->get()->result();
		}
	
		public function componente($id_proyecto){
			$this->db->select('*');
			$this->db->from('componente_proy');
			$this->db->where('id_proyecto',$id_proyecto);
			return $this->db->get()->result();
		}
	
		public function modificacion($id_contrato,$descrip_compo_proy){
			$this->db->select('*');
			$this->db->from('modif_contrato');
			$this->db->where('id_contrato',$id_contrato);
			$this->db->where('descrip_compo_proy',$descrip_compo_proy);
			return $this->db->get()->result();
		}

		public function segui_fisico($id_proyecto,$mes,$gestion){
			return $this->db->query('
				select s.porcentaje as mes
				,
				(SELECT sum(s.porcentaje) as gestion from seguimiento_fisico s where id_proyecto='.$id_proyecto.' and gestion='.$gestion.') as gestion,


				(select sum(s.porcentaje)from seguimiento_fisico s where id_proyecto='.$id_proyecto.') as historico
				from seguimiento_fisico s where id_proyecto='.$id_proyecto.' and mes="'.$mes.'" and gestion='.$gestion.'
				group by id_seguimiento_fisico asc'
			)->result();
		}

		public function falta_porcentaje($id_proyecto){
			return $this->db->query('

			select sum(s.porcentaje) as historico,

			(select 100-sum(s.porcentaje) from seguimiento_fisico s where id_proyecto='.$id_proyecto.') as falta
			from seguimiento_fisico s where id_proyecto='.$id_proyecto.'
			')->result();
		}

		public function sisinfecha($id_proyecto){
			$this->db->where("id_proyecto",$id_proyecto);
			$resultado = $this->db->get("sisin");
			return $resultado->row();
		}

		public function verificarSeguimientoFinanciero($id_proyecto,$descrip_compo_proy,$mes,$gestion){
			$this->db->where("id_proyecto", $id_proyecto);
			$this->db->where("descrip_compo_proy", $descrip_compo_proy);
			$this->db->where("mes", $mes);
			$this->db->where("gestion", $gestion);
			$resultado = $this->db->get("seguimiento_financiero")->result();			
			return count($resultado);
		}
}
