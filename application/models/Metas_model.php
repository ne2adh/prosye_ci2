<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Metas_model extends CI_Model
{

		public function __construct()
			{
			parent::__construct();
			}

	public function index(){
			$this->db->select('*');
			$result = $this->db->get('metas_proy');
			return $result->result_array();
			}

	public function metas($id_proyecto)
		 {
		 	$this->db->select('*');
		 	$this->db->from('metas');
		 	$this->db->where('id_proyecto',$id_proyecto);
		 	return $this->db->get()->result();
		 }

		  public function meta($id)
		 {
		 	$this->db->select('*');
		 	$this->db->from('metas');
		 	$this->db->where('id',$id);
		 	return $this->db->get()->row();
		 }

		  public function unidad_proy()
		 {
			$this->db->select('*');
			$this->db->order_by('unidad_metrica','ASC');
		 	$this->db->from('unidad_metrica');
		 	return $this->db->get()->result();
		 }
		 public function metas_proy()
		 {
		 	$this->db->select('*');
		 	$this->db->from('metas_proy');
		 	return $this->db->get()->result();
		 }
		 public function metas_proy1($num)
		 {
		 	$this->db->select('descripcion');
			$this->db->from('metas_proy');
			$this->db->where('id_metas_proy',$num);
			$query=$this->db->get();
			$result=$query->row();
			 if (isset($result))
			 {
				 return $result->descripcion;
			 }

		 }

		  public function update($id,$data)
		 {

			$this->db->where("id",$id);
			return $this->db->update("metas",$data);

		 }


		 public function insert($data)
		 {
			return $this->db->insert("metas",$data);
		 }

		public function insertDescripcion($data){
		return $this->db->insert("metas_proy",$data);
		}

		 public function delete($id)
		{
		    $this->db->delete('metas', array('id' => $id));
		}

		public function proy($id_proyecto){
			if($id_proyecto>=100000){
				//si es tramo
				$tabla = 'tramo';
				$id = 'tramo.id_tramo';
				//$join ='componente_proy.id_proyecto = tramo.id_tramo';
			}else{
				$tabla = 'proyectos';
				$id = 'proyectos.id_proyecto';
				//$join ='componente_proy.id_proyecto = proyectos.id_proyecto';
			}
		 	$this->db->select('*');
		 	$this->db->from($tabla);
		 	$this->db->where($id,$id_proyecto);
		 	return $this->db->get()->row();
		 }
		public function compara($descripcion)
		 {
			$this->db->select('*');
		 	$this->db->from('metas_proy');
			$this->db->where("descripcion",$descripcion);
			$query=$this->db->get();
			$result=$query->row();
			 if (isset($result))
			 {
				 return $resp=1;
			 }
			//if (($this->db->get()->result())>1){return (1);}else{ return (0);}

		 }
		 public function area($id_proyecto)
		 {
		 	$this->db->select('*');
		 	$this->db->from('area');
		 	$this->db->join('proyectos', 'proyectos.id_secretaria=area.cod');
		 	$this->db->where('id_proyecto', $id_proyecto);
		 	return $this->db->get()->row();

		 }
		 public function subarea($id_proyecto)
		 {
		 	$this->db->select('*');
		 	$this->db->from('sub_area');
		 	$this->db->join('proyectos', 'proyectos.id_area=sub_area.cod');
		 	$this->db->where('id_proyecto', $id_proyecto);
		 	return $this->db->get()->row();

		 }

		public function edit($id_metas_proy,$data){
			$this->db->where("id_metas_proy",$id_metas_proy);
			return $this->db->update("metas_proy",$data);
			}

}//fin class
