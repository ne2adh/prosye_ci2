<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permisos_model extends CI_Model {

	public function getPermisos()
	{
		$this->db->select("*");
		$this->db->from("permisos");
		$this->db->join("rol","rol_id=permisos.rol_id");
		//$this->db->join("rol.id=permisos.rol_id");
		$resultados=$this->db->get();
		return $resultados->result();
	}

	public function getMenus()
	{
		$resultados=$this->db->get("menu");
		return $resultados->result();
	}
	public function save($data)
	{
		
		return $this->db->insert("permisos",$data);
		
	}

	
}

