<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rol_menu_model extends CI_Model{

	public function __construct(){
			parent::__construct();
	}  

    public function deleteMenu($id_menu){
	    $this->db->delete('rol_menu', array('id_menu' => $id_menu));
    }
    
}//fin class
