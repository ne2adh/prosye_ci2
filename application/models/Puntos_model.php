<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Puntos_model extends CI_Model
{

		public function __construct()
			{
			parent::__construct();
			}


		 public function puntos($id_proyecto)
		 {
		 	$this->db->select('*');
		 	$this->db->from('coordenada_inicial');
		 	$this->db->where('id_proyecto',$id_proyecto);
		 	return $this->db->get()->result();
		 }
		 public function puntosProy($id_proyecto,$id_tramo,$id_tipo)
		 {
		 	$this->db->select('*');
		 	$this->db->from('coordenadas_proy');
			$this->db->where('id_proyecto',$id_proyecto);
			$this->db->where('id_tramo',$id_tramo);
			$this->db->where('id_tipo',$id_tipo);
		 	return $this->db->get()->result();
		 }
		  public function getTramo($id_tipo)
		 {
		 	$this->db->select('*');
		 	$this->db->from('tipo');
		 	$this->db->join('proyectos','proyectos.id_proyecto=tipo.id_proyecto');
		 	$this->db->join('tramo','tramo.id_tipo=tipo.id_tipo');
		 	$this->db->where('tramo.id_tipo',$id_tipo);
		 	return $this->db->get()->result();
		 }
		 public function getTramo1($id_proyecto)
		 {
		 	$this->db->select('*');
		 	$this->db->from('tipo');
		 	$this->db->join('proyectos','proyectos.id_proyecto=tipo.id_proyecto');
		 	$this->db->join('tramo','tramo.id_tipo=tipo.id_tipo');
		 	$this->db->where('tramo.id_proyecto',$id_proyecto);
		 	return $this->db->get()->result();
		 }

		  public function nombTramo($id_tramo)
		 {
		 	$this->db->select('*');
		 	$this->db->from('tramo');
		 	//$this->db->join('proyectos','proyectos.id_proyecto=tipo.id_proyecto');
		 	//$this->db->join('tramo','tramo.id_tipo=tipo.id_tipo');
		 	$this->db->where('id_tramo',$id_tramo);
		 	return $this->db->get()->row();
		 }
		 public function proyecto($id_proyecto)
		 {
		 	$this->db->select('*');
		 	$this->db->from('proyectos');
		 	$this->db->where('id_proyecto',$id_proyecto);
		 	return $this->db->get()->row();
		 }


		public function insertPuntos($data)
		 {
       //validar aqui
			return $this->db->insert("coordenada_inicial",$data);
		 }

		 public function insertPuntosTramo($data)
		 {
			return $this->db->insert("coordenadas_proy",$data);
		 }

		 public function delete($id,$id_proyecto)
		 {
			$this->db->where('id_proyecto',$id_proyecto);
			$this->db->delete('coordenada_inicial', array('id_coordenada' => $id));
		 }

		  public function traerPunto($id_coordenada,$id_proyecto)
		 {
		 	$this->db->select('*');
		 	$this->db->from('coordenada_inicial');
			$this->db->where('id_proyecto',$id_proyecto);
			$this->db->where('id_coordenada',$id_coordenada);
			return $this->db->get()->row();
		 }

		 public function updatePuntos($id_coordenada,$data)
		 {
		 	$this->db->where('coordenada_inicial.id_coordenada',$id_coordenada);
		 	return $this->db->update('coordenada_inicial',$data);
		 }

		 public function compara($id_proyecto, $latitud, $longitud)
		 {
			$this->db->select('*');
		 	$this->db->from('coordenada_inicial');
			//$this->db->where("id_coordenada",$id_coordenada);
			$this->db->where("id_proyecto",$id_proyecto);
			$this->db->where("latitud",$latitud);
			$this->db->where("longitud",$longitud);
			$query=$this->db->get();
			$result=$query->row();
			 if (isset($result))
			 {
				 return true;
			 }
		}

}//fin class
