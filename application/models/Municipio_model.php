<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Municipio_model extends CI_Model
{

		public function __construct()
			{
			parent::__construct();
			}

		public function provincia()
		 {
			$this->db->order_by('nombre_provincia','ASC');
			$query=$this->db->get('provincias');
			return $query->result();

		 }

		public function municipio()
		 {
			$this->db->order_by('municipio','ASC');
			$query=$this->db->get('municipios');
			return $query->result();

		 }

		public function get_municipio_model($id_provincia)
		 {
			$this->db->where('id_provincia',$id_provincia);
			$this->db->order_by('municipio','ASC');
			$query=$this->db->get('municipios');
			$output='<option value="">Municipio</option>';
			foreach($query->result() as $row)
			{
				$output.='<option value="'.$row->id_municipio.'">'.$row->municipio.'</option>';

			}
			return $output;
		 }
		 public function proyecto($id_proyecto)
		 {
		 	$this->db->select('*');
		 	$this->db->from('proyectos');
		 	$this->db->where('id_proyecto',$id_proyecto);
		 	return $this->db->get()->row();
		 }
		 public function det_municipio($id_proyecto)
		 {
		 	$this->db->select('*');
		 	$this->db->from('detalle_municipio');
		 	$this->db->where('id_proyecto',$id_proyecto);
			 $this->db->join('municipios','municipios.id_municipio=detalle_municipio.id_municipio');
			 $this->db->join('provincias','provincias.id_provincia=municipios.id_provincia');
		 	return $this->db->get()->result();
		 }

		 public function getProyecto($id)
		 {
			$this->db->select('*');
			$this->db->from('proyectos');
			$this->db->join('asignacion','asignacion.id_proyecto=proyectos.id_proyecto');
			$this->db->where('id_usuario',$id);
			$this->db->where('proyectos.estado',1);
			$this->db->where('proyectos.alta',0);
			return $this->db->get()->result();
		 }

		 public function insert($data)
		 {

			return $this->db->insert("detalle_municipio",$data);
		 }
		  public function por_provincia()
		 {

			$query=$this->db->get('provincias');
			return $query->result();

		 }

		 public function por_municipio(){
			$query=$this->db->get('municipios');
			return $query->result();
		 }

		 public function por_provincia_proy(){
			$this->db->select('*');
			$this->db->from('detalle_municipio');
			//$this->db->where('id_provincia',$provincia);
			$this->db->join('municipios','municipios.id_municipio=detalle_municipio.id_municipio');
			$this->db->join('provincias','provincias.id_provincia=municipios.id_provincia');
			$this->db->join('proyectos','proyectos.id_proyecto = detalle_municipio.id_proyecto');
			$this->db->order_by('nombre_provincia','ASC');
			return $this->db->get()->result();
			//$query=$this->db->get('provincias');
			//return $query->result();
		 }

		 public function por_municipio_proy(){
			$this->db->select('*');
			$this->db->from('detalle_municipio');
			//$this->db->where('id_provincia',$provincia);
			$this->db->join('municipios','municipios.id_municipio=detalle_municipio.id_municipio');
			$this->db->join('provincias','provincias.id_provincia=municipios.id_provincia');
			$this->db->join('proyectos','proyectos.id_proyecto = detalle_municipio.id_proyecto');
			$this->db->order_by('municipio','ASC');
			return $this->db->get()->result();
			//$query=$this->db->get('provincias');
			//return $query->result();
		 }


		public function selec_provincia($id_provincia){

			$this->db->select('*');
			$this->db->from('detalle_municipio');
			//$this->db->order_by('nombre_provincia','ASC');
			$this->db->where('municipios.id_provincia',$id_provincia);
			$this->db->join('proyectos ','proyectos.id_proyecto = detalle_municipio.id_proyecto');
			$this->db->join('municipios','municipios.id_municipio=detalle_municipio.id_municipio');
			$this->db->join('provincias','provincias.id_provincia=municipios.id_provincia');
			return $this->db->get()->result();

		 }

		public function selec_municipio($id_municipio){

			$this->db->select('*');
			$this->db->from('detalle_municipio');
			//$this->db->order_by('nombre_provincia','ASC');
			$this->db->where('municipios.id_municipio',$id_municipio);
			$this->db->join('proyectos ','proyectos.id_proyecto = detalle_municipio.id_proyecto');
			$this->db->join('municipios','municipios.id_municipio=detalle_municipio.id_municipio');
			//$this->db->join('provincias','provincias.id_provincia=municipios.id_provincia');
			return $this->db->get()->result();

		 }

		 public function delete($id,$id_proyecto){
			$this->db->where('id_proyecto',$id_proyecto);
			$this->db->delete('detalle_municipio', array('id_municipio' => $id));
		 }

		 public function actualizar($id_municipio,$id_proyecto){
				$this->db->select('*');
				$this->db->from('detalle_municipio');
				$this->db->where('municipios.id_municipio',$id_municipio);
				//$this->db->where('id_municipio',$id_municipio);
				$this->db->join('municipios','municipios.id_municipio=detalle_municipio.id_municipio');
				$this->db->join('provincias','provincias.id_provincia=municipios.id_provincia');
				return $this->db->get()->result();
		 }
		  
		 public function traer($id){
			$this->db->select('*');
			$this->db->from('detalle_municipio');
			$this->db->where('id',$id);
			$this->db->join('municipios','municipios.id_municipio = detalle_municipio.id_municipio');
			$this->db->join('provincias','provincias.id_provincia=municipios.id_provincia');
			return $this->db->get()->row();
		 }

		 public function nombres($num){
			$this->db->select("municipio");
			$this->db->from("municipios");
			$this->db->where("id_municipio",$num);
			$query=$this->db->get();
			$result=$query->row();
			 if (isset($result))
			 {
				 return $result->municipio;
			 }
		 }
		  
		 public function compara($id_municipio,$id_proyecto){
			$this->db->select('*');
		 	$this->db->from('detalle_municipio');
			$this->db->where("id_municipio",$id_municipio);
			$this->db->where("id_proyecto",$id_proyecto);
			$query=$this->db->get();
			$result=$query->row();
			 if (isset($result))
			 {
				 return $resp=1;
			 }
			//if (($this->db->get()->result())>1){return (1);}else{ return (0);}

		 }

		public function getProvin($num){
		 	$this->db->select('nombre_provincia');
			$this->db->from('provincias');
			$this->db->where('id_municipio',$num);
			$this->db->join('municipios','municipios.id_provincia=provincias.id_provincia');

			$query=$this->db->get();
			$result=$query->row();
			 if (isset($result))
			 {
				 return $result->nombre_provincia;
			 }
		 }
		
		 public function getMuni($num){
		 	$this->db->select('municipio');
			$this->db->from('municipios');
			$this->db->where('id_municipio',$num);
			$query=$this->db->get();
			$result=$query->row();
			 if (isset($result))
			 {
				 return $result->municipio;
			 }
		 }
		 
		 public function update($id,$data){
		 	$this->db->where('detalle_municipio.id',$id);
		 	return $this->db->update('detalle_municipio',$data);
		 }
		 
		 public function muni($id_proyecto){
		 	$this->db->select('*');
		 	$this->db->from('municipios');
		 	$this->db->where('id_proyecto', $id_proyecto);
		 	$this->db->join('detalle_municipio','detalle_municipio.id_municipio=municipios.id_municipio');

		 	return $this->db->get()->result();

		 }

		public function prov($id_proyecto){
		 	$this->db->select('*');
		 	$this->db->from('provincias');
		 	$this->db->where('id_proyecto', $id_proyecto);
		 	$this->db->join('municipios','municipios.id_provincia=provincias.id_provincia');
		 	$this->db->join('detalle_municipio','detalle_municipio.id_municipio=municipios.id_municipio');
		 	return $this->db->get()->result();
		}
		
    	public function total($id_proyecto){
		 	$this->db->select('sum(monto_comp_proy) as suma');
		 	$this->db->from('componente_proy');
		 	$this->db->where('id_proyecto', $id_proyecto);
		 	return $this->db->get()->row();
		 }

     	public function total_mes($id_proyecto,$mes){
			$this->db->select('sum(monto_ejecutado) as suma');
			$this->db->from('seguimiento_financiero');
			$this->db->where('id_proyecto', $id_proyecto);
			$this->db->where('mes',$mes);
			//$this->db->join('proyectos', 'proyectos.id_proyecto=detalle_municipio.id_proyecto');
			//$this->db->join('proyectos','municipios.id_provincia=provincias.id_provincia');
			//$this->db->join('detalle_municipio','detalle_municipio.id_municipio=municipios.id_municipio');
			return $this->db->get()->row();
		}

}//fin class
