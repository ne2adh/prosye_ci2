<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seccion_model extends CI_Model{
		public function __construct(){
			parent::__construct();
		}

        public function index(){
			$this->db->select('*'); 
			$this->db->from('seccion');			
			$this->db->order_by("prioridad", "asc");
			$resultados=$this->db->get();
			return $resultados->result();
		}

		public function save($data){
			return $this->db->insert("seccion",$data);
		}

		public function delete($idSeccion){
			$this->db->where('id', $idSeccion);
			return $this->db->delete('seccion');
		}

}//fin class
