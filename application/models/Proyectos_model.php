<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proyectos_model extends CI_Model
{

  		public $id_proyecto;
		public function __construct()
		{
			parent::__construct();

		}

		public function getProyectos(){
			$this->db->select("*");
			$this->db->from("proyectos");		   
			$result = $this->db->get()->result();
			return $result;
		}


		public function get_proyectos_espe($id)
		{
		$this->db->select('*');
		$this->db->from('proyectos');
		$this->db->join('asignacion','asignacion.id_proyecto=proyectos.id_proyecto');
		$this->db->where('id_usuario',$id);
		return $this->db->get()->result();

		 }
		public function get_proyectos_mios($id_usuario){
			$this->db->select('*');
			$this->db->from('proyectos');
			$this->db->join('asignacion', 'asignacion.id_proyecto=proyectos.id_proyecto');
			$this->db->join('pantalla', 'pantalla.id_proyecto=proyectos.id_proyecto');
			$this->db->where('proyectos.estado', 1);
			//$this->db->where('proyectos.alta', 0);
			$this->db->where('id_usuario',$id_usuario); //para listar solo los proyectos de un determinado usuario
			$result = $this->db->get()->result();
			//$sql = $this->db->last_query();
			//var_dump($sql);
			return $result;
		}

		public function get_proyectos_mios_convenio($id_usuario){
			$this->db->select('*');
			$this->db->from('proyectos');
			$this->db->join('asignacion','asignacion.id_proyecto=proyectos.id_proyecto');
			$this->db->where('proyectos.estado',1);
			$this->db->where('proyectos.convenio', 'SI');
			$this->db->where('proyectos.alta',0);
			$this->db->where('id_usuario',$id_usuario);
			return $this->db->get()->result();
		}
		
		public function get_proyectos_espe_convenio(){
			$this->db->select('*');
			$this->db->from('asignacion');
			$this->db->join('usuario','usuario.id_usuario=asignacion.id_usuario');
			$this->db->join('proyectos','proyectos.id_proyecto = asignacion.id_proyecto');
			$this->db->join('personal','personal.ci=usuario.ci_persona');
			$this->db->join('convenio','convenio.id_proyecto=proyectos.id_proyecto');
			return $this->db->get()->row();
		}

		public function get_area(){
			$this->db->order_by('nombre','ASC');
			$query=$this->db->get('areamostrar');
			return $query->result();
		}

		public function get_usuario(){
			$this->db->order_by('nombre','ASC');
			$query=$this->db->get('areamostrar');
			return $query->result();
		 }

		public function get_subarea_model($cod){
			$this->db->where('rel',$cod);
			$this->db->order_by('nombre','ASC');
			$subarea=$this->db->get('sub_areamostrar');
			if($subarea->num_rows() > 0){
				return $subarea->result();
			}
		 }

		public function get_subarea_model3($cod){
			$this->db->where('rel',$cod);
			$this->db->order_by('nombre','ASC');
			$query=$this->db->get('sub_area3nivel');
			$output='<option value="">Sub-Area</option>';
			foreach($query->result() as $row){
				$output.='<option value="'.$row->cod.'">'.$row->nombre.'</option>';
			}
			return $output;
		 }
		
		public function get_ruta($id){
			$this->db->select('pant');
			$query=$this->db->get_where('pantalla',['id_proyecto'=>$id]);
			return $query->row()->pant;
		 }

		public function get_Sisin($id_proyecto){
			$this->db->select('estado');
			$query=$this->db->get_where('proyectos',['id_proyecto'=>$id_proyecto]);
			return $query->row()->estado;
		 }
		 
		public function insert($data){
		 	return $this->db->insert("proyectos",$data);
			$insert_id = $this->db->insert_id();
			return  $insert_id;
		}
		
		public function insert_asig($data2){
		 	return $this->db->insert("asignacion",$data2);
		 }
		
		 public function insert_pant($data3){
		 	return $this->db->insert("pantalla",$data3);
		 }

		public function update_pant($data1,$id_proyecto){
		 	$this->db->where('pantalla.id_proyecto',$id_proyecto);
		 	return $this->db->update('pantalla',$data1);
		 }
		 
		 public function update_alta($id_proyecto){
		 	$this->db->select('*');
			$this->db->from('proyectos');
			$this->db->where('proyectos.id_proyecto',$id_proyecto);
			return $this->db->get()->row();
		 }
		 
		 public function update_alta1($id_proyecto,$data1){
		 	$this->db->where('proyectos.id_proyecto',$id_proyecto);
			return $this->db->update('proyectos',$data1);

			return $this->db->get()->result();
		}

		public function updateAlta($id_proyecto, $alta){
			$this->db->set('alta', $alta);
			$this->db->where('id_proyecto', $id_proyecto);
			return $this->db->update('proyectos');
		}

		public function miosSisin($id){

			// 	$this->db->where('proyectos.id_proyecto',$id_proyecto);
			//	return $this->db->update('proyectos',$data1);
			$this->db->select('*');
			$this->db->from('proyectos');
			$this->db->join('asignacion','asignacion.id_proyecto=proyectos.id_proyecto');
			//$this->db->join('sisin','sisin.id_proyecto=proyectos.id_proyecto');
			$this->db->where('proyectos.estado','1');
			$this->db->where('proyectos.alta','1');
			$this->db->where('id_usuario',$id);
			$this->db->where("sisin","0");
			return $this->db->get()->result();
		 }

		 public function miosConSisin($id){
			$this->db->select('*');
			$this->db->from('proyectos');
			$this->db->join('asignacion','asignacion.id_proyecto=proyectos.id_proyecto');
			$this->db->join('sisin','sisin.id_proyecto=proyectos.id_proyecto');
			$this->db->where('proyectos.estado','1');
			$this->db->where('proyectos.alta','1');
			$this->db->where('id_usuario',$id);
			$this->db->where("sisin","1");
			return $this->db->get()->result();
		 }

		public function ingresoSisin(){
			$this->db->from('proyectos');
			//$this->db->join('sisin','sisin.id_proyecto=proyectos.id_proyecto');
			$this->db->where('proyectos.estado','1');
			$this->db->where('proyectos.alta','1');
			$this->db->where('sisin','0');
			//$this->db->where(!'sisin.codigo_sisin','');
			//$this->db->join('sisin','sisin.id_proyecto=proyectos.id_proyecto');

			return $this->db->get()->result();
		 }

		 function current_num(){
			$query = $this->db->query('SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA=DATABASE() AND   TABLE_NAME="proyectos" ');
			$resultado= $query->row_array();
			return $resultado['AUTO_INCREMENT'];
		 }


		 public function update($id_proyecto,$data){
			$this->db->where("id_proyecto",$id_proyecto);
			return $this->db->update("proyectos",$data);
		 }
		 public function insert_metas($data){

		 	return $this->db->insert("metas",$data);
		 }

		 public function getMetas($id){
			$this->db->select('*');
			$this->db->from('metas');
			$this->db->where("id_proyecto",$id);
			$metas = $this->db->get();
		 	return $metas;
		 }


		public function insert_tipo($data){

		 	return $this->db->insert("tipo",$data);
		 }

		 public function getConvenio($id){
		 	$this->db->select("*");
			$this->db->from("proyectos");
			$this->db->where("convenio",$id);
			return $this->db->get()->result();
			//$results=$this->db->get();
			//return $results;
		 }

		 public function getSinconvenio($id){
		 	$this->db->select("*");
			$this->db->from("proyectos");
			$this->db->where("proyectos.convenio",$id);
			return $this->db->get()->result();

		 }

		 public function porFecha($fecha_inicio,$fecha_conclusion){
		 	$this->db->select("*");
			$this->db->from("proyectos");
			$this->db->where("proyectos.fecha_inicio >=",$fecha_inicio);
			$this->db->where("proyectos.fecha_conclusion <=",$fecha_conclusion);
			return $this->db->get()->result();
		 }

		public function getProyecto($id_proyecto){
		 	$this->db->select("*");
			$this->db->from("proyectos");
			$this->db->where("id_proyecto",$id_proyecto);
			return $this->db->get()->row();
		}

		public function getProyectos_area($id_area){
			$this->db->select("*");
		   $this->db->from("proyectos");
		   $this->db->where("id_secretaria", $id_area);
		   $result = $this->db->get()->result();
		   return $result;
	    }


		public function getDetalleProyecto($id_proyecto){
			$this->db->select('proyectos.`id_proyecto`, proyectos.`convenio`, proyectos.`fase`,
							   proyectos.`modalidad`, proyectos.`fecha_inicio`, proyectos.`fecha_conclusion`,
							   proyectos.`id_secretaria`, proyectos.`id_area`, proyectos.`id_subarea`,
							   proyectos.`nombre_proyecto`, proyectos.`costo_proyecto`,
							   proyectos.`descripcion_proyecto`, proyectos.`objetivo_proyecto`,
							   proyectos.`id_clasificacion_sectorial`, proyectos.`codigo_tramo`,
							   proyectos.`monto_modalidad`, proyectos.`id_provincia`, proyectos.`id_municipio`,
							   proyectos.`id_localidad`, proyectos.`costo_proyecto_ajustado`,
							   proyectos.`estado`, proyectos.`tipo_proyecto`, proyectos.`duracion_anos`,
							   proyectos.`alta`, proyectos.`sisin`, area.`cod` as cod_secretaria,
							   area.`sigla` as sigla_secretaria, area.`nombre` as nombre_secretaria,
							   area.`activo` as activo_secretaria, area.`apertura_programatica` as apertura_programatica_secretaria,
							   sub_area.`cod` AS cod_sub_area, sub_area.`sigla` AS sigla_sub_area, sub_area.`nombre` AS nombre_sub_area,
							   sub_area.`partida_presupuestaria` AS partida_presupuestaria_sub_area,
							   sub_area.`administracion` AS administracion_sub_area, sub_area.`activo` AS activo_sub_area, sub_area.`rel` AS rel_sub_area,
							   sub_area3nivel.`cod`, sub_area3nivel.`sigla`, sub_area3nivel.`nombre`, sub_area3nivel.`partida_presupuestaria`,
							   sub_area3nivel.`administracion` AS administracion_sub_area3nivel, sub_area3nivel.`activo` AS activo_sub_area3nivel,
							   sub_area3nivel.`rel` AS rel_sub_area3nivel');
			$this->db->from("proyectos");
			$this->db->join('area','area.cod=proyectos.id_secretaria');
			$this->db->join('sub_area','sub_area.cod=proyectos.id_area');
			$this->db->join('sub_area3nivel','sub_area3nivel.cod=proyectos.id_subarea', 'left');
			$this->db->where("id_proyecto",$id_proyecto);
			//$sql = $this->db->get_compiled_select();
			//echo $sql;
			return $this->db->get()->row();
	    }

		public function tiempo($f1,$f2)
		 {

			if(($f1!='') && ($f2!=''))
			{
				$fecha1='';
				$fecha2='';
				$fecha1= $f1;
				$fecha_partida=explode("-",$fecha1 );
				$anio= $fecha_partida[0];
				$mes= $fecha_partida[1];
				$dia= $fecha_partida[2];
				//echo $di a."/".$mes."/".$anio;
			$fecha2= $f2;
				$fecha_partida1=explode("-",$fecha2 );
				$anio1= $fecha_partida1[0];
				$mes1= $fecha_partida1[1];
				$dia1= $fecha_partida1[2];
				//echo $dia."/".$mes."/".$anio;
			$dura=(intval($anio1)-intval($anio)+1);
			return ($dura);
			}

			else {
				return ('');
			}


		 }
		  public function nombres($num)
		 {
			$this->db->select("nombre");
			$this->db->from("sub_area3nivel");
			$this->db->where("sub_area3nivel.cod",$num);
			$query=$this->db->get();
			$result=$query->row();
			 if (isset($result))
			 {
				 return $result->nombre;
			 }

		 }
		 public function nombres1($num)
		 {
			$this->db->select("nombre");
			$this->db->from("sub_area");
			$this->db->where("sub_area.cod",$num);
			$query=$this->db->get();
			$result=$query->row();
			 if (isset($result))
			 {
				 return $result->nombre;
			 }

		 }
		
		public function calculaTiempo(){
			$fechaInicio=$this->input->POST('fecha_inicio');
			$fechaFin=$this->input->POST('fecha_conclusion');
			$datetime1 = date_create($fechaInicio);
			$datetime2 = date_create($fechaFin);
			$interval = date_diff($datetime1, $datetime2);

			$tiempo=array();

			foreach ($interval as $valor) {
				$tiempo[]=$valor;
			}

			return $tiempo;

		}

		public function proyectosPorTipo(){
			$query = $this->db->query('SELECT DISTINCT(tipo_proyecto) AS tipo_proyecto, SUM(costo_proyecto) as costo_proyecto, MIN(fecha_inicio) as fecha_inicio, max(fecha_conclusion) as fecha_conclusion FROM `proyectos` GROUP BY tipo_proyecto');
			$resultado= $query->result_array();
			return $resultado;
		}

		public function buscarPorTipo($tipo_proyecto){
			$this->db->select("*");
			$this->db->from("proyectos");
			$this->db->where("tipo_proyecto",$tipo_proyecto);
			$query=$this->db->get();
			$result=$query->result_array();	
			return $result;		 
		}

		public function listaTipoProyecto(){
			$query = $this->db->query('SELECT DISTINCT(tipo_proyecto) FROM `proyectos`');
			$resultado= $query->result_array();
			return $resultado;	
		}

		public function listProyecto(){
			$this->db->select("*");
			$this->db->from("proyectos");			
			return $this->db->get()->result();
		}

		public function listAreaProyecto($id_secretaria){
			$this->db->select("*");
			$this->db->from("proyectos");
			$this->db->where("id_secretaria", $id_secretaria);
			$query=$this->db->get();
			$result=$query->result();	
			return $result;		 
		}

		public function listProvinciasProyecto($id_provincia){
			$this->db->select('*');
			$this->db->from('proyectos');
			$this->db->join('detalle_municipio', 'detalle_municipio.id_proyecto = proyectos.id_proyecto');
			$this->db->join('municipios', 'municipios.id_municipio = detalle_municipio.id_municipio');
			$this->db->join('provincias', 'provincias.id_provincia = municipios.id_provincia');
			$this->db->where('provincias.id_provincia', $id_provincia); 
			$query = $this->db->get();
			return $query->result();
		}

		public function listMunicipiosProyecto($id_municipio){
			$this->db->select('*');
			$this->db->from('proyectos');
			$this->db->join('detalle_municipio', 'detalle_municipio.id_proyecto = proyectos.id_proyecto');
			$this->db->join('municipios', 'municipios.id_municipio = detalle_municipio.id_municipio');			
			$this->db->where('municipios.id_municipio', $id_municipio); 
			$query = $this->db->get();
			return $query->result();
		}

		public function editProyecto($id_proyecto, $data){
			$this->db->where("id_proyecto",$id_proyecto);
			$result = $this->db->update("proyectos", $data);			
			return $result;
		}

	}//fin class
