<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AsignacionMenu_model extends CI_Model{
		public function __construct(){
			parent::__construct();
		}

public function traer_menu($id_rol){

	return $this->db->query('
	select r.id, r.id_rol,r.id_menu, m.nombre_menu  from rol_menu r join menu m on m.id=r.id_menu where id_rol='.$id_rol.'
	')->result();
	}

public function menuasignado($id_rol){

	return $this->db->query('
	select * from menu where id not in (select id_menu from rol_menu join rol using(id_rol) where id_rol='.$id_rol.')
	')->result();
}
public function delete($id)
{
	 $this->db->delete('rol_menu', array('id' => $id));
}
public function insertMenu($data){
	return $this->db->insert("rol_menu",$data);
 }
}//fin class
