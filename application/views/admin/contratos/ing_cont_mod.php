<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div id="" class="content-wrapper">
    <!-- Content Header (Page header) -->
<section class="content-header">
<h1>

    Datos del contrato de la Actividad <?php echo $descrip_compo_proy;?> dentro del Proyecto
    <small> <?php echo $proyecto->nombre_proyecto;?></small>

</h1>
</section>
    <!-- Main content -->
<section class="content">
        <!-- Default box -->
<div class="box box-solid">
    <div class="box-body">

        <div class="form-group">
      <div class="row">
        <div class="col-xs-1"></div>
          <div class="col-xs-12">

                      <br>
                      <div class="col-xs-12 text-center">
                  <label>Tiempo de Duracion del Contrato</label>
                </div>
                  <div class="row">
                    <div class="col-xs-6">
                              <label for="fecha_inicio">
                                Fecha de Inicio:
                              </label>
                              <input type="date" id="fecha_inicio" class="form-control" name="fecha_inicio" value="<?php echo $proyecto->fecha_inicio;?>" readonly="">
                    </div>
                            <div class="col-xs-6">
                              <label for="fecha_conclusion">
                                Fecha de Conclusion:
                              </label>
                              <input type="date" id="fecha_fin"  class="form-control" name="fecha_conclusion" value="<?php echo $proyecto->fecha_conclusion;?>" readonly="">
                            </div>
                  </div>
                <br>
                <?php
                $tabla=false;
                 if(isset($_POST['submit']))
                 {
                 $tabla=TRUE;
                 }
                 ?>
                <div class="col-xs-12 text-center">
                  <label><h1>Datos de la Modificacion del contrato</h1></label>
                </div>
            <form <?php if($tabla){echo "hidden";} ?> action="<?php echo base_url();?>proyecto/contratos_controller/store_mod" method="POST" >
                <input type="hidden" name="id_proyecto" value="<?php echo $id_proyecto;?>  " class="form-control" >
                <input type="hidden" name="id_contrato" value="<?php echo $contrato[0]->id_contrato;?>  " class="form-control" >
                <input type="hidden" name="fecha_modif" value="<?php echo date("d/m/Y");?>  " class="form-control" >
                <input type="hidden" name="descrip_compo_proy" value="<?php echo $descrip_compo_proy;?> " class="form-control" >
                <div class="row">

                  <div class="col-xs-12">
                            <label for="motivo">
                              Motivo de Modificion de contrato:
                            </label>
                            <input type="text"  class="form-control"  placeholder="MOTIVO POR EL CUAL SE HACE LA MODIFICACION AL CONTRATO"
                            name="motivo"  style="text-trasnform:uppercase;"  value="<?php echo set_value('motivo');?>"requerid>
                            <?php echo form_error("motivo","<span class='help-block'>","</span>"); ?><br>
                  </div>
                  <div class="col-xs-6">
                            <label for="monto_incremento">
                              Monto Incremento en Bs:
                              </label>
                              <input type="text"  class="form-control"  placeholder="MONTO DE INCREMENTO EN BS"
          										name="monto_incremento"  style="text-trasnform:uppercase;" value="<?php echo set_value('monto_incremento');?>"requerid>
          										<?php echo form_error("monto_incremento","<span class='help-block'>","</span>"); ?><br>
                  </div>
                  <div class="col-xs-6">
                            <label for="plazo_contractual">
                              fecha_inicio (Mod/contrato):
                            </label>
                            <input type="date"  class="form-control"
                            name="fecha_ini"  style="text-trasnform:uppercase;"  value="<?php echo set_value('fecha_ini');?>"requerid>
                            <?php echo form_error("fecha_initra","<span class='help-block'>","</span>"); ?><br>
                  </div>
                  <div class="col-xs-6">
                              <label for="Fecha_con">Fecha Conclucion (Mod/contrato):</label>
                      <input id="" type="date" name="fecha_fin" class="form-control"   value="<?php echo set_value('fecha_fin');?>"requerid>
                      <?php echo form_error("fecha_fin","<span class='help-block'>","</span>"); ?>
                  </div>
                   </div>
                 <br>
                <div  align="center" class="form-group">
                  <button type="submit" name="submit"class="btn btn-primary" class="btn btn-success btn-flat">
                    Guardar
                  </button>

                </div>
                </form>
                <br>
                <div class="col-xs-12 text-center">
            <label><h2>Contrato Realizado</h2></label>
          </div>

                      <form action="<?php echo base_url();?>proyecto/contratos_controller/store" method="POST" >
                <div class="row">
                                  <div class="col-md-12">
                                      <table  class="table table-striped" name="tabla">
                                          <thead >
                                              <tr class="success">
                                                <th>N°</th>
                                                  <th >Codigo Contrato</th>
                                                  <th >Motivo de modificacion</th>
                                                  <th >fecha inicio</th>
                                                  <th >Fecha fecha conclucion</th>
                                                  <th >Monto Incremento</th>
                                                  <th>Opciones</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                            <?php if(!empty($modificacion)):?>
                                             <?php foreach($modificacion as $met ):?>
                                             <tr id="fila">
                                                 <td><?php global $i; $i=$i+1; echo $i;?></td>
                                                 </td>
                                                 <td><?php echo $met->id_contrato;?> </td>
                                                 <td><?php echo $met->motivo;?> </td>
                                                 <td><?php echo $met->fecha_ini;?> </td>
                                                 <td><?php echo $met->fecha_fin;?> </td>
                                                 <td><?php echo $met->monto_incremento;?> </td>                                                 
                                                 <td>
                                                     <!-- Botón para guardar la información actualizada -->
                                                     <span >

                                                         <!-- Botón para mostrar el formulario de actualizar -->
                                                         <input type="hidden" name="descrip_compo_proy" value="<?php echo $descrip_compo_proy;?> " class="form-control" >
                                                         <input type="hidden" name="id_proyecto" value="<?php echo $id_proyecto;?>  " class="form-control" >
                                                         <a href="<?php echo base_url();?>proyecto/Contratos_controller/delete2/<?php echo $met->id;?>/<?php echo $id_proyecto;?>/<?php echo $descrip_compo_proy;?>" class="btn btn-danger btn-remove"title="Eliminar Proyeccion">
                                                             <?php //<button type="button" class="btn btn-danger">Borrar</button>?>
                                                             <?php //<button type="button" class="btn btn-danger">Borrar</button>?>
                                                              <span class="fa fa-remove">  </span>
                                                         </a>
                                                     </span>
                                                 </td>
                                             </tr>
                                                 <?php endforeach;?>
                                                 <?php endif;?>
                                          </tbody>
                                      </table>
                                  </div>
                                          <!-- /.box-body -->
                              </div>
                              </form>
                            <div  align="center" class="form-group" >
                              <a href="<?php echo base_url();?>proyecto/Contratos_controller/ingreso2/<?php echo $id_proyecto;?>" class="btn btn-success btn-flat" >
                                    Continuar
                              </a>
                            </div>
          </div>
        </div>
      </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
