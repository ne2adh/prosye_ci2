<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div id="" class="content-wrapper">
    <!-- Content Header (Page header) -->
<section class="content-header">
<h1>
  Contrato del Proyecto
  <small> <?php echo $proyecto->nombre_proyecto;?></small>
  para la Actividad <?php echo $descrip_compo_proy;?>
</h1>
</section>
    <!-- Main content -->
<section class="content">
        <!-- Default box -->
<div class="box box-solid">
    <div class="box-body">
        <!-- inicio -->
        <div class="form-group">
      <div class="row">
        <div class="col-xs-1"></div>
          <div class="col-xs-12">
                <input type="" name="" value="<?php echo' La Actividad en el proyecto tiene la modalidad de '.$proyecto->modalidad.' con un costo total de '.$proyecto->monto_comp_proy .' Bolivianos';?>  " class="form-control" readonly="">
                      <br>
                      <div class="col-xs-12 text-center">
                  <label>Tiempo de Duracion del Proyecto</label>
                </div>
                  <div class="row">
                    <div class="col-xs-6">
                              <label for="fecha_inicio">
                                Fecha de Inicio:
                              </label>
                              <input type="date" id="fecha_inicio" class="form-control" name="fecha_inicio" value="<?php echo $proyecto->fecha_inicio;?>" readonly="">
                    </div>
                            <div class="col-xs-6">
                              <label for="fecha_conclusion">
                                Fecha de Conclusion:
                              </label>
                              <input type="date" id="fecha_fin"  class="form-control" name="fecha_conclusion" value="<?php echo $proyecto->fecha_conclusion;?>" readonly="">
                            </div>
                  </div>
                <br>
                      <div class="col-xs-12 text-center">
                  <label><h1>Datos del contrato</h1></label>
                </div>
                <form action="<?php echo base_url();?>proyecto/seguir_fisico_controller/store_fisico" method="POST" >
                <input type="hidden" name="id_proyecto" value="<?php echo $id_proyecto;?>  " class="form-control" >
                <input type="hidden" name="descrip_compo_proy" value="<?php echo $descrip_compo_proy;?> " class="form-control" >
                <div class="row">
                  <div class="col-xs-4">

                  </div>

                  <div class="col-xs-4">
                              <label for="monto_actividad">Monto de la Actividad gastado en el mes:</label>
                      <input id="" type="number" step="0.01" name="monto_actividad" class="form-control"  value=""  mim="1.00" max="<?php echo $proyecto->costo_proyecto;?>">
                      <?php echo form_error("monto_actividad","<span class='help-block'>","</span>"); ?>
                  </div>

                 </div>
                 <br>
                <div  align="center" class="form-group">
                  <button type="submit" class="btn btn-primary" class="btn btn-success btn-flat">
                            Guardar
                  </button>
                      </div>
                      </form>
                      <br>
                      <div class="col-xs-12 text-center">
                  <label><h2>Seguimientos Realizados</h2></label>
                </div>


                      <form action="<?php echo base_url();?>proyecto/seguir_fisico_controller/store_fisico" method="POST" >
                <div class="row">
                                  <div class="col-md-12">
                                      <table  class="table table-striped" name="tabla">
                                          <thead >
                                              <tr class="success">
                                                  <th >N°</th>
                                                  <th >Mes</th>
                                                  <th >Año</th>
                                                  <th >Monto</th>
                                                  <th >Opciones</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                            <?php if(!empty($gestion2)):?>
                                             <?php foreach($gestion2 as $met ):?>
                                             <tr id="fila">

                                                 <td><?php global $i; $i=$i+1; echo $i;?></td>
                                                 </td>
                                                 <td><?php echo $met->mes;?> </td>
                                                 <td><?php echo $met->gestion;?> </td>
                                                 <td><?php echo $met->monto_ejecutado;?> </td>
                                                 <td>
                                                     <!-- Botón para guardar la información actualizada -->
                                                     <span >
                                                       <a href="<?php echo base_url();?>proyecto/Seguir_fisico_controller/edit/<?php echo $met->id_segui_finan;?>/<?php echo $met->id_proyecto;?>"class="btn btn-warning" title="Editar Metas">
                                                            <span class="fa fa-pencil">   </span>
                                                          <?php //<button type="button" data-toggle="modal" id="submitBtn"  class="btn btn-warning" >Actualizar</button>?>
                                                       </a>
                                                         <!-- Botón para mostrar el formulario de actualizar -->
                                                         <input type="hidden" name="descrip_compo_proy" value="<?php echo $descrip_compo_proy;?> " class="form-control" >
                                                         <a href="<?php echo base_url();?>proyecto/Seguir_fisico_controller/delete1/<?php echo $met->id_segui_finan;?>/<?php echo $met->id_proyecto;?>/<?php echo $descrip_compo_proy;?>" class="btn btn-danger btn-remove"title="Eliminar Proyeccion">
                                                             <?php //<button type="button" class="btn btn-danger">Borrar</button>?>
                                                              <span class="fa fa-remove">  </span>
                                                         </a>
                                                     </span>
                                                 </td>
                                             </tr>
                                                 <?php endforeach;?>
                                                 <?php endif;?>
                                          </tbody>
                                      </table>
                                  </div>
                                          <!-- /.box-body -->
                              </div>

                                <div  align="center" class="form-group" >
                        <button type="submit" class="btn btn-success btn-flat" >
                                        Continuar
                        </button>
                                </div>
                    </form>
          </div>
        </div>
      </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
