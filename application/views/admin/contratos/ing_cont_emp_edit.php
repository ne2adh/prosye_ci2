<style media="screen">
/* Customize the label (the container) */
.container {
display: block;
position: relative;
padding-left: 35px;
margin-bottom: 12px;
cursor: pointer;
font-size: 22px;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;

}

/* Hide the browser's default checkbox */
.container input {
position: absolute;
opacity: 0;
cursor: pointer;
height: 0;
width: 0;

}

/* Create a custom checkbox */
.checkmark {
position: absolute;
top: 0;
left: 0;
height: 25px;
width: 25px;
border-style: solid;
border-color: #2196F3;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container input:checked ~ .checkmark {
background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
content: "";
position: absolute;
display: none;
}

/* Show the checkmark when checked */
.container input:checked ~ .checkmark:after {
display: block;
}

/* Style the checkmark/indicator */
.container .checkmark:after {
left: 9px;
top: 5px;
width: 5px;
height: 10px;
border: solid white;
border-width: 0 3px 3px 0;
-webkit-transform: rotate(45deg);
-ms-transform: rotate(45deg);
transform: rotate(45deg);
}
</style>
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div id="" class="content-wrapper">
    <!-- Content Header (Page header) -->
<section class="content-header">
<h1>
    Datos del contrato de la Actividad <?php echo $descrip_compo_proy;?> dentro del Proyecto
    <small> <?php echo $proyecto->nombre_proyecto;?>
      <?php
      if(($id_proyecto>=100000)){
          echo "EN EL ".$proyectod->nombre_tramo;
      }?></small>
    <input type="hidden" name="monto_proy" value="<?php echo $proyecto->monto_comp_proy;?>">

</h1>
</section>
    <!-- Main content -->
<section class="content">
        <!-- Default box -->
<div class="box box-solid">
    <div class="box-body">
        <div class="form-group">
      <div class="row">
      <?php
          function formato($cantidad){
              $formatted = number_format(sprintf('%0.2f', preg_replace("/[^0-9.]/", "", $cantidad)), 2);
              return $cantidad < 0 ? "({$formatted})" : "{$formatted}";
          }											                
      ?>
        <div class="col-xs-1"></div>
          <div class="col-xs-12">
                      <input type="" name="" value="<?php echo' La Actividad en el proyecto tiene la modalidad de '.$proyecto->modalidad.' con un costo total de '.$proyecto->monto_comp_proy .' Bolivianos';?>  " class="form-control" readonly="">

                      <br>
                      <div class="col-xs-12 text-center">
                  <label>Tiempo de Duracion del Proyecto</label>
                </div>
                  <div class="row">
                    <div class="col-xs-6">
                              <label for="fecha_inicio">
                                Fecha de Inicio:
                              </label>
                              <input type="date" id="fecha_inicio" class="form-control" name="fecha_inicio" value="<?php echo $proyecto->fecha_inicio;?>" readonly="">
                    </div>
                            <div class="col-xs-6">
                              <label for="fecha_conclusion">
                                Fecha de Conclusion:
                              </label>
                              <input type="date" id="fecha_fin"  class="form-control" name="fecha_conclusion" value="<?php echo $proyecto->fecha_conclusion;?>" readonly="">
                            </div>
                  </div>
                <br>
                  <div class="col-xs-12 text-center">
                  <label><h1>Datos del contrato con empresa</h1></label>
                </div>
                <?php /*foreach($contrato as $met ):?>
                    <?php $tabla=false;
                     if(isset($met->id_contrato)){
                     $tabla=true;
                      }?>
                    <?php endforeach;*/?>

                <form action="<?php echo base_url();?>proyecto/contratos_controller/update" method="POST" >
                <input type="hidden" name="id_proyecto" value="<?php echo $id_proyecto;?>  " class="form-control" >
                <input type="hidden" name="descrip_compo_proy" value="<?php echo $descrip_compo_proy;?> " class="form-control" max="<?php echo $proyecto->monto_comp_proy;?>">
                <input type="hidden" id="m_proy" name="m_proy" value="<?php echo $proyecto->monto_comp_proy;?>">
                <div class="row">
                  <div class="col-xs-2">
                    <label class="container">ADM                                      
                        <input type="checkbox" name="adm" value="false" id="adm" <?php echo $contrato[0]->nombre_empresa=="ADM DIRECTA"?'checked':'';?>>                      
                      <span class="checkmark"></span>
                    </label>

                  </div>
                  <div class="col-xs-10 <?php echo !empty(form_error('nombre_empresa'))? 'has-error':'';?>">
                      <label for="nombre">Nombre de la Empresa:</label>
                      <input type="text" id="nombre_empresa"  class="form-control" name="nombre_empresa"  style="text-trasnform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" <?php foreach ($contrato as $item):?> value="<?php echo $item->nombre_empresa;?>" requerid>
  										<?php echo form_error("nombre_empresa","<span class='help-block'>","</span>"); ?><br>
                  </div>
                  <div class="col-xs-6 <?php echo !empty(form_error('costo_adj'))? 'has-error':'';?>">
                            <label for="costo_adjudicado">
                              Costo Adjudicado en Bs:
                            </label>
                            <input type="text"  class="form-control"  placeholder="COSTO ADJUDICADO EN BS"
                              data-type="currency" name="costo_adj"  style="text-trasnform:uppercase;"  value="<?php echo formato($item->costo_adj);?>" min="1.00" max="<?php echo $proyecto->monto_comp_proy;?>" 
                              onfocus="ShowHide('ayuda_costo_contrato','block')" onblur="ShowHide('ayuda_costo_contrato','none')"
                            >
                            <div class="alert alert-info ayuda" id="ayuda_costo_contrato">
                                Ingrese el Costo del Contrato, costo maximo sugerido <?php echo formato($proyecto->monto_comp_proy);?> (Campo requerido).
                            </div>
                            <?php echo form_error("costo_adj","<span class='help-block'>","</span>"); ?><br>
                  </div>
                  <div class="col-xs-6 <?php echo !empty(form_error('plazo_contra'))? 'has-error':'';?>">
                            <label for="plazo_contractual">
                              Plazo contractual:
                            </label>
                            <input type="text" id="plazo_contra"  class="form-control"  placeholder="PLAZO CONTRACTUAL"
                            name="plazo_contra"  style="text-trasnform:uppercase;"  value="<?php echo $item->plazo_contra;?>" onKeyUp="calcularFecha();" required>
                            <?php echo form_error("plazo_contra","<span class='help-block'>","</span>"); ?><br>
                  </div>
                  <div class="col-xs-6 <?php echo !empty(form_error('fecha_ini'))? 'has-error':'';?>">
                              <label for="Fecha_ini">Fecha Inicio (S/orden proced):</label>
                      <input id="fecha_ini" type="date" name="fecha_ini" class="form-control"  value="<?php echo $item->fecha_ini;?>"  onchange="calcularFecha();" required>
                      <?php echo form_error("fecha_ini","<span class='help-block'>","</span>"); ?>
                  </div>
                  <div class="col-xs-6 <?php echo !empty(form_error('fecha_con'))? 'has-error':'';?>">
                              <label for="Fecha_con">Fecha Conclucion (S/contrato):</label>
                      <input id="fecha_con" type="date" name="fecha_con" class="form-control"  value="<?php echo $item->fecha_con;?>" required readonly>
                      <?php echo form_error("fecha_con","<span class='help-block'>","</span>"); ?>
                  </div>
                 </div>
                 <br><?php endforeach;?>
                <div  align="center" class="form-group">
                  <button type="submit" name="submit"class="btn btn-success" class="btn btn-success btn-flat">
                    Actualizar
                  </button>
                  <?php if(!empty($contrato)):?>
                  <?php foreach($contrato as $met ):?>
                  <input type="hidden" name="id_contrato" value="<?php echo $met->id_contrato;?>">
                 <?php endforeach;?>
                 <?php endif;?>

                      </div>
                      </form>
                      <?php
                      if(isset($error)){
                        if($error == 1){
                          echo '<div class="alert alert-danger" role="alert">  <strong>El componente '.$descrip_compo_proy.'  dentro del proyecto '.$proyecto->nombre_proyecto.' ya tiene un contrato</strong> Elige otro componente para hacer un contrato.
                            </div>';
                                }
                                }
                               ?>
                      <br>
                      <div class="col-xs-12 text-center">
                  <label><h2>Contrato Realizado</h2></label>
                </div>

                      <form action="<?php echo base_url();?>proyecto/contratos_controller/store" method="POST" >
                <div class="row">
                                  <div class="col-md-12">
                                      <table name="ocultar" class="table table-striped" name="tabla">
                                          <thead >
                                              <tr class="success">
                                                <th>N°</th>
                                                  <th >Nombre empresa</th>
                                                  <th >Costo Adjudicado</th>
                                                  <th >Plazo Contractual</th>
                                                  <th >Fecha Inicio</th>
                                                  <th >Fecha Conclucion</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                            <?php if(!empty($contrato)):?>
                                             <?php foreach($contrato as $met ):?>
                                             <tr id="fila">
                                                 <td>
                                                    <?php global $i; $i=$i+1; echo $i;?>
                                                  </td>
                                                  <td><?php echo $met->nombre_empresa;?> </td>
                                                  <td><?php echo formato($met->costo_adj);?> </td>
                                                  <td><?php echo $met->plazo_contra;?> </td>
                                                  <td><?php echo $met->fecha_ini;?> </td>
                                                  <td><?php echo $met->fecha_con;?> </td>
                                             </tr>
                                                 <?php endforeach;?>
                                                 <?php endif;?>
                                          </tbody>
                                      </table>
                                  </div>
                                    <!-- /.box-body -->
                              </div>
                    </form>
          </div>
        </div>
      </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>


<script type="text/javascript">

  $( document ).ready(function() {
    let adm = document.getElementById('adm');
    if(adm.checked){
      nombre_empresa.value = 'ADM DIRECTA';
      nombre_empresa.readOnly = true;
    }
  });

  let adm = document.getElementById('adm');
  let nombre_empresa = document.getElementById('nombre_empresa');

  adm.addEventListener("change", function(){
    console.log(adm.checked);
    if(adm.checked){
      nombre_empresa.value = 'ADM DIRECTA';
      nombre_empresa.readOnly = true;
    }else{
      nombre_empresa.value = '';
      nombre_empresa.readOnly = false;
    }
  });

  function calcularFecha(){
      let fecha_inicio = document.getElementById("fecha_ini").value;
      let diasContrato = document.getElementById("plazo_contra").value;
      let vfecha = fecha_inicio.split("-");      
      if(diasContrato!='' && fecha_inicio!=''){          
          fecha = new Date(vfecha[0], vfecha[1]-1, vfecha[2],0,0,0);          
          fecha.setDate(fecha.getDate() + parseInt(diasContrato));
          d = fecha.getDate();
          m = fecha.getMonth()+1;
          if(m<10){
            m='0'+m;
          }

          if(d<10){
            d = '0'+d;
          }
          y = fecha.getFullYear();
          fecha_actual = y+"-"+m+"-"+d;
          document.getElementById("fecha_con").value = fecha_actual;          
      }
  }
</script>
