<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div id="" class="content-wrapper">
    <!-- Content Header (Page header) -->
<section class="content-header">
<h1>edicion
    Datos de la garantia en la Actividad <?php echo $descrip_compo_proy;?> dentro del Proyecto
    <small> <?php echo $proyecto->nombre_proyecto;?>
      <?php
      if(($id_proyecto>=100000)){
          echo "EN EL ".$proyectod->nombre_tramo;
      }?>
    </small>

</h1>
</section>
    <!-- Main content -->
<section class="content">
        <!-- Default box -->
<div class="box box-solid">
    <div class="box-body">
    <?php
                        function formato($cantidad){
                            $formatted = number_format(sprintf('%0.2f', preg_replace("/[^0-9.]/", "", $cantidad)), 2);
                            return $cantidad < 0 ? "({$formatted})" : "{$formatted}";
                        }											                
    ?>   
    <div class="form-group">
      <div class="row">
        <div class="col-xs-1"></div>
          <div class="col-xs-12" >
          <?php foreach($contrato as $met ):?>
            <input type="" name="form" value="<?php echo' Nombre de la empresa '.$met->nombre_empresa.' con un costo adjudicado de: '.formato($met->costo_adj).' Bolivianos';?>  " class="form-control" readonly="">
            <input type="hidden" name="nombre" value="<?php echo $met->nombre_empresa;?>" id="nombre">
                      <br>
                <br>
                <?php
                  //function porcentaje($cantidad,$porciento,$decimales){
                   // return number_format($cantidad*$porciento/100,$decimales);
                 // }
                  $porciento= number_format(($met->costo_adj*7)/100,2);

                ?>
                <?php endforeach;?>
                <?php
                  /*$tabla=false;
                    if(isset($_POST['submit'])){
                    $tabla=TRUE;
                  }*/
                ?>
                <?php
                  $ocultar=true;
                  if($met->nombre_empresa==='ADM DIRECTA'){
                      echo '<div class="alert alert-danger" role="alert">  <strong>no correspnde la garantia por que es ADMINISTRACION DIRECTA!</strong>
                                </div>';
                      $ocultar=false;
                  }
                ?>
                 <div>
                  <div  name="ocultar" <?php if(!$ocultar){echo "hidden";}?>>
                <div class="col-xs-12 text-center">
                  <label><h1>Datos de la Garantia </h1></label>
                </div>
                    <form <?php //if($tabla){echo "hidden";} ?>action="<?php echo base_url();?>proyecto/contratos_controller/updateGarantia" method="POST" >
                    <input type="hidden" name="id_proyecto" value="<?php echo $id_proyecto;?>  " class="form-control" >
                    <?php foreach($contrato as $met ):?>
                    <input type="hidden" name="id_contrato" value="<?php echo $met->id_contrato;?>  " class="form-control" >
                    <?php endforeach;?>
                    <input type="hidden" name="descrip_compo_proy" value="<?php echo $descrip_compo_proy;?> " class="form-control" >
                    <?php foreach($garantia as $met ):?>
                    <input type="hidden" name="id_garantia" value="<?php echo $met->id_garantia;?> " class="form-control" > <?php endforeach;?>
                    <div class="row">
                      <div class="col-xs-12 <?php echo !empty(form_error('tipo'))? 'has-error':'';?>">
                        <label for="tipo">
                          Tipo de Garantia:
                          </label>
                          <?php foreach($garantia as $met ):?>

                          <select class="form-control" name="tipo" value="<?php echo $met->tipo;?>"required>
                          <option value="<?php echo $met->tipo;?>" <?php  echo   set_select ( 'tipo' ,  '' ,  TRUE );?>><?php echo $met->tipo;?></option>
                          <option value="BOLETA DE GARANTIA" <?php  echo   set_select ( 'tipo' ,  'BOLETA DE GARANTIA' );  ?>>BOLETA DE GARANTIA</option>
                          <option value="POLIZA" <?php  echo   set_select ( 'tipo' ,  'POLIZA' );  ?>>POLIZA</option>
                          <option value="RETENCION" <?php  echo   set_select ( 'tipo' ,  'RETENCION' );  ?>>RETENCION</option>
                          <?php echo form_error("tipo","<span class='help-block'>","</span>"); ?>
                          </select requerid><br>
                      </div>
                      <div class="col-xs-6 <?php echo !empty(form_error('entidad_financiera'))? 'has-error':'';?>">
                        <label for="unidad financiera">
                          Entidad Financiera (abreviatura):
                        </label>
                        <input type="text"  class="form-control" placeholder="ABREVIATURA DE LA ENTIDAD FINANCIERA"
                        name="entidad_financiera"  style="text-trasnform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"  value="<?php echo $met->entidad_financiera;?>"requerid>
                        <?php echo form_error("entidad_financiera","<span class='help-block'>","</span>"); ?><br>
                      </div>
                      <div class="col-xs-6 <?php echo !empty(form_error('monto'))? 'has-error':'';?>">
                        <label for="monto">
                          Monto en bs:
                        </label>
                        <input type="text"  class="form-control"
                        name="monto"   value="<?php echo $porciento;?>" readonly="">
                        <?php echo form_error("monto","<span class='help-block'>","</span>"); ?><br>
                      </div>
                     <br>
                         <?php endforeach;?>

                       <div  align="center" class="form-group">
                        <button type="submit" name="submit" class="btn btn-primary" class="btn btn-success btn-flat">
                          Actualizar
                        </button>
                       </div>
                    </form></div>

                      <?php
                      if(isset($error)){
                        if($error == 1){
                          echo '<div class="alert alert-danger" role="alert">  <strong>El contrato de la actividad '.$descrip_compo_proy.'  dentro del proyecto '.$proyecto->nombre_proyecto.' con la empresa '.$met->nombre_empresa.' ya tiene una garantia </strong> Elige otro componente para hacer un contrato.
                            </div>';
                                }
                                }
                       ?>
                      <br>
                      <div class="col-xs-12 text-center">
                      <label><h2>Datos de la Garantia</h2></label>
                      </div>
                    <form action="<?php echo base_url();?>proyecto/contratos_controller/store_garantia" method="POST">
                     <div class="row">
                        <div class="col-md-12">
                          <table  class="table table-striped" name="tabla">
                            <thead>
                              <tr class="success">
                                  <th >Nro.</th>
                                  <th >Tipo de Garantia</th>
                                  <th >Entidad Financiera</th>
                                  <th >Monto de la garantia</th>

                              </tr>
                            </thead>
                              <tbody>
                                <?php if(!empty($garantia)):?>
                                 <?php foreach($garantia as $met ):?>
                                 <tr id="fila">
                                     <td><?php global $i; $i=$i+1; echo $i;?></td>
                                     <td><?php echo $met->tipo;?> </td>
                                     <td><?php echo $met->entidad_financiera;?> </td>
                                     <td><?php echo formato($met->monto);?></td>
                                     <td>
                                     </td>
                                 </tr>
                                     <?php endforeach;?>
                                     <?php endif;?>
                              </tbody>
                                </table>
                            </div>      <!-- /.box-body -->
                            </div>
                            <div  class="text-center" class="form-group" >
                              <a  href="<?php echo base_url();?>proyecto/Contratos_controller/ingreso1/<?php echo $met->id_proyecto;?>" class="btn btn-success btn-flat" >
                                  Continuar
                              </a>
                            </div>
                        </form>
        </div>
      </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
</div>
