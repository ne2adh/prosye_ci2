
        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Seguimiento
                <small>Usted esta trabajando en el proyecto:</small>
                </h1>
            </section>
            <?php  foreach($proyecto as $result):?>
            <input type="hidden" id="fecha_inicio" class="form-control" name="fecha_inicio" value="<?php echo $result->fecha_inicio;?>" readonly="">
            <input type="hidden" id="fecha_fin"  class="form-control" name="fecha_conclusion" value="<?php echo $result->fecha_conclusion;?>" readonly="">
            <?php endforeach;?>

            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                      <hr>

                        <div class="row">
                            <div class="col-md-12">
                                <h1>
                                    <?php echo $nombre->nombre_proyecto;?>
                                 </h1>
                                 <br>

                                <h3>
                                    <?php
                                    if(isset($proyectod->id_tramo)){
                                        echo $proyectod->nombre_tramo;
                                    }?>
                                 </h3><br>
                                 <form method="post" action="<?php echo base_url();?>proyecto/seguir_fisico_controller/elegir_proy">
                                    <input type="hidden" name="id_proyecto" value="<?php echo $id_proyecto ?>">
                                    <div class="col-xs-4">
       						                				<label for="mes">
       						                					Escoga mes del seguimiento:
       						                				</label>
       						                				<select class="form-control" name="mes">
       															<option value="" <?php  echo   set_select ( 'mes' ,  '' ,  TRUE );  ?>>Seleccione una Opcion</option>
       															<option value="ENERO" <?php  echo   set_select ( 'mes' ,  'ENERO' );  ?>>ENERO</option>
       															<option value="FEBRERO" <?php  echo   set_select ( 'mes' ,  'FEBRERO' );  ?>>FEBRERO</option>
       															<option value="MARZO" <?php  echo   set_select ( 'mes' ,  'MARZO' );  ?>>MARZO</option>
       															<option value="ABRIL" <?php  echo   set_select ( 'mes' ,  'ABRIL' );  ?>>ABRIL</option>
       															<option value="MAYO" <?php  echo   set_select ( 'mes' ,  'MAYO' );  ?>>MAYO</option>
       															<option value="JUNIO" <?php  echo   set_select ( 'mes' ,  'JUNIO' );  ?>>JUNIO</option>
       															<option value="JULIO" <?php  echo   set_select ( 'mes' ,  'JULIO' );  ?>>JULIO</option>
       															<option value="AGOSTO" <?php  echo   set_select ( 'mes' ,  'AGOSTO' );  ?>>AGOSTO</option>
       															<option value="SEPTIEMBRE" <?php  echo   set_select ( 'mes' ,  'SEPTIEMBRE' );  ?>>SEPTIEMBRE</option>
       															<option value="OCTUBRE" <?php  echo   set_select ( 'mes' ,  'OCTUBRE' );  ?>>OCTUBRE</option>
       															<option value="NOVIEMBRE" <?php  echo   set_select ( 'mes' ,  'NOVIEMBRE' );  ?>>NOVIEMBRE</option>
       															<option value="DICIEMBRE" <?php  echo   set_select ( 'mes' ,  'DICIEMBRE' );  ?>>DICIEMBRE</option>
       														</select>
       														<?php echo form_error("mes","<span class='help-block'>","</span>"); ?>
       													</div>
                                    <div class="col-xs-4">
                                    <label for="gestion">
                                        Escoga año del seguimiento:
                                    </label>
                                    <input type="number" id=""  class="form-control" name="gestion" value="<?php echo set_value("gestion");?>" min="<?php echo $inicio;?>" max="<?php echo $fin;?>" >
                                        <?php echo form_error("gestion","<span class='help-block'>","</span>"); ?>                                        
                                    </div>
                                    <button type="submit" name="submit" class="btn btn-success btn-flat" value="Submit Form" >
                                        Guardar
                                    </button>
                              </form>
                          </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                                            <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
