
        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Seguimiento
                <small>Usted esta trabajando en el proyecto:</small>
                </h1>
            </section>
            <?php  foreach($proyecto as $result):?>
            <input type="hidden" id="fecha_inicio" class="form-control" name="fecha_inicio" value="<?php echo $result->fecha_inicio;?>" readonly="">
            <input type="hidden" id="fecha_fin"  class="form-control" name="fecha_conclusion" value="<?php echo $result->fecha_conclusion;?>" readonly="">
            <?php endforeach;?>

            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                      <hr>

                        <div class="row">
                            <div class="col-md-12">
                                <h1>
                                    <?php echo $nombre->nombre_proyecto;?>
                                 </h1>

                                     <?php
                                     if(isset($proyectod->id_tramo)){
                                         echo $proyectod->nombre_tramo;
                                     }?>
                                     <br><br><br>
                                 <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
                                 <div class="col-xs-4">
       						            <label for="mes">
       						            	Mes del seguimiento:
       						            </label>
                                        <input readonly type="text" class="form-control" name="mes" value="<?php echo $mes; ?>" min="<?php echo $inicio;?>" max="<?php echo $fin;?>" required>
   										<?php echo form_error("mes","<span class='help-block'>","</span>"); ?>
       							</div>
                                <div class="col-xs-4">
                                    <label for="gestion">
                                            Año del seguimiento:
                                    </label>
                                    <input readonly type="text" class="form-control" name="gestion" value="<?php echo $gestion; ?>" min="<?php echo $inicio;?>" max="<?php echo $fin;?>" required>
                                 </div>
                                 <div class="col-xs-4">
                                  <a href="<?php  echo base_url();?>proyecto/seguir_fisico_controller/ingreso/<?php echo $id_proyecto; ?>" class="btn btn-default"> Volver</a>
                                  </div>
                              </form>

                                 <br><br><br><br>

                                 <table  class="table table-bordered btn-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Codigo</th>
                                                        <th>Nombre Actividad</th>
                                                        <th>Costo Adjudicado</th>
                                                        <th>Opciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                         <?php  foreach($contrato as $result):?>
                                                                <tr>
                                                                    <td><?php echo $result->id_proyecto;?></td>
                                                                    <td><?php echo $result->descrip_compo_proy;?></td>
                                                                    <td><?php echo $result->costo_adj;?></td>
                                                                    <td>
                                                                        <div class="btn-group">
                                                                            <a href="<?php echo base_url();?>proyecto/seguir_fisico_controller/ing_seg_fin/<?php echo $result->id_proyecto;?>/<?php echo trim($result->descrip_compo_proy);?>/<?php echo $mes;?>/<?php echo $gestion;?>" class="btn btn-warning"title="Trabajar Proyecto">
                                                                                <span class="fa fa-pencil">
                                                                                </span>
                                                                            </a>

                                                                    </td>

                                                           </tr>
                                                    <?php endforeach;?>

                                                </tbody>
                                </table>

                                </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                                            <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
