<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div id="" class="content-wrapper">
    <!-- Content Header (Page header) -->
<section class="content-header">
<h1>
    Programacion Financiera Proyectada por Gestion
    <small> <?php echo $proyecto->nombre_proyecto;?></small>
</h1>
</section>
    <!-- Main content -->
<section class="content">
        <!-- Default box -->
<div class="box box-solid">
    <div class="box-body">
        <!-- inicio -->
        <input type="hidden" name="id_proyecto" value="<?php echo $proyecto->id_proyecto;?>">
        <!--<input type="" name="nombre_tramo" value="<?php echo $proyecto->nombre_tramo;?>">
        <input type="" name="nombre_tramo" value="<?php echo $proyecto->descrip_componente_proy;?>">
        <input type="" name="nombre_tramo" value="<?php echo $proyecto->descripcion_componente;?>">-->
        <div class="form-group">
      <div class="row">
        <div class="col-xs-1"></div>
          <div class="col-xs-12">
                <input type="" name="" value="<?php echo' con un costo total de '.$proyecto->costo_proyecto .' Bolivianos';?>  " class="form-control" readonly="">
                      <br>
                      <div class="col-xs-12 text-center">
                  <label>Tiempo de Duracion del Proyecto</label>
                </div>
                  <div class="row">
                    <div class="col-xs-6">
                              <label for="fecha_inicio">
                                Fecha de Inicio:
                              </label>
                              <input type="date" id="fecha_inicio" class="form-control" name="fecha_inicio" value="<?php echo $proyecto->fecha_inicio;?>" readonly="">
                    </div>
                            <div class="col-xs-6">
                              <label for="fecha_conclusion">
                                Fecha de Conclusion:
                              </label>
                              <input type="date" id="fecha_fin"  class="form-control" name="fecha_conclusion" value="<?php echo $proyecto->fecha_conclusion;?>" readonly="">
                            </div>
                  </div>
                <br>
                      <div class="col-xs-12 text-center">
                  <label><h1>Datos de la Proyeccion</h1></label>
                </div>
                <form action="<?php echo base_url();?>proyecto/seguir_fisico_controller/store_gestion" method="POST" >
                <input type="hidden" name="id_proyecto" value="<?php echo $id_proyecto;?>  " class="form-control" >

                <div class="row">
                  <!--<div class="col-xs-4">
                            <label for="mes">
                              Escoga Gestion:
                            </label>
                            <select class="form-control" name="mes" >
                      <option value="" <?php  echo   set_select ( 'mENEROes' ,  '' ,  TRUE );  ?>>Seleccione una Opcion</option>
                      <option value="ENERO" <?php  echo   set_select ( 'mes' ,  'ENERO' );  ?>>ENERO</option>
                      <option value="FEBRERO" <?php  echo   set_select ( 'mes' ,  'FEBRERO' );  ?>>FEBRERO</option>
                      <option value="MARZO" <?php  echo   set_select ( 'mes' ,  'MARZO' );  ?>>MARZO</option>
                      <option value="ABRIL" <?php  echo   set_select ( 'mes' ,  'ABRIL' );  ?>>ABRIL</option>
                      <option value="MAYO" <?php  echo   set_select ( 'mes' ,  'MAYO' );  ?>>MAYO</option>
                      <option value="JUNIO" <?php  echo   set_select ( 'mes' ,  'JUNIO' );  ?>>JUNIO</option>
                      <option value="JULIO" <?php  echo   set_select ( 'mes' ,  'JULIO' );  ?>>JULIO</option>
                      <option value="AGOSTO" <?php  echo   set_select ( 'mes' ,  'AGOSTO' );  ?>>AGOSTO</option>
                      <option value="SEPTIEMBRE" <?php  echo   set_select ( 'mes' ,  'SEPTIEMBRE' );  ?>>SEPTIEMBRE</option>
                      <option value="OCTUBRE" <?php  echo   set_select ( 'mes' ,  'OCTUBRE' );  ?>>OCTUBRE</option>
                      <option value="NOVIEMBRE" <?php  echo   set_select ( 'mes' ,  'NOVIEMBRE' );  ?>>NOVIEMBRE</option>
                      <option value="DICIEMBRE" <?php  echo   set_select ( 'mes' ,  'DICIEMBRE' );  ?>>DICIEMBRE</option>
                    </select>
                    <?php echo form_error("mes","<span class='help-block'>","</span>"); ?>

                  </div>-->
                          <div class="col-xs-4">
                            <label for="gestion">
                              Escoga la Gestion:
                            </label>
                            <input type="number" id=""  class="form-control" name="gestion" value="<?php echo set_value("gestion");?>" min="<?php echo $inicio;?>" max="<?php echo $fin;?>">
                    <?php echo form_error("anio","<span class='help-block'>","</span>"); ?>

                  </div>

                  <div class="col-xs-4">
                              <label for="monto_actividad">Monto Proyectado de la gestion:</label>
                      <input id="" type="number" step="0.01" name="monto_proyectado" class="form-control"  value=""  mim="1.00" max="<?php echo $proyecto->costo_proyecto;?>">
                      <?php echo form_error("monto_actividad","<span class='help-block'>","</span>"); ?>
                  </div>

                 </div>
                 <br>
                <div  align="center" class="form-group">
                  <button type="submit" class="btn btn-primary" class="btn btn-success btn-flat">
                            Guardar
                          </button>
                      </div>
                      </form>
                      <br>
                      <div class="col-xs-12 text-center">
                  <label><h2>Monto Proyectado por Gestion</h2></label>
                </div>


                <form action="<?php echo base_url();?>proyecto/seguir_fisico_controller/store_fisico" method="POST" >
                <div class="row">
                                  <div class="col-md-12">
                                      <table  class="table table-striped" name="tabla">
                                          <thead >
                                              <tr class="success">
                                                  <th >N°</th>
                                                  <th >Año</th>
                                                  <th >Monto Proyectado</th>
                                                  <th >Opciones</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                             <?php if(!empty($gestion1)):?>
                                              <?php foreach($gestion1 as $met ):?>
                                              <tr id="fila">

                                                  <td><?php global $i; $i=$i+1; echo $i;?></td>
                                                  </td>
                                                  <td><?php echo $met->gestion;?> </td>
                                                  <td><?php echo $met->monto_proyectado;?> </td>
                                                  <td>
                                                      <!-- Botón para guardar la información actualizada -->
                                                      <span >
                                                        <a href="<?php echo base_url();?>proyecto/Seguir_fisico_controller/edit/<?php echo $met->id_progFinan;?>/<?php echo $met->id_proyecto;?>" class="btn btn-warning" title="Editar Metas">
                                                             <span class="fa fa-pencil">   </span>
                                                           <?php //<button type="button" data-toggle="modal" id="submitBtn"  class="btn btn-warning" >Actualizar</button>?>
                                                        </a>
                                                          <!-- Botón para mostrar el formulario de actualizar -->
                                                          <a href="<?php echo base_url();?>proyecto/Seguir_fisico_controller/delete/<?php echo $met->id_progFinan;?>/<?php echo $met->id_proyecto;?>" class="btn btn-danger btn-remove"title="Eliminar Proyeccion">
                                                              <?php //<button type="button" class="btn btn-danger">Borrar</button>?>
                                                               <span class="fa fa-remove">  </span>
                                                          </a>
                                                      </span>
                                                  </td>
                                              </tr>
                                                  <?php endforeach;?>
                                                  <?php endif;?>
                                          </tbody>
                                      </table>
                                  </div>
                                          <!-- /.box-body -->
                              </div>

                                <div  align="center" class="form-group" >
                        <button type="submit" class="btn btn-success btn-flat" >
                                        Continuar
                        </button>
                                </div>
                    </form>
          </div>
        </div>
      </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
