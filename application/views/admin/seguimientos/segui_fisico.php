<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div id="" class="content-wrapper">
    <!-- Content Header (Page header) -->
<section class="content-header">
<h1>
    Seguimiento Fisico dentro del Proyecto
    <small> <?php echo $proyecto->nombre_proyecto;?>
      <?php if(isset($proyectod->id_tramo)){
          echo 'EN EL ';
          echo $proyectod->nombre_tramo;
      }
      ?></small>
</h1>


</section>
    <!-- Main content -->
<section class="content">
        <!-- Default box -->
<div class="box box-solid">
    <div class="box-body">
        <!-- inicio -->
        <!--<input type="" name="nombre_tramo" value="<?php echo $proyecto->nombre_tramo;?>">
        <input type="" name="nombre_tramo" value="<?php echo $proyecto->descrip_componente_proy;?>">
        <input type="" name="nombre_tramo" value="<?php echo $proyecto->descripcion_componente;?>">-->
        <div class="form-group">
      <div class="row">
        <div class="col-xs-1"></div>
          <div class="col-xs-12">
            <input type="hidden" name="" value="<?php echo' El proyecto tiene un costo total de '.$proyecto->costo_proyecto.' Bolivianos';?>  " class="form-control" readonly="">
                  <br>
                  <div class="col-xs-12 text-center">
                    <input type="hidden" id="fecha_inicio" class="form-control" name="fecha_inicio" value="<?php echo $proyecto->fecha_inicio;?>" readonly="">
                    <input type="hidden" id="fecha_fin"  class="form-control" name="fecha_conclusion" value="<?php echo $proyecto->fecha_conclusion;?>" readonly="">
              <label>Porcentaje avanzado dentro del proyecto</label>              
            </div>
            <?php foreach($falta_porcentaje as $met):?>
              <div class="row">
                <div class="col-xs-6">
                    <input type="text" id="por" class="form-control" name="porc" value="<?php echo 'El proyecto tiene un porcentaje avanzado de '.$met->historico.' % ';?>" readonly="">
                </div>
                <div class="col-xs-6">

                  <input type="text" id="porc" class="form-control" name="porce" value="<?php echo 'tiene '.$met->falta.' % por avanzar';?>" readonly="">
                </div><?php endforeach;?>
              </div>
            <br>
                  <div class="col-xs-12 text-center">
              <label><h1>Datos del Seguimiento</h1></label>
            </div>
            <table>

            <tr>
            <form action="<?php echo base_url();?>proyecto/seguir_fisico_controller/store_fisico1" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="id_proyecto" value="<?php echo $id_proyecto;?>  " class="form-control" >
              <div class="form-group">
                <div class="row">
                    <div class="col-xs-2">
                    </div>
                    <div class="col-xs-4 <?php echo !empty(form_error('mes'))? 'has-error':'';?>">
                              <label for="mes">
                                Escoga mes del seguimiento:
                              </label>
                              <select class="form-control" name="mes" id="mes" onchange="buscarSeguimiento();">
                                <option value="" <?php  echo   set_select ( 'mENEROes' ,  '' ,  TRUE );  ?>>Seleccione una Opcion</option>
                                <option value="ENERO" <?php  echo   set_select ( 'mes' ,  'ENERO' );  ?>>ENERO</option>
                                <option value="FEBRERO" <?php  echo   set_select ( 'mes' ,  'FEBRERO' );  ?>>FEBRERO</option>
                                <option value="MARZO" <?php  echo   set_select ( 'mes' ,  'MARZO' );  ?>>MARZO</option>
                                <option value="ABRIL" <?php  echo   set_select ( 'mes' ,  'ABRIL' );  ?>>ABRIL</option>
                                <option value="MAYO" <?php  echo   set_select ( 'mes' ,  'MAYO' );  ?>>MAYO</option>
                                <option value="JUNIO" <?php  echo   set_select ( 'mes' ,  'JUNIO' );  ?>>JUNIO</option>
                                <option value="JULIO" <?php  echo   set_select ( 'mes' ,  'JULIO' );  ?>>JULIO</option>
                                <option value="AGOSTO" <?php  echo   set_select ( 'mes' ,  'AGOSTO' );  ?>>AGOSTO</option>
                                <option value="SEPTIEMBRE" <?php  echo   set_select ( 'mes' ,  'SEPTIEMBRE' );  ?>>SEPTIEMBRE</option>
                                <option value="OCTUBRE" <?php  echo   set_select ( 'mes' ,  'OCTUBRE' );  ?>>OCTUBRE</option>
                                <option value="NOVIEMBRE" <?php  echo   set_select ( 'mes' ,  'NOVIEMBRE' );  ?>>NOVIEMBRE</option>
                                <option value="DICIEMBRE" <?php  echo   set_select ( 'mes' ,  'DICIEMBRE' );  ?>>DICIEMBRE</option>
                              </select>
                              <?php echo form_error("mes","<span class='help-block'>","</span>"); ?>
                    </div>
                    <div class="col-xs-4 <?php echo !empty(form_error('anio'))? 'has-error':'';?>">
                              <label for="anio">
                                Escoga año del seguimiento:
                              </label>
                              <select id="anio" name="anio" class="form-control" onChange="buscarSeguimiento();" value="<?php echo set_value("anio");?>">
                                  <option value="">Seleccione un Año</option>
                                  <?php
                                      for($i=$inicio; $i<=$fin; $i++){
                                          echo '<option value="'.$i.'">'.$i.'</option>';
                                      }
                                  ?>
                              </select>                            
                              <?php echo form_error("anio","<span class='help-block'>","</span>"); ?>
                    </div>
                </div>
              </div>
              <div id="seguimiento" name="seguimiento">
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-4 <?php echo !empty(form_error('porcentaje'))? 'has-error':'';?>">
                              <label for="porcentaje">Porcentaje avanzado del proyecto en el mes:</label>
                              <?php foreach($falta_porcentaje as $met):?>
                              <input id=""  min="0.01" max="100.00" step="0.01" type="number" name="porcentaje" class="form-control"
                              value="<?php echo set_value('porcentaje'); ?>"  mim="1" max="<?php echo $met->falta;?>">
                              <?php echo form_error("porcentaje","<span class='help-block'>","</span>"); ?>
                              <?php endforeach;?>
                        </div>
                        <div class="col-xs-4 <?php echo !empty(form_error('resultado_mes'))? 'has-error':'';?>">
                              <label for="resultado_mes">Resultado del mes:</label>
                              <input id="resultado_mes" type="text" name="resultado_mes" class="form-control"  value="" placeholder="situacion actual del proyecto/programa">
                                 <?php echo form_error("resutado_mes","<span class='help-block'>","</span>"); ?>
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                </div>
                <div class="form-group">
                  <div class="row">
                     <div class="col-xs-2"></div>
                     <div class="col-xs-4 <?php echo !empty(form_error('problemas_mes'))? 'has-error':'';?>">
                          <label for="problemas_mes">Problemas identificados del mes:</label>
                          <input id="problemas_mes" type="text" name="problemas_mes" class="form-control"  value="" >
                          <?php echo form_error("problemas_mes","<span class='help-block'>","</span>"); ?>
                     </div>
                     <div class="col-xs-4 <?php echo !empty(form_error('acciones_solucion'))? 'has-error':'';?>">
                          <label for="acciones_solucion">Acciones de solucion:</label>
                          <input id="acciones_solucion" type="text" name="acciones_solucion" class="form-control"  value="" >
                          <?php echo form_error("acciones_solucion","<span class='help-block'>","</span>"); ?>
                     </div>
                     <div class="col-xs-2"></div>
                  </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                                <input type="hidden" value="<?php echo $id_proyecto; ?>" name="id_proyecto" id="id_proyecto">
                                <label > <strong>Seleccione imagen de avance fisico:</strong>
                                </label>
                        </div>                          
                        <div class="col-xs-2"></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-8">
                             <input name="imagen" accept="image/x-png,image/gif,image/jpeg,image/jpg" type="file" id="imagen">
                        </div>
                        <div class="col-xs-2"></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-4"></div>
                        <div class="col-xs-4 text-center">
                            <button type="submit" class="btn btn-primary" class="btn btn-success btn-flat">
                                Guardar
                            </button>
                        </div>
                        <div class="col-xs-4"></div>
                    </div>
                </div>
              </div>
              <div id="mensaje" name="mensaje" class="form-group">
                  <div class="row">
                    <div class="col-xs-4"></div>
                    <div class="col-xs-4 text text-danger">
                        Seguimiento fisico Registrado en el Mes y Gestion Seleccionado.
                    </div>
                    <div class="col-xs-4"></div>
                  </div>
              </div>
            </form>
            
               <label><h2>Seguimientos Realizados</h2></label>
                </div>


                      <form action="<?php echo base_url();?>proyecto/seguir_fisico_controller/store_fisico" method="POST" >
                <div class="row">
                                  <div class="col-md-12">
                                      <table  class="table table-striped" name="tabla">
                                          <thead >
                                              <tr class="success">
                                                  <th >N°</th>
                                                  <th >Mes</th>
                                                  <th >Año</th>
                                                  <th >Porcentaje</th>
                                                  <th >Resultado</th>
                                                  <th >Problemas</th>
                                                  <th >Acciones de solucion</th>
                                                  <th>Imagen</th>
                                                  <th >Opciones</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                            <?php if(!empty($fisico)):?>
                                             <?php foreach($fisico as $met ):?>
                                             <tr id="fila">

                                                 <td><?php global $i; $i=$i+1; echo $i;?></td>
                                                 </td>
                                                 <td><?php echo $met->mes;?> </td>
                                                 <td><?php echo $met->gestion;?> </td>
                                                 <td><?php echo $met->porcentaje;?> </td>
                                                 <td><?php echo $met->resultado_mes;?> </td>
                                                 <td><?php echo $met->problemas_mes;?> </td>
                                                 <td><?php echo $met->acciones_solucion;?> </td>
                                                 <td>
                                                   <img src="<?php echo base_url().$met->ruta;?>" height="50px" width="50px" alt="Sin imagen">
                                                 </td>
                                                 <td>
                                                     <!-- Botón para guardar la información actualizada -->
                                                     <span >
                                                         <!-- Botón para mostrar el formulario de actualizar -->
                                                         <a href="<?php echo base_url();?>proyecto/Seguir_fisico_controller/deleteFisico/<?php echo $met->id_seguimiento_fisico;?>/<?php echo $met->id_proyecto;?>" class="btn btn-danger btn-remove"title="Eliminar Proyeccion">
                                                             <?php //<button type="button" class="btn btn-danger">Borrar</button>?>
                                                              <span class="fa fa-remove">  </span>
                                                         </a>

                                                     </span>
                                                 </td>
                                             </tr>
                                                 <?php endforeach;?>
                                                 <?php endif;?>
                                          </tbody>
                                      </table>
                                  </div>
                                          <!-- /.box-body -->
                              </div>
                              </form>
                                <div  align="center" class="form-group" >
                        <a href="<?php echo base_url();?>proyecto/Seguir_fisico_controller/index1/<?php echo $id_proyecto;?>" class="btn btn-success btn-flat" >
                            Continuar
                        </a>
                  </div>
          </div>
        </div>
      </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

<script type="text/javascript">

      const imagen = document.querySelector("#imagen");
      const id_proyecto =  document.querySelector("#id_proyecto");
      const ruta = "<?php echo base_url();?>proyecto/procesar_controller/imagen";
      let seguimiento = <?php echo json_encode($fisico);?>;
      let mensaje = document.getElementById('mensaje');

      $(document).ready(function(){
          mensaje.style.visibility = 'hidden';
      });

      function buscarSeguimiento(){
        let mes = document.getElementById('mes').value;
        let gestion = document.getElementById('anio').value;
        let sw = true;        
        for(i=0;i<seguimiento.length;i++){
          console.log(seguimiento[i].mes == mes && seguimiento[i].gestion == gestion);
          if(seguimiento[i].mes == mes && seguimiento[i].gestion == gestion){
              sw= false;
          }
        }

        let divSeg = document.getElementById('seguimiento');
        let mensaje = document.getElementById('mensaje');
        if(sw){
            divSeg.style.visibility = 'visible';// Hide
            mensaje.style.visibility = 'hidden';
        }
        else{
            divSeg.style.visibility = 'hidden';
            mensaje.style.visibility = 'visible';
        }
      }
      //const mensaje = document.querySelector("#mensaje");

</script>
