<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Seguimiento Fisico
        <small>Escoga el proyecto en el cual trabajara</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body"> 
              <hr>
                <div class="row">
                    <div class="col-md-12">
                <table id="example1" name="example1" class="table table-bordered btn-hover">
                                        <thead>
                                            <tr>
                                                <th>Codigo</th>
                                                <th>Nombre Proyecto</th>
                                                <th>Descripcion Proyecto</th>
                                                <th>Monto Proyecto</th>
                                                <th>Codigo Sisin</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                                 <?php  foreach($results as $result):?>
                                                        <tr>
                                                            <td><?php echo $result->id_proyecto;?></td>
                                                            <td><?php echo $result->nombre_proyecto;?></td>
                                                            <td><?php echo $result->descripcion_proyecto;?></td>
                                                            <td><?php echo $result->costo_proyecto;?></td>
                                                            <td><?php echo $result->codigo_sisin;?></td>
                                                            <td>




                                                                <div class="btn-group">

                                                                    <a href="<?php echo base_url();?>proyecto/seguir_fisico_controller/ingreso2/<?php echo $result->id_proyecto;?>" class="btn btn-warning" title="Escojer Proyecto">
                                                                        <span class="fa fa-pencil"title="Ver Proyecto">
                                                                        </span>
                                                                    </a>

                                                                </div>
                                                            </td>
                                                   </tr>
                                            <?php endforeach;?>

                                        </tbody>
                        </table>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
