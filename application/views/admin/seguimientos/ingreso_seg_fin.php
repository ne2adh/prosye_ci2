        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
<div id="" class="content-wrapper">
            <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Seguimiento Financiero de la Actividad
             <?php foreach($contrato as $met ):?><strong>
            <?php echo $met->descrip_compo_proy;?></strong> <?php endforeach;?>dentro del Proyecto
   	        <small> <?php echo $proyecto->nombre_proyecto;?>
              <?php
              if(($id_proyecto>=100000)){
                  echo "EN EL ".$proyectod->nombre_tramo;
              }?></small>
        </h1> 
    </section>
            <!-- Main content -->
    <section class="content">
                <!-- Default box -->
        <div class="box box-solid">
			<?php
                function formato($cantidad){
                    $formatted = number_format(sprintf('%0.2f', preg_replace("/[^0-9.]/", "", $cantidad)), 2);
                    return $cantidad < 0 ? "({$formatted})" : "{$formatted}";
				}															                
            ?>
            <div class="box-body">
              <?php foreach($contrato as $met ):?>
					<?php 
						$monto_total=$met->costo_adj;						
					?>
            	<?php endforeach;?>
            		<?php $sum=0; ?>
                    <?php foreach($gestion2 as $result):?>
                        <?php  $sum=$sum+$result->monto_ejecutado;
                        //echo $sum;
                        ?>
					<?php endforeach; 
						$total=$monto_total-$sum;
					?>

            		<!-- inicio -->
            		<!--<input type="" name="nombre_tramo" value="<?php echo $proyecto->nombre_tramo;?>">
            		<input type="" name="nombre_tramo" value="<?php echo $proyecto->descrip_componente_proy;?>">
            		<input type="" name="nombre_tramo" value="<?php echo $proyecto->descripcion_componente;?>">-->
            		<div class="form-group">
							<div class="row">
								<div class="col-xs-1"></div>
									<div class="col-xs-12">
												<input type="" name="" value="<?php echo' La Actividad en el proyecto tiene la modalidad de '.$proyecto->modalidad.' con un costo total adjudicado de '.$met->costo_adj .' Bolivianos';?>  " class="form-control" readonly="">
            									<br>
            									<div class="col-xs-12 text-center">
													<label>Tiempo de Duracion del Proyecto</label>
												</div>
													<div class="row">
														<div class="col-xs-6">
							                				<label for="fecha_inicio">
							                					Fecha de Inicio:
							                				</label>
							                				<input type="date" id="fecha_inicio" class="form-control" name="fecha_inicio" value="<?php echo $proyecto->fecha_inicio;?>" readonly="">
														</div>
							                			<div class="col-xs-6">
							                				<label for="fecha_conclusion">
									                			Fecha de Conclusion:
									                		</label>
									                		<input type="date" id="fecha_fin"  class="form-control" name="fecha_conclusion" value="<?php echo $proyecto->fecha_conclusion;?>" readonly="">
									                	</div>
													</div>
												<br>
											<?php if($estado==0):?>
            									<div class="col-xs-12 text-center">
													<label><h1>Datos del Seguimiento</h1></label>
												</div>
												<form action="<?php echo base_url();?>proyecto/seguir_fisico_controller/store_fisico" method="POST" autocomplete="off" >
												<input type="hidden" name="id_proyecto" value="<?php echo $id_proyecto;?>  " class="form-control" >
												<input type="hidden" name="mes" value="<?php echo $mes;?>  " class="form-control" >
												<input type="hidden" name="gestion" value="<?php echo $gestion;?>  " class="form-control" >
												<input type="hidden" name="descrip_compo_proy" value="<?php echo $descrip_compo_proy;?> " class="form-control" >
												<div class="row">
													<div class="col-xs-4">
                            				<label for="mes">
						                					Mes del seguimiento:
						                				</label><br>
						                				<input type="text" name="mes" value="<?php echo $mes;?>" disabled>
														<?php echo form_error("mes","<span class='help-block'>","</span>"); ?>

													</div>
						              <div class="col-xs-4">
						                				<label for="anio">
								                			Año del seguimiento:
								                		</label>
								                		<input type="text" id="gestion"  class="form-control" name="gestion" value="<?php echo $gestion;?>" disabled>
														<?php echo form_error("gestion","<span class='help-block'>","</span>"); ?>

													</div>

													<div class="col-xs-4">
					                						<label for="monto_actividad">Monto de la Actividad gastado en el mes:</label>
															<input id="monto_actividad" type="text" name="monto_actividad" class="form-control"  value=""                               									 
																  data-type="currency" onfocus="ShowHide('ayuda_monto_actividad','block')" onblur="ShowHide('ayuda_monto_actividad','none')" 																  	
																  >
																<div class="alert alert-info ayuda" id="ayuda_monto_actividad">
																	Escriba el Monto de la Actividad, valor maximo permitido <?php  echo $total;?> (Campo requerido).
																</div>
															<?php echo form_error("monto_actividad","<span class='help-block'>","</span>"); ?>
													</div>

												 </div>
												 <br>
												<div  class="form-group text-center">
													<button type="submit" class="btn btn-primary" class="btn btn-success btn-flat">
									                	Guardar
									                </button>
						           				</div>
						           				</form>
						           				<br>
												<?php endif;?>   
            									<div class="col-xs-12 text-center">
													<h2>Seguimientos Realizados</h2>
													<?php
														if($total<0){
															echo '<h4 class="text text-danger">Revise la informacion Introducida, el monto asignado supera al monto del componente: '.$total.'</h4>';
														}
														else{
															echo '<h4>Valor maximo sugerido: '.formato($total).'</h4>';		
														}
													?>
													
												</div>


						           				<form action="<?php echo base_url();?>index.php/proyecto/seguir_fisico_controller/elegir_proy/<?php echo $id_proyecto;?>" method="POST" >
                                <input type="hidden" name="id_proyecto" value="<?php echo $id_proyecto; ?>">
                                <input type="hidden" name="gestion" value="<?php echo $gestion;?> " >
                                <input type="hidden" name="mes" value="<?php echo $mes;?> " >

                    	           <div class="row">
					                                <div class="col-md-12">
					                                    <table  class="table table-striped" name="tabla">
					                                        <thead >
					                                            <tr class="success">
					                                                <th >N°</th>
					                                                <th >Mes</th>
					                                                <th >Año</th>
					                                                <th >Monto</th>
					                                                <th >Opciones</th>
					                                            </tr>
					                                        </thead>
					                                        <tbody>
                                                    <?php if(!empty($gestion2)):?>
                                                     <?php foreach($gestion2 as $met ):?>
                                                     <tr id="fila">

                                                         <td><?php global $i; $i=$i+1; echo $i;?></td>
                                                         </td>
                                                         <td><?php echo $met->mes;?> </td>
                                                         <td><?php echo $met->gestion;?> </td>
                                                         <td><?php echo formato($met->monto_ejecutado);?> </td>
                                                         <td>
                                                             <!-- Botón para guardar la información actualizada -->
                                                             <span >

                                                                 <!-- Botón para mostrar el formulario de actualizar -->
                                                                 <input type="hidden" name="descrip_compo_proy" value="<?php echo $descrip_compo_proy;?> " class="form-control" >
                                                                 <a href="<?php echo base_url();?>proyecto/Seguir_fisico_controller/delete1/<?php echo $met->id_segui_finan;?>/<?php echo $met->id_proyecto;?>/<?php echo trim($descrip_compo_proy);?>/<?php echo trim($met->mes);?>/<?php echo $met->gestion;?>" class="btn btn-danger btn-remove"title="Eliminar Proyeccion">
                                                                     <?php //<button type="button" class="btn btn-danger">Borrar</button>?>
                                                                      <span class="fa fa-remove">  </span>
                                                                 </a>
                                                             </span>
                                                         </td>
                                                     </tr>
                                                         <?php endforeach;?>
                                                         <?php endif;?>
					                                        </tbody>
					                                    </table>
					                                </div>
					                                        <!-- /.box-body -->
					                            </div>

				                                <div  align="center" class="form-group" >
        										            <button type="submit" class="btn btn-success btn-flat" 
																<?php if($total < 0)
																	echo "disabled";
																?>
															>
			                                        			Continuar
        										            </button>
			                                </div>

									</div>
								</div>
							</div>
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
