<!-- =============================================== -->

                 <!-- Content Wrapper. Contains page content -->
        
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Permisos
                <small>Listado</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                
                <div class="box box-solid">
                    <div class="box-body">
                       <div class="row">
                            <div class="col-md-12">
                                <a href="<?php echo base_url();?>administrador/permisos/agregar" class="btn btn-primary btn-flat">
                                <span class="fa fa-plus">
                                </span>Agregar Permisos
                                </a>
                            </div>
                        </div> 
                        <div  class="row">
                            <div align="center" class="col-md-12" >
                                <h2>Listado General de Permisos</h2>      
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="example1" class="table table-bordered btn-hover">
                                    <thead>
                                        <tr>
                                           
                                            <th>id</th>
                                            <th>Menu</th>
                                            <th>Rol</th>
                                            <th>Leer</th>
                                            <th>Insertar</th>
                                            <th>Actualizar</th>
                                            <th>Borrar</th>
                                             <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(!empty($permisos)):?>
                                         
                                          
                                            <?php foreach($permisos as $permiso ):?>
                                             
                                               <tr>
                                                    <td><?php echo $permiso->id_permisos;?></td>
                                                    <td><?php echo $permiso->menu_id;?></td>
                                                    <td><?php echo $permiso->rol_id;?></td>
                                                   
                                                    
                                                    <td>
                                                        <?php if ($permiso->leer==0):?>
                                                            <span class="fa fa-times"></span>
                                                        <?php else:?>
                                                            <span class="fa fa-check"></span>
                                                            
                                                        <?php endif;?>    
                                                    </td>
                                                    <td>
                                                        <?php if ($permiso->agregar==0):?>
                                                            <span class="fa fa-times"></span>
                                                        <?php else:?>
                                                            <span class="fa fa-check"></span>
                                                            
                                                        <?php endif;?>    
                                                    </td>
                                                    <td>
                                                        <?php if ($permiso->actualizar==0):?>
                                                            <span class="fa fa-times"></span>
                                                        <?php else:?>
                                                            <span class="fa fa-check"></span>
                                                            
                                                        <?php endif;?>    
                                                    </td>
                                                    <td>
                                                        <?php if ($permiso->borrar==0):?>
                                                            <span class="fa fa-times"></span>
                                                        <?php else:?>
                                                            <span class="fa fa-check"></span>
                                                            
                                                        <?php endif;?>    
                                                    </td>
                                                    <td>
                                                         <div class="btn-group">
                                                            <a href="#" class="btn btn-info">
                                                                <span class="fa fa-pencil">                 
                                                                </span>
                                                            </a>
                                                            <a href="#" class="btn btn-info">
                                                                <span class="fa fa-remove">                 
                                                                </span>
                                                            </a>
                                                            
                                                        </div>

                                                    </td>

                                                </tr>
                                            <?php endforeach;?>  
                                        <?php endif;?>
                                    </tbody>
                                    
                                </table>
                                
                           </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>