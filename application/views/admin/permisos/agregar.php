<!-- =============================================== -->

                 <!-- Content Wrapper. Contains page content -->
        
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Permisos
                <small>Nuevos</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                
                <div class="box box-solid">
                    <div class="box-body">
                       <div class="row">
                         <div class="col-md-12">
                              <form action="<?php echo base_url();?>administrador/permisos/store" method="POST">
                                <div class="form-group">
                                    <label for="rol" >Roles:</label>
                                    <select name="rol" id="rol" class="form-control">
                                        <?php foreach ($roles as $rol):?>
                                           <option value="<?php echo $rol->id_rol;?>"><?php echo $rol->nombre;?></option> 
                                        <?php endforeach;?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="menu" >Menus:</label>
                                    <select name="menu" id="menu" class="form-control">
                                        <?php foreach ($menus as $menu):?>
                                           <option value="<?php echo $menu->id;?>"><?php echo $menu->nombre_menu;?></option> 
                                        <?php endforeach;?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="leer">Leer:</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="leer" value="1" checked="checked">Si
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="leer" value="0" checked="checked">No
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="insertar">Agregar:</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="insertar" value="1" checked="checked">Si
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="insertar" value="0" checked="checked">No
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="actualizar">Actualizar:</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="actualizar" value="1" checked="checked">Si
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="actualizar" value="0" checked="checked">No
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="borrar">Borrar:</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="borrar" value="1" checked="checked">Si
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="borrar" value="0" checked="checked">No
                                    </label>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success"><span class="fa fa-save"></span>Guardar</button>
                               </div>    
                            </form>
                          </div>
                            
                    </div>
                    <!-- /.box-body -->
                </div>
              </div>
                <!-- /.box -->
            </section>

            <!-- /.content -->
        </div>