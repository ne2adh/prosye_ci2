
<!-- dom para angular-->
<div class="content-wrapper" ng-app="app" ng-controller="ctrl">
  <section class="content-header">
    <h1>
        USUARIOS
        <small></small>
    </h1>
  </section>
  <section class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                  <div class="panel panel-info">
                    <div class="panel-heading">
                      <h3 class="panel-title">NUEVO USUARIO</h3>
                    </div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-12">                        
                          <form action="<?php echo base_url();?>administrador/Usuarios/saveEditUsuarios" method="POST">                           
                            <input type="hidden" name="id_usuario" id="id_usuario" class="form-control" value="<?php echo $usuario[0]->id_usuario;?>" ng-model="id_usuario" readonly="" hidden ="true">
                            <div>
                              <label>Nombre persona :</label>
                              <input type="text" name="nombrepersona" class="form-control" readonly="" ng-model="nombrepersona" required>
                            </div><br>
                            <div>
                              <label>Usuario :</label>
                              <input type="text" name="cuenta" class="form-control" readonly="" ng-model="usuario" required>
                            </div><br>
                            <div>
                              <label>Contraseña :</label>
                              <input type="text" name="contraseña" class="form-control" ng-model="CI" readonly="" required>
                            </div>
                            <br>
                            <div>
                              <label>Rol :</label>
                              <select name="rol" class="form-control" required>
                                  <option value="">Seleccione un Rol</option>
                                  <?php
                                    foreach ($roles as $item) {
                                       if($item->id_rol== $usuario[0]->id_rol)
                                            echo '<option value="'.$item->id_rol.'" selected>'.$item->nombre.'</option>';
                                        else
                                            echo '<option value="'.$item->id_rol.'">'.$item->nombre.'</option>';
                                   }
                                ?>
                              </select>
                            </div>
                            <div class="form-group">
                                <label>
                                   Area:
                                </label>
                                <select name="area" id="area" class="form-control" required>
                                    <option value="">Seleccione un Area</option>
                                    <?php
                                      foreach ($area as $item) {
                                        if($item->cod== $usuario[0]->id_area)
                                            echo '<option value="'.$item->cod.'" selected>'.$item->nombre.'</option>';
                                        else
                                            echo '<option value="'.$item->cod.'">'.$item->nombre.'</option>';                                          
                                      }
                                    ?>
                                </select>

                            </div>
                          </div>
                        </div>
                      </div>
                        <div class="panel-footer" aling="center">
                          <p align="center">
                            <button type="submit" class="btn btn-success btn-flat btn-guardar">Guardar</button>
                          </p>
                        </div>
                          </form>
                  </div>
                </div>
            </div>
          </div>
      </div>
  </section>

</div>


<!--angularjs :) -->
<script src="<?php echo base_url();?>assets/angular.min.js"></script>
<script src="<?php echo base_url();?>assets/ui-bootstrap.min.js"></script>

<script>
let ngApp = function(){
  'use strict';
  let app = angular.module('app', ['ui.bootstrap']);
  app.controller('ctrl', function($scope, $http, $uibModal) {
    $scope.id_usuario = <?php echo $usuario[0]->id_usuario;?>;
    $scope.CI= <?php echo $usuario[0]->ci_persona;?>;    
    $scope.nombrepersona = '<?php echo $usuario[0]->nombres.' '.$usuario[0]->ap_paterno.' '.$usuario[0]->ap_materno;?>' ;
    $scope.usuario = '<?php echo $usuario[0]->cuenta;?>';    
    $scope.elegirCarnet=(CI)=>{
      $http.get("<?php echo base_url('administrador/usuarios/listarNombre?CI='); ?>"+CI).then(function(response) {
        $scope.nombrepersona = response.data[0].nombrecompleto;
        let c1 = response.data[0].nombres.substring(0,2);
        let c2 = response.data[0].ap_paterno.substring(0,2);
        let c3 = response.data[0].ap_materno.substring(0,2);
        $scope.usuario = c1+c2+c3;
      });
    }
  });
}();
</script>
<!---->
