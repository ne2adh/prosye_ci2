<div class="content-wrapper"  ng-app="app" ng-controller="ctrl">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Perfil 
                    <small>de Usuario</small>
                </h1>                
            </section>
            <section class="content"> 
                <div class="box box-solid">
                    <div class="box-body">                   
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6 text-center">
                                <h3>Perfil de Usuario</h3>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-5">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        Datos personales
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            CI:<?php echo $dataUsuario[0]->CI;?> <?php echo strToUpper($dataUsuario[0]->expedicion);?>
                                        </div>
                                        <div class="form-group">
                                            Nombre: <?php echo strToUpper($dataUsuario[0]->nombres);?> <?php echo strToUpper($dataUsuario[0]->ap_paterno);?> <?php echo strToUpper($dataUsuario[0]->ap_materno);?>
                                        </div>
                                        <div class="form-group">
                                            Fecha de Nacimiento: <?php echo $dataUsuario[0]->fecha_nacimiento;?>
                                        </div>
                                        <div class="form-group">
                                            Estado Civil: <?php echo strToUpper($dataUsuario[0]->est_civil);?>
                                        </div>
                                        <div class="form-group">
                                           Profesion: <?php echo strToUpper($dataUsuario[0]->profesion);?>
                                        </div>
                                        <div class="form-group">
                                            Telefono: <?php echo $dataUsuario[0]->telefono;?>
                                        </div>
                                        <div class="form-group">
                                            AFP: <?php echo $dataUsuario[0]->afp;?>
                                        </div>  
                                        <div class="form-group">
                                            Fecha de Registro: <?php echo $dataUsuario[0]->fech_registro;?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="panel panel-warning">
                                    <div class="panel-heading">
                                        Datos de Usuario
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            Cargo: <?php echo $dataUsuario[0]->nombre_cargo;?>
                                        </div>
                                        <div class="form-group">
                                            Cuenta: <?php echo $dataUsuario[0]->cuenta;?>
                                        </div>
                                        <div class="form-group">
                                            Rol de Usuario:  <?php echo $dataUsuario[0]->nombre_rol;?>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-info" ng-click="abrirDialogo()">Cambiar Password</button>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    </div>
                </div>
            </section>
   <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-lock"></span>Cambiar Password</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form name="formCambiarPassword" ng-submit="submitFunc()" role="form" novalidate>
            <div class="form-group" >
              <label for="actual">Password Actual</label>
              <input type="password" class="form-control" ng-model="actual" name="actual" id="actual" placeholder="Password Actual" required="">
              <p class="alert alert-warning" ng-show="formCambiarPassword.$submitted && formCambiarPassword.actual.$error.required">Password Actual Requerido.</p>
            </div>            
            <div class="form-group">
              <label for="psw">Password Nuevo</label>
              <input type="password" class="form-control" id="psw" name="psw" ng-model="psw" placeholder="Nuevo Password" required="">
              <p class="alert alert-warning" ng-show="formCambiarPassword.$submitted && formCambiarPassword.psw.$error.required">Nuevo Password Requerido.</p>
            </div>
            <div class="form-group">
              <label for="pswn">Repita Password Nuevo</label>
              <input type="password" class="form-control" id="pswn" ng-model="pswn" placeholder="Nuevo Password" required="">
              
              <p class="alert alert-warning" ng-show="formCambiarPassword.$submitted && formCambiarPassword.pswn.$error.required">Nuevo Password Requerido.</p>              
            </div>
            <div class="form-group alert alert-warning" ng-show="estado">
                El pasword nuevo no coincide
            </div>
            <button type="submit" class="btn btn-success btn-block" ng-disabled="formCambiarPassword.$invalid"><span class="glyphicon glyphicon-off"></span>Aceptar</button>
          </form>
        </div>
        <div class="modal-footer">          
        </div>
      </div>
    </div>
  </div>
</div>
<!--angularjs :) -->
<script src="<?php echo base_url();?>assets/angular.min.js"></script>
<script src="<?php echo base_url();?>assets/ui-bootstrap.min.js"></script>

<script>
    let ngApp = function(){
        'use strict';
        let app = angular.module('app', ['ui.bootstrap']);        
            app.controller('ctrl', function($scope, $http) {
                $scope.abrirDialogo=function(){
                    $("#myModal").modal("show");
                }
                $scope.estado = false;
                $scope.submitFunc = function(){
                    if($scope.psw != $scope.pswn){
                        alert("El password a cambiar no coincide");
                        $scope.estado = true;
                    }
                    else{
                        let actual = $scope.actual;
                        let psw = $scope.psw;                        
                        var req = {
                            method: 'POST',
                            url: "<?php echo base_url('administrador/usuarios/cambiarPassword'); ?>",
                            data: {
                                actual: actual,
                                nuevo : psw
                            }
                        };

                        $http(req).then(function(response){                                                                                                                
                            let error = response.data.Error;
                            let mensaje = response.data.Mensaje;
                            alert(mensaje);
                            if(error=="no"){
                                window.location.href = "<?php echo base_url();?>auth/logout";
                            }
                        });
                    }
                    
                }
        });
    }();
</script>