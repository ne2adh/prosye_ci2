
<!-- dom para angular-->
<div class="content-wrapper" ng-app="app" ng-controller="ctrl">
  <section class="content-header">
    <h1>
        Lista General
        <small></small>
    </h1>
  </section>
  <section class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                  <div class="panel panel-info">
                    <div class="panel-heading">
                      <h3 class="panel-title"> Usuarios:</h3>
                    </div>
                      <div class="panel-body">
                        <div class="col-md-12">
                            <div class="col-md-6">
                              <input type="text" class="form-control" placeholder="Buscar..." ng-model="buscar">
                            </div>
                              <div class="col-md-6">
                                <a  href="<?php echo base_url();?>administrador/usuarios/agregar" class="btn btn-primary btn-flat pull-right">
                                <span class="fa fa-plus">
                                </span>Agregar Usuario
                                </a><br>
                              </div><br><br><br>
                          <table class="table table-bordered striped">
                              <tr>
                                <th width="5%">#</th>
                                <th>Nombre</th>
                                <th>Area</th>
                                <th>Cuenta</th>
                                <th>C.I.</th>
                                <th>Rol</th>
                                <th>Editar Usuario</th>                                
                              </tr>
                              <tr ng-repeat="item in usuarios | filter:buscar">
                                <td>{{$index + 1}}</td>
                                <td>{{item.nombres}} {{item.ap_paterno}} {{item.ap_materno}}</td>
                                <td>{{item.nombre_area}}</td>
                                <td>{{item.cuenta}}</td>
                                <td>{{item.ci_persona}}</td>
                                <td>{{item.nombre}}</td>
                                <td align="center">
                                  <a href="<?php echo base_url();?>administrador/Usuarios/editUsuario/{{item.id_usuario}}" class="btn btn-warning"><span class="fa fa-pencil"></span></a>
                                </td>
                                </td>
                              </tr>
                            </table>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
      </div>
  </section>


<script type="text/ng-template" id="agregar.html">
  <div>
      <div class="box box-warning">
          <div class="modal-header box-header with-border text-center">
              <h4 class="modal-title box-title">{{titulo}}</h4>
              <button class="close" type="button" ng-click="cerrar()">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body box-body">
            <form name="form">
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                      <label class="control-label">Rol:</label>
                      <input type="text" class="form-control" name="rol" ng-model="rol.nombre" placeholder="Rol..." required>
                      <div ng-show="form.rol.$touched">
                          <span class="text-help text-danger" ng-show="form.rol.$error.required">(*) Valor requerido</span>
                          <span class="text-help text-danger" ng-show="form.rol.$error.pattern">(*) Solo texto</span>
                      </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer box-footer text-center">
              <button class="btn btn-warning" ng-click="editar(rol)"> Editar</button>
              <a class="btn btn-default" ng-click="cerrar()"> <i class="fa fa-times"></i> Cerrar </a>
          </div>
          </div>
      </div>
  </div>
</script>

</div>

<!--angularjs :) -->
<script src="<?php echo base_url();?>assets/angular.min.js"></script>
<script src="<?php echo base_url();?>assets/ui-bootstrap.min.js"></script>

<script>
let ngApp = function(){
  'use strict';
  let app = angular.module('app', ['ui.bootstrap']);
  app.controller('ctrl', function($scope, $http, $uibModal) {


    $scope.listarUsuarios=()=>{
      $http.get("<?php echo base_url('administrador/usuarios/listarUsuarios'); ?>").then(function(response) {
        $scope.usuarios = response.data;
      });
    }
    $scope.listarUsuarios();

    $scope.amRol = (rol) => {
        $uibModal.open({
          templateUrl: 'agregar.html',
          backdrop: 'static',
          scope:$scope,
          controller: function ($scope,$uibModalInstance) {

                $scope.titulo='Editar Rol';
                $scope.rol = rol;

              $scope.editar = (rol)=> {
                let ruta = '<?php echo base_url('administrador/Rol/editRol?id=');?>'+rol.id_rol+'&nombre='+rol.nombre;
                let req = {
                   method: 'GET',
                   url: ruta
                   //data: { rol:rol }
                }
                $http(req).then(function(response) {
                  $scope.cerrar();
                  $scope.listarRoles();
                });
              };

              $scope.cerrar = () => {
                  $uibModalInstance.dismiss();
              };
          }
        });
    };


    console.log('funciona');
  });
}();
</script>
<!---->
