
<!-- dom para angular-->
<div class="content-wrapper" ng-app="app" ng-controller="ctrl">
  <section class="content-header">
    <h1>
        USUARIOS
        <small></small>
    </h1>
  </section>
  <section class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                  <div class="panel panel-info">
                    <div class="panel-heading">
                      <h3 class="panel-title">NUEVO USUARIO</h3>
                    </div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-12">                        
                          <form action="<?php echo base_url();?>administrador/Usuarios/saveUsuarios" method="POST">
                            <div >
                              <label for="ci">Carnet :</label>
                              <select name="CI" ng-model="CI" ng-change="elegirCarnet(CI)" class="form-control" required>
                                <option value="">Seleccione un CI</option>
                                <?php
                                 foreach ($persona as $item) {                                     
                                     echo '<option value="'.$item->CI.'">'.$item->CI2.' - '.$item->ap_paterno.' '.$item->ap_materno.' '.$item->nombres.'</option>';
                                 }
                                ?>
                              </select>
                            </div><br>
                            <div>
                              <label>Nombre persona :</label>
                              <input type="text" name="nombrepersona" class="form-control" readonly="" ng-model="nombrepersona" required>
                            </div><br>
                            <div>
                              <label>Usuario :</label>
                              <input type="text" name="cuenta" class="form-control" readonly="" ng-model="usuario" required>
                            </div><br>
                            <div>
                              <label>Contraseña :</label>
                              <input type="text" name="contraseña" class="form-control" ng-model="CI" readonly="" required>
                            </div>
                            <br>
                            <div>
                              <label>Rol :</label>
                              <select name="rol" ng-model="rol" class="form-control" required>
                                  <option value="">Seleccione un Rol</option>
                                  <?php
                                   foreach ($roles as $item) {
                                       echo '<option value="'.$item->id_rol.'">'.$item->nombre.'</option>';
                                   }
                                ?>
                              </select>
                            </div>
                            <div class="form-group">
                                <label>
                                   Area:
                                </label>
                                <select name="area" id="area" ng-model="area" class="form-control" required>
                                    <option value="">Seleccione un Area</option>
                                    <?php
                                      foreach ($area as $item) {
                                          echo '<option value="'.$item->cod.'">'.$item->nombre.'</option>';
                                      }
                                    ?>
                                </select>

                            </div>
                          </div>
                        </div>
                      </div>
                        <div class="panel-footer" aling="center">
                          <p align="center">
                            <button type="submit" class="btn btn-success btn-flat btn-guardar">Guardar</button>
                          </p>
                        </div>
                          </form>
                  </div>
                </div>
            </div>
          </div>
      </div>
  </section>

</div>


<!--angularjs :) -->
<script src="<?php echo base_url();?>assets/angular.min.js"></script>
<script src="<?php echo base_url();?>assets/ui-bootstrap.min.js"></script>

<script>
let ngApp = function(){
  'use strict';
  let app = angular.module('app', ['ui.bootstrap']);
  app.controller('ctrl', function($scope, $http, $uibModal) {

    $scope.CI= '';
    $scope.rol= '';

    $scope.elegirCarnet=(CI)=>{
      $http.get("<?php echo base_url('administrador/usuarios/listarNombre?CI='); ?>"+CI).then(function(response) {
        $scope.nombrepersona = response.data[0].nombrecompleto;
        let c1 = response.data[0].nombres.substring(0,1);
        let c2 = response.data[0].ap_paterno;
        let c3 = CI.substring(0,2);
        $scope.usuario = c1+c2+c3;
      });
    }

  });
}();
</script>
<!---->
