<!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        
        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Usuario
        	        <small>Nuevo</small>
                </h1>
            </section>
            
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                        <form action="<?php echo base_url();?>administrador/usuarios/store" method="POST">  
                           <div class="btn form-group">
                            <button type="button" class="btn btn-info btn-view" data-toggle="modal" data-target="#modal-default">
                                <span class="fa fa-search"></span>
                              </button>
                            </div>        
                                    <div class="form-group">
                                        <label for="ci">Introduca carnet:</label>                           
                                        <select id="ci" name="ci"  class="form-control">
                                            <?php foreach($persona as $per ):?> 

                                            <option value="<?php echo $per->CI2;?>"><?php echo $per->CI2;?>
                                            </option>
                                            <?php endforeach;?>    
                                        </select>
                                    </div>
                                    <div class="form-group">
                                    <label for="nombre">Nombre del Usuario:</label>                           
                                    <input type="text" id="cuenta" name="cuenta"  class="form-control">
                                    </div>
                                    <div class="form-group">
                                    <label for="cuenta">Usuario:</label>                           
                                    <input type="text" id="cuenta" name="cuenta"  class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="clave">Contraseña:</label>                           
                                        <input type="password" id="clave" name="clave"  class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="rol">Rol:</label>                           
                                        <select id="rol" name="rol"  class="form-control">
                                            <?php foreach($roles as $rol ):?>
                                            <option value="<?php echo $rol->id_rol;?>"><?php echo $rol->nombre;?>
                                            </option>
                                            <?php endforeach;?>    
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-success" value="Guardar">
                                    </div>
                        </form>     
                        </div>
                    </div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>