
        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Entes
                <small>Financiamiento proyecto</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                   
                        <div class="row">
                            <div class="col-md-12">
                                <a href="<?php echo base_url();?>ente/registro_ente/add" class="btn btn-primary btn-flat">
                                <span class="fa fa-plus">
                                </span>Agregar Ente Financiador
                                </a>
                            </div>
                        </div>
                   
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                    
                        <table id="example1" name="example1" class="table table-bordered btn-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Codigo</th>
                                                        <th>Nombre</th>
                                                        <th>Descripcion</th>
                                                        <th>Opciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                                                                 
                                                         <?php  foreach($results as $result):?> 
                                                                <tr>
                                                                    <td><?php echo $result->cod_inst;?></td>
                                                                    <td><?php echo $result->nombre;?></td>
                                                                    <td><?php echo $result->descripcion;?></td>
                                                                    <td>
                                                                        <div class="btn-group">
                                                                            <a href="#" class="btn btn-info">
                                                                                <span class="fa fa-eye">                 
                                                                                </span>
                                                                            </a>
                                                                            <a href="#" class="btn btn-warning">
                                                                                <span class="fa fa-pencil">               
                                                                                </span>
                                                                            </a>
                                                                            <a href="#" class="btn btn-danger">
                                                                                <span class="fa fa-remove">               
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                    </td>
                                                           </tr>
                                                    <?php endforeach;?>  
                                                </tbody>
                                </table>
                               
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
