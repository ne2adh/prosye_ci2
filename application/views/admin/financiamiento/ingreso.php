        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
       <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Ente de Financiamiento
                    <small>Registro</small>
                </h1>
            </section>
           
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                     
                    <form action="<?php base_url();?>/prosye_ci/ente/registro_ente/ingreso" method="POST">
                          
                        <div class="form-group">
                            <label for="cod_inst">Ingrese el Codigo del Ente Financiador</label>
                            <input class="form-control" name="cod_inst"/>
                        </div>
                         <div class="form-group">
                            <label for="nombre">Ingrese el Nombre del Ente Financiador</label>
                            <input class="form-control" name="nombre"/>
                        </div>
                         <div class="form-group">
                            <label for="descripcion">Descricion del Ente Financiador</label>
                            <input class="form-control" name="descripcion"/>
                        </div>
                        

                        <div > 
                                    <button  type="submit" class="btn btn-success btn-flat" aling="center">
                                        Guardar
                                    </button>
                        </div>
                   </form> 
                </div>
                    <!-- /.box-body -->
            </div>
                <!-- /.box -->

         </section>
            <!-- /.content -->
      </div>
        <!-- /.content-wrapper -->
