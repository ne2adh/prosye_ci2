<!DOCTYPE html>
<html>
<head>
<link rel="icon" href="<?php echo base_url('assets/favicon.ico') ?>">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ProSyE</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- base_url() = http://localhost/prosye_ci/-->
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template/font-awesome/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/template/dist/css/AdminLTE.min.css">
</head>
<body class="hold-transition login-page">
<div class="wrapper">
	<header class="main-header">
	   <!-- Logo -->
		    <a href="<?php echo base_url('/index.php/publico/login'); ?>" class="logo">
		      <!-- mini logo for sidebar mini 50x50 pixels -->
		      	<span class="logo-mini"><b>Pro</b>SyE</span>
		      <!-- logo for regular state and mobile devices -->
		      	<span class="logo-lg"><b>Pro</b>SyE</span>
	    	</a>
	    <!-- Header Navbar: style can be found in header.less -->
	    <nav class="navbar navbar-static-top">
	    </nav>
    </header>
    <div id="app_login" class="login-box">
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Introduzca sus datos de ingreso</p>
            <!-- /.login-logo  aqui se verifica datos de introduccion del usuario para loguearse-->
            <?php if ($this->session->flashdata("error")):?>
                <div class="alert alert-danger">
                <p><?php echo $this->session->flashdata("error")?> </p>
                </div>
            <?php endif; ?>
            <form action="<?php echo base_url();?>auth/login" method="post" v-on:submit="validarForm">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Usuario" name="cuenta" v-model="usuario" v-on:keyUp="cambiarError" >
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    <div class="label label-warning" v-if="submit && verificarUsuario">
                        Ingrese el Nombre de Usuario
                    </div>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Password" name="clave" v-model="password" v-on:keyUp="cambiarError">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="label label-warning" v-if="submit && verificarPassword">
                  Ingrese la Contraseña del Usuario
                </div>
                <div class="row">
                <div class="col-xs-4">
                  </div>        
                  <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Aceptar</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <a href="<?php echo base_url();?>.proyecto.mis_proyectos/?id=ci"></a>
        </div>
        <!-- /.login-box-body -->
        <div class="alert alert-danger" v-show="error">
          {{ mensaje }}
        </div>
    </div>
    <!-- /.login-box -->
<!-- Vue -->    
    <script src="<?php echo base_url('assets/node_modules/vue/dist/vue.js') ?>"></script>
<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/template/jquery/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/template/bootstrap/js/bootstrap.min.js"></script>
<!-- assets -->  
    <script src="<?php echo base_url('assets/funciones.js') ?>"></script>
<script>
    var app_login = new Vue({
        "el":"#app_login",
        data:{
          url:'<?php echo base_url('/index.php/'); ?>',
          usuario:'',
          password:'',
          submit:false,
          error:false,
          mensaje:''          
        },
        created:function(){
            getUserIP(function(ip){
                localStorage.setItem('ip', ip);
            });
        },
        computed:{
          verificarUsuario:function(){
              return this.usuario === '';
          },
          verificarPassword:function(){
              return this.password === '';
          }
        },
        methods:{
              cambiarError:function(){
                app_login.error  = false;
              },
              validarForm: function (event) {
                this.submit = true;
                if (this.verificarUsuario || this.verificarPassword){
                    event.preventDefault();                    
                }
                else{
                  this.enviar();  
                }                 
              },
              enviar:function(){
                axios.post(this.url+"publico/loginProcess", {
                    cuenta: this.usuario,
                    password: this.password
                  }).then(function(response){                      
                      if(response.data.estado == 1){
                        var r = response.data.resultado;
                        localStorage.setItem('id_usuario', r.id_usuario);
                        localStorage.setItem('estado', r.estado);
                        localStorage.setItem('fecha_ingreso', r.fecha_ingreso);
                        localStorage.setItem('cuenta', r.cuenta);
                        localStorage.setItem('grado', r.grado);
                        localStorage.setItem('ci_persona', r.ci);
                        localStorage.setItem('nombres', r.nombres);
                        localStorage.setItem('nombre_cargo', r.nombre_cargo);
                        localStorage.setItem('nombre_area', r.nombre_area);
                        localStorage.setItem('nombre_subarea', r.nombre_subarea);
                        localStorage.setItem('id_area', r.id_area);
                        localStorage.setItem('id_subarea', r.id_subarea);
                        bootbox.alert({
                            size: "small",
                            title: "BIENVENIDO",
                            message: "Usuario: "+r.nombres,
                            callback: function(){ 
                              location.href ="../layouts/header";
                             location.href ="../prosye_ci2/mis_proyectos";
                            }
                        });
                      }else{
                        Vue.set(this.app_login, 'error',true);
                        Vue.set(this.app_login, 'mensaje',response.data.resultado);

                        bootbox.alert({
                            size: "small",
                            title: "BIENVENIDO",
                            message: "Usuario: "+r.nombres,
                            callback: function(){ 
                              //location.href ="../layouts/header";
                             location.href ="../prosye_ci2/dashboard";
                            }
                        });

                      }
                  });
              }
        }
    });
</script
</body>
</html>
