
<!-- dom para angular-->
<div class="content-wrapper" ng-app="app" ng-controller="ctrl">
  <section class="content-header">
    <h1>
        ASIGNACION DE MENU
        <small></small>
    </h1>
  </section>
  <section class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">

                <div class="col-md-12">
                  <div class="panel panel-info">
                    <div class="panel-heading">
                      <h3 class="panel-title"> Rol: <?php echo $rol->nombre?></h3>
                    </div>
                      <div class="panel-body">
                        <div class="col-md-6">
                          <table class="table table-bordered striped">
                            <tr>
                            </tr>
                            <th>Numero</th>
                            <th>Nombre Menu</th>
                            <th>Opcion</th>
                            <?php if(!empty($menuasignado)):?>
                              <?php foreach($menuasignado as $item ):?>
                            <form action="<?php echo base_url();?>administrador/AsignacionMenu/saveAsignacionMenu" method="POST">
                              <input type="hidden" name="id_rol" value="<?php echo $id_rol?>">
                              <tr>
                                <td><?php global $i; $i=$i+1; echo $i;?></td>
                                <td><?php echo $item->nombre_menu?></td>
                                <input type="hidden" name="id_menu" value="<?php echo $item->id;?>">
                                <td><button type="submit" class="btn btn-success btn-flat btn-guardar">Agregar</button>
                                </td>
                              </tr>
                            </form>
                           <?php endforeach;?>
                          <?php endif;?>
                          </table>
                        </div>
                          <div class="col-md-6">
                            <table class="table table-bordered striped">
                              <tr>
                                <th>Numero</th>
                                <th>nombre_menu</th>
                                <th>Opcion</th>
                              </tr>
                              <?php if(!empty($traer_menu)):?>
                                <?php foreach($traer_menu as $item ):?>
                                <tr>
                                  <td><?php global $c; $c=$c+1; echo $c;?></td>
                                  <td><?php echo $item->nombre_menu?></td>
                                  <td><a href="<?php echo base_url();?>administrador/AsignacionMenu/deleteAsignacionMenu/<?php echo $item->id;?>/<?php echo $id_rol?>"
                                     class="btn btn-danger btn-remove">
                                       Quitar
                                  </a></td>
                                </tr>
                              <?php endforeach;?>
                            <?php endif;?>
                            </table>
                          </div>
                    </div>
                    <div class="panel-footer" aling="center">
                      <p align="center">
                        <a href="<?php echo base_url();?>administrador/Rol" type="submit" class="btn btn-primary btn-flat btn-guardar">Volver</a>
                      </p>
                    </div>
                  </div>
                </div>
            </div>
          </div>
      </div>
  </section>



  <script type="text/ng-template" id="agregar.html">
      <div class="modal-primary">
          <div class="modal-header">
              <h4 class="modal-title">{{titulo}}</h4>
              <button class="close" type="button" ng-click="cerrar()">
              <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12">
                      <form class="form-horizontal" name="form">
                          <div class="form-group">
                              <label class="control-label">Rol:</label>
                              <input type="text" class="form-control" name="rol" ng-model="rol" placeholder="Rol..." required>
                              <div ng-show="form.rol.$touched">
                                  <span class="text-help text-danger" ng-show="form.rol.$error.required">(*) Valor requerido</span>
                                  <span class="text-help text-danger" ng-show="form.rol.$error.pattern">(*) Solo texto</span>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
          <div class="modal-footer">
              <div class="row">
                  <button class="btn btn-success" ng-disabled="!form.$valid || form.$pending" ng-click="registrar(rol)"> Registrar</button>
                  <a class="btn btn-default" ng-click="cerrar()"> Cancelar</a>
              </div>
          </div>
      </div>
  </script>

</div>




<!--angularjs :) -->
<script src="<?php echo base_url();?>assets/angular.min.js"></script>
<script src="<?php echo base_url();?>assets/ui-bootstrap.min.js"></script>

<script>
let ngApp = function(){
  'use strict';
  let app = angular.module('app', ['ui.bootstrap']);
  app.controller('ctrl', function($scope, $http, $uibModal) {

    //lstar roles


    $scope.id_rol='19';

    $scope.agregarRol = () => {
        $uibModal.open({
          templateUrl: 'agregar.html',
          backdrop: 'static',
          scope:$scope,
          controller: function ($scope,$uibModalInstance) {
              $scope.titulo='Nuevo Registro';

              $scope.registrar = (rol)=> {

                let req = {
                   method: 'GET',
                   url: '<?php echo base_url('administrador/Rol/saveRol?rol='); ?>'+rol,
                   //data: { rol:rol }
                }

                $http(req).then(function(response) {
                  $scope.cerrar();
                  location.reload();
                });
              };

              $scope.cerrar = () => {
                  $uibModalInstance.dismiss();
              };
          }
        });
    };

    console.log('funciona');
  });
}();
</script>
<!---->
