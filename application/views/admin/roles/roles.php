
<!-- dom para angular-->
<div class="content-wrapper" ng-app="app" ng-controller="ctrl">
  <section class="content-header">
    <h1>
        ROLES
        <small>Para el Proyecto</small>
    </h1>

  </section>
  <section class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                  <div class="panel panel-info">
                    <div class="panel-heading">
                      <h3 class="panel-title">LISTA ROLES</h3>
                    </div>
                    <div class="panel-body">
                      <div class="row">
                          <div class="col-md-12">
                            <div class="row">
                              <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="Buscar..." ng-model="buscar">
                              </div>
                              <div class="col-md-6">
                                <button ng-click="amRol(undefined)" class="btn btn-success btn-flat pull-right" data-toggle="modal" data-target="#dialogoSeccion">
                                    <span class="fa fa-plus">
                                    </span> Nuevo Rol
                                </button>
                              </div>
                            </div>
                            <table class="table table-bordered striped">
                              <tr>
                                <th>Numero</th>
                                <th>nombre_menu</th>
                                <th>Opcion</th>
                              </tr>
                              <tr ng-repeat="item in roles | filter:buscar">
                                <td>{{$index + 1}}</td>
                                <td>{{item.nombre}}</td>
                                <td>
                                  <a href="<?php echo base_url();?>administrador/AsignacionMenu/index/{{item.id_rol}}" class="btn btn-info"><span class="fa fa-list"></span></a>
                                  <a ng-click="amRol(item)" class="btn btn-warning"><span class="fa fa-pencil"></span></a>
                                  <a ng-click="eliminar(item)" class="btn btn-danger btn-remove"><span class="fa fa-remove"></span></a>
                                </td>
                                </td>
                              </tr>
                            </table>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
      </div>
  </section>

<script type="text/ng-template" id="agregar.html">
  <div>
      <div class="box box-primary">
          <div class="modal-header box-header with-border text-center">
              <h4 class="modal-title box-title">{{titulo}}</h4>
              <button class="close" type="button" ng-click="cerrar()">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body box-body">
              <form name="form">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group">
                        <label class="control-label">Rol:</label>
                        <input type="text" class="form-control" name="rol" ng-model="rol.nombre" placeholder="Rol..." required>
                        <div ng-show="form.rol.$touched">
                            <span class="text-help text-danger" ng-show="form.rol.$error.required">(*) Valor requerido</span>
                            <span class="text-help text-danger" ng-show="form.rol.$error.pattern">(*) Solo texto</span>
                        </div>
                    </div>
                  </div>
                </div>
              </form>
          </div>
          <div class="modal-footer box-footer text-center">
              <button class="btn btn-success" ng-show="estado" ng-disabled="!form.$valid || form.$pending" ng-click="registrar(rol)"> Registrar</button>
              <button class="btn btn-warning" ng-show="!estado" ng-disabled="!form.$valid || form.$pending" ng-click="editar(rol)"> Editar</button>
              <a class="btn btn-default" ng-click="cerrar()"> <i class="fa fa-times"></i> Cerrar </a>
          </div>
          </div>
      </div>
  </div>
</script>

<script type="text/ng-template" id="eliminar.html">
  <div>
      <div class="box box-danger">
          <div class="modal-header box-header with-border text-center">
              <h4 class="modal-title box-title">{{titulo}}</h4>
              <button class="close" type="button" ng-click="cerrar()">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body box-body">
              <form name="form">
                <div class="row">
                  <div class="col-lg-12">
                    <p class="text-center">{{mensaje}}</p>
                  </div>
                </div>
              </form>
          </div>
          <div class="modal-footer box-footer text-center">
              <button class="btn btn-danger" ng-click="confirmar(rol)"> Confirmar</button>
              <a class="btn btn-default" ng-click="cerrar()"> <i class="fa fa-times"></i> Cerrar </a>
          </div>
      </div>
  </div>
</script>

</div>




<!--angularjs :) -->
<script src="<?php echo base_url();?>assets/angular.min.js"></script>
<script src="<?php echo base_url();?>assets/ui-bootstrap.min.js"></script>

<script>
let ngApp = function(){
  'use strict';
  let app = angular.module('app', ['ui.bootstrap']);
  app.controller('ctrl', function($scope, $http, $uibModal) {

    $scope.listarRoles=()=>{
      $http.get("<?php echo base_url('administrador/Rol/listarRoles'); ?>").then(function(response) {
        $scope.roles = response.data;
      });
    }
    $scope.listarRoles();

    $scope.amRol = (rol) => {
        $uibModal.open({
          templateUrl: 'agregar.html',
          backdrop: 'static',
          scope:$scope,
          controller: function ($scope,$uibModalInstance) {
              if(rol===undefined){
                $scope.titulo='Nuevo Registro';
                $scope.estado = true;
                $scope.rol = [];
              }else{
                $scope.titulo='Editar Registro';
                $scope.rol = rol;
                $scope.estado = false;
              }

              $scope.registrar = (rol)=> {
                let req = {
                   method: 'GET',
                   url: '<?php echo base_url('administrador/Rol/saveRol?rol='); ?>'+rol.nombre,
                   //data: { rol:rol }
                }
                $http(req).then(function(response) {
                  $scope.cerrar();
                  $scope.listarRoles();
                });
              };

              $scope.editar = (rol)=> {
                let ruta = '<?php echo base_url('administrador/Rol/editRol?id=');?>'+rol.id_rol+'&nombre='+rol.nombre;
                let req = {
                   method: 'GET',
                   url: ruta
                   //data: { rol:rol }
                }
                $http(req).then(function(response) {
                  $scope.cerrar();
                  $scope.listarRoles();
                });
              };

              $scope.cerrar = () => {
                  $uibModalInstance.dismiss();
              };
          }
        });
    };


    $scope.eliminar =(rol)=>{
      $uibModal.open({
        templateUrl: 'eliminar.html',
        backdrop: 'static',
        scope:$scope,
        controller: function ($scope,$uibModalInstance) {

            $scope.titulo='Eliminar Registro';
            $scope.mensaje = `Esta seguro que desea eliminar el registro: ${rol.nombre}`;

            $scope.confirmar =()=>{
              let ruta = '<?php echo base_url('administrador/Rol/deleteRol?id_rol=');?>'+rol.id_rol;
              let req = {
                 method: 'GET',
                 url: ruta
                 //data: { rol:rol }
              }
              $http(req).then(function(response) {
                $scope.cerrar();
                $scope.listarRoles();
              });
            }

            $scope.cerrar = () => {
                $uibModalInstance.dismiss();
            };
        }
      });

    };

    console.log('funciona');
  });
}();
</script>
<!---->
