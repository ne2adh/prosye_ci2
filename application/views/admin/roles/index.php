<!-- =============================================== -->
                 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper"  ng-app="app" ng-controller="ctrl">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Secciones
                <small>y Menus</small>
                </h1>                
            </section>
            <!-- Main content -->
            <section class="content">                
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">                        
                        <div  class="row">
                            <div align="center" class="col-md-12" >
                                <h2>Menu General</h2>      
                            </div>    
                        </div>                       
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-flat" data-toggle="modal" data-target="#dialogoSeccion">
                                    <span class="fa fa-plus">
                                    </span>Agregar Seccion
                                </button>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="panel-group">
                                    <div class="panel panel-default" ng-repeat="seccion in lista_seccion">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h4 class="panel-title">
                                                        <i class="{{seccion.logo}}"></i><a data-toggle="collapse" href="#seccion{{seccion.id}}">{{seccion.nombre}}</a>                                               
                                                        
                                                    </h4>                                                
                                                </div>
                                                <div class="col-md-4"></div>
                                                <div class="col-md-4">
                                                    <button class="btn btn-warning" ng-show="seccion.menu.length==0" ng-click="eliminarSeccion(seccion.id)"><i class="glyphicon glyphicon-trash"></i></button>
                                                </div>
                                            </div>
                                            
                                           
                                        </div>
                                        <div id="seccion{{seccion.id}}" class="panel-collapse collapse">
                                            <div class="row">                                                
                                                <div class="col-md-12">
                                                    <div class="text-center">
                                                        <button class="btn btn-default" ng-click="abrirDialogo(seccion.id)"><i class="glyphicon glyphicon-plus"></i></button>
                                                    </div>                                                    
                                                    <ul class="list-group">
                                                        <li class="list-group-item" ng-repeat="menu in seccion.menu">
                                                            <div class="row">
                                                                <div class="col-md-2">
                                                                    <i class="{{menu.logo}}"></i>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    {{menu.nombre_menu}}
                                                                </div>
                                                                <div class="col-md-4">
                                                                    {{menu.direccion}}
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <button class="btn btn-warning btn-small"><i class="glyphicon glyphicon-wrench"></i></button>
                                                                    <button type="button" ng-click="eliminarMenu(menu.id)"class="btn btn-danger btn-small"><i class="glyphicon glyphicon-trash"></i></button>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>                                       
                                                </div>                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>                              
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    </div>                       
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        
        <div class="modal fade" id="dialogoSeccion" role="dialog">
                <div class="modal-dialog">
                
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header" style="padding:35px 50px;">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4>Registrar Seccion</h4>
                        </div>
                        <div class="modal-body" style="padding:40px 50px;">
                        <form role="form"  ng-submit="enviar()" name="formulario" novalidate>
                            <div class="form-group">
                                 <div class="btn-group  btn-group-justified">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" style="width:auto;">
                                        Seleccione un Icono <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu list-inline" role="menu">
                                        <li ng-repeat="avatar in avatars" ng-click="avatarSelected(avatar)">
                                            <span ng-class="avatar"></span>
                                        </li>
                                    </ul>
                                </div>
                                Icono Seleccionado: <i ng-class="icono"></i>
                                <div class="alert alert-warning" ng-show="verificar && isEmpty(icono)">
                                    Icono es Requerido
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Nombre Seccion</label>
                                <input type="text" class="form-control" ng-model="nombre" id="nombre" name="nombre" placeholder="Nombre de la Seccion" required/>
                                <div class="alert alert-warning" ng-show="verificar && isEmpty(nombre)">
                                    Nombre es Requerido
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Orden</label>
                                <input type="number"  class="form-control"  ng-model="prioridad" id="prioridad" name="prioridad" min="0">
                                <div class="alert alert-warning" ng-show="verificar && isEmpty(prioridad)">
                                    Prioridad es Requerido
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success btn-block" ng-disabled="formulario.nombre.$dirty && formulario.nombre.$invalid"><span class="glyphicon glyphicon-off"></span>Registrar</button>
                        </form>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
        </div>

        <div class="modal fade" id="dialogoMenu" role="dialog">
                <div class="modal-dialog">
                
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header" style="padding:35px 50px;">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4>Agregar Menu</h4>                           
                        </div>
                        <div class="modal-body" style="padding:40px 50px;">
                        <form role="form"  ng-submit="registrarMenu()" name="formularioMenu" novalidate>
                            <div class="form-group">
                                 <div class="btn-group  btn-group-justified">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" style="width:auto;">
                                        Seleccione un Icono <span class="caret"></span>
                                    </button>
                                    
                                    <ul class="dropdown-menu list-inline" role="menu">
                                        <li ng-repeat="ic in iconosMenu" ng-click="menuSelected(ic)">
                                            <span class="{{ic}}"></span>
                                        </li>
                                    </ul>
                                </div>
                                Icono Seleccionado: <i ng-class="iconoMenu"></i>
                                <div class="alert alert-warning" ng-show="verificarMenu && isEmpty(iconoMenu)">
                                    Icono es Requerido
                                </div>
                                
                            </div>

                            <div class="form-group">
                                <label>Nombre Menu</label>
                                <input type="text" class="form-control" ng-model="nombreMenu" id="nombreMenu" name="nombreMenu" placeholder="Nombre del Menu" required/>
                                <div class="alert alert-warning" ng-show="verificarMenu && isEmpty(nombreMenu)">
                                    Nombre del Menu es Requerido
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Url del Controlador</label>
                                <input type="text" class="form-control" ng-model="url" id="url" name="url" placeholder="Url del Controlador" required/>
                                <div class="alert alert-warning" ng-show="verificarMenu && isEmpty(url)">
                                    Url del Controlador es Requerido
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Prioridad</label>
                                <input type="number"  class="form-control"  ng-model="prioridadMenu" id="prioridadMenu" name="prioridadMenu" min="0">
                                <div class="alert alert-warning" ng-show="verificarMenu && isEmpty(prioridadMenu)">
                                    Prioridad es Requerido
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success btn-block" ><span class="glyphicon glyphicon-off"></span>Registrar</button>
                            
                        </form>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
        </div>
</div>
<!--angularjs :) -->
<script src="<?php echo base_url();?>assets/angular.min.js"></script>
<script src="<?php echo base_url();?>assets/ui-bootstrap.min.js"></script>
<script>
let ngApp = function(){
    'use strict';

    let app = angular.module('app', ['ui.bootstrap']);        
    app.controller('ctrl', function($scope, $http) {
        $scope.lista_seccion = <?php echo json_encode($seccion);?>;
        $scope.avatars=["glyphicon glyphicon-asterisk", "glyphicon glyphicon-plus", "glyphicon glyphicon-minus", "glyphicon glyphicon-euro",
                        "glyphicon glyphicon-cloud", "glyphicon glyphicon-envelope", "glyphicon glyphicon-pencil", "glyphicon glyphicon-glass",
                        "glyphicon glyphicon-music", "glyphicon glyphicon-search", "glyphicon glyphicon-heart", "glyphicon glyphicon-star",
                        "glyphicon glyphicon-star-empty", "glyphicon glyphicon-user", "glyphicon glyphicon-film", "glyphicon glyphicon-th-large",
                        "glyphicon glyphicon-th", "glyphicon glyphicon-th-list", "glyphicon glyphicon-ok", "glyphicon glyphicon-remove", "glyphicon glyphicon-zoom-in",
                        "glyphicon glyphicon-zoom-out", "glyphicon glyphicon-off", "glyphicon glyphicon-signal", "glyphicon glyphicon-cog", "glyphicon glyphicon-trash",
                        "glyphicon glyphicon-home", "glyphicon glyphicon-file", "glyphicon glyphicon-time", "glyphicon glyphicon-road", "glyphicon glyphicon-download-alt",
                        "glyphicon glyphicon-download", "glyphicon glyphicon-upload", "glyphicon glyphicon-inbox", "glyphicon glyphicon-play-circle", "glyphicon glyphicon-repeat",
                        "glyphicon glyphicon-refresh", "glyphicon glyphicon-list-alt", "glyphicon glyphicon-lock", "glyphicon glyphicon-flag", "glyphicon glyphicon-headphones", 
                        "glyphicon glyphicon-volume-off", "glyphicon glyphicon-volume-down", "glyphicon glyphicon-volume-up", "glyphicon glyphicon-qrcode", "glyphicon glyphicon-barcode",
                        "glyphicon glyphicon-tag", "glyphicon glyphicon-tags", "glyphicon glyphicon-book", "glyphicon glyphicon-bookmark", "glyphicon glyphicon-print", "glyphicon glyphicon-camera",
                        "glyphicon glyphicon-font", "glyphicon glyphicon-bold", "glyphicon glyphicon-italic", "glyphicon glyphicon-text-height", "glyphicon glyphicon-text-width", 
                        "glyphicon glyphicon-align-left", "glyphicon glyphicon-align-center", "glyphicon glyphicon-align-right", "glyphicon glyphicon-align-justify", "glyphicon glyphicon-list",
                        "glyphicon glyphicon-indent-left", "glyphicon glyphicon-indent-right", "glyphicon glyphicon-facetime-video", "glyphicon glyphicon-picture", "glyphicon glyphicon-map-marker",
                        "glyphicon glyphicon-adjust", "glyphicon glyphicon-tint" , "glyphicon glyphicon-edit", "glyphicon glyphicon-share", "glyphicon glyphicon-check", 
                        "glyphicon glyphicon-move", "glyphicon glyphicon-step-backward", "glyphicon glyphicon-fast-backward", "glyphicon glyphicon-backward", "glyphicon glyphicon-play",
                        "glyphicon glyphicon-pause", "glyphicon glyphicon-stop", "glyphicon glyphicon-forward", "glyphicon glyphicon-fast-forward", "glyphicon glyphicon-step-forward", "glyphicon glyphicon-eject",
                        "glyphicon glyphicon-chevron-left", "glyphicon glyphicon-chevron-right", "glyphicon glyphicon-plus-sign", "glyphicon glyphicon-minus-sign", "glyphicon glyphicon-remove-sign",
                        "glyphicon glyphicon-ok-sign", "glyphicon glyphicon-question-sign", "glyphicon glyphicon-info-sign", "glyphicon glyphicon-screenshot", "glyphicon glyphicon-remove-circle", "glyphicon glyphicon-ok-circle",
                        "glyphicon glyphicon-ban-circle", "glyphicon glyphicon-arrow-left", "glyphicon glyphicon-arrow-right", "glyphicon glyphicon-arrow-up", "glyphicon glyphicon-arrow-down", "glyphicon glyphicon-share-alt",
                        "glyphicon glyphicon-resize-full", "glyphicon glyphicon-resize-small" , "glyphicon glyphicon-exclamation-sign", "glyphicon glyphicon-gift", "glyphicon glyphicon-leaf",
                        "glyphicon glyphicon-fire", "glyphicon glyphicon-eye-open", "glyphicon glyphicon-eye-close", "glyphicon glyphicon-warning-sign", "glyphicon glyphicon-plane", "glyphicon glyphicon-calendar", "glyphicon glyphicon-random",
                        "glyphicon glyphicon-comment", "glyphicon glyphicon-magnet", "glyphicon glyphicon-chevron-up", "glyphicon glyphicon-chevron-down", "glyphicon glyphicon-retweet", "glyphicon glyphicon-shopping-cart",
                        "glyphicon glyphicon-folder-close", "glyphicon glyphicon-folder-open", "glyphicon glyphicon-resize-vertical", "glyphicon glyphicon-resize-horizontal", "glyphicon glyphicon-hdd", "glyphicon glyphicon-bullhorn",
                        "glyphicon glyphicon-bell", "glyphicon glyphicon-certificate", "glyphicon glyphicon-thumbs-up", "glyphicon glyphicon-thumbs-down", "glyphicon glyphicon-hand-right",
                        "glyphicon glyphicon-hand-left", "glyphicon glyphicon-hand-up", "glyphicon glyphicon-hand-down", "glyphicon glyphicon-circle-arrow-right", "glyphicon glyphicon-circle-arrow-left", "glyphicon glyphicon-circle-arrow-up",
                        "glyphicon glyphicon-circle-arrow-down", "glyphicon glyphicon-globe", "glyphicon glyphicon-wrench", "glyphicon glyphicon-tasks", "glyphicon glyphicon-filter",
                        "glyphicon glyphicon-briefcase", "glyphicon glyphicon-fullscreen",  "glyphicon glyphicon-dashboard", "glyphicon glyphicon-paperclip", "glyphicon glyphicon-heart-empty",
                        "glyphicon glyphicon-link", "glyphicon glyphicon-phone", "glyphicon glyphicon-pushpin", "glyphicon glyphicon-usd", "glyphicon glyphicon-gbp", "glyphicon glyphicon-sort",
                        "glyphicon glyphicon-sort-by-alphabet", "glyphicon glyphicon-sort-by-alphabet-alt", "glyphicon glyphicon-sort-by-order", "glyphicon glyphicon-sort-by-order-alt", "glyphicon glyphicon-sort-by-attributes",
                        "glyphicon glyphicon-sort-by-attributes-alt", "glyphicon glyphicon-unchecked", "glyphicon glyphicon-expand", "glyphicon glyphicon-collapse-down", "glyphicon glyphicon-collapse-up", "glyphicon glyphicon-log-in",
                        "glyphicon glyphicon-flash", "glyphicon glyphicon-log-out", "glyphicon glyphicon-new-window", "glyphicon glyphicon-record", "glyphicon glyphicon-save", "glyphicon glyphicon-open", "glyphicon glyphicon-saved",
                        "glyphicon glyphicon-import", "glyphicon glyphicon-export", "glyphicon glyphicon-send", "glyphicon glyphicon-floppy-disk", "glyphicon glyphicon-floppy-saved", "glyphicon glyphicon-floppy-remove", "glyphicon glyphicon-floppy-save",
                        "glyphicon glyphicon-floppy-open", "glyphicon glyphicon-credit-card", "glyphicon glyphicon-transfer", "glyphicon glyphicon-cutlery", "glyphicon glyphicon-header", "glyphicon glyphicon-compressed", "glyphicon glyphicon-earphone",
                        "glyphicon glyphicon-phone-alt", "glyphicon glyphicon-tower", "glyphicon glyphicon-stats", "glyphicon glyphicon-sd-video", "glyphicon glyphicon-hd-video", "glyphicon glyphicon-subtitles", "glyphicon glyphicon-sound-stereo",
                        "glyphicon glyphicon-sound-dolby", "glyphicon glyphicon-sound-5-1", "glyphicon glyphicon-sound-6-1", "glyphicon glyphicon-sound-7-1", "glyphicon glyphicon-copyright-mark", "glyphicon glyphicon-registration-mark",
                        "glyphicon glyphicon-cloud-download", "glyphicon glyphicon-cloud-upload", "glyphicon glyphicon-tree-conifer", "glyphicon glyphicon-tree-deciduous", "glyphicon glyphicon-cd", "glyphicon glyphicon-save-file",
                        "glyphicon glyphicon-open-file", "glyphicon glyphicon-level-up", "glyphicon glyphicon-copy", "glyphicon glyphicon-paste", "glyphicon glyphicon-alert", "glyphicon glyphicon-equalizer", "glyphicon glyphicon-king", "glyphicon glyphicon-queen",
                        "glyphicon glyphicon-pawn", "glyphicon glyphicon-bishop", "glyphicon glyphicon-knight", "glyphicon glyphicon-baby-formula", "glyphicon glyphicon-tent", "glyphicon glyphicon-blackboard", "glyphicon glyphicon-bed", "glyphicon glyphicon-apple",
                        "glyphicon glyphicon-erase", "glyphicon glyphicon-hourglass", "glyphicon glyphicon-lamp", "glyphicon glyphicon-duplicate", "glyphicon glyphicon-piggy-bank", "glyphicon glyphicon-scissors", "glyphicon glyphicon-bitcoin", "glyphicon glyphicon-yen", "glyphicon glyphicon-ruble",
                        "glyphicon glyphicon-scale", "glyphicon glyphicon-ice-lolly", "glyphicon glyphicon-ice-lolly-tasted", "glyphicon glyphicon-education", "glyphicon glyphicon-option-horizontal", "glyphicon glyphicon-option-vertical", "glyphicon glyphicon-menu-hamburger",
                        "glyphicon glyphicon-modal-window", "glyphicon glyphicon-oil", "glyphicon glyphicon-grain", "glyphicon glyphicon-sunglasses", "glyphicon glyphicon-text-size", "glyphicon glyphicon-text-color", "glyphicon glyphicon-text-background", "glyphicon glyphicon-object-align-top",
                        "glyphicon glyphicon-object-align-bottom", "glyphicon glyphicon-object-align-horizontal", "glyphicon glyphicon-object-align-left", "glyphicon glyphicon-object-align-vertical", "glyphicon glyphicon-object-align-right", "glyphicon glyphicon-triangle-right", "glyphicon glyphicon-triangle-left",
                        "glyphicon glyphicon-triangle-bottom", "glyphicon glyphicon-triangle-top", "glyphicon glyphicon-superscript", "glyphicon glyphicon-subscript", "glyphicon glyphicon-menu-left", "glyphicon glyphicon-menu-right", "glyphicon glyphicon-menu-down", "glyphicon glyphicon-menu-up"];
        $scope.icono = '';
        $scope.avatarSelected=function(avatar){            
            $scope.icono = avatar;
        };

        $scope.getAvatar=function(avatar){
            return avatar;
        };

        $scope.verificar = false;
        $scope.isEmpty = function(texto){
            return texto==='' || texto===undefined || texto===null;
        }
        
        $scope.enviar = function () {
            $scope.verificar = true;    
            if(!$scope.isEmpty($scope.nombre) && !$scope.isEmpty($scope.icono) && !$scope.isEmpty($scope.prioridad)){
                
                var req = {
                method: 'POST',
                url: "<?php echo base_url('administrador/seccion/saveSeccion'); ?>",
                data: { 
                        nombre: $scope.nombre,
                        logo: $scope.icono,
                        prioridad: $scope.prioridad                    
                    }
                };

                $http(req).then(function(){
                    //Seccion registrada
                    alert("Seccion registrada");
                    //Recargar secciones
                    $http.get("<?php echo base_url('administrador/seccion/listSeccion'); ?>").then(function(response) {                        
                        $scope.lista_seccion = response.data;
                        $scope.nombre = null;
                        $scope.icono = null;
                        $scope.prioridad  = null;
                        $scope.verificar = false;
                        $('#dialogoSeccion').modal('hide');
                    });                    
                });
            }
        }

        $scope.iconosMenu = ["fa fa-circle-o text-yellow", "fa fa-circle-o text-purple", "fa fa-circle-o text-muted",
                             "fa fa-circle-o text-light-blue", "fa fa-circle-o text-red", "fa fa-circle-o text-teal",
                             "fa fa-circle-o text-lime", "fa fa-circle-o text-blue", "fa fa-circle-o text-green", 
                             "fa fa-circle-o text-aqua",];
        
        $scope.verificarMenu = false;
        $scope.iconoMenu = '';
        $scope.menuSelected=function(ic){
            $scope.iconoMenu = ic;
        };
        $scope.abrirDialogo = function(id_seccion){
            $('#dialogoMenu').modal('show');
            $scope.idSeccion = id_seccion;
        };

        $scope.registrarMenu = function(){
            $scope.verificarMenu = true;
            if(!$scope.isEmpty($scope.nombreMenu) && !$scope.isEmpty($scope.iconoMenu) && !$scope.isEmpty($scope.prioridadMenu) && !$scope.isEmpty($scope.url)){
                
                var req = {
                    method: 'POST',
                    url: "<?php echo base_url('administrador/menu/saveMenu'); ?>",
                    data: {
                        nombre_menu: $scope.nombreMenu,
                        direccion: $scope.url,
                        id_seccion: $scope.idSeccion,
                        tipo_menu: $scope.prioridadMenu, 
                        logo: $scope.iconoMenu
                    }
                };

                $http(req).then(function(){
                    //Seccion registrada
                    alert("Menu registrado");
                    //Recargar secciones
                    $http.get("<?php echo base_url('administrador/seccion/listSeccion'); ?>").then(function(response) {                        
                        $scope.lista_seccion = response.data;
                        $scope.nombre = null;
                        $scope.icono = null;
                        $scope.prioridad  = null;
                        $scope.verificar = false;                        
                        $('#dialogoMenu').modal('hide');
                    });
                });
            }
        };

        $scope.eliminarMenu = function(idMenu){
            let r = confirm("Desea eliminar este Menu?");
            if (r == true) {                
                var req = {
                    method: 'POST',
                    url: "<?php echo base_url('administrador/menu/deleteMenu'); ?>",
                    data: {
                        idMenu: idMenu
                    }
                };

                $http(req).then(function(response){
                    //Seccion registrada
                    alert("Menu Eliminado");
                    //Recargar secciones                    
                    $http.get("<?php echo base_url('administrador/seccion/listSeccion'); ?>").then(function(response) {                        
                        $scope.lista_seccion = response.data;                        
                    });
                });
            }            
        };

        $scope.eliminarSeccion=function(idSeccion){
            let r = confirm('Desea eliminar la sección?');
            if(r==true){
                var req = {
                    method: 'POST',
                    url: "<?php echo base_url('administrador/seccion/deleteSeccion'); ?>",
                    data: {
                        idSeccion: idSeccion
                    }
                };

                $http(req).then(function(response){
                    //Seccion registrada
                    alert("Seccion Eliminada");
                    //Recargar secciones                    
                    $http.get("<?php echo base_url('administrador/seccion/listSeccion'); ?>").then(function(response) {                        
                        $scope.lista_seccion = response.data;                        
                    });
                });
            }
        }
    });
}();

</script>
