
        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Proyectos
                <small>reporte Sisin</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="row">
                              <div class="col-md-12">


                              <table id="example" name="example" width="100%" border="1" cellpadding="1" cellspacing="0">
                              <thead>
                              <tr>
                                  <td rowspan="2" colspan="8" align="center"><FONT FACE="impact" SIZE="6">FICHA DE SEGUIMIENTO Y EVALUACION DE PROGRAMAS/PROYECTOS (SyE)</FONT></td><td align="center" colspan="2">MES/AÑO</td>      
                              </tr>
                              <tr >
                                  <td  colspan="2" align="center" height="20"> </td>
                              </tr>
                              </thead>
                              <tbody>
                              <tr>
                                  <td colspan="2" width="20%" >NOMBRE DEL PROYECTO / PROGRAMA</td><td colspan="5" align="center" width="50%"></td><td colspan="2" width="20%">CUENTA CON CONVENIO DE COFINANCIAMIENTO (SI/NO)</td><td align="center" width="10%"></td>
                              </tr>
                              <tr>
                                  <td colspan="2" width="20%">CODIGO SISIN</td><td colspan="2" width="20%"></td><td width="10%" align="center">FASE <br>(PREINV/EJEC.)</td><td colspan="3" width="30%" align="center"></td><td width="10%" align="center"> MODALIDAD<br>DE ADM.</td><td width="10%" align="center"></td>
                              </tr>
                              <tr>
                                  <td colspan="2" width="20%">PROGRAMA(PARA PROYECTOS):<br>CAT. PROG (PARA PROGRAMAS):</td><td colspan="2" align="center"></td><td align="center">MES-AÑO<br>INIC. / CONCL.</td><td colspan="2" align="center"></td><td align="center"></td><td align="center">TIEMPO EJEC.<br>INIC.</td><td align="center"></td>
                              </tr>
                              <tr>
                                  <td colspan="2">SECRETARÍA DEPTAL.<br> DE TUICION</td><td colspan="8" align="center"></td>
                              </tr>
                              <tr >
                                  <td colspan="2">DIRECCIÓN / SERVICIO / UNIDAD<br>Y/O AREA EJECUTORA</td><td colspan="8" align="center"></td>
                              </tr>
                              <tr>
                                <td colspan="4">DATOS DE LA EMPRESA EJECUTORA (*)</td><td colspan="2">COSTO ADJUDICADO</td><td>FECHA INICIO<br>(s/Ord.Preced.)</td><td>FECHA CONCL.<br>(s/Contrato)</td><td colspan="2">TIPO DE GARANTÍA<br>(INCL./CODIGO)</td>
                              </tr>
                              <tr>  
                                <td colspan="4">CONSTRUCTORA (OBRA)</td><td colspan="2" align="center"></td><td></td><td></td><td colspan="2"></td>
                              </tr>
                              <tr>
                                <td colspan="4">SUPERVISIÓN</td><td colspan="2" align="center"></td><td></td><td></td><td colspan="2"></td>
                              </tr>
                              <tr>  
                                <td colspan="4">ACOMPAÑAMIENTO</td><td colspan="2" align="center"></td><td></td><td></td><td colspan="2"></td>
                              </tr>
                              <tr>
                                <td colspan="4">AUDITORÍA</td><td colspan="2" align="center"></td><td></td><td></td><td colspan="2"></td>
                              </tr>
                              <tr>
                                  <td colspan="10" align="center">DESCRIPCIÓN TÉCNICA DEL PROGRAMA/PROYECTO</td>
                              </tr>
                              <tr>
                                  <td colspan="10" height="50"></td>
                              </tr>
                              <tr>
                                  <td colspan="5" align="center">OBJETIVO DEL PROGRAMA/PROYECTO</td><td colspan="5" align="center">OBJETIVO DE GESTIÓN</td>
                              </tr>
                              <tr>
                                  <td colspan="5" height="50"></td><td colspan="5" height="50"></td>
                              </tr>
                              <tr>
                                  <td colspan="10" align="center">LOCALIZACION DEL PROYECTO</td>
                              </tr>
                              <tr>
                                  <td colspan="2" align="center">PROVINCIA(S)</td><td colspan="3"></td><td colspan="2" align="center">MUNICIPIO(S)</td><td colspan="4"></td>
                              </tr>
                              <tr>
                                  <td colspan="2">LOCALIDAD(ES)<br>COMUNIDAD(ES)</td><td colspan="8"></td>
                              </tr>
                              <tr>
                                <td colspan="2" rowspan="2">COSTO DEL PROYECTO</td><td rowspan="2">INICAL</td><td rowspan="2" align="center"><font> INCREMENTO AL COSTO</font></td><td rowspan="2">COSTO TOTAL<br>ACTUALIZADO</td><td rowspan="2">COSTO<br>EJECUTADO</td><td rowspan="2">COSTO POR<br>EJECUTAR</td><td colspan="3">TIEMPO DE<br>EJECUCIÓN (FECHAS)</td>
                              </tr>
                              <tr>
                                <td>FECHA DE<br>CONCL. INIC.</td><td>INCREMENTO<br>DIAS</td><td>FECHA DE<br>CONCL.ACT.</td>
                              </tr>
                              <tr>
                                  <td colspan="2" align="center">CONSTRUCCIÓN</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td>
                              </tr>
                              <tr>
                                  <td colspan="2" align="center">SUPERVISIÓN (ADM. DIRECTA O DELEGADA)</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td>
                              </tr>
                              <tr>
                                  <td colspan="2" align="center">AUDITORÍA</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td>
                              </tr>
                              <tr>
                                  <td colspan="2" align="center">TOTALES</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td>
                              </tr>
                              <tr>
                                  <td colspan="4" align="center">SEGUIMIENTO FISICO</td><td colspan="3" align="center">SEGUIMIENTO FINANCIERO</td><td colspan="3" align="center">PROGRAMACION PRESUP.</td>
                              </tr>
                              <tr>
                                  <td colspan="3" align="center">AVANCE FÍSICO DE LA GESTIÓN (%)</td><td></td><td colspan="2" align="center">EJECUCIÓN FINANCIERA<br>DE GESTION (Bs.)</td><td></td><td colspan="2" align="center">PLANILLAS PROGRAMADAS EN<br> LA GESTION - OBRA</td><td></td>
                              </tr>
                              <tr>
                                  <td colspan="3" align="center">AVANCE FÍSICO EJECUTADO ACUMULADO/HISTORICO (%)</td><td></td><td colspan="2" align="center">EJECUCION FINANCIERA ACUMULADO/HISTORICO (Bs)</td><td></td><td colspan="2" align="center">PLANILLAS PROGRAMADAS EN LA GESTION - SUPERVISION Y ACOMPAÑAMIENTO</td><td></td>
                              </tr>
                              <tr>
                                  <td colspan="10" height="6"></td>
                              </tr>
                              <tr>
                                  <td colspan="2" height="45">RESULTADOS DEL<br>MES/SITUACION ACTUAL DEL<br>PROGRAMA/PROYECTO</td><td colspan="8"></td>
                              </tr>
                              <tr>
                                  <td colspan="2" height="45">PROBLEMAS IDENTIFICADOS<br>DEL MES</td><td colspan="8"></td>
                              </tr>
                              <tr>
                                  <td colspan="2" height="45">ACCIONES DE SOLUCION</td><td colspan="8"></td>
                              </tr>
                              <tr>
                                  <td colspan="2">FIRMA Y NOMBRE DEL<br>RESPONSABLE DEL<br>PROGRAMA/PROYECTO</td><td colspan="3"></td><td colspan="3">FIRMA Y NOMBRE DEL: Secretario Deptal.<br>Director, Jefe de Unidad, encargado de Area</td><td colspan="2"></td>
                              </tr>
                              <tr>
                                  <td colspan="2">FECHA DE ELABORACION:</td><td colspan="8"></td>
                              </tr>
                            </tbody>
                            </table> 
                         </div>
                        </div>                               
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
