
        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Proyectos
                <small>por provincias</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                     
                                <form action="<?php echo current_url(); ?>"  method="POST"> 
                                  <div class="col-md-8"> 
                                         <select class="form-control" name="buscador" >
                                                <option value="">Provincias</option>
                                                <?php  
                                                    foreach($provincias as $row)
                                                    {  
                                                    echo '<option value="'.$row->id_provincia.'">'.$row->nombre_provincia.'</option>';

                                                    }

                                                ?>
                                                <a href="<?php echo base_url();?>proyecto/mis_proyectos/por_provincia/<?php echo $row->id_provincia;?>">
                                         </select> 
                                    </div>            
                                    <?php /*<input id="buscar" name="buscar" type="search" placeholder="Buscar aquí..." autofocus >*/?>
                                   <input type="submit" value="Buscar" name='buscar' class="btn btn-primary">
                                    <a href="<?php echo base_url();?>proyecto/mis_proyectos/por_provincia" class="btn btn-danger">Restablecer</a>
                                </form>
                                 


                                     <div class="form-group">

                                        <?php /*<label form="buscador" >Municipio:</label>
                                            <select class="form-control"  id="municipios" name="municipios">
                                                <option value="">Municipios</option>
                                                <?php  
                                                    foreach($municipios as $row)
                                                    {  
                                                    echo '<option value="'.$row->id_provincia.'">'.$row->municipio.'</option>';
                                                    }
                                                ?>
                                            </select>     */?>              
                                            
                                    </div>
                             <table id="example" name="example" class="table table-bordered btn-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Nombre Provincia</th>
                                                        <th>Nombre Proyecto</th>
                                                        <th>Costo del Proyecto</th>
                                                        <th>Opciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                       <?php //$i=0;?>                                           
                                                         <?php  foreach($provincia as $result):?> 
                                                                <tr>
                                                                    <td><?php echo $result->nombre_provincia;?></td>
                                                                    <td><?php echo $result->nombre_proyecto;?></td>
                                                                    <td><?php echo $result->costo_proyecto;?></td>
                                                                    <td>
                                                                        <div class="btn-group">
                                                                            <a href="#" class="btn btn-info">
                                                                                <span class="fa fa-eye">                 
                                                                                </span>
                                                                            </a>
                                                                            <a href="#" class="btn btn-warning">
                                                                                <span class="fa fa-pencil">               
                                                                                </span>
                                                                            </a>
                                                                            
                                                                        </div>
                                                                    </td>
                                                                 <?php //$i=$i+$result->costo_proyecto;?>   
                                                           </tr>
                                                    <?php endforeach;?>  
                                                  
                                                </tbody>
                                </table>
                                <hr>
                                 <?php //echo 'El monto total de inversion en estos proyectos es => ', print_r($i); ?> 

                               
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
