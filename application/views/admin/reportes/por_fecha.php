
        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Proyectos
                <small>por fechas</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="row">
                              <form action="<?php echo current_url(); ?>" method="POST"> 
                                 <div class="from-horizontal">
                                    <label for="" class="col-md-1 control-label"> Desde:</label>
                                  <div class="col-md-3"> 
                                    <input type="date" class="form-control" name="fecha_inicio">
                                  </div>
                                  <label for="" class="col-md-1 control-label"> Hasta:</label>
                                  <div class="col-md-3">  
                                    <input type="date" class="form-control" name="fecha_conclusion">
                                  </div>            
                                   <div class="col-md-4">  
                                    <input type="submit" value="Buscar" name="buscar" class="btn btn-primary">
                                    <a href="<?php echo base_url();?>proyecto/mis_proyectos/por_fecha" class="btn btn-danger">Restablecer</a>  
                                  </div> 
                                </div>
                              </form>
                            <hr>
                        </div>
                        <div class="form-group">
                            <div class="row">
                              <div class="col-md-12">
                                 <table id="example" name="example" class="table table-bordered btn-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Codigo </th>
                                                        <th>Nombre Proyecto</th>
                                                        <th>Fecha Inicio</th>
                                                        <th>Opciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                       <?php $c=1;//$i=0;?>                                           
                                                         <?php  foreach($proyectos as $result):?> 
                                                                <tr>
                                                                    <td><?php //echo $result->id_proyecto;
                                                                     echo $c;?></td>
                                                                    <td><?php echo $result->nombre_proyecto;?></td>
                                                                    <td><?php echo $result->fecha_inicio;?></td>
                                                                    <td>
                                                                        <div class="btn-group">
                                                                            <a href="#" class="btn btn-info">
                                                                                <span class="fa fa-eye">                 
                                                                                </span>
                                                                            </a>
                                                                            <a href="#" class="btn btn-warning">
                                                                                <span class="fa fa-pencil">               
                                                                                </span>
                                                                            </a>
                                                                            
                                                                        </div>
                                                                    </td>
                                                                 <?php $c=$c+1; //$i=$i+$result->costo_proyecto;?>   
                                                           </tr>
                                                    <?php endforeach;?>  
                                                  
                                                </tbody>
                                  </table>
                                
                             </div>    <?php //echo 'El monto total de inversion en estos proyectos es => ', print_r($i); ?> 
                         </div>
                        </div>                               
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
