<!-- =============================================== -->

       <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
         <h1>Proyectos
         <small>reporte SyE</small>
         </h1>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-12 text-center">
              <button type="button" class="btn btn-primary btn-flat pull-right" onclick="printDiv();">
              <span class="fa fa-print"aria-hidden="true"> Imprimir</span>
              </button>
            </div>
            <div class="col-md-4"></div>
        </div>
     </section>
     <!-- Main content -->
     <section class="content">
         <!-- Default box -->
         <div class="box box-solid">
             <div class="box-body">
                 <div class="row">
                       <div class="col-md-12" id="reporte">
                       <table id="example" name="example" width="100%" border="1" cellpadding="1" cellspacing="0">
                       <thead>
                       <tr>
                           <td rowspan="2" colspan="9" align="center"><FONT FACE="impact" SIZE="6">FICHA DE SEGUIMIENTO Y EVALUACION DE PROGRAMAS/PROYECTOS (SyE)</FONT></td><td align="center" colspan="2">MES/AÑO</td>
                       </tr>
                       <tr >
                           <td  colspan="2" align="center" height="20"><?php $num=getdate();
                                 switch ($num['mon']) {
                                     case 1:
                                         echo "Enero";
                                         break;
                                     case 2:
                                         echo "Febrero";
                                         break;
                                     case 3:
                                         echo "Marzo";
                                         break;
                                     case 4:
                                         echo "Abril";
                                         break;
                                     case 5:
                                         echo "Mayo";
                                         break;
                                     case 6:
                                         echo "Junio";
                                         break;
                                     case 7:
                                         echo "Julio";
                                         break;
                                     case 8:
                                         echo "Agosto";
                                         break;
                                     case 9:
                                         echo "Septiembre";
                                         break;
                                     case 10:
                                         echo "Octubre";
                                         break;
                                     case 11:
                                         echo "Noviembre";
                                         break;
                                     case 12:
                                         echo "Diciembre";
                                         break;
                                 }
                                 //echo $num['mon'];
                                 echo "/";
                                 echo $num['year']; ?>
                             </td>
                       </tr>
                       </thead>
                       
                       <tbody>
                         <tr>
                             <td colspan="2" width="20%" >NOMBRE DEL PROYECTO / PROGRAMA</td>
                             <td colspan="6" align="center" width="50%">
                            <?php if($proyec>=100000){
                              echo $results->nombre_proyecto." ";
                              echo "<strong>".$proyectod->nombre_tramo."</strong>";
                             }else{
                              echo $results->nombre_proyecto;}
                            ?>
                            </td>
                             <td colspan="2" width="20%" align="center">CUENTA CON CONVENIO DE COFINANCIAMIENTO (SI/NO)</td><td align="center" width="10%"><?php echo $results->convenio;?></td>
                         </tr>
                         <tr>
                             <td colspan="2" width="20%">CODIGO SISIN</td><td colspan="2" aling="center" ><?php
                             echo $sisin->codigo_sisin; ?></td><td width="10%" align="center">FASE <br>(PREINV/EJEC.)</td>
                             <td colspan="2" width="30%" align="center"><?php echo $results->fase;?>
                               <td aling="center" width="20%">COSTO TOTAL<br>PROY./PROG</td>
                               <td colspan="1" aling="center" width="10%"><?php echo $results->costo_proyecto;?></td>
                             </td><td width="10%" align="center"> MODALIDAD<br>DE ADM.</td>
                             <td width="10%" align="center"><?php echo $results->modalidad;?></td>
                         </tr>
                         <tr>
                             <td colspan="2" width="20%">PROGRAMA(PARA PROYECTOS):<br>CAT. PROG (PARA PROGRAMAS):</td>
                             <td colspan="2" align="center"></td>
                             <td align="center">MES-AÑO<br>INIC.</td>
                             <td colspan="2" align="center"><?php $mes=date("m", strtotime($sisinfecha->fecha_inicio));
                                                    $anio=date("Y", strtotime($sisinfecha->fecha_inicio));
                                                     echo $mes.'/'.$anio;?>
                             <td align="center">MES-AÑO<br>CONCL.</td>
                         </td><td align="center"><?php   $mes=date("m", strtotime( $results->fecha_conclusion));
                                         $anio=date("Y", strtotime($results->fecha_conclusion));
                                         echo $mes.'/'.$anio;;?>
                         </td><td align="center">TIEMPO EJEC.<br>AÑOS.</td>
                         <td align="center"><?php echo $results->duracion_anos?></td>
                         </tr>
                         <tr>
                             <td colspan="2">SECRETARÍA DEPTAL.<br> DE TUICION</td>
                             <td colspan="9" align="center"><?php echo $res->nombre;?></td>
                         </tr>
                         <tr >
                             <td colspan="2">DIRECCIÓN / SERVICIO / UNIDAD<br>Y/O AREA EJECUTORA</td>
                             <td colspan="9" align="center"><?php echo $resp->nombre;?></td>
                         </tr>
                         <tr>
                           <td colspan="5">DATOS DE LA EMPRESA EJECUTORA (*)</td>
                           <td colspan="1" align="center">COSTO <BR>ADJUDICADO</td>
                             <td colspan="1" align="center">PLAZO CONTRACT <BR>UAL</td>
                               <td align="center">FECHA INICIO<br>(s/Ord.Preced.)</td>
                               <td align="center">FECHA CONCL.<br>(s/Contrato)</td>
                               <td colspan="2" align="center">TIPO DE GARANTÍA<br>(INCL./CODIGO)</td>
                         </tr>
                         <?php
                         foreach($empresa_ejecutora as $result):?>
                         <tr>
                           <td colspan="3"><?php echo $result->descrip_compo_proy;?></td>
                            <td colspan="2" align="center"><?php echo $result->nombre_empresa;?> </td>
                            <td colspan="1" align="center"><?php echo $result->costo_adj;?></td>
                            <td colspan="1" align="center"><?php echo $result->plazo_contra;?></td>
                            <td colspan="1" align="center"><?php echo $result->fecha_ini;?></td>
                            <td colspan="1" align="center"><?php echo $result->fecha_con?></td>
                            <td colspan="2" aling="center"><?php echo $result->tipo."(".$result->entidad_financiera.")";?></td>
                            <?php endforeach; ?>
                         </tr>
                         <tr>
                         </tr>
                         <tr>
                             <td colspan="11" align="center">DESCRIPCIÓN TÉCNICA DEL PROGRAMA/PROYECTO</td>
                         </tr>
                         <tr>
                             <td colspan="11" height="50"><?php echo $results->descripcion_proyecto;?></td>
                         </tr>
                         <tr>
                             <td colspan="6" align="center">OBJETIVO DEL PROGRAMA/PROYECTO</td><td colspan="5" align="center">OBJETIVO DE GESTIÓN</td>
                         </tr>
                         <tr>
                             <td colspan="6" height="50"><?php echo $results->objetivo_proyecto;?></td>
                             <td colspan="5" height="50"></td>
                         </tr>
                         <tr>
                             <td colspan="11" align="center">LOCALIZACION DEL PROYECTO</td>
                         </tr>
                         <tr>
                             <td colspan="2" align="center">PROVINCIA(S)</td>
                             <td colspan="3" align="center">
                              <?php
                              $prefix='';
                              foreach($prov as $prov):?>
                              <?php echo $prefix.''.$prov->nombre_provincia;
                              $prefix=', '?>
                            <?php endforeach;?>
                            </td>
                             <td colspan="2" align="center">MUNICIPIO(S)</td>
                             <td align="center" colspan="4">
                               <?php
                               $prefix='';
                               foreach($municipio as $prov):?>
                               <?php echo $prefix.''.$prov->municipio;
                               $prefix=', '?>
                             <?php endforeach;?>
                             </td>
                         </tr>
                         <tr>
                             <td colspan="2">LOCALIDAD(ES)<br>COMUNIDAD(ES)</td>
                             <td colspan="9" align="center"></td>
                         </tr>
                       <tr>
                         <td colspan="2" rowspan="2">COSTO DEL PROYECTO</td>
                         <td rowspan="2">INICIAL</td>
                         <td rowspan="2" align="center"><font> INCREMENTO AL COSTO</font></td>
                         <td rowspan="2">COSTO TOTAL<br>ACTUALIZADO</td>
                         <td rowspan="2">COSTO EJECUTADO<br>DEL MES</td>
                         <td rowspan="2">COSTO<br>EJECUTADO</td>
                         <td rowspan="2">COSTO POR<br>EJECUTAR</td>
                         <td colspan="3">TIEMPO DE<br>EJECUCIÓN (FECHAS)</td>
                       </tr>
                       <tr>
                         <td>FECHA DE<br>CONCL. INIC.</td>
                         <td>INCREMENTO<br>DIAS</td>
                         <td>FECHA DE<br>CONCL.ACT.</td>
                       </tr>
                       <tr>
                       <?php
                        foreach($reporte_cos as $result):?>
                          <td colspan="2" align="center"><?php echo $result[0]->descrip_compo_proy;?></td>
                          <td><?php echo $result[0]->inicial;?></td><td><?php echo $result[0]->incremento_costo ;?></td>
                          <td> <?php echo $result[0]->costo_total_actualizado;?></td><td> <?php echo $result[0]->costo_mes;?></td>
                          <td><?php echo $result[0]->costo_ejecutado;?></td><td> <?php echo $result[0]->costo_por_ejecutar;?></td>
                          <td><?php echo $result[0]->fecha_con ;?></td><td aling="center"><?php echo $result[0]->dias;?> </td>
                          <td><?php echo $result[0]->fechafin ;?> </td>
                       </tr>
                       <?php endforeach;?>
                       <tr>
                           <td colspan="2" align="center">TOTALES</td>
                           <td><?php echo $total->suma;?></td>
                           <td> </td>
                           <td> </td>
                           <td><?php echo $total_m->suma;?> </td>
                           <td> </td><td> </td><td> </td><td> </td><td> </td>
                       </tr>
                       <tr>
                           <td colspan="4" align="center">SEGUIMIENTO FISICO</td>
                           <td colspan="4" align="center">SEGUIMIENTO FINANCIERO</td>
                           <td colspan="4" align="center">PROGRAMACION PRESUP.</td>
                       </tr>
                       <tr>
                         <td colspan="3" align="center">AVANCE FÍSICO DEL MES (%)</td>
                         <?php foreach($segui_fisico as $result):?><td><?php echo $result->mes; ?></td><?php endforeach;?>
                           <td colspan="2" rowspan="2"align="center">EJECUCIÓN FINANCIERA<br>DE GESTION (Bs.)</td>
                           <?php foreach($monto_gestion_fin as $result):?><td colspan="2"rowspan="2" align="center"><?php echo $result->suma;?></td><?php endforeach;?>
                           <td colspan="2" rowspan="2"align="center">EJECUCION FINANCIERA<br>MES DE REPORTE</td>
                           <td rowspan="2"><?php echo $total_m->suma;?> </td>
                       </tr>
                       <tr>

                         <td colspan="3" align="center">AVANCE FÍSICO DE LA GESTIÓN (%)</td>
                         <?php foreach($segui_fisico as $result):?><td><?php echo $result->gestion; ?></td><?php endforeach;?>

                       </tr>
                       <tr>

                           <td colspan="3" align="center">AVANCE FÍSICO EJECUTADO ACUMULADO/HISTORICO (%)</td>
                           <?php foreach($segui_fisico as $result):?><td><?php echo $result->historico; ?></td><?php endforeach;?>
                           <td colspan="2" align="center">EJECUCION FINANCIERA ACUMULADO/HISTORICO (Bs)</td>
                           <?php foreach($reporte_cos_historico as $result):?><td colspan="2" align="center"><?php echo $result->suma;?></td><?php endforeach;?>
                           <td colspan="2" align="center">PROGRAMACION FINANCIERA <BR> PROYECTADA DE LA PRESENTE<BR>GESTION</td>

                           <td><?php echo $results->costo_proyecto;?></td>
                       </tr>

                       <?php foreach($reporte_fis as $result):?>
                       <tr>

                           <td colspan="2" height="45">RESULTADOS DEL<br>MES/SITUACION ACTUAL DEL<br>PROGRAMA/PROYECTO</td><td colspan="9"><?php echo $result->resultado_mes?></td>
                       </tr>
                       <tr>
                           <td colspan="2" height="45">PROBLEMAS IDENTIFICADOS<br>DEL MES</td>
                           <td colspan="9"><?php echo $result->problemas_mes?></td>
                       </tr>
                       <tr>
                           <td colspan="2" height="45">ACCIONES DE SOLUCION</td>
                           <td colspan="9"><?php echo $result->acciones_solucion?></td>
                       </tr><?php endforeach;?>
                       <tr>
                           <td colspan="2">FIRMA Y NOMBRE DEL<br>RESPONSABLE DEL<br>PROGRAMA/PROYECTO</td><td colspan="3"></td><td colspan="3">FIRMA Y NOMBRE DEL: Secretario Deptal.<br>Director, Jefe de Unidad, encargado de Area</td><td colspan="3"></td>
                       </tr>
                       <tr>
                           <td colspan="2">FECHA DE ELABORACION:</td>
                           <td colspan="9"><?php $diassemana = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
                               $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                                 echo $diassemana[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y');?></td></td>
                       </tr>
                     </tbody>
                     </table>
                  </div>
                 </div>
             </div>
             <!-- /.box-body -->
         </div>
         <!-- /.box -->
     </section>
     <!-- /.content -->
 </div>
 <!-- /.content-wrapper -->

<script>
    function printDiv()
    {
        var divToPrint=document.getElementById('reporte');
        var newWin=window.open('','Print-Window');
        newWin.document.open();
        newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
        newWin.document.close();
        setTimeout(function(){newWin.close();},10);
    }
</script>
