<div class="content-wrapper" id="   ">
            <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Registro Coordenadas (Puntos)
            <small>Proyecto: <?php echo $proyecto->nombre_proyecto;?></small>
        </h1>
    </section>
            <!-- Main content -->
    <section class="content">
                <!-- Default box -->
        <div class="box-body">
            <div class="box">
                <div class="box-header with-border">

                    <div class="row">
                        <div class="col-md-8">
			                 <form action="<?php echo base_url();?>proyecto/puntos_controller/store" method="POST" autocomplete="off">
                                <div class="form-group">
                                    <div align="center" class="col-md-12" >
                                        <h4><strong>Coordenadas en UTM</strong></h4>
                                    </div>
						          <input type="hidden" value="<?php echo $id_proyecto; ?>" name="id_proyecto" id="" >
                                <div class="col-md-4 <?php echo !empty(form_error('utm'))? 'has-error':'';?>">
                                    <label for="utm">Este</label>
                                        <input type="text" title="solo debes introducir 6 digitos" name="utm" min="0.01" step="0.01" id="utm" class="form-control"
                                        value="<?php echo set_value('utm');?>"
                                         required onfocus="ShowHide('ayuda_este_proyecto','block')" onblur="ShowHide('ayuda_este_proyecto','none')">
											<div class="alert alert-info ayuda" id="ayuda_este_proyecto">
												Ingrese la coordenada en Este (Campo requerido).
											</div>
                                        <br>
                                        <?php echo form_error("utm","<span class='help-block'>","</span>"); ?>
                                </div>
                                <div class="col-md-4 <?php echo !empty(form_error('utme'))? 'has-error':'';?>">
                                    <label for="utme">Norte</label>
                                        <input type="number" name="utme" min="0.01" max="" step="0.01" id="utme" class="form-control" 
                                        value="<?php echo set_value('utme');?>"
                                        required onfocus="ShowHide('ayuda_norte_proyecto','block')" onblur="ShowHide('ayuda_norte_proyecto','none')">
											<div class="alert alert-info ayuda" id="ayuda_norte_proyecto">
												Ingrese la coordenada en Norte (Campo requerido).
											</div>
                                        <br>
                                        <?php echo form_error("utme","<span class='help-block'>","</span>"); ?>
                               </div>

                                <div class="col-md-2">
                                    <label for="norte">Zona/Hemisferio</label>
                                        <input type="text" name="zona" id="zona" class="form-control" value="19 S" readonly=""><br>
                                </div>
                                        <div class="form-group <?php echo !empty(form_error('latitud'))? 'has-error':'';?>">
                                            <input id= "lat" type="hidden" class="form-control" name="latitud">
                                        <?php echo form_error("latitud","<span class='help-block'>","</span>"); ?>
                                        </div>
                                            <div class="form-group <?php echo !empty(form_error('longitud'))? 'has-error':'';?>">
                                                <input id="lon" type="hidden" class="form-control" name="longitud" >
                                                <?php echo form_error("longitud","<span class='help-block'>","</span>"); ?>
                                            </div>
                                            <div class="col-xs-4"></div>
                                            <button  type="submit" class="btn btn btn-info" aling="center">
                                                <span class="glyphicon glyphicon-plus">Agregar puntos</span>
                                            </button>
                                    </div>
                                </form>
                                <div class="row">
                                    <div class="col-xs-2"></div>
                                    <div class="col-xs-8">
                                        Nota: Para la correcta visualizacion de los puntos en el mapa, estos deben ser introducidos
                                        segun un orden determinado por el funcionario.
                                    </div>
                                    <div class="col-xs-2"></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                    <!--form action="<?php echo base_url();?>proyecto/procesar_controller/excel" method="POST" enctype="multipart/form-data" -->
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <input type="hidden" value="<?php echo $id_proyecto; ?>" name="id_proyecto" id="id_proyecto">
                                            <label><strong>Seleccione un Archivo si lo tuviera en formato csv:</strong>
                                            </label>
                                        </div>
                                        <div class="col-md-12">
                                            <input accept=".csv" type="file" name="excel" id="archivo">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group"><br>
                                        <div class="col-md-10"><br>
                                            <div class="col-xs-5"></div>
                                            <div class="hidden" id="mensaje">
                                              El archivo se subio correctamente...
                                            </div>
                                            <button name="button" class="btn btn-info" id="btnEnviar"> <i class="fa fa-upload"></i> Subir Archivo</button>
                                            <button name="button" class="btn btn-default hidden" id="btnMensaje"> <i class="fa fa-refresh fa-spin"></i> Subiendo Archivo...</button>
                                        </div>
                                    </div>
                                </div>
                                  <!--/form-->
                            </div>
                            <br>
                            <div class="col-md-12" >
                                <div class="form-group">
                                    <div class="col-xs-1"></div>
                                        <div class="col-xs-10">
                                                <div align="center" class="col-md-12" >
                                                    <h3>Puntos del Proyectos</h3>
                                                </div>
                                                    <table class="table table-bordered btn-hover" role="grid" id="">
                                                            <thead >
                                                                <tr class="success">
                                                                    <th>Numero</th>
                                                                    <th>Este</th>
                                                                    <th>norte</th>
                                                                    <th>Zona</th>
                                                                    <th>Hemisferio</th>
                                                                    <th>Opciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php $i=0;if(!empty($punto)):?>
                                                                <?php foreach($punto as $met ):?>
                                                                <tr>
                                                                    <td><?php  $i; $i=$i+1; echo $i;?></td>
                                                                    <td><?php echo $met->norte;?></td>
                                                                    <td><?php echo $met->este;?></td>
                                                                    <td>19</td>
                                                                    <td>S</td>
                                                                    <td>
                                                                        <!-- Botón para guardar la información actualizada -->
                                                                        <span >
                                                                            <!-- Botón para mostrar el formulario de actualizar -->
                                                                            <a href="<?php echo base_url();?>proyecto/puntos_controller/edit/<?php echo $met->id_coordenada;?>/<?php echo $met->id_proyecto;?>">
                                                                                <button type="button" id="submitBtn"  class="btn btn-warning" >Actualizar</button>
                                                                            </a>
                                                                                    <!-- Botón para borrar -->
                                                                            <a href="<?php echo base_url();?>proyecto/puntos_controller/delete/<?php echo $met->id_coordenada;?>/<?php echo $met->id_proyecto;?> ">
                                                                                <button type="button" class="btn btn-danger">Borrar</button>
                                                                            </a>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                <?php endforeach;?>
                                                             <?php endif;?>
                                                            </tbody>
                                                    </table>
                                        </div>
                                    </div>
                            </div>

                        </div>
                                <!--button  type="submit" class="btn btn btn-info" aling="center">
                                    <span class="glyphicon glyphicon-plus">Agregar puntos</span>
                                </button-->
					 <div class="col-md-6" >
                                <div class="form-group">
                                    <form action="<?php echo base_url();?>proyecto/componente_proy_controller" method="POST">
                                        <input type="hidden" name="id_proyecto" value="<?php echo $id_proyecto;?>">
                                            <div class="box box-solid">
                                                <div class="box-body">
                                                    <div  class="row"><div class="col-xs-5"></div>
                                                        <div class="form-group">
                                                            <button aling='center' type="submit" class="btn btn-success btn-flat" >
                                                                Continuar
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </form>
                                 </div>
                     </div>
                     <div class="col-md-6" >
                                <div class="form-group">
                                    <?php foreach($punto as $met ):?>
                                     <form action="<?php echo base_url();?>proyecto/gmaps/index/<?php echo $id_proyecto;?>" method="POST">
                                        <input type="hidden" value="<?php echo $id_proyecto; ?>" name="id_proyecto" id="" >
                                         <?php endforeach;?>
                                            <div class="box box-solid">
                                                <div class="box-body">
                                                    <div  class="row"><div class="col-xs-5"></div>
                                                        <div class="form-group">
                                                            <button aling='center' type="submit" class="btn btn-success btn-flat" >
                                                                Ver en Mapa
                                                            </button>
                                                            <div id="mapa" style="width: 450px; height: 350px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </form>
                                </div>
                    </div>
                </div>
                    <!-- /.box-body -->
            </div>
        </div>
                <!-- /.box -->
    </section>
            <!-- /.content -->
</div>
<!--<script src="https://d3js.org/d3.v3.js"></script>-->
 <script type="text/javascript">

        const btnEnviar = document.querySelector("#btnEnviar");
        const archivo = document.querySelector("#archivo");
        const id_proyecto =  document.querySelector("#id_proyecto").value;
        const ruta = "<?php echo base_url();?>proyecto/procesar_controller/excel";
        const mensaje = document.querySelector("#mensaje");

        btnEnviar.addEventListener("click", () => {
            let tipoArchivo = archivo.files[0].type;
            //if(tipoArchivo.includes('csv')){
              if (archivo.files.length > 0) {
                  let formData = new FormData();
                  formData.append("excel", archivo.files[0]);
                  formData.append("id_proyecto", id_proyecto);

                  btnEnviar.className = "hidden";
                  btnMensaje.className = "btn btn-default disabled";
                  fetch(ruta, {
                      method: 'POST',
                      body: formData,
                  }).then(response => response.json())
                  .then(res => {
                      if(res.status===201){
                        btnEnviar.className = "btn btn-info";
                        btnMensaje.className = "hidden";
                        archivo.value = "";
                        mensaje.className = 'alert alert-success text-center';
                        window.location.reload();
                      }
                  }).catch(function(error) {
                    console.log('Hubo un problema con la petición Fetch:' + error.message);
                  });
              //} else {
                //  alert("Selecciona un archivo...");
              //}
            }else{
              alert("Selecciona un archivo .csv...");
            }
        });

        const utm = document.querySelector("#utm");
        const utme = document.querySelector("#utme");
        const lat = document.querySelector("#lat");
        const lon = document.querySelector("#lon");

        utme.addEventListener("keyup", () => {
          console.log(utme.value);
          ///zona, hemisferio, norte, este
          convierteALatLon(19+" J "+utm.value+" "+utme.value);
        });
        ////
        let latitude;
        let longitude;
        const convierteALatLon = (UTM) => {
            let parts = UTM.split(" ");
            let Zone= parseInt(parts[0]);
            let Letter=parts[1].toUpperCase().charAt(0);
            let Easting= parseFloat(parts[2]);
            let Northing= parseFloat(utme.value);
            let Hem;
            if (Letter>'M')
                Hem='N';
            else
                Hem='S';
            let north;
            if (Hem == 'S')
                north = Northing - 10000000;
            else
                north = Northing;
            latitude = (north/6366197.724/0.9996+(1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)-0.006739496742*Math.sin(north/6366197.724/0.9996)*Math.cos(north/6366197.724/0.9996)*(Math.atan(Math.cos(Math.atan(( Math.exp((Easting - 500000) / (0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((Easting - 500000) / (0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2)/3))-Math.exp(-(Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*( 1 -  0.006739496742*Math.pow((Easting - 500000) / (0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2)/3)))/2/Math.cos((north-0.9996*6399593.625*(north/6366197.724/0.9996-0.006739496742*3/4*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.pow(0.006739496742*3/4,2)*5/3*(3*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996 )/2)+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/4-Math.pow(0.006739496742*3/4,3)*35/27*(5*(3*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/4+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/3))/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2))+north/6366197.724/0.9996)))*Math.tan((north-0.9996*6399593.625*(north/6366197.724/0.9996 - 0.006739496742*3/4*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.pow(0.006739496742*3/4,2)*5/3*(3*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.sin(2*north/6366197.724/0.9996 )*Math.pow(Math.cos(north/6366197.724/0.9996),2))/4-Math.pow(0.006739496742*3/4,3)*35/27*(5*(3*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/4+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/3))/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2))+north/6366197.724/0.9996))-north/6366197.724/0.9996)*3/2)*(Math.atan(Math.cos(Math.atan((Math.exp((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2)/3))-Math.exp(-(Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2)/3)))/2/Math.cos((north-0.9996*6399593.625*(north/6366197.724/0.9996-0.006739496742*3/4*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.pow(0.006739496742*3/4,2)*5/3*(3*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/4-Math.pow(0.006739496742*3/4,3)*35/27*(5*(3*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/4+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/3))/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2))+north/6366197.724/0.9996)))*Math.tan((north-0.9996*6399593.625*(north/6366197.724/0.9996-0.006739496742*3/4*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.pow(0.006739496742*3/4,2)*5/3*(3*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/4-Math.pow(0.006739496742*3/4,3)*35/27*(5*(3*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/4+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/3))/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2))+north/6366197.724/0.9996))-north/6366197.724/0.9996))*180/Math.PI;
            latitude=Math.round(latitude*10000000);
            latitude=latitude/10000000;
            longitude =Math.atan((Math.exp((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2)/3))-Math.exp(-(Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2)/3)))/2/Math.cos((north-0.9996*6399593.625*( north/6366197.724/0.9996-0.006739496742*3/4*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.pow(0.006739496742*3/4,2)*5/3*(3*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.sin(2* north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/4-Math.pow(0.006739496742*3/4,3)*35/27*(5*(3*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/4+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/3)) / (0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2))+north/6366197.724/0.9996))*180/Math.PI+Zone*6-183;
            longitude=Math.round(longitude*10000000);
            longitude=longitude/10000000;
            console.log(latitude);
            console.log(longitude);
            lat.value = latitude;
            lon.value = longitude;
        }

        /*let validarDigitos = (valor)={
          let regexp = \d{6}\.\d{2} ;

          console.log(valor+" returns " + regexp.test(valor));

        }*/

</script>
