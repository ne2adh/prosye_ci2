        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" ng-app="app" ng-controller="ctrl">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Reporte
            <small>Incidencia de Proyectos</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row ocultarImprimir">
                    <div class="col-lg-12 text-center">
                        <h4>Reporte de Incidencia de Proyectos</h4>
                    </div>
                </div>
                <div class="row ocultarImprimir">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4 text-center">
                        Tipo reporte:
                        <select class="form-control" name="tipo_reporte" id="tipo_reporte" ng-model="opcion" ng-change="elegirReporte()">
                            <option value="">Seleccione una opcion</option>
                            <option value="Por Tipo Proyecto">Por Tipo Proyecto</option>
                            <option value="Por Secretaria">Por Secretaria</option>
                            <option value="Por Provincia">Por Provincia</option>
                            <option value="Por Municipio">Por Municipio</option>                            
                        </select>
                    </div>
                    <div class="col-lg-4"></div>
                </div>                
                <div class="row" ng-show="estado">
                    <div class="col-lg-12">
                        
                        <hr>
                        <div id="reporteArea">
                            <div class="row">
                                <div class="col-lg-12 text-center">                            
                                    <h3>Reporte {{opcion}}</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <table class="table table-bordered table-striped" border="1" cellpadding="1" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th width="3%">#</th>
                                                <th>Detalle</th>
                                                <th>Cantidad Proyectos</th>
                                                <th>Monto Total Asignado</th>
                                                <th width="10%">Monto Ejecutado por Gestiones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item in listaProyectos">
                                                <td>{{$index+1}}</td>
                                                <td>{{item.detalle}}</td>
                                                <td>{{item.cantidad_proyectos}}</td>
                                                <td>{{item.monto_proyectos}}</td>
                                                <td>
                                                    <table width="100%" border="1" cellpadding="1" cellspacing="0">
                                                        <tr ng-repeat="(key, gestion) in item.gestiones">
                                                            <td width="50%" class="text-center" style="border:solid 1px;">{{key}}</td>
                                                            <td width="50%" class="text-center" style="border:solid 1px;">{{gestion}}</td>
                                                        </tr>
                                                    </table>
                                            </td>
                                            </tr>
                                        </tbody>
                                    </table>                               
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-lg-2"></div>
                                <div class="col-lg-8 text-center">
                                    <h3>
                                        Gasto Financiero por Gestion segun {{opcion}}
                                    </h3>
                                </div>
                                <div class="col-lg-2"></div>                                
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <canvas class="chart chart-line" chart-data="data" chart-labels="labels" chart-series="series" chart-click="onClick">
                                    </canvas> 
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!--angularjs :) -->
<script src="<?php echo base_url();?>assets/angular.min.js"></script>
<script src="<?php echo base_url();?>assets/ui-bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/Chart.bundle.js"></script>
<script src="<?php echo base_url();?>assets/angular-chart.js"></script>
<script>
let ngApp = function(){
    'use strict';

    let app = angular.module('app', ['ui.bootstrap', 'chart.js']);
    app.controller('ctrl', function($scope, $http, $timeout) {
        $scope.estado = false;
        $scope.listaProyectos = [];
        $scope.elegirReporte = function(){            
            if($scope.opcion!=''){
                $scope.estado = true;
            }
            else{
                $scope.estado = false;
            }
            if($scope.opcion == 'Por Tipo Proyecto'){                
                $http.get("<?php echo base_url('proyecto/reportes_controller/tipoProyecto'); ?>").then(function(response) {                        
                        $scope.listaProyectos = response.data['datos'];
                        let gestion_menor = response.data['gestion_menor'];
                        let gestion_mayor = response.data['gestion_mayor'];
                        $scope.labels = [];
                        for(let i=gestion_menor;i<=gestion_mayor;i++){
                            $scope.labels.push(i);
                        }
                        $scope.series = [];
                        $scope.data = [];
                        $scope.listaProyectos.forEach(function(element) {
                            $scope.series.push(element['detalle']);                            
                            let gestiones = element['gestiones'];
                            let temp = [];
                            for(const item in gestiones ){
                                if(item!='TOTAL'){                                   
                                    temp.push(gestiones[item]);
                                }
                            };
                            $scope.data.push(temp);
                        });
                });
            }
            else if($scope.opcion == 'Por Secretaria'){
                $http.get("<?php echo base_url('proyecto/reportes_controller/areaProyecto'); ?>").then(function(response) {                        
                        $scope.listaProyectos = response.data['datos'];
                        let gestion_menor = response.data['gestion_menor'];
                        let gestion_mayor = response.data['gestion_mayor'];
                        $scope.labels = [];
                        for(let i=gestion_menor;i<=gestion_mayor;i++){
                            $scope.labels.push(i);
                        }
                        $scope.series = [];
                        $scope.data = [];
                        $scope.listaProyectos.forEach(function(element) {
                            $scope.series.push(element['detalle']);                            
                            let gestiones = element['gestiones'];
                            let temp = [];
                            for(const item in gestiones ){
                                if(item!='TOTAL'){                                   
                                    temp.push(gestiones[item]);
                                }
                            };
                            $scope.data.push(temp);
                        });
                });

            }
            else if($scope.opcion == 'Por Provincia'){
                $http.get("<?php echo base_url('proyecto/reportes_controller/provinciaProyecto'); ?>").then(function(response) {                        
                        $scope.listaProyectos = response.data['datos'];
                        let gestion_menor = response.data['gestion_menor'];
                        let gestion_mayor = response.data['gestion_mayor'];
                        $scope.labels = [];
                        for(let i=gestion_menor;i<=gestion_mayor;i++){
                            $scope.labels.push(i);
                        }
                        $scope.series = [];
                        $scope.data = [];
                        $scope.listaProyectos.forEach(function(element) {
                            $scope.series.push(element['detalle']);                            
                            let gestiones = element['gestiones'];
                            let temp = [];
                            for(const item in gestiones ){
                                if(item!='TOTAL'){                                   
                                    temp.push(gestiones[item]);
                                }
                            };
                            $scope.data.push(temp);
                        });
                });

            }
            else{
                $http.get("<?php echo base_url('proyecto/reportes_controller/municipioProyecto'); ?>").then(function(response) {                        
                        $scope.listaProyectos = response.data['datos'];
                        let gestion_menor = response.data['gestion_menor'];
                        let gestion_mayor = response.data['gestion_mayor'];
                        $scope.labels = [];
                        for(let i=gestion_menor;i<=gestion_mayor;i++){
                            $scope.labels.push(i);
                        }
                        $scope.series = [];
                        $scope.data = [];
                        $scope.listaProyectos.forEach(function(element) {
                            $scope.series.push(element['detalle']);                            
                            let gestiones = element['gestiones'];
                            let temp = [];
                            for(const item in gestiones ){
                                if(item!='TOTAL'){                                   
                                    temp.push(gestiones[item]);
                                }
                            };
                            $scope.data.push(temp);
                        });
                });
            }
        }
        
       
        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };        
       
    });
}();
</script>

<script>

    function printDiv(){

        var mywindow = window.open('', 'PRINT', 'height=600,width=800');

        mywindow.document.write('<html><head>');    
        mywindow.document.write("<link href=\"<?php echo base_url();?>assets/template/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\">")
        mywindow.document.write('</head><body >');
        mywindow.document.write(document.getElementById('reporteArea').innerHTML);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10*/

        setTimeout(function () {
            mywindow.print();
            mywindow.close();
        }, 1000)
        return true;        
    }
</script>

