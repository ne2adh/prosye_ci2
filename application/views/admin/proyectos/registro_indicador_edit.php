
<!-- dom para angular-->
<?php
      function formato($cantidad){
            $formatted = number_format(sprintf('%0.2f', preg_replace("/[^0-9.]/", "", $cantidad)), 2);
            return $cantidad < 0 ? "({$formatted})" : "{$formatted}";
      }											                
?>   
<div class="content-wrapper" ng-app="app" ng-controller="ctrl">
  <section class="content-header">
    <h1>
    Indicadores
      <small><?php  echo 'Proyecto:  '. $proyecto->nombre_proyecto; ?></small>
    </h1>
  </section>
  <section class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                  <div class="panel panel-info">
                    <div class="panel-heading"></div>

                    <div class="panel-body">

              <div class="row">
                <div class="col-md-12">
                  <?php //if ($dato=="ECONOMICO") :?>
                 <form action="<?php echo base_url();?>proyecto/indicador_controller/update" method="POST">
                  <input type="hidden" name="id_proyecto" value="<?php echo isset($id_tramo)?$id_tramo:$id_proyecto; ?>">
                  <input type="hidden" name="id" value="<?php echo $indicadorEdit->id;?>">
                   <!--Se recupera el id del proyecto -->
                   <input type="hidden" name="convenio" value="<?php echo $proyecto->convenio;?>"> <!--Se recupera el id de convenio -->
                  <div class="col-md-8">
                      <div class="form-group <?php echo !empty(form_error('indicador'))? 'has-error':'';?>" >
                         <label for="nombre">Escoga el Indicador</label>
                         <select ng-model="indicador" class="form-control" ng-options="item.abreviatura as (item.descripcion+' - '+item.abreviatura) for item in indicadores"></select>
                         <input type="text" style="display: none;" name="indicador" ng-model="indicador">
                         <?php echo form_error("indicador","<span class='help-block'>","</span>"); ?>
                      </div>
                  </div>

                  </div><br>

                  <div class="col-md-12">
                  <div class="col-md-8">
                  <?php ?>
                      <div class="form-group <?php echo !empty(form_error('valor_porc'))? 'has-error':'';?> ">
                        <label for="valor_porc">Ingrese el Valor del Indicador</label>
                        <input class="form-control" name="valor_porc" type="text"
                            value="<?php echo formato($indicadorEdit->valor_indicador_porc);
                            echo set_value('valor_porc');?>"  required=""  data-type="currency" onfocus="ShowHide('ayuda_costo_proyecto','block')" onblur="ShowHide('ayuda_costo_proyecto','none')" >
                            <div class="alert alert-info ayuda" id="ayuda_costo_proyecto">
                                  Ingrese el Valor del Indicador(Campo requerido).
                            </div>
                            <?php echo form_error("valor_porc","<span class='help-block'>","</span>"); ?>
                        </div>
                  </div>
                  </div>
                  <br>

                  <div class="col-md-12">
                  <div class="col-md-8">
                  <?php ?>
                      <div class="form-group <?php echo !empty(form_error('tipo'))? 'has-error':'';?> ">
                      <label for="tipo">Ingrese unidad del indicador</label>
                      <!--<select name="tipo" class="form-control">
                        <option value=" "<?php echo  set_select( 'tipo' , '', TRUE );?>>seleccione una opcion</option>
                        <option value="Bs" <?php echo set_select ( 'tipo' ,  'Bs' );?>>Bs.</option>
                        <option value="%" <?php echo set_select ( 'tipo', '%');?>>%</option>
                        <option value=" " <?php echo set_select ('tipo', ' ')?>>Adimensional</option>
                      </select>-->
                      <input type="text" name="tipo" class="form-control" value="<?php echo $indicadorEdit->tipo;?>"
                             onfocus="ShowHide('ayuda_unidad_proyecto','block')" onblur="ShowHide('ayuda_unidad_proyecto','none')" >
                      <div class="alert alert-info ayuda" id="ayuda_unidad_proyecto">
                          Escriba la unidad del Indicador(Campo requerido).
                      </div>
                      <?php echo form_error("tipo","<span class='help-block'>","</span>");?>
                    </div>
                  </div>
                  </div><br>

                  <div class="col-md-5"></div>
                   <div class="form-group">
                      <button aling="center" type="submit" class="btn btn-success btn-flat" >
                          Actualizar
                      </button>
                   </div>
                   <?php //endif; ?>
                              <?php
                                if(isset($error)){
                                  if($error == 1){
                                  echo '<div class="alert alert-danger" role="alert">  <strong>El indicador esta Repetido!</strong> Ingrese otro indicador.
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                  </div>';
                                  };
                                }
                              ?>
                        </form>

                  <br><br>
                    <form action="<?php echo base_url();?>proyecto/mis_proyectos/propios"   method="POST">
                       <input type="hidden" name="id_proyecto" value="<?php echo isset($id_tramo)?$id_tramo:$id_proyecto; ?>"> <!--Se recupera el id del proyecto -->
                       <input type="hidden" name="convenio" value="<?php echo $proyecto->convenio;?>"> <!--Se recupera el id de convenio -->
                <div class="box box-solid">
                  <div class="box-body">
                    <div  class="row">
                      <div align="center" class="col-md-12">
                          <h2>Indicadores del Proyectos</h2>
                      </div>
                  <hr>
                  <div class="row">
                      <div class="col-md-12">
                          <table  class="table table-bordered btn-hover">
                              <thead>
                                  <tr>
                                      <th>Numero</th>
                                      <th>Nombre Indicador</th>
                                      <th>Valor Indicador</th>

                                  </tr>
                              </thead>
                              <tbody>
                                  <?php $c=0; ?>
                                  <?php   if(!empty($indicador)):?>
                                      <?php  foreach($indicador as $met ):?>
                                          <tr>
                                              <td><?php echo $c=$c+1; //echo $met->id_proyecto;?></td>
                                              <td><?php echo $met->nombre_indicador;?></td>
                                              <td><?php if ($met->valor_indicador==0)
                                                  {echo $met->valor_indicador_porc." ".$met->tipo;}?>
                                              </td>

                                          </tr>
                                      <?php endforeach; ?>
                                  <?php endif;?>
                              </tbody>
                          </table>
                     </div>
                  <!-- /.box-body -->
                  </div>
                </div>
              </div>
            </div>
                  <?php if ($c!=0):?>
                   <div class="col-md-5"></div>
                   <div   class="form-group">
                      <button type="submit" class="btn btn-success btn-flat" >
                          Continuar
                      </button>
                   </div>
                   <?php endif;?>
                    </form>
              </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
      </div>
  </section>

</div>


<!--angularjs :) -->
<script src="<?php echo base_url();?>assets/angular.min.js"></script>
<script src="<?php echo base_url();?>assets/ui-bootstrap.min.js"></script>

<script>
let ngApp = function(){
  'use strict';
  let app = angular.module('app', ['ui.bootstrap']);
  app.controller('ctrl', function($scope, $http, $uibModal) {


$scope.indicador='<?php echo $indicadorEdit->nombre_indicador;?>';
//console.log($scope.indicador);

    $scope.listarIndicadores=()=>{
      $http.get("<?php echo base_url('proyecto/indicador_controller/listarIndicadores'); ?>").then(function(response) {
        $scope.indicadores = response.data;
        //$scope.indicador = $scope.indicadores[0].abreviatura;
      });
    }
    $scope.listarIndicadores();

    $scope.amRol = (indicador) => {
        $uibModal.open({
          templateUrl: 'agregar.html',
          backdrop: 'static',
          scope:$scope,
          controller: function ($scope,$uibModalInstance) {
              if(indicador===undefined){
                $scope.titulo='Nuevo Registro';
                $scope.estado = true;
                $scope.indicador = [];
              }else{
                $scope.titulo='Editar Registro';
                $scope.indicador = indicador;
                $scope.estado = false;
              }

              $scope.registrar = (indicador)=> {
                let req = {
                   method: 'GET',
                   url: '<?php echo base_url('proyecto/indicador_controller/saveIndicador?indicador='); ?>'+indicador.abreviatura+'&indicador2='+indicador.descripcion,
                   //data: { rol:rol }
                }
                $http(req).then(function(response) {
                  $scope.cerrar();
                  //console.log($scope.indicador);
                  $scope.listarIndicadores();

                });
              };

              $scope.editar = (rol)=> {
                let ruta = '<?php echo base_url('administrador/Rol/editRol?id=');?>'+rol.id_rol+'&nombre='+rol.nombre;
                let req = {
                   method: 'GET',
                   url: ruta
                   //data: { rol:rol }
                }
                $http(req).then(function(response) {
                  $scope.cerrar();
                  $scope.listarRoles();
                });
              };

              $scope.cerrar = () => {
                  $uibModalInstance.dismiss();
              };
          }
        });
    };


    console.log('funciona');
  });
}();
</script>
<!---->
