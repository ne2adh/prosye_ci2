<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Subir archivo excel en php</title>
	<script type="text/javascript">
		function subirArchivoExcel()
		{
			if(document.frmSubirArchivo.excel.value=="")
			{
				alert("Error debe subir archivo");
				document.frmSubirArchivo.excel.focus();
				return false;

			}
			document.frmSubirArchivo.action="procesar.php";
			document.frmSubirArchivo.submit();
		}
	</script>
</head>
<body>
	<article>
		<section>
			<form name="frmSubirArchivo" method="post" enctype="multipart/form-data">
			<p>Archivo Excel</p>
			<p><input type="file" name="excel"/></p>
			<p><input type="button" value="Subir archivo" onclick="subirArchivoExcel();"/></p>
			
			</form>
		</section>
	</article>


</body>
</html>
