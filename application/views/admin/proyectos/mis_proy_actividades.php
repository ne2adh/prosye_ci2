
        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Mis Proyectos
                <small>Escoga el proyecto al cual le asignara Actividades</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                    <?php /*
                        <div class="row">
                            <div class="col-md-12">
                                <a href="<?php echo base_url();?>proyecto/mis_proyectos/area_controller" class="btn btn-primary btn-flat">
                                <span class="fa fa-plus">
                                </span>Agregar Proyecto
                                </a>
                            </div>
                        </div>
                     */?>   
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                    
                        <table id="example1" name="example1" class="table table-bordered btn-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Codigo</th>
                                                        <th>Nombre Proyecto</th>
                                                        <th>Descripcion</th>
                                                        <th>Opciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                                                                 
                                                         <?php  foreach($results as $result):?> 
                                                                <tr>
                                                                    <td><?php echo $result->id_proyecto;?></td>
                                                                    <td><?php echo $result->nombre_proyecto;?></td>
                                                                    <td><?php echo $result->descripcion_proyecto;?></td>
                                                                    <td>
                                                                        <div class="btn-group">
                                                                        <!--    <a href="#" class="btn btn-info">
                                                                                <span class="fa fa-eye">                 
                                                                                </span>
                                                                            </a>-->
                                                                            <a href="<?php echo base_url();?>proyecto/componente_proy_controller/nuevo/<?php echo $result->id_proyecto;?>" class="btn btn-warning"title="Editar Componente">
                                                                                <span class="fa fa-pencil">               
                                                                                </span>
                                                                            </a>
                                                                            <!--<a href="#" class="btn btn-danger">
                                                                                <span class="fa fa-remove">               
                                                                                </span>
                                                                            </a>-->
                                                                        </div>
                                                                    </td>
                                                           </tr>
                                                    <?php endforeach;?>  
                                                </tbody>
                                </table>
                               
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
