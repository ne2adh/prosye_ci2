
        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>Edicion 
                <small>Componente</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <form action="<?php echo base_url();?>proyecto/componente_controller/update" method="POST">
                     <input type=""  value="<?php echo $id_componente_tramo; ?>" name="id_componente_tramo" >
                      <input type=""  value="<?php echo $id_tramo; ?>" name="id_tramo" >
                      <input type=""  value="<?php echo $id_proyecto; ?>" name="id_proyecto">
                        <div class="form-group">
                            <label for="nombre_tramo">El Tramo en el cual Trabaja es:</label>
                            <input type=""  value="<?php echo $tramo_compo->nombre_tramo; ?>" name="nombre_tramo" >
                        </div>
                        
                        <div class="form-group">
                            <label for="descripcion">Descripcion del Componente</label>
                            <input class="form-control" name="descripcion" value="<?php echo $componente->descripcion; ?>">
                        </div>
                        <div class="form-group">
                            <label for="monto">Monto del Convenio en Bolivianos</label>
                            <input class="form-control" name="monto"  value="<?php echo $componente->monto; ?>">
                        </div>
                        <div class="form-group">
                            <div class="row">
                               
                                     <div class="col-xs-6">
                                        <label for="fecha_inicio">
                                         Fecha de Inicio:
                                        </label>
                                        <input type="date" id="fecha_inicio" class="form-control" name="fecha_inicio" value="<?php echo $componente->fecha_inicio; ?>">
                                     </div>  
                                     <div class="col-xs-6">
                                        <label for="fecha_fin">
                                        Fecha de Conclusion:
                                        </label>     
                                        <input type="date" id="fecha_fin"  class="form-control" name="fecha_fin" value="<?php echo $componente->fecha_fin; ?>">   
                                     </div>  
                                </div>
                           
                        </div>
                     
                        <div class="form-group">
                            <label for="tiempo_evaluacion">Tiempo de Evaluacion</label>
                            <input class="form-control" name="tiempo_evaluacion" value="<?php echo $componente->tiempo_evaluacion; ?>">
                        </div>
                        <div class="form-group">
                            <label for="tiempo_ejecucion">Tiempo de Ejecucion</label>
                            <input class="form-control" name="tiempo_ejecucion" value="<?php echo $componente->tiempo_ejecucion; ?>">
                        </div>
                        
                            
                            <div   class="form-group">
                                    <button aling='center' type="submit" class="btn btn-success btn-flat" >
                                        Guardar
                                    </button>
                            </div>
                            </form> 
                           
                            <form action="<?php echo base_url();?>proyecto/mis_proyectos/propios" method="POST"> 
                            
                             <div class="box box-solid">
                           
                             <input type=""  value="<?php echo $id_tramo; ?>" name="id_tramo" >
                             <input type=""  value="<?php echo $id_proyecto; ?>" name="id_proyecto">  
                                 <div class="box-body">
                                    <div  class="row">
                            <div align="center" class="col-md-12" >
                                <h2>Componentes del Tramo</h2>      
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <table  class="table table-bordered btn-hover">
                                    <thead>
                                        <tr>
                                           
                                            <th>Descion</th>
                                            <th>Monto</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                                                                
                                        <?php if(!empty($compo)):?>
                                        
                                            <?php foreach($compo as $met):?>
                                             
                                                <tr>
                                                    
                                                    <td><?php echo $met->descripcion;?></td>
                                                    <td><?php echo $met->monto;?></td>
                                                    <td>
                                                                        <div class="btn-group">
                                                                            <a href="<?php echo base_url();?>proyecto/componente_controller/edit/<?php echo $met->id_componente_tramo;?>/<?php echo $id_proyecto;?>/<?php echo $id_tramo;?>" class="btn btn-warning">
                                                                                <span class="fa fa-pencil">               
                                                                                </span>
                                                                            </a>
                                                                            <a href="<?php echo base_url();?>proyecto/componente_controller/delete/<?php echo $met->id_componente_tramo;?>/<?php echo $met->id_proyecto;?>/<?php echo $id_tramo;?>" class="btn btn-danger btn-remove">
                                                                                <span class="fa fa-remove">               
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                    </td>
                                                </tr>
                                            <?php endforeach;?>  
                                        <?php endif;?>
                                    </tbody>
                                    
                                </table>
                                
                           </div>
                    <!-- /.box-body -->
                </div>
                <div  align="center" class="form-group">
                                <button type="submit" class="btn btn-success btn-flat" >
                                    Continuar
                                </button>
                            </div>


                        </form>                   
                            </div>       



                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
