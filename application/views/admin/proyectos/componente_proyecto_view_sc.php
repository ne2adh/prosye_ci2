
        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>Registro Componentes</h1>
                        <h4><small>PROYECTO: <?php echo $proyecto->nombre_proyecto;?></small></h4>
                        <h5><small>FASE DEL PROYECTO: <?php echo $proyecto->fase;?></small></h5>    
                        <h5><small>DURACION DEL PROYECTO: <?php echo $proyecto->fecha_inicio. ' / '.$proyecto->fecha_conclusion;?></small></h5>   

            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <?php
                    function formato($cantidad)
                    {
                        $formatted = number_format(sprintf('%0.2f', preg_replace("/[^0-9.]/", "", $cantidad)), 2);
                        return $cantidad < 0 ? "({$formatted})" : "{$formatted}";
                    }											                
                ?>
                 <div class="box box-solid">
                    <div class="box-body">
                            <?php  $monto_total=$proyecto->costo_proyecto; $sum=0; 
                                foreach($componente as $result):?> 
                                    <?php  $sum=$sum+$result->monto_comp_proy; 
                                    ?>

                                    <?php endforeach; ?> 
                            <!-- <h2 align="center">Asignacion de Componentes</h2>-->
                            <?php $s=0;  ?>
                                         
                            <form action="<?php echo base_url();?>proyecto/componente_proy_controller/store_proy" method="POST" autocomplete="off">

                            <input type="hidden"  value="<?php echo isset($proyecto->id_tramo)?$proyecto->id_tramo:$proyecto->id_proyecto; ?>" name="id_proyecto"> <!--Se recupera el id del proyecto -->
                            <input type="hidden"  value="<?php echo $proyecto->convenio; ?>" name="convenio"> <!--Se recupera el id del proyecto -->
                             
                            <div class="col-xs-12 ">
                                <div class="form-group <?php echo !empty(form_error('descripcion'))? 'has-error':'';?>">
                                        <label for="descripcion">Descripcion del Componente del Proyecto</label>
                                                <div class="alert alert-info ayuda" id="ayuda_descripcion_proyecto">
													Seleccione la descripcion del Componente del Proyecto (Campo requerido).
												</div>
                                                <?php if ($proyecto->fase=='INVERSION'):?>    
                                                <select class="form-control" name="descripcion" 
                                                onfocus="ShowHide('ayuda_descripcion_proyecto','block')" onblur="ShowHide('ayuda_descripcion_proyecto','none')">
                                                    <option  value="" <?php echo set_select ( 'descripcion' ,'');?>>Elija una opcion</option>
                                                    <option value="INFRAESTRUCTURA" <?php echo set_select ( 'descripcion' ,'INFRAESTRUCTURA');?>>INFRAESTRUCTURA(Construcciones y Edificaciones)</option>
                                                    <option value="FISCALIZACION" <?php echo set_select ( 'descripcion' ,'FISCALIZACION');?>>FISCALIZACION</option>
                                                    <option value="AUDITORIA EXTERNA" <?php echo set_select ( 'descripcion' ,'AUDITORIA EXTERNA');?>>AUDITORIA EXTERNA</option>
                                                    <option value="CAPACITACION, ASISTENCIA TECNICA y ORGANIZACION" <?php echo set_select ( 'descripcion' ,'CAPACITACION, ASISTENCIA TECNICA y ORGANIZACION');?>>CAPACITACION, ASISTENCIA TECNICA y ORGANIZACION</option>
                                                    <option value="ADMINISTRACION" <?php echo set_select ( 'descripcion' ,'ADMINISTRACION');?>>ADMINISTRACION</option>
                                                    <option value="SUPERVISION" <?php echo set_select ( 'descripcion' ,'SUPERVISION');?>>SUPERVISION</option>
                                                    <option value="AUDITORIA INTERNA" <?php echo set_select ( 'descripcion' ,'AUDITORIA INTERNA');?>>AUDITORIA INTERNA</option>
                                                    <option value="EQUIPAMIENTO" <?php echo set_select ( 'descripcion' ,'EQUIPAMIENTO');?>>EQUIPAMIENTO</option>
                                                </select>
                                                <?php endif; ?>
                                                <?php if ($proyecto->fase=='PREINVERSION'):?>    
                                                <select class="form-control" name="descripcion" 
                                                onfocus="ShowHide('ayuda_descripcion_proyecto','block')" onblur="ShowHide('ayuda_descripcion_proyecto','none')">
                                                    <option value="" <?php echo set_select ( 'descripcion' ,'');?>>Elija una opcion</option>
                                                    <option value="CONSULTORIA" <?php echo set_select ( 'descripcion' ,'CONSULTORIA');?>>CONSULTORIA</option>
                                                    <option value="SUPERVISION" <?php echo set_select ( 'descripcion' ,'SUPERVISION');?>>SUPERVISION</option>
                                                    <option value="AUDITORIA" <?php echo set_select ( 'descripcion' ,'AUDITORIA');?>>AUDITORIA</option>
                                                </select>
                                                <?php endif; ?>
                                                <?php echo form_error("descripcion","<span class='help-block'>","</span>"); ?>
                                        
                                    </div>
                                </div> 
                             
                                <div class="form-group">
                                   <div class="col-md-12"> 
                                    <div class="row">
                                        <div class="col-xs-4 ">
                                            <div class="form-group <?php echo !empty(form_error('tiempo_actividad'))? 'has-error':'';?>">                           
                                                
                                                    <!--<label for="fecha_inicio">
                                                     Tiempo de ejecucion planificada en años
                                                    </label>-->
                                                    <label for="duracion_anos">Duracion en Dias</label>      
                                                     <input type="number"  class="form-control"  placeholder="Duracion en dias del componente"
                                                        name="tiempo_actividad" id="tiempo_actividad" style="text-trasnform:uppercase;"  value="<?php echo set_value('tiempo_actividad');?>" onKeyUp="calcularFecha();" required onfocus="ShowHide('ayuda_duracion_proyecto','block')" onblur="ShowHide('ayuda_duracion_proyecto','none')">
                                                    
                                                        <?php echo form_error("tiempo_actividad","<span class='help-block'>","</span>"); ?><br>
                                                <div class="alert alert-info ayuda" id="ayuda_duracion_proyecto">
                                                        Introduzca la Duracion del Componente, 
                                                         (Campo requerido).
                                                    </div>       
                                                   
                                                    <?php echo form_error("tiempo_actividad","<span class='help-block'>","</span>"); ?>
                                                </div> 
                                            </div>
                                            
                                        
                                        <div class="col-xs-4 <?php echo !empty(form_error('fecha_inicio'))? 'has-error':'';?>">
                  
                                            <label for="fecha_inicio">Fecha Inicio de Componente</label>
                                           <input id="fecha_inicio" type="date" name="fecha_inicio" class="form-control" min="<?php echo $proyecto->fecha_inicio;?>" value="<?php echo set_value('fecha_inicio');?>"  onchange="calcularFecha();" required onfocus="ShowHide('ayuda_inicio_proyecto','block')" onblur="ShowHide('ayuda_inicio_proyecto','none')">
                                           <?php echo form_error("fecha_inicio","<span class='help-block'>","</span>"); ?>
                                                
                                            <div class="alert alert-info ayuda" id="ayuda_inicio_proyecto">
                                                Introduzca la fecha de inicio del Componente,
                                                 (Campo requerido).
                                            </div>
                                        </div>
                                        


                                            <div class="col-xs-4 <?php echo !empty(form_error('fecha_conclusion'))? 'has-error':'';?>">
                                                <label for="fecha_conclusion">Fecha conclusion del Componente</label>
                                                <input id="fecha_conclusion" type="date" name="fecha_conclusion" class="form-control"  value="<?php echo set_value('fecha_conclusion');?>" required readonly>
                                                    <?php echo form_error("fecha_conclusion","<span class='help-block'>","</span>"); ?>
                  
                                            </div>


                                     </div>
                                     </div>

                                     <div class="col-md-12">
                                         <div class="row">
                                             
                                            
                                            <div class="col-xs-6 ">
                                            <div class="form-group <?php echo !empty(form_error('monto'))? 'has-error':'';?> ">
                                                 
                                                    <label for="monto">Monto del Componente en Bolivianos dentro del Proyecto</label>
                                                    <input class="form-control" name="monto" type="text"  max="<?php $Mon_Com=$sum-$proyecto->costo_proyecto;$Mon_Com=abs($Mon_Com);echo $Mon_Com;?>"
                                                    value="<?php echo set_value("monto");?>" data-type="currency" 
                                                        onfocus="ShowHide('ayuda_costo_proyecto','block')" onblur="ShowHide('ayuda_costo_proyecto','none')"
                                                    >
                                                    <div class="alert alert-info ayuda" id="ayuda_costo_proyecto">
                                                        Escriba el Costo total del Componente, valor maximo sugerido <?php $Mon_Com=$sum-$proyecto->costo_proyecto;$Mon_Com=abs($Mon_Com);echo formato($Mon_Com);?>(Campo requerido).
                                                    </div>
                                                    <?php echo form_error("monto","<span class='help-block'>","</span>"); ?>
                                                </div>
                                            </div>
                                      

                                            <div class="col-xs-6 ">
                                                <div class="form-group <?php echo !empty(form_error('modalidad'))? 'has-error':'';?>">
                                                    <label for="modalidad">Modalidad de Administracion</label>
                                                    <div class="alert alert-info ayuda" id="ayuda_modalidad_proyecto">
                                                        Seleccione la modalidad (Campo requerido).
                                                    </div>
                                                    <select class="form-control" name="modalidad"
                                                        onfocus="ShowHide('ayuda_modalidad_proyecto','block')" onblur="ShowHide('ayuda_modalidad_proyecto','none')">
                                                        <option value="" <?php echo set_select ( 'modalidad' ,'');?>>Seleccione una Opcion</option>
                                                        <option value="PROPIA" <?php echo set_select ( 'modalidad' ,'PROPIA');?>>PROPIA</option>
                                                        <option value="DELEGADA A TERCEROS" <?php echo set_select ( 'modalidad' ,'DELEGADA A TERCEROS');?>>DELEGADA&nbspA TERCEROS</option>
                                                    </select>
                                                    <?php echo form_error("modalidad","<span class='help-block'>","</span>"); ?>
                                                 </div>  
                                            </div>
                                         </div>
                                     </div>
                                     <?php if ($sum<$proyecto->costo_proyecto):?> 
                                        <div class="col-xs-5"></div>
                                        <div   class="form-group">
                                            <button aling='center' type="submit" class="btn btn-success btn-flat" >
                                                  Guardar
                                            </button>
                                        </div>
                                        <?php endif;?> 
                                   </div>
                                   
                          
                              </form> 
                         
                          
                              <form action="<?php echo base_url();?>proyecto/indicador_controller/ingresoIndicador/<?php echo isset($proyecto->id_tramo)?$proyecto->id_tramo:$proyecto->id_proyecto;?>/" method="POST"> 
                                    
                                     <div class="box box-solid">
                                            <input type="hidden"  value="<?php echo isset($proyecto->id_tramo)?$proyecto->id_tramo:$proyecto->id_proyecto;?>" name="id_proyecto">  <!--Se recupera el id del proyecto --> 
                                            
                                         <div class="box-body">
                                            <div  class="row">
                                    <div align="center" class="col-md-12" >
                                        <h2>Componentes del Proyecto</h2>      
                                    </div>
                                    <div class="col-md-12 text-center">
                                        <h4>Monto restante en el componente: <?php $Mon_Com=$sum-$proyecto->costo_proyecto;$Mon_Com=abs($Mon_Com);echo formato($Mon_Com);?></h4>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table  class="table table-bordered btn-hover">
                                            <thead>
                                                <tr>
                                                   
                                                    <th>Descripcion</th>
                                                    <th>Modalidad de Administracion</th>
                                                    <th>Fecha Inicio</th>
                                                    <th>Fecha Conclusion</th>
                                                    <th>Monto</th>
                                                    <th>Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                                                        
                                                <?php if(!empty($componente)):?>
                                                
                                                    <?php foreach($componente as $met):?>
                                                     
                                                        <tr>
                                                            
                                                            <td><?php echo $met->descrip_compo_proy;?></td>
                                                            <td
                                                            ><?php echo $met->modalidad;?></td>
                                                            <td><?php echo date("d/m/Y",strtotime($met->fecha_inicio));?></td>
                                                            <td><?php echo date("d/m/Y",strtotime($met->fecha_conclusion));?></td>
                                                            <td><?php echo formato($met->monto_comp_proy);?></td>
                                                                            <td>
                                                                                <div class="btn-group">
                                                                                    <a href="<?php echo base_url();?>proyecto/componente_proy_controller/edit/<?php echo $met->id_componente_proy;?>/<?php echo isset($proyecto->id_tramo)?$proyecto->id_tramo:$proyecto->id_proyecto;?>" class="btn btn-warning">
                                                                                        <span class="fa fa-pencil">               
                                                                                        </span>
                                                                                    </a>
                                                                                    <a href="<?php echo base_url();?>proyecto/componente_proy_controller/delete/<?php echo $met->id_componente_proy;?>/<?php echo isset($proyecto->id_tramo)?$proyecto->id_tramo:$proyecto->id_proyecto;?>" class="btn btn-danger btn-remove">
                                                                                        <span class="fa fa-remove">               
                                                                                        </span>
                                                                                    </a>
                                                                                   
                                                                                </div>
                                                                            </td>
                                                        </tr>
                                                       
                                                    <?php endforeach;?>  
                                                <?php endif;?>
                                                        <tr>
                                                            <td >TOTAL</td>
                                                            <td ></td>
                                                            <td ></td>
                                                            <td ></td>
                                                            <td class="right"><?php  echo formato( $sum );?></td>
                                                        </tr>
                                            </tbody>
                                            
                                        </table>
                                        
                                   </div>
                            <!-- /.box-body -->
                                </div>
                                    <?php $total=$monto_total-$sum; if($total==0):?>
                                        <div  align="center" class="form-group">
                                            <button type="submit" class="btn btn-success btn-flat" >
                                                Continuar
                                            </button>
                                        </div>
                                    <?php else:?>
                                        <div  align="center" class="form-group">
                                            <strong>
                                                Para continuar con el Registro la suma de los montos de los componentes debe ser igual
                                                al monto total del Proyecto.
                                            </strong>
                                        </div>
                                    <?php endif;?>
                                    <?php  // echo 'El monto por asignar a componentes del proyecto es: '.$total;?>

                          <?php $total=$proyecto->costo_proyecto-$sum;?>
                          <?php if ($sum>100) {echo 'El monto por asignar a componentes del proyecto es: '.formato($total);}else{echo 'Revise los montos asignados a los componentes: '. formato($total);} ?> Bs.
                                    
                                </form>                   

                          </div>
                          </div>
                          </div>
                          </section>
                          </div>

<script type="text/javascript">

function calcularFecha(){
      let fecha_inicio = document.getElementById("fecha_inicio").value;
      console.log(fecha_inicio);
      let diasContrato = document.getElementById("tiempo_actividad").value;
      let vfecha = fecha_inicio.split("-");      
      if(diasContrato!='' && fecha_inicio!=''){          
          fecha = new Date(vfecha[0], vfecha[1]-1, vfecha[2],0,0,0);          
          fecha.setDate(fecha.getDate() + parseInt(diasContrato));
          d = fecha.getDate();
          m = fecha.getMonth()+1;
          if(m<10){
            m='0'+m;
          }

          if(d<10){
            d = '0'+d;
          }
          y = fecha.getFullYear();
          fecha_actual = y+"-"+m+"-"+d;
          document.getElementById("fecha_conclusion").value = fecha_actual;          
      }
  }
</script>