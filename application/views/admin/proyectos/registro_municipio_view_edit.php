<div id ="" class="content-wrapper">
            <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Actualizar Municipio
            <small>Proyecto: <?php echo $proyecto->nombre_proyecto;?></small>
        </h1> 
    </section>
            <!-- Main content -->
    <section class="content">
                <!-- Default box --> 
        <div class="box box-solid">
            <div class="box-body">
                <div style="display: none">
                    <?php $sum=0; $sum1=0; $sum2=0; $sum3=0; $s1=0;
                    foreach($d_municipio as $result):?> 
                        <tr>
                            <td><?php echo $sum=$sum+$result->por_benef_poblacion;?></td>
                            <td><?php echo $sum1=$sum1+$result->por_participacion;?></td>
                            <td><?php echo $sum2=$sum2+$result->por_incidencia_proy;?></td>
                        </tr>
                    <?php endforeach; $sum3=$sum+$sum1+$sum2; $s1=100-$sum;?> 
	            </div>
		            <form action="<?php echo base_url();?>proyecto/municipio_controller/update" method="POST">
		                <input type="hidden" name="sum" value="<?php echo $sum;?>">
		                <input type="hidden" name="sum1" value="<?php echo $sum1;?>">
		                <input type="hidden" name="sum2" value="<?php echo $sum2;?>" >
		                <input type="hidden" name="sum3" value="<?php echo $sum3;?>" >
		                <input type="hidden" name="id_proyecto" value="<?php echo $proyecto->id_proyecto;?>" >
		                <div class="form-group">
							<div class="row" >
								<div class="col-xs-12 ">
									<?php if ($sum2<100 OR $sum2>100)  :?>
									<div class="col-xs-6 <?php echo !empty(form_error('provincia'))? 'has-error':'';?> "> 
		                       			<label for="provincias">Provincias:</label>
										<div class="alert alert-info ayuda" id="ayuda_provincia_proyecto">
											Seleccione la provincia del proyecto (Campo requerido).
										</div>
		                       			<select class="form-control"  id="provincia" name="provincia"
										   onfocus="ShowHide('ayuda_provincia_proyecto','block')" onblur="ShowHide('ayuda_provincia_proyecto','none')"
										>
		                           			<option value="">Provincias</option>
		                           				<?php  
		                               				foreach($provincias as $row):?>
		                               					{  
		                                   					<?php //echo '<option value="'.$row->id_provincia.'">'.$row->nombre_provincia.'</option>';?>
		                                   					echo <option value="<?php echo $row->id_provincia;?>" <?php echo set_select("provincia",$row->id_provincia);?>><?php echo $row->nombre_provincia;?> <?php echo set_select("provincia",$row->nombre_provincia);?> </option>
		                               					}
		                               				<?php endforeach;?>	
		                           				
										</select>
										<?php echo form_error("provincia","<span class='help-block'>","</span>"); ?>
		                           		                
		                        	</div>
									<div class="<?php echo !empty(form_error('municipio'))? 'has-error':'';?>">
										<div class="col-xs-6"> 
											<label for="municipio">Municipio:</label>
											<div class="alert alert-info ayuda" id="ayuda_municipio_proyecto">
												Seleccione el municipio del proyecto (Campo requerido).
											</div>
											<select class="form-control" id="municipios" name="municipio" 
											onfocus="ShowHide('ayuda_municipio_proyecto','block')" onblur="ShowHide('ayuda_municipio_proyecto','none')"
											>												
												<option value="<?php if(isset($_POST['municipio'])){ echo $_POST['municipio'];} ?>"><?php echo $this->municipio_model->nombres($this->input->POST('municipio'));?></option>
												
											</select>  
											<?php echo form_error("provincia","<span class='help-block'>","</span>"); ?>                   
			                        	</div>
			                       </div> 	 
								</div>
							</div>
		                </div>
		                <div class="form-group">
							<div class="row">
								<div class="col-xs-12">
									<div class="<?php echo !empty(form_error('cant_genera_empleo'))? 'has-error':'';?>">
										<div class="col-xs-6">
			                           		<label for="cant_genera_empleo">Cantidad de Empleo Generado:</label>
											<input type="number" name="cant_genera_empleo" id="cant_genera_empleo" class="form-control" value="<?php echo set_value("cant_genera_empleo");?>"
											onfocus="ShowHide('ayuda_empleo_proyecto','block')" onblur="ShowHide('ayuda_empleo_proyecto','none')">
											<div class="alert alert-info ayuda" id="ayuda_empleo_proyecto">
												Ingrese la cantidad de empleo generado en el Municipio (Campo requerido).
											</div>
											<?php echo form_error("cant_genera_empleo","<span class='help-block'>","</span>"); ?> 
										</div>
									</div>	
									<div class="<?php echo !empty(form_error('total_pobla_beneficio'))? 'has-error':'';?>">
										<div class="col-xs-6" >
											<label for="total_pobla_beneficio">Total Poblacion Beneficiada:</label>
											<input type="number" name="total_pobla_beneficio" id="total_pobla_beneficio" class="form-control" value="<?php echo set_value("total_pobla_beneficio");?>"
											onfocus="ShowHide('ayuda_poblacion_proyecto','block')" onblur="ShowHide('ayuda_poblacion_proyecto','none')">
											<div class="alert alert-info ayuda" id="ayuda_poblacion_proyecto">
												Ingrese la poblacion beneficiada en el Municipio (Campo requerido).
											</div>
											<?php echo form_error("total_pobla_beneficio","<span class='help-block'>","</span>"); ?> 
										</div>
									</div>	
								</div>
							</div>
		                </div>
                        <div class="form-group">
							<div class="row">
								<div class="col-xs-12">
									<div class="col-xs-6">	
                            		    <?php //<label for="por_benef_poblacion">Porcentaje de Poblacion Beneficiada:</label> ?>
                                		<input type="hidden" name="por_benef_poblacion" id="por_benef_poblacion" class="form-control" placeholder="<?php $s1;?>" onKeyPress="return numeros();" >
										
									</div>
									<div class="col-xs-6">
										<?php //<label for="por_participacion">Porcentaje de Participacion:</label>?>
										<input type="hidden" name="por_participacion" id="por_participacion" class="form-control" onKeyPress="return numeros();">
										
									</div>
								</div>
							</div>
						</div>
	                    <div class="form-group">
							<div class="row">
								<div class="col-xs-12">
									<div class="<?php echo !empty(form_error('por_incidencia_proy'))? 'has-error':'';?>">	
										<div class="col-xs-6">   
			                           		<label for="por_incidencia_proy">Porcentaje de Incidencia del Proyecto:</label>
											   <input type="number" min="1" max="<?php $res=100-$sum2;echo $res;?>" name="por_incidencia_proy" id="por_incidencia_proy" class="form-control" value="<?php echo set_value("por_incidencia_proy");?>"
											   onfocus="ShowHide('ayuda_porcentaje_proyecto','block')" onblur="ShowHide('ayuda_porcentaje_proyecto','none')">
											   Porcentaje Maximo Sugerido:<?php echo $res;?> %
											    <div class="alert alert-info ayuda" id="ayuda_porcentaje_proyecto">
													Ingrese el porcentaje de Incidencia del municipio en relacion al proyecto, si el numero es entero debe completar con .00 (Campo requerido).
												</div>
											<?php echo form_error("por_incidencia_proy","<span class='help-block'>","</span>"); ?> 


										</div>
									</div>	
									
									<div class="col-xs-6"> 
										<div class="col-xs-4"></div> 
										<br>
										<div class="form-group">
											<button aling='center' type="submit" class="btn btn-primary" id="">
												Agregar Municipio al Proyecto
											</button>
										</div>
									<?php endif ; ?>	
										<!--div class="form-group">
											<button aling='center' type="button" class="btn btn-primary" v-on:click="cargarmunicipio" id="btn1">
												Agregar Municipio al Proyecto
											</button>
                            			</div-->
										<?php
											if(isset($error)){
												if($error == 1){
													echo '<div class="alert alert-danger" role="alert">  <strong>El municipio esta Repetido!</strong> Ingrese otro Municipio.
    													<button type="button" class="close" data-dismiss="alert" aria-label="Close">
   														<span aria-hidden="true">&times;</span>
														</div>';
											
																}
                        										}
                     					 ?>
						
									</div>
								</div>
							</div>
	                    </div>
		            </form> 
		           	<form action="<?php echo base_url();?>proyecto/municipio_controller" method="POST">	
						<input type="hidden" name="id_proyecto" value="<?php echo $proyecto->id_proyecto;?>">
						<div class="box box-solid">
							<div class="box-body">
								<div class="row">
									<div align="center" class="col-md-12" >
										<h3>Municipios del Proyectos</h3>      
									</div>
								</div>
								<input type="hidden" name="id_proyecto" value="<?php echo $proyecto->id_proyecto;?>">
								<div class="col-md-12">
                                	<table class="table table-bordered btn-hover" role="grid">
                                		<thead>
											<tr class="success">
												<th scope="col" class="col-sm-4" class="text-left">Municipio</th>
												<th scope="col" class="col-sm-1">Cantidad de Empleo Generado</th>
												<th scope="col" class="col-sm-1">Cantidad de Poblacion beneficiada</th>
												<?php /*<th scope="col" class="col-sm-1">Porcentaje de Poblacion beneficiada</th>
												<th scope="col" class="col-sm-1" class="text-Right">Porcentaje de Participacion</th>*/?>
												<th scope="col" class="col-sm-1" text-align:center>Porcentaje de Incidencia del Proyecto</th>
												<th scope="col" class="col-sm-2">Opciones</th>
											</tr>
                                    	</thead>
                                    	<tbody>
                                        	<?php if(!empty($d_municipio)):?>
                                        	<?php foreach($d_municipio as $met ):?>
	                                        <tr>
												<td><?php echo $met->municipio;?></td>
												<td><?php echo $met->cant_genera_empleo;?></td>
												<td><?php echo $met->total_pobla_beneficio;?></td>
												<?php /*<td><?php echo $met->por_benef_poblacion;?></td>
												<td><?php echo $met->por_participacion;?></td>*/ ?>
												<td><?php echo $met->por_incidencia_proy;?></td>
												<td>
													<div class="btn-group">
														<span> 
															
															<a href="<?php echo base_url();?>proyecto/municipio_controller/delete/<?php echo $met->id_municipio;?>/<?php echo $met->id_proyecto;?>" >
															<button type="button" class="btn btn-danger " >Borrar</button>            
														</span>
													</div>
													<div class="btn-group">
														<span> 
															
															<a href="<?php echo base_url();?>proyecto/municipio_controller/edit/<?php echo $met->id;?>/<?php echo $met->id_municipio;?>/<?php echo $met->id_proyecto;?>" >
															<button type="button" class="btn btn-success" >Actualizar</button>            
														</span>
													</div>
												</td>
												
                                            </tr>
											<?php endforeach;?> 
										</tbody>
										<tfoot>
											<tr class="danger">
												<td><?php echo 'TOTALES';?></td>
												<td><?php echo '';?></td>
												<td><?php echo '';?></td>
												<?php /*<td><?php echo $sum;?></td>
												<td><?php echo $sum1;?></td>*/?>
												<td class="right"><?php echo $sum2;?></td>
											</tr>
												<?php endif;?>
										</tfoot>
                           			</table>
			                    </div>
								<div class="row">
									<div class="col-xs-12 text-center">							
										<?php if ($sum2==100)
															{ 												
																echo '<button aling="center" type="submit" class="btn btn-success btn-flat">
																		Continuar
																		</button>';
															}else{
																echo '<button aling="center" type="submit" class="btn btn-success btn-flat" disabled="true" >
																		Continuar
																		</button>
																		<br><br>
																		
																		<div class="alert alert-white text-danger role="alert"> <h4>  <strong> 
																		Revise el porcentaje de incidencia del proyecto tiene que ser igual a 100 </strong> </h4>
																	
																		</div>';
														
															}

														

															?>
																
									</div>
								</div>
							</div>
						</div>
						
					
                   	</form>     
            </div>
        </div>	
    </section>
           <!-- /.content -->
</div>
        <!-- /.content-wrapper -->
<script>
      $(document).ready(function()
      {
         $("#mostrarmodal").modal("show");
      });
</script>