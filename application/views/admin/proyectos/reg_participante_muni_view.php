        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div id ="detalleMunicipioProyecto" class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>Registro Participantes
                    <small> </small>
                </h1> 
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        
	                        <div class="form-group">
	                            <label>Nombre del Convenio: </label>
	                            <input class="form-control" type="" name="nombre_convenio" value="<?php echo $convenio->nombre_convenio;?>" readonly="">
							</div>
                            <div class="form-group">
                                <label>Costo del Proyecto en Convenio: </label>
                                <input class="form-control" type="" name="costo_proyecto" value="<?php echo $monto;?>" readonly="">
                            </div>
		                      <form action="<?php echo base_url();?>proyecto/participantes/store1" method="POST">
    		                        <input type="hidden"  value="<?php echo $id_proyecto; ?>" name="id_proyecto">
                                    <input type="hidden"  value="<?php echo $id_convenio; ?>" name="id_convenio">
                                    <input type="hidden"  value="<?php echo $id_componente_proy; ?>" name="id_componente_proy">
                                    <div class="form-group">
    		                            <h1 align="center">Porcentaje de Co-Financiamiento</h1>
    		                        </div>
                                    <?php  $sum=0; $con=$monto;?>   
                                    <?php foreach($participa as $met ){ 
                                      $sum=$sum+($met->monto_municipio);}
                                      
                                      foreach($ente1 as $met ){ 
                                       $sum=$sum+($met->monto_ente);} ?>
    		                        <div class="form-group">
                                        <label for="municipio_finan">Financiamiento por el Municipio al Proyecto</label>
                                        <select class="form-control"  id="municipio_finan" name="municipio_finan" >
                                                    <option value="">Municipio</option>
                                                    <?php  
                                                        foreach($municipio as $row)
                                                        {  
                                                            echo '<option value="'.$row->id_municipio.'">'.$row->municipio. '</option>';
                                                        }
                                                    ?>
                                        </select>
                                    </div>     
                                    <div class="form-group">
                                        <label for="participacion_muni">Monto de Participacion en el Proyecto del Municipio</label>
                                                <input type="number" step="0.01" class="form-control" id="participacion_muni" placeholder="Participacion dentro Proyecto" name="participacion_muni" max="<?php $res=$con-$sum;echo $res;?>" min="00.00" value="<?php echo set_value("participacion_muni");?>" onkeypress="return filterFloat(event,this);">
                                                <?php echo form_error("participacion_muni","<span class='help-block'>","</span>"); ?>
                                    </div>
                                     <?php $res=$con-$sum;
                                    echo "EL MONTO QUE TIENE AUN POR ASIGNAR ES DE: $res" ?> 
                            
                                   <div   class="form-group">
                                                <button aling='center' type="submit" class="btn btn-success btn-flat" >
                                                    Guardar 
                                                </button>
                                    </div>
                               
                             </form>
                            
                            </div>
                        </div>
                    </section>
                </div>


		                        