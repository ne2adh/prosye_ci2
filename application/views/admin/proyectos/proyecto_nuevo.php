        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
<div id="datos1"  class="content-wrapper">
            <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Proyecto
   			<small>
				Nuevo Proyecto
			</small>
        </h1>
    </section>

            <!-- Main content -->
    <section class="content">

                <!-- Default box -->
        <div class="box box-primary">
            <div class="box-body">
						<!-- inicio -->
				<form action="<?php echo base_url();?>proyecto/mis_proyectos/store" method="POST" ref="form" autocomplete="off">

					<input type="hidden" name="rol_id" value="<?php echo $this->session-> userdata('rol');?>" >
					    <div class="form-group <?php echo !empty(form_error('nombre_proyecto'))? 'has-error':'';?> ">
							<div class="row">
								<div class="col-xs-1"></div>
									<div class="col-xs-12">
										<label for="nombre_proyecto">Nombre del Proyecto</label>
										<input type="text"  class="form-control"  placeholder="NOMBRE DEL PROYECTO"
										name="nombre_proyecto"  style="text-trasnform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"  value="<?php echo set_value('nombre_proyecto');?>"
										onfocus="ShowHide('ayuda_nombre_proyecto','block')" onblur="ShowHide('ayuda_nombre_proyecto','none')"
										>
										<?php echo form_error("nombre_proyecto","<span class='help-block'>","</span>"); ?>
										<div class="alert alert-info ayuda" id="ayuda_nombre_proyecto">
											Escriba el nombre completo del proyecto (Campo requerido).
										</div>
									</div>
              				</div>
						</div>
              <div class="col-xs-12 <?php echo !empty(form_error('modalidad'))? 'has-error':'';?>">			  		
                	<label for="mod">Modalidad del Proyecto:</label>
					<div class="alert alert-info ayuda" id="ayuda_modalidad_proyecto">
						Seleccione la modalidad del Proyecto (Campo requerido).
					</div>
                  	<select class="form-control" name="modalidad" value="<?php echo set_value("mod");?>" required 
				  		onfocus="ShowHide('ayuda_modalidad_proyecto','block')" onblur="ShowHide('ayuda_modalidad_proyecto','none')"
					>
                      <option value="" <?php  echo   set_select ( 'modalidad' ,  '',  TRUE );?>>Seleccione una Opcion</option>
                      <option value="PROPIA" <?php  echo   set_select ( 'modalidad' ,  'PROPIA' );  ?>>PROPIA</option>
                      <option value="DELEGADA A TERCEROS" <?php  echo   set_select ( 'modalidad' ,  'DELEGADA A TERCEROS' );  ?>>DELEGADA A TERCEROS</option>
                 	</select>				  
                  <?php echo form_error("modalidad","<span class='help-block'>","</span>"); ?>
                </select><br>
              </div>

						<div class="form-group <?php echo !empty(form_error('area'))? 'has-error':'';?> ">
							<div class="row">
								<div class="col-xs-1"></div>
								<div class="col-xs-12">
									<label for="area">Secretaria:</label>
									<div class="alert alert-info ayuda" id="ayuda_secretaria_proyecto">
										Seleccione la Secretaria del Proyecto (Campo requerido).
									</div>
										<select class="form-control"  id="area" name="area" 
											onfocus="ShowHide('ayuda_secretaria_proyecto','block')" onblur="ShowHide('ayuda_secretaria_proyecto','none')"
										>
											<option value="" >Secretaria</option>
											<?php
												foreach($areas as $area):?>
												{
												echo <option value="<?php echo $area->cod;?>" <?php echo set_select("area",$area->cod);?>><?php echo $area->nombre;?> <?php echo set_select("area",$area->nombre);?> </option>';
												}
											<?php endforeach;?>

										</select>
										<?php echo form_error("area","<span class='help-block'>","</span>"); ?>
								</div>
							</div>
						</div>
						<div class="form-group <?php echo !empty(form_error('subarea'))? 'has-error':'';?> <?php echo !empty(form_error('sub_area3'))? 'has-error':'';?> ">
							<div class="row">
								<div class="col-xs 1"></div>
									<div class="col-xs-6">
										<label for="subarea">Area:</label>
											<div class="alert alert-info ayuda" id="ayuda_area_proyecto">
												Seleccione el Area del Proyecto (Campo requerido).
											</div>											
											<select class="form-control" id="subarea" name="subarea" 
												onfocus="ShowHide('ayuda_area_proyecto','block')" onblur="ShowHide('ayuda_area_proyecto','none')"
											>
										 		<option value="<?php if(isset($_POST['subarea'])){ echo $_POST['subarea'];} ?>"><?php echo $this->proyectos_model->nombres1($this->input->POST('subarea'));?></option>
											</select>
											<?php echo form_error("subarea","<span class='help-block'>","</span>");?>
									</div>
									<div class="col-xs-6">
											<label for=" ">Sub-Area:</label>
											<div class="alert alert-info ayuda" id="ayuda_subarea_proyecto">
												Seleccione el Sub-Area del Proyecto (Campo requerido).
											</div>	
											<select class="form-control" id="sub_area3" name="sub_area3" onfocus="ShowHide('ayuda_subarea_proyecto','block')" onblur="ShowHide('ayuda_subarea_proyecto','none')"
											>
											<option value="<?php if(isset($_POST['sub_area3'])){ echo $_POST['sub_area3'];} ?>"><?php echo $this->proyectos_model->nombres($this->input->POST('sub_area3'));?></option>
											</select>
											<?php echo form_error("sub_area3","<span class='help-block'>","</span>"); ?>

									</div>

                </div>
              </div>
						<div class="">
							<div class="row">
									<div class="col-xs-6 form-group <?php echo !empty(form_error('tipo_fase'))? 'has-error':'';?>">
                    					<label for="fase">Fase del Proyecto</label>
											<div class="alert alert-info ayuda" id="ayuda_fase_proyecto">
												Seleccione la Fase del Proyecto (Campo requerido).
											</div>
											<select class="form-control" name="tipo_fase" value="<?php echo set_value("fase");?>"
											onfocus="ShowHide('ayuda_fase_proyecto','block')" onblur="ShowHide('ayuda_fase_proyecto','none')"
											>
									 				<option value="" <?php  echo   set_select ( 'tipo_fase' ,  '',  TRUE );?>>Seleccione una Opcion</option>
									     			<option value="PREINVERSION" <?php  echo   set_select ( 'tipo_fase' ,  'PREINVERSION');  ?>>PREINVERSION</option>
									       			<option value="INVERSION" <?php  echo   set_select ( 'tipo_fase' ,  'INVERSION' );  ?>>INVERSION/EJECUCION</option>
											</select>
											<?php echo form_error("tipo_fase","<span class='help-block'>","</span>"); ?>
									</div>
									<div class="col-xs-6 form-group <?php echo !empty(form_error('costo_proyecto'))? 'has-error':'';?>">
									    <label for="costo_proyecto">Costo del Proyecto</label>
										<input type="text" class="form-control" id="costo_proyecto"  name="costo_proyecto" placeholder="Costo del Proyecto 00.00" 
										value="<?php echo set_value("costo_proyecto");?>" data-type="currency" 
										onfocus="ShowHide('ayuda_costo_proyecto','block')" onblur="ShowHide('ayuda_costo_proyecto','none')"
										>
										<div class="alert alert-info ayuda" id="ayuda_costo_proyecto">
											Escriba el Costo total del Proyecto(Campo requerido).
										</div>
										<?php echo form_error("costo_proyecto","<span class='help-block'>","</span>"); ?>
									</div>
							</div>
						</div>
						<div class="">
							<div class="row">
									<div class="col-xs-6 form-group <?php echo !empty(form_error('tipo_proyecto'))? 'has-error':'';?> ">
										<label for="tipo_proyecto">Tipo del Proyecto</label>
											<div class="alert alert-info ayuda" id="ayuda_tipo_proyecto">
												Seleccione el Tipo del Proyecto(Campo requerido).
											</div>
                    						<select class="form-control" name="tipo_proyecto" 
												onfocus="ShowHide('ayuda_tipo_proyecto','block')" onblur="ShowHide('ayuda_tipo_proyecto','none')"
											>
												<option value="" <?php  echo   set_select ( 'tipo_proyecto' ,  '' ,  TRUE );?>>Seleccione una Opcion</option>
												<option value="PROYECTO DE DESARROLLO EMPRESARIAL PRODUCTIVO" <?php  echo   set_select ( 'tipo_proyecto' ,  'PROYECTO DE DESARROLLO EMPRESARIAL PRODUCTIVO' );?>>PROYECTO DE DESARROLLO EMPRESARIAL PRODUCTIVO</option>
												<option value="PROYECTO DE APOYO AL DESARROLLO PRODUCTIVO" <?php  echo   set_select ( 'tipo_proyecto' ,  'PROYECTO DE APOYO AL DESARROLLO PRODUCTIVO' ); ?>>PROYECTO DE APOYO AL DESARROLLO PRODUCTIVO</option>
												<option value="PROYECTO AL DESARROLLO SOCIAL" <?php  echo   set_select ( 'tipo_proyecto' ,  'PROYECTO AL DESARROLLO SOCIAL' );  ?>>PROYECTO AL DESARROLLO SOCIAL</option>
												<option value="PROYECTO DE FORTALECIMIENTO INSTITUCIONAL" <?php  echo   set_select ( 'tipo_proyecto' ,  'PROYECTO DE FORTALECIMIENTO INSTITUCIONAL' );  ?>>PROYECTO DE FORTALECIMIENTO INSTITUCIONAL</option>
												<option value="PROYECTO DE INVESTIGACION Y DESARROLLO TECNOLOGICO" <?php  echo   set_select ( 'tipo_proyecto' ,  'PROYECTO DE INVESTIGACION Y DESARROLLO TECNOLOGICO' );  ?>>PROYECTO DE INVESTIGACION Y DESARROLLO TECNOLOGICO</option>
											</select>
											<?php echo form_error("tipo_proyecto","<span class='help-block'>","</span>"); ?>
									</div>
									<div class="col-xs-6 form-group <?php echo !empty(form_error('convenio'))? 'has-error':'';?> ">
										<label for="convenio">Proyecto con Convenio</label>
											<div class="alert alert-info ayuda" id="ayuda_convenio_proyecto">
												Seleccione si Proyecto tiene o no convenio.(Campo requerido).
											</div>
											<select class="form-control" name="convenio" 
											onfocus="ShowHide('ayuda_convenio_proyecto','block')" onblur="ShowHide('ayuda_convenio_proyecto','none')"
											>
												<option value="" <?php  echo   set_select ( 'convenio' ,  '' ,  TRUE );  ?>>Seleccione una Opcion</option>
												<option value="SI" <?php  echo   set_select ( 'convenio' ,  'SI' );  ?>>SI</option>
												<option value="NO" <?php  echo   set_select ( 'convenio' ,  'NO' );  ?>>NO</option>
											</select>
											<?php echo form_error("convenio","<span class='help-block'>","</span>"); ?>
									</div>

              </div>
						</div>

						<div class="col-xs-12 text-center">
								<label>Tiempo de Duracion</label>
						</div>

						<div class="">
							<div class="row">
								<div class="col-xs-6 form-group <?php echo !empty(form_error('fecha_inicio'))? 'has-error':'';?>">
	                				<label for="fecha_inicio">
	                					Fecha de Inicio:
	                				</label>
	                				<input type="month" id="fecha_inicio" class="form-control" name="fecha_inicio" value="<?php echo set_value("fecha_inicio");?>" onChange="resta_anios();" 
									onfocus="ShowHide('ayuda_fecha_inicio_proyecto','block')" onblur="ShowHide('ayuda_fecha_inicio_proyecto','none')" min="1990-01" max="2050-12">
									<?php echo form_error("fecha_inicio","<span class='help-block'>","</span>" ); ?>
									<div class="alert alert-info ayuda" id="ayuda_fecha_inicio_proyecto">
										Seleccione el Mes y el Año de inicio del Proyecto.(Campo requerido).
									</div>
								</div>
								<div class="col-xs-6 form-group <?php echo !empty(form_error('fecha_conclusion'))? 'has-error':'';?>">
									<label for="fecha_conclusion">
										Fecha de Conclusión:
									</label>
									<input type="month" id="fecha_conclusion"  class="form-control" name="fecha_conclusion" value="<?php echo set_value("fecha_conclusion");?>" 
									onfocus="ShowHide('ayuda_fecha_fin_proyecto','block')" onblur="ShowHide('ayuda_fecha_fin_proyecto','none')" min="1990-01" max="2050-12>
									<?php echo form_error("fecha_conclusion","<span class='help-block'>","</span>"); ?>
									<div class="alert alert-info ayuda" id="ayuda_fecha_fin_proyecto">
										Seleccione el Mes y el Año de Conclusion del Proyecto.(Campo requerido).
									</div>
								</div>
								 <?php /*
									<div class="col-xs-4">
										<label for="duracion_anos">Duracion en Años</label>
										<input id="duracion" type="" name="duracion_anos" class="form-control"  value="<?php $datos=$this->proyectos_model->calculaTiempo(); if ($datos[0]>0) {echo $datos[0]+1;}else{$datos='';} ?>"  readonly="">
										<?php echo form_error("duracion_anos","<span class='help-block'>","</span>"); ?>
									</div> */
								?>
							 </div>
						</div>

	            <div class="form-group <?php echo !empty(form_error('descripcion_proyecto'))? 'has-error':'';?>">
  							<div class="row">
  							<div class="col-xs-12">
  								<label for="descripcion_proyecto">
  									Descripcion del Proyecto:
  								</label>
  								<input type="text"  class="form-control"  placeholder="DESCRIPCION DEL PROYECTO"
  										name="descripcion_proyecto" id="" style="text-trasnform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"  value="<?php echo set_value("descripcion_proyecto");?>"
									    onfocus="ShowHide('ayuda_descripcion_proyecto','block')" onblur="ShowHide('ayuda_descripcion_proyecto','none')">
										<div class="alert alert-info ayuda" id="ayuda_descripcion_proyecto">
											Ingrese la descripcion del Proyecto.(Campo requerido).
										</div>
  										<?php echo form_error("descripcion_proyecto","<span class='help-block'>","</span>"); ?>
  								</div>
  							</div>
              </div>

              <div class="form-group <?php echo !empty(form_error('objetivo_proyecto'))? 'has-error':'';?> ">
    						<div class="row">
    							<div class="col-xs-12">
    								<label for="objetivo_proyecto">
    									Objetivo General del Proyecto:
    								</label>
    								<input type="text"  class="form-control"  placeholder="OBJETIVO DEL PROYECTO"
    										name="objetivo_proyecto" id="" style="text-trasnform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"  value="<?php echo set_value("objetivo_proyecto");?>"
											onfocus="ShowHide('ayuda_objetivo_proyecto','block')" onblur="ShowHide('ayuda_objetivo_proyecto','none')">
											<br>
										<div class="alert alert-info ayuda" id="ayuda_objetivo_proyecto">
											Ingrese la descripcion del Proyecto.(Campo requerido).
										</div>
    								<?php echo form_error("objetivo_proyecto","<span class='help-block'>","</span>"); ?>
    			        </div>
                </div>
            </div>

            <div>              
                <div class="col-xs-12 text-center" >
                  <h4>
				  	<label for="tramos">¿El proyecto tiene Modulos o Paquetes?</label><br><br>
				  </h4>
                </div>              
              <script>
                $(document).ready(function(){
                      $(".codigo_tramo").click(function(evento){

                          var valor = $(this).val();

                          if(valor == 1){
                              $("#div1").css("display", "block");
                              //$("#div2").css("display", "none");
                          }else{
                              $("#div1").css("display", "none");
                              //$("#div2").css("display", "block");
                          }
                  });
                });
            </script>
                <div class="col-xs-12">
                  <center>
                    <input name="codigo_tramo" class="codigo_tramo" type="radio"
                      style="width: 25px; height: 25px" value="1"/>&nbsp;<span class="auto-style4">SI</span>
                      &nbsp;&nbsp;
                    <input checked="checked" class="codigo_tramo" name="codigo_tramo" type="radio"
                      style="width: 25px; height: 25px" value="0"/><span class="auto-style4">NO</span>
                    <p>&nbsp;</p>
                  </center>
                </div>
            <div class="col-xs-4">
            </div>
            <section aling="center" class="container">
              <div class="caja">
                <div id="div1" class="col-xs-4" style="display:none;">
                  <p class="auto-style3"><span class="auto-style4">Selecciona numero de tramos:</span></p>
                    <select class="form-control" name="numero_tramo" id="numero_tramo">
                      <?php
                      for ($i=1; $i <=100 ; $i++){
                         echo "<option value=" . $i . ">" . $i . "</option>";
                      }
                      ?>
                    </select>
                </div>
              </div>
            </section>
          </div>

          <div  align="center" class="form-group"><br>
    					<button type="submit" class="btn btn-success btn-flat" >
    						Guardar
    				</button>
   				</div>

				</form>
        </div>
    </div>
  </section>
            <!-- /.content -->
</div>
		<script>
			function filterFloat(evt,input){
			// Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
			var key = window.Event ? evt.which : evt.keyCode;
			var chark = String.fromCharCode(key);
			var tempValue = input.value+chark;
			if(key >= 48 && key <= 57){
				if(filter(tempValue)=== false){
					return false;
				}else{
					return true;
				}
			}else{
				if(key == 8 || key == 13 || key == 0) {
					return true;
				}else if(key == 46){
						if(filter(tempValue)=== false){
							return false;
						}else{
							return true;
						}
				}else{
					return false;
				}
			}
		}
		function filter(__val__){
			var preg = /^([0-9]+\.?[0-9]{0,2})$/;
			if(preg.test(__val__) === true){
				return true;
			}else{
			return false;
			}




		}
		</script>
