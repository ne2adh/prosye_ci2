<?php
          function formato($cantidad){
              $formatted = number_format(sprintf('%0.2f', preg_replace("/[^0-9.]/", "", $cantidad)), 2);
              return $cantidad < 0 ? "({$formatted})" : "{$formatted}";
          }											                
      ?>
        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>Ficha de Registro
                <small><?php echo $detalle_proyecto->nombre_proyecto;?></small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">                        
                        <div class="row" id="reporte">
                            <div class="col-xs-12">                                
                                <table width="100%" border="1" cellpadding="1" cellspacing="0">
                                    <tr>
                                        <td colspan="11"  align="center" style="padding:10px;">
                                            <FONT FACE="impact" SIZE="6">FICHA DE REGISTRO DE PROYECTOS</FONT>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" width="20%" >NOMBRE DEL PROYECTO / PROGRAMA</td>
                                        <td colspan="5" align="center" width="50%">
                                            <?php echo $detalle_proyecto->nombre_proyecto;?>
                                        </td>
                                        <td colspan="2" width="20%" align="center">
                                            CUENTA CON CONVENIO DE COFINANCIAMIENTO (SI/NO)
                                        </td>
                                        <td align="center" width="10%" colspan="2">
                                            <?php echo $detalle_proyecto->convenio;?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" width="20%">CODIGO SISIN</td>
                                        <td colspan="3" aling="center" >
                                            <?php 
                                                if(isset($detalle_proyecto->sisin) && $detalle_proyecto->sisin!='0'){
                                                    echo $detalle_proyecto->sisin;
                                                }
                                                else{
                                                    echo "Sin Codigo Sisin";
                                                }
                                            ?>
                                        </td>
                                        <td align="center" colspan="3">TIEMPO EJEC.<br>AÑOS.</td>
                                        <td align="center" colspan="3">
                                                <?php
                                                        $date1 = new DateTime($detalle_proyecto->fecha_inicio);
                                                        $date2 = new DateTime($detalle_proyecto->fecha_conclusion);
                                                        $diff = $date1->diff($date2);
                                                        $y = $diff->y;
                                                        $m = $diff->m;
                                                        $d =  $diff->d;
                                                        echo $y."A ";
                                                        echo $m."M ";
                                                        echo $d."D";                                            
                                                ?>
                                        </td>
                                    </tr>
                                    <tr>                                    
                                        <td colspan="2" width="10%" >FASE <br>(PREINV/EJEC.)</td>
                                        <td colspan="2" width="30%" align="center"><?php echo $detalle_proyecto->fase;?>
                                        <td aling="center" width="20%">COSTO TOTAL<br>PROY./PROG</td>
                                        <td colspan="2" align="center" width="10%" >
                                                <?php echo formato($detalle_proyecto->costo_proyecto);?>
                                        </td>
                                        </td><td width="10%" align="center"> MODALIDAD<br>DE ADM.</td>
                                        <td width="10%" align="center" colspan="3">
                                        <?php
                                            echo $detalle_proyecto->modalidad;
                                        ?>
                                        </td>
                                    </tr>
                         <tr>
                             <td colspan="2" width="20%">PROGRAMA(PARA PROYECTOS):<br>CAT. PROG (PARA PROGRAMAS):</td>
                             <td colspan="2" align="center"></td>
                             <td align="center">MES-AÑO<br>INIC.</td>
                             <td colspan="2" align="center">
                                    <?php echo date("d/m/Y", strtotime($detalle_proyecto->fecha_inicio));?>
                             </td>
                             <td align="center">MES-AÑO<br>CONCL.</td>                         
                             <td align="center" colspan="4">
                                    <?php echo date("d/m/Y", strtotime($detalle_proyecto->fecha_conclusion));?>
                             </td>
                             
                         </tr>
                         <tr>
                             <td colspan="2">SECRETARÍA DEPTAL.<br> DE TUICION</td>
                             <td colspan="9" align="center"><?php echo $detalle_proyecto->nombre_secretaria;?></td>
                         </tr>
                         <tr >
                             <td colspan="2">DIRECCIÓN / SERVICIO / UNIDAD<br>Y/O AREA EJECUTORA</td>
                             <td colspan="9" align="center"><?php echo $detalle_proyecto->nombre_sub_area;?></td>
                         </tr>
                         <tr>
                            <td colspan="11" align="center" style="padding:5px;">
                                <b>METAS</b>
                            </td>
                         </tr>
                         <tr>                            
                            <td colspan="11" align="center">
                                <table width="100%" border="1" cellpadding="1" cellspacing="0">
                                    <tr>
                                        <td width="5%" align="center">#</td>
                                        <td width="25%" align="center">Cantidad</td>
                                        <td align="center">Unidad</td>
                                        <td align="center">Observacion</td>
                                    </tr>
                                    <?php
                                        $k=1;
                                        foreach($metas as $meta):
                                    ?>
                                       <tr>
                                            <td align="center"><?php echo $k?></td>
                                            <td align="center"><?php echo $meta->cantidad;?></td>
                                            <td align="center"><?php echo $meta->unidad;?></td>
                                            <td align="center"><?php echo $meta->observacion;?></td>
                                       </tr>     
                                    <?php
                                        $k++;
                                        endforeach
                                    ?>
                                </table>
                            </td>                            
                         </tr>
                        <tr>
                            <td colspan="11" align="center" style="padding:5px;">
                                <b>CONVENIO</b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="11" align="center">                                
                                <?php
                                    if( $detalle_proyecto->convenio=='SI'):
                                ?>
                                    <?php if(count($convenios)==0):?>
                                        <span class="text text-danger">Convenios no registrados.</span>
                                    <?php 
                                        else:
                                    ?>
                                    <table width="100%" border="1" cellpadding="1" cellspacing="0">
                                        <tr>
                                            <td width="5%" align="center">#</td>
                                            <td width="25%" align="center">Nombre</td>
                                            <td align="center">Descripcion</td>
                                            <td align="center">Vigencia</td>
                                            <td align="center">Fecha Subscripcion</td>
                                            <td align="center">Fecha Ratificacion</td>
                                        </tr>
                                        <?php
                                            $k=1;
                                            foreach($convenios as $convenio):
                                        ?>
                                        <tr>
                                                <td align="center"><?php echo $k?></td>
                                                <td align="center"><?php echo $convenio->nombre_convenio;?></td>
                                                <td align="center"><?php echo $convenio->descripcion_convenio;?></td>
                                                <td align="center">
                                                    <?php 
                                                        $vigencia = ($convenio->vigencia)*12;
                                                        $a = round(round($vigencia)/12);
                                                        $m = round($vigencia)%12;
                                                        echo $a." años y ".$m." meses.";
                                                    ?>
                                                </td>
                                                <td align="center"><?php echo date("d/m/Y",strtotime($convenio->fecha));?></td>
                                                <td align="center">
                                                    <?php 
                                                        if($convenio->fecha_ratificacion=="0000-00-00"){
                                                            echo "Sin Fecha de ratificacion";
                                                        }
                                                        else{
                                                            echo date("d/m/Y",strtotime($convenio->fecha_ratificacion));
                                                        }
                                                    ?>
                                                </td>
                                        </tr>     
                                        <?php
                                            $k++;
                                            endforeach
                                        ?>
                                    </table>
                                    <?php
                                     endif
                                    ?>
                                <?php else:?>
                                    Proyecto sin convenio
                                <?php endif?>
                            </td>
                        </tr>
                         <tr>
                            <td colspan="11" align="center" style="padding:5px;">
                                <b>COMPONENTES</b>
                            </td>
                         </tr>
                         <tr>
                            <td colspan="11" align="center">                                
                                <table width="100%" border="1" cellpadding="1" cellspacing="0">
                                    <tr>
                                        <td width="5%" align="center">#</td>
                                        <td width="25%" align="center">Descripcion</td>
                                        <td align="center">Monto del Componente</td>
                                        <td align="center">Duracion(en días)</td>
                                        <td align="center">Modalidad</td>
                                    </tr>
                                    <?php
                                        $k=1;
                                        foreach($componentes as $componente):
                                    ?>
                                       <tr>
                                            <td align="center"><?php echo $k?></td>
                                            <td align="center"><?php echo $componente->descrip_compo_proy;?></td>
                                            <td align="center"><?php echo formato($componente->monto_comp_proy);?></td>
                                            <td align="center"><?php echo $componente->tiempo_actividad;?></td>
                                            <td align="center"><?php echo $componente->modalidad;?></td>
                                       </tr>     
                                    <?php
                                        $k++;
                                        endforeach
                                    ?>
                                </table>
                            </td>
                         </tr>
                         <tr>
                            <td colspan="11" align="center" style="padding:5px;">
                                <b>INDICADORES</b>
                            </td>
                         </tr>
                         <tr>
                            <td colspan="11" align="center">
                               <?php
                                    if(count($indicador)>0):
                               ?>
                                    <table width="100%" border="1" cellpadding="1" cellspacing="0">
                                        <tr>
                                            <td width="5%" align="center">#</td>
                                            <td width="25%" align="center">Nombre</td>
                                            <td align="center">Valor</td>
                                            <td align="center">Unidad</td>                                            
                                        </tr>
                                        <?php
                                            $k=1;
                                            foreach($indicador as $item):
                                        ?>
                                        <tr>
                                                <td align="center"><?php echo $k?></td>
                                                <td align="center"><?php echo $item->nombre_indicador;?></td>
                                                <td align="center"><?php echo formato($item->valor_indicador_porc);?></td>
                                                <td align="center"><?php echo $item->tipo;?></td>                                                
                                        </tr>     
                                        <?php
                                            $k++;
                                            endforeach
                                        ?>
                                    </table>
                                <?php
                                    else:
                                ?>
                                    <h5 class="text text-danger">Indicadores no registrados</h5>
                                <?php
                                    endif
                                ?>
                            </td>
                         </tr>
                         <tr>
                            <td colspan="11" align="center" style="padding:5px;">
                                <b>MUNICIPIOS</b>
                            </td>
                         </tr>
                         <tr>
                            <td colspan="11" align="center">                                
                                <table width="100%" border="1" cellpadding="1" cellspacing="0">
                                        <tr>
                                            <td width="5%" align="center">#</td>
                                            <td width="25%" align="center">Municipio</td>
                                            <td align="center">Provincia</td>
                                            <td align="center">Cantidad Empleo Generado</td>
                                            <td align="center">Poblacion Beneficiada</td>
                                            <td align="center">Incidencia en el Proyecto</td>
                                        </tr>
                                        <?php
                                            $k=1;
                                            foreach($municipio as $item):
                                        ?>
                                        <tr>
                                                <td align="center"><?php echo $k?></td>
                                                <td align="center"><?php echo $item->municipio;?></td>
                                                <td align="center"><?php echo $item->nombre_provincia;?></td>
                                                <td align="center"><?php echo $item->cant_genera_empleo;?></td>
                                                <td align="center"><?php echo $item->total_pobla_beneficio;?></td>
                                                <td align="center"><?php echo $item->por_incidencia_proy;?> %</td>
                                        </tr>     
                                        <?php
                                            $k++;
                                            endforeach
                                        ?>
                                </table>
                            </td>
                         </tr>                      
                        <tr>
                            <td colspan="11" align="center" style="padding:5px;">
                                <b>PUNTOS</b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="11" align="center">                                
                                <table width="100%" border="1" cellpadding="1" cellspacing="0">
                                        <tr>
                                            <td width="5%" align="center">#</td>
                                            <td align="center">Norte</td>
                                            <td align="center">Este</td>
                                        </tr>
                                        <?php
                                            $k=1;
                                            foreach($puntos as $item):
                                        ?>
                                        <tr>
                                                <td align="center"><?php echo $k?></td>
                                                <td align="center"><?php echo $item->norte;?></td>
                                                <td align="center"><?php echo $item->este;?></td>                                                                                           
                                        </tr>     
                                        <?php
                                            $k++;
                                            endforeach
                                        ?>
                                    </table>
                            </td>
                        </tr>
                        </table>                        
                        </div>
                    </div>
                    <div class="box-footer text-center">
                        <div class="row">
                                <div class="col-xs-4">
                                    <a class="btn btn-info" href="
                                            <?php
                                                /*
                                                Determinar si el proyecto tiene Sisis para regresar al listado del proyecto
                                                */
                                                        //retornar listado de proyectos con sisin
                                                        echo base_url()."proyecto/mis_proyectos/revisionProyectos";
                                            ?>
                                        "
                                    >Volver</a>
                                </div>
                                <?php if($detalle_proyecto->alta==-1):?>
                                <div class="col-xs-4">
                                    <a class="btn btn-warning" href="
                                            <?php                                             
                                                echo base_url().'proyecto/mis_proyectos/rechazarProyecto/'.$detalle_proyecto->id_proyecto;
                                            ?>
                                        "
                                    >Rechazar</a>
                                </div>
                                <div class="col-xs-4">
                                    <a class="btn btn-success" href="
                                            <?php
                                               echo base_url().'proyecto/mis_proyectos/aceptarProyecto/'.$detalle_proyecto->id_proyecto;
                                            ?>
                                        "
                                    >Aceptar</a>
                                </div>
                                <?php endif;?>
                        </div>                       
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
<script>
    function printDiv()
    {
        var divToPrint=document.getElementById('reporte');
        var newWin=window.open('','Print-Window');
        newWin.document.open();
        newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
        newWin.document.close();
        setTimeout(function(){newWin.close();},10);
    }
</script>