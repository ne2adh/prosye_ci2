        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div >
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>Registro Municipio
                    <small>Cod. proyecto: <?php echo $proyecto->id_proyecto;?></small>
                </h1> 
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
	                          <form action="<?php echo base_url();?>proyecto/municipio_controller/store" method="POST">
		                        <div class="form-group">
		                            <label for="ente_finan">Ente Financiador:</label>
		                                <select class="form-control"  id="ente_finan" name="ente_finan">
		                                    <option value="">Nombre</option>
		                                    <?php  
		                                        foreach($ente_finan as $row)
		                                        {  
		                                            echo '<option value="'.$row->id_ente.'">" '.$row->nombre. ' ' .$row->descripcion.'</option>';
		                                        }
		                                    ?>
		                                </select> 
		                       			                 
		                        </div>
		                        <div   class="form-group">
		                                    <button aling='center' type="submit" class="btn btn-success btn-flat" >
		                                        Guardar
		                                    </button>
		                        </div>
		            		</form>            
		            </div>
		        </div>
		    </section>
	</div>
	


		                       