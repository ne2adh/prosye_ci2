
        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>Registro
                <small>Componentes de Actividades</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <form action="<?php echo base_url();?>proyecto/componente_controller/store" method="POST">
                     
                              <input type="hidden"  value="<?php echo $id_tramo; ?>"  name="id_tramo" ><!--recupera el id del tramo-->
                              <input type="hidden"  value="<?php echo $proyecto->id_proyecto; ?>"  name="id_proyecto"><!--recupera el id del proyecto-->
                              <input type="hidden"  value="<?php echo $tipo->id_tipo;?>"  name="id_tipo"><!--recupera el id del tipo del proyecto-->
                                <div class="form-group">
                                    <div class="col-md-12 ">
                                    <label for="nombre_tramo">El Paquete en el cual Trabaja es:</label><br>
                                    <input class="col-md-12" type="text"  value="<?php echo $tramo_compo->nombre_tramo; ?>" name="nombre_tramo" readonly=""/>
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <div class="col-md-12">
                                    <label for="nombre_tramo">Elija la actividad a la cual le asignara componente:</label>
                                                <select class="form-control"  id="descrip_compo_proy" name="descrip_compo_proy" >
                                                            <option value="">Escoja Actividad</option>
                                                            <?php  
                                                            
                                                                foreach($actividad as $row){  
                                                                    echo '<option value="'.$row->descrip_compo_proy.'">'.$row->descrip_compo_proy. '</option>';
                                                                }
                                                            ?>
                                                </select>
                                </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                    <label for="descripcion">Descripcion del Componente</label>
                                    <input class="form-control" name="descripcion"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                    <label for="monto">Monto del Componente en Bolivianos</label>
                                    <input class="form-control" name="monto"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12"> 
                                    <div class="row">
                                       
                                             <div class="col-xs-6">
                                                <label for="fecha_inicio">
                                                 Fecha de Inicio:
                                                </label>
                                                <input type="date" id="fecha_inicio" class="form-control" name="fecha_inicio"/>
                                             </div>  
                                             <div class="col-xs-6">
                                                <label for="fecha_fin">
                                                Fecha de Conclusion:
                                                </label>     
                                                <input type="date" id="fecha_fin"  class="form-control" name="fecha_fin"/>   
                                             </div>  
                                        </div>
                                   </div>
                                </div>
                                <br>
                     
                              
                              
                                    <div class="col-xs-5"></div>
                                        <div   class="form-group">
                                            <button aling='center' type="submit" class="btn btn-success btn-flat" >
                                                  Guardar
                                            </button>
                                        </div> 
                                
                            </form> 
                           
                            <form action="<?php echo base_url();?>proyecto/convenio_controller/nuevo" method="POST"> 
                            
                             <div class="box box-solid">
                             <input type="hidden"  value="<?php echo $id_tramo; ?>" name="id_tramo" ><!--recupera el id del tramo-->
                             <input type="hidden"  value="<?php echo $proyecto->id_proyecto; ?>" name="id_proyecto"><!--recupera el id del proyecto-->
                             <input type="hidden"  value="<?php echo $tipo->id_tipo;?>" name="id_tipo"><!--recupera el id del tipo del proyecto-->
                             <input type="hidden"  value="<?php echo $proyecto->convenio;?>" name="convenio"><!--recupera el si el proy tiene convenio-->
                                 <div class="box-body">
                                    <div  class="row">
                            <div align="center" class="col-md-12" >
                                <h2>Componentes del Tramo</h2>      
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <table  class="table table-bordered btn-hover">
                                    <thead>
                                        <tr>
                                           
                                            <th>Descripcion</th>
                                            <th>Monto</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                                                                
                                        <?php if(!empty($componente)):?>
                                        
                                            <?php foreach($componente as $met):?>
                                             
                                                <tr>
                                                    
                                                    <td><?php echo $met->descripcion_componente;?></td>
                                                    <td><?php echo $met->monto;?></td>
                                                    <td>
                                                                        <div class="btn-group">
                                                                            <a href="#" class="btn btn-warning">
                                                                                <span class="fa fa-pencil">               
                                                                                </span>
                                                                            </a>
                                                                            <a href="#" class="btn btn-danger btn-remove">
                                                                                <span class="fa fa-remove">               
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                    </td>
                                                </tr>
                                            <?php endforeach;?>  
                                        <?php endif;?>
                                    </tbody>
                                    
                                </table>
                                
                           </div>
                    <!-- /.box-body -->
                </div>
                <div  align="center" class="form-group">
                                <button type="submit" class="btn btn-success btn-flat" >
                                    Continuar
                                </button>
                            </div>


                        </form>                   
                            </div>       



                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
