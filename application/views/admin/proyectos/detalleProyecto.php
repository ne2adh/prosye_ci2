
        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>Detalle del Proyecto
                <small><?php echo $detalle_proyecto->nombre_proyecto;?></small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-5">
                                Nombre proyecto:<?php echo $detalle_proyecto->nombre_proyecto;?><br>
                                Tipo proyecto:<?php echo $detalle_proyecto->tipo_proyecto;?><br>
                                Descripcion proyecto:<?php echo $detalle_proyecto->descripcion_proyecto;?><br>
                                Objetivo proyecto:<?php echo $detalle_proyecto->objetivo_proyecto;?><br>
                                Fase:<?php echo $detalle_proyecto->fase;?><br>
                                Modalidad:<?php echo $detalle_proyecto->modalidad;?><br>
                                Fecha Inicio: <?php echo date("d/m/Y", strtotime($detalle_proyecto->fecha_inicio));?><br>
                                Fecha Fin: <?php
                                    $v = explode("-", $detalle_proyecto->fecha_conclusion);
                                    echo $v[2]."/".$v[1]."/".$v[0];
                                ?><br>
                                Costo proyecto:<?php echo $detalle_proyecto->costo_proyecto;?> Bs.<br>
                            </div>
                            <div class="col-md-5">
                                Secretaria : <?php echo $detalle_proyecto->nombre_secretaria;?><br>
                                Area: <?php echo $detalle_proyecto->nombre_sub_area;?><br>
                                SubArea: <?php echo $detalle_proyecto->nombre;?><br>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-5"></div>
                            <div class="col-md-5"></div>
                            <div class="col-md-1"></div>
                        </div>
                    </div>
                    <div class="box-footer text-center">
                       <a class="btn btn-success" href="
                       <?php
                           /*
                           Determinar si el proyecto tiene Sisis para regresar al listado del proyecto
                           */
                            if($detalle_proyecto->sisin == 1){
                                //retornar listado de proyectos con sisin
                                echo base_url()."proyecto/sisin_controller/conSisin";
                            }
                            else{
                                //retornar listado de proyectos con sisin
                                echo base_url()."proyecto/sisin_controller/";
                            }
                       ?>
                       "
                       >Volver</a>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
