        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div  class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>Registro Financiamiento por Componente
                    <small> </small>
                </h1> 
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        
	                        <div class="form-group ">
	                            <label>Nombre del Convenio: </label>
	                            <input class="form-control" type="" name="nombre_convenio" value="<?php echo $convenio->nombre_convenio;?>" readonly="">
							</div>
                            <div class="form-group">
                                <label>Costo del Proyecto en Convenio: </label>
                                <input class="form-control" type="" name="costo_proyecto" value="<?php echo $componente->monto_comp_proy;?>" readonly="">
                            </div>
                            <?php  $sum=0; $con=$componente->monto_comp_proy;?>   
                             <?php foreach($participa as $met ){ 
                                      $sum=$sum+($met->monto_municipio);}
                                      
                                      foreach($ente1 as $met ){ 
                                       $sum=$sum+($met->monto_ente);} ?>
                             <?php if ($sum<$con):?>
                               
		                      <form action="<?php echo base_url();?>proyecto/participantes/escoge/<?php echo $componente->monto_comp_proy;?>" method="POST">
    		                        <input type="hidden"  value="<?php echo $id_proyecto; ?>" name="id_proyecto">
                                    <input type="hidden"  value="<?php echo $id_convenio; ?>" name="id_convenio">
                                    <input type="hidden"  value="<?php echo $id_componente_proy; ?>" name="id_componente_proy">
                                    <div class="form-group">
    		                            <h1 align="center">Estructura de Financiamiento</h1>
    		                        </div>
    		                          <div class="form-group <?php echo !empty(form_error('finan'))? 'has-error':'';?>">
                                        <label for="finan">Escoja participante para el Proyecto</label>   
                                        <select class="form-control"  id="" name="finan" >
                                                    <option value="">Seleccione una Opcion</option>
                                                    <option value="ente">Ente Financiamiento</option>
                                                    <option value="municipio">Municipio</option>
                                        </select> 
                                        <?php echo form_error("finan","<span class='help-block'>","</span>"); ?>
                                      </div>
                                
                                        <div   class="form-group">
                                                    <button aling='center' type="submit" class="btn btn-success btn-flat" >
                                                        Continuar
                                                    </button>
                                        </div>
                               
                                 </form>

                             <?php endif; ?>
                             <form action="<?php echo base_url();?>proyecto/componente_proy_controller/nuevo/<?php echo $id_proyecto;?>" method="POST">    
                                     
                                    <input type="hidden"  value="<?php echo $id_proyecto; ?>" name="id_proyecto">
                                    <input type="hidden"  value="<?php echo $id_convenio; ?>" name="id_convenio">
                                    <input type="hidden"  value="<?php echo $id_componente_proy; ?>" name="id_componente_proy">
                                   
                            <div class="box box-solid">
                                <div class="box-body">
                                    <div  class="row">
                                        <div align="center" class="col-md-12" >
                                            <h3>Paticipacion de Municipios en el Proyecto</h3>      
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table  class="table table-striped" role="grid">
                                                <thead >
                                                    <tr class="success">
                                                        <th  class="col-sm-1">N°</th>
                                                        <th  class="col-sm-3">Municipio</th>
                                                        <th  class="col-sm-3">Monto</th>
                                                       
                                                        <th scope="col" class="col-sm-3">Opciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php $sum=0; if(!empty($municipio)):?>
                                                    <?php foreach($participa as $met ):?>
                                                       
                                                    <tr>
                                                        <td><?php global $i; $i=$i+1; echo $i;?></td>
                                                           <td><?php echo $met->municipio;?>
                                                           </td>
                                                            <td><?php $sum=$sum+($met->monto_municipio); echo $met->monto_municipio;?>
                                                            </td>
                                                            <td>    
                                                            
                                                            <a href="<?php echo base_url();?>proyecto/participantes/editMunicipio/<?php echo $met->id_participa;?>/<?php echo $id_componente_proy;?>/<?php echo $id_proyecto;?>/<?php echo $id_convenio;?>" class="btn btn-warning">
                                                             <button type="button"  id=""  class="btn btn-warning" >Actualizar</button></a>
                                                            <!-- Botón para borrar -->
                                                            <a href="<?php echo base_url();?>proyecto/participantes/delete/<?php echo $met->id_participa;?>/<?php echo $id_componente_proy;?>/<?php echo $id_proyecto;?>/<?php echo $id_convenio;?>" class="btn btn-danger btn-remove">
                                                                <button type="button" class="btn btn-danger" >Borrar</button>
                                                            </a>
                                                            
                                                        </td>
                                                    </tr>
                                                    <?php endforeach;?>  
                                                    <?php endif;?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>




                                    <div class="box box-solid">
                                <div class="box-body">
                                    <div  class="row">
                                        <div align="center" class="col-md-12" >
                                            <h3>Financiadores en el Proyecto</h3>      
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table  class="table table-striped" role="grid">
                                                <thead >
                                                    <tr class="success">
                                                        <th  class="col-sm-1">N°</th>
                                                        <th  class="col-sm-3">Entidades</th>
                                                        <th  class="col-sm-3">Monto</th>
                                                       
                                                        <th  class="col-sm-3">Opciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i=0; if(!empty($ente1)):?>
                                                    <?php foreach($ente1 as $met ):?>
                                                    <tr>
                                                        <td><?php global $i; $i=$i+1; echo $i;?></td>
                                                           <td><?php echo $met->nombre;?>
                                                           </td>
                                                            <td><?php $sum=$sum+($met->monto_ente); echo $met->monto_ente;?>
                                                            </td>
                                                            <td>    
                                                            
                                                            <a href="<?php echo base_url();?>proyecto/participantes/editEnte/<?php echo $met->id_participa;?>/<?php echo $id_componente_proy;?>/<?php echo $id_proyecto;?>/<?php echo $id_convenio;?>" class="btn btn-warning">
                                                             <button type="button" data-toggle="modal" id="submitBtn"  class="btn btn-warning" >Actualizar</button></a>
                                                            <!-- Botón para borrar -->
                                                            <a href="<?php echo base_url();?>proyecto/participantes/delete/<?php echo $met->id_participa;?>/<?php echo $id_componente_proy;?>/<?php echo $id_proyecto;?>/<?php echo $id_convenio;?>" class="btn btn-danger btn-remove">
                                                                <button type="button" class="btn btn-danger" >Borrar</button>
                                                            </a>
                                                            
                                                        </td>
                                                    </tr>
                                                    <?php endforeach;?>  
                                                    <?php endif;?>
                                                <td><?php $sum; ?></td>
                                                </tbody>

                                            </table>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <?php  $con=$componente->monto_comp_proy;
                                     $nom=$componente->descrip_compo_proy;
                                     $resta=$con-$sum;
                                    $tot=$total_participa;
                                    //echo 'esto es la suma'.$tot;
                                    echo 'TIENE UN MONTO POR ASIGNAR AL COMPONENTE  '.$nom.'  DE  '.$resta.' Bolivianos'; 
                                    if ($resta==0) : ?>
                                        <div  align="center" class="form-group">
                                        <button type="submit" class="btn btn-success btn-flat" >
                                            Continuar
                                        </button>
                                    </div>
                                    <?php  endif;?>
                                    
                                    
                                </div>
                            </div>   
                        </form>
                        <?php $con=0;
                        $nom='';$resta=''; ?>
                            </div>
                        </div>
                    </section>
                </div>


		                        