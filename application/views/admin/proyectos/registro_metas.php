<?php
                        function formato($cantidad){
                            $formatted = number_format(sprintf('%0.2f', preg_replace("/[^0-9.]/", "", $cantidad)), 2);
                            return $cantidad < 0 ? "({$formatted})" : "{$formatted}";
                        }											                
    ?>
<!-- dom para angular-->
<div class="content-wrapper" ng-app="app" ng-controller="ctrl">
  <section class="content-header">
    <h1>Registro Metas
        <small>Proyecto: <?php echo $proyecto->nombre_proyecto; ?></small>
    </h1>
  </section>
  <section class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                  <div class="panel panel-info">
                    <div class="panel-heading">
                      </div>
                    <div class="panel-body">
                      <div class="row">
                          <div class="col-md-12">
                            <div class="row">
                              <div class="col-md-6">
                                </div>
                              <div class="col-md-6">
                                <button ng-click="amRol(undefined)" class="btn btn-primary btn-flat pull-right" data-toggle="modal" data-target="#dialogoSeccion">
                                    <span class="fa fa-plus">
                                    </span> agregar descripcion de meta
                                </button>
                              </div>
                            </div>
                            <!---->
                            <form action="<?php echo base_url();?>proyecto/metas_controller/store" method="POST" autocomplete="off">
                            <input type="hidden" name="id_proyecto" value="<?php echo $id_proyecto; ?>" >
                              <div class="form-group">
                                <div class=" <?php echo !empty(form_error('meta'))? 'has-error':'';?> ">
                                <label for="meta">Escoja una descripcion de la meta del Proyecto:
                                </label>
                                <div class="alert alert-info ayuda" id="ayuda_descripcion_meta">
                                  Seleccione la descripcion de la meta del proyecto.(Campo requerido).
                                </div>

                                  <select ng-model="meta" class="form-control" ng-options="item.id_metas_proy as item.descripcion for item in metas"></select>
                                 <input type="text" style="display: none;" name="meta" ng-model="meta">
                                 <?php echo form_error("meta","<span class='help-block'>","</span>"); ?>
                             
                                            </div>
                                            <br>

                                            <div class="row">
                                                <div class="<?php echo !empty(form_error('cantidad'))? 'has-error':'';?>" >
                                                    <div class="col-xs-4">
                                                      <label for="cantidad">
                                                        Ingrese Cantidad de medicion:
                                                      </label>
                                                        <input class='form-control' type='text' name='cantidad' value='<?php echo set_value("cantidad");?>' min="0"
                                                        onfocus="ShowHide('ayuda_medicion_meta','block')" onblur="ShowHide('ayuda_medicion_meta','none')" data-type="currency">
                                                        <div class="alert alert-info ayuda" id="ayuda_medicion_meta">
                                                          Ingrese la Cantidad de Medicion de la meta.(Campo requerido).
                                                        </div>
                                                        <?php echo form_error("cantidad","<span class='help-block'>","</span>"); ?>
                                                    </div>
                                                </div>
                                                <div class="<?php echo !empty(form_error('unidad'))? 'has-error':'';?>" >
                                                    <div class="col-xs-2">
                                                      <label for="unidad">
                                                        Unidad de medicion:
                                                      </label>
                                                        <input class='form-control' type='text' name='unidad' value='<?php echo set_value("unidad");?>' onkeyup="javascript:this.value=this.value.toUpperCase();"
                                                        onfocus="ShowHide('ayuda_unidad_meta','block')" onblur="ShowHide('ayuda_unidad_meta','none')">
                                                        <div class="alert alert-info ayuda" id="ayuda_unidad_meta">
                                                          Ingrese la Unidad de Medicion de la meta.(Campo requerido).
                                                        </div>
                                                        <?php echo form_error("unidad","<span class='help-block'>","</span>"); ?>
                                                    </div>
                                                </div>
                                                <div class="<?php echo !empty(form_error('observacion'))? 'has-error':'';?>" >
                                                    <div class="col-xs-6">

                                                        <label for="observacion">
                                                           Observaciones Adicionales :
                                                        </label>
                                                        <input class='form-control' name='observacion' style="text-trasnform:uppercase;"
                                                        onkeyup="javascript:this.value=this.value.toUpperCase();" value='<?php echo set_value("observacion");?>'
                                                        onfocus="ShowHide('ayuda_observacion_meta','block')" onblur="ShowHide('ayuda_observacion_meta','none')">
                                                        <?php /*<textarea type="text"  class="form-control" name="observacion" id="observacion" rows="1" >
                                                        <?php echo set_value("observacion");?>
                                                        </textarea>*/?>
                                                        <div class="alert alert-info ayuda" id="ayuda_observacion_meta">
                                                          Ingrese una Observacion.(Campo opcional).
                                                        </div>
                                                        <?php echo form_error("observacion","<span class='help-block'>","</span>"); ?>

                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div align="center" class="form-group">
                                      <button type="submit" class="btn btn-success btn-flat" >
                                       Agregar Meta
                                      </button>
                                            </div>
                                       </div>
                                                            <?php
                                                                if(isset($error)){
                                                                    if($error == 1){
                                                                        echo '<div class="alert alert-danger" role="alert">  <strong>La meta esta Repetida!</strong> Ingrese otra meta.
                                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                            </div>';

                                                                                    }
                                                                                    }
                                                             ?>

                            </form>

                            <!---->
                            <form action="<?php echo base_url();?>proyecto/municipio_controller/ingreso/<?php echo $id_proyecto; ?>" method="POST">
                              <div class="box box-solid">
                                        <div class="box-body">
                                              <div  class="row">
                                                    <div align="center" class="col-md-12" >
                                                        <h3>Metas del Proyectos</h3>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <table  class="table table-striped" name="tabla">
                                                            <thead >
                                                                <tr class="success">
                                                                    <th >N°</th>
                                                                    <th >Descripcion</th>
                                                                    <th >Cantidad</th>
                                                                    <th >Unidad</th>
                                                                    <th >Observaciones</th>
                                                                    <th >Opciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php if(!empty($metas1)):?>
                                                                <?php foreach($metas1 as $met ):?>
                                                                <tr id="fila">
                                                                    <td><?php global $i; $i=$i+1; echo $i;?></td>
                                                                    <td><?php $num=$met->descripcion;
                                                                        print_r($this->metas_model->metas_proy1($num));?>
                                                                    </td>
                                                                    <td><?php echo formato($met->cantidad);?> </td>
                                                                    <td><?php echo $met->unidad;?> </td>

                                                                    <td><?php echo $met->observacion;?> </td>
                                                                    <td>
                                                                        <!-- Botón para guardar la información actualizada -->
                                                                        <span >
                                                                            <!-- Botón para mostrar el formulario de actualizar -->
                                                                            <a href="<?php echo base_url();?>proyecto/metas_controller/edit/<?php echo $met->id;?>/<?php echo $met->id_proyecto;?>" class="btn btn-warning" title="Editar Metas">
                                                                                 <span class="fa fa-pencil">   </span>
                                                                               <?php //<button type="button" data-toggle="modal" id="submitBtn"  class="btn btn-warning" >Actualizar</button>?>
                                                                            </a>
                                                                                    <!-- Botón para borrar -->
                                                                            <a href="<?php echo base_url();?>proyecto/metas_controller/delete/<?php echo $met->id;?>/<?php echo $met->id_proyecto;?>" class="btn btn-danger btn-remove"title="Eliminar Meta">
                                                                                 <span class="fa fa-remove">  </span>
                                                                            </a>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                    <?php endforeach;?>
                                                                    <?php endif;?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                            <!-- /.box-body -->
                                                </div>

                                                    <div  align="center" class="form-group" >
                                                      <button type="submit" class="btn btn-success btn-flat" >
                                                                      Continuar
                                                      </button>
                                                    </div>

                                            </div>
                              </div>
                              </form>
                            <!---->
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
      </div>
  </section>

<script type="text/ng-template" id="agregar.html">
  <div>
      <div class="box box-primary">
          <div class="modal-header box-header with-border text-center">
              <h4 class="modal-title box-title">{{titulo}}</h4>
              <button class="close" type="button" ng-click="cerrar()">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body box-body">
              <form name="form">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group">
                        <label class="control-label">Descripcion Meta:</label>
                        <input type="text" class="form-control" name="meta" ng-model="meta.descripcion" placeholder="Descripcion de la meta..." onkeyup="javascript:this.value=this.value.toUpperCase();" required>
                        <div ng-show="form.meta.$touched">
                            <span class="text-help text-danger" ng-show="form.meta.$error.required">(*) Valor requerido</span>
                            <span class="text-help text-danger" ng-show="form.meta.$error.pattern">(*) Solo texto</span>
                        </div>
                    </div>
                  </div>
                </div>
              </form>
          </div>
          <div class="modal-footer box-footer text-center">
              <button class="btn btn-success" ng-show="estado" ng-disabled="!form.$valid || form.$pending" ng-click="registrar(meta)"> Registrar</button>
              <button class="btn btn-warning" ng-show="!estado" ng-disabled="!form.$valid || form.$pending" ng-click="editar(meta)"> Editar</button>
              <a class="btn btn-default" ng-click="cerrar()"> <i class="fa fa-times"></i> Cerrar </a>
          </div>
          </div>
      </div>
  </div>
</script>

<script type="text/ng-template" id="eliminar.html">
  <div>
      <div class="box box-danger">
          <div class="modal-header box-header with-border text-center">
              <h4 class="modal-title box-title">{{titulo}}</h4>
              <button class="close" type="button" ng-click="cerrar()">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body box-body">
              <form name="form">
                <div class="row">
                  <div class="col-lg-12">
                    <p class="text-center">{{mensaje}}</p>
                  </div>
                </div>
              </form>
          </div>
          <div class="modal-footer box-footer text-center">
              <button class="btn btn-danger" ng-click="confirmar(rol)"> Confirmar</button>
              <a class="btn btn-default" ng-click="cerrar()"> <i class="fa fa-times"></i> Cerrar </a>
          </div>
      </div>
  </div>
</script>

</div>




<!--angularjs :) -->
<script src="<?php echo base_url();?>assets/angular.min.js"></script>
<script src="<?php echo base_url();?>assets/ui-bootstrap.min.js"></script>

<script>
let ngApp = function(){
  'use strict';
  let app = angular.module('app', ['ui.bootstrap']);
  app.controller('ctrl', function($scope, $http, $uibModal) {

    //$scope.metas=<?php echo json_encode($metas);?>;

    $scope.listarMetas=()=>{
      $http.get("<?php echo base_url('proyecto/metas_controller/listarMetas'); ?>").then(function(response) {
        $scope.metas = response.data;
        $scope.meta = $scope.metas[0].id_metas_proy;
      });
    }
    $scope.listarMetas();

    //console.log($scope.listarMetas);
	$scope.amRol = (meta) => {
        $uibModal.open({
          templateUrl: 'agregar.html',
          backdrop: 'static',
          scope:$scope,
          controller: function ($scope,$uibModalInstance) {
              if(meta===undefined){
                $scope.titulo='Nuevo Registro';
                $scope.estado = true;
                $scope.meta = [];
              }else{
                $scope.titulo='Editar Registro';
                $scope.meta = meta;
                $scope.estado = false;
              }

              $scope.registrar = (meta)=> {
                let req = {
                   method: 'GET',
                   url: '<?php echo base_url('proyecto/metas_controller/saveMeta?meta=');?>'+meta.descripcion,
                   //data: { rol:rol }
                }

                $http(req).then(function(response) {
                  $scope.cerrar();
                  $scope.listarMetas();
                  //console.log($scope.listarMetas);
                });
              };

              $scope.editar = (meta)=> {
                let ruta = '<?php echo base_url('administrador/Rol/editRol?id=');?>'+rol.id_rol+'&nombre='+rol.nombre;
                let req = {
                   method: 'GET',
                   url: ruta
                   //data: { rol:rol }
                }
                $http(req).then(function(response) {
                  $scope.cerrar();
                  //$scope.listarRoles();
                });
              };

              $scope.cerrar = () => {
                  $uibModalInstance.dismiss();
              };
          }
        });
    };


    $scope.eliminar =(rol)=>{
      $uibModal.open({
        templateUrl: 'eliminar.html',
        backdrop: 'static',
        scope:$scope,
        controller: function ($scope,$uibModalInstance) {

            $scope.titulo='Eliminar Registro';
            $scope.mensaje = `Esta seguro que desea eliminar el registro: ${rol.nombre}`;

            $scope.confirmar =()=>{
              let ruta = '<?php echo base_url('administrador/Rol/deleteRol?id_rol=');?>'+rol.id_rol;
              let req = {
                 method: 'GET',
                 url: ruta
                 //data: { rol:rol }
              }
              $http(req).then(function(response) {
                $scope.cerrar();
                $scope.listarRoles();
              });
            }

            $scope.cerrar = () => {
                $uibModalInstance.dismiss();
            };
        }
      });

    };

    console.log('funciona');
  });
}();
</script>
<!---->
