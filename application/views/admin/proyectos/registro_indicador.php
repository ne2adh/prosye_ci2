
<!-- dom para angular-->
<div class="content-wrapper" ng-app="app" ng-controller="ctrl">
  <section class="content-header">
    <h1>
    Indicadores
      <small><?php  echo 'Proyecto:  '. $proyecto->nombre_proyecto; ?></small>
    </h1>
  </section>
  <section class="content">
    <div class="box box-solid">
        <div class="box-body">
        	<?php
                    function formato($cantidad){
                        $formatted = number_format(sprintf('%0.2f', preg_replace("/[^0-9.]/", "", $cantidad)), 2);
                        return $cantidad < 0 ? "({$formatted})" : "{$formatted}";
                    }				                
            ?>
            <div class="row">
                <div class="col-md-12">
                  <div class="panel panel-info">
                    <div class="panel-heading"></div>

                    <div class="panel-body">

              <div class="row">
                <div class="col-md-12">
                  <?php //if ($dato=="ECONOMICO") :?>
                 <form action="<?php echo base_url();?>proyecto/indicador_controller/store_porc" method="POST" autocomplete="off">
                  <input type="hidden" name="id_proyecto" value="<?php echo isset($id_tramo)?$id_tramo:$id_proyecto; ?>">
                   <!--Se recupera el id del proyecto -->
                   <input type="hidden" name="convenio" value="<?php echo $proyecto->convenio;?>"> <!--Se recupera el id de convenio -->
                  <div class="col-md-8">
                      <div class="form-group <?php echo !empty(form_error('indicador'))? 'has-error':'';?>" >
                         <label for="nombre">Escoga el Indicador</label>
                         <select ng-model="indicador" class="form-control" ng-options="item.abreviatura as (item.descripcion+' - '+item.abreviatura) for item in indicadores" required>
                              <option value="">--Seleccione una Opcion--</option>
                         </select>
                         <input type="text" style="display: none;" name="indicador" ng-model="indicador">
                         <?php echo form_error("indicador","<span class='help-block'>","</span>"); ?>
                      </div>
                  </div>

                  
                    <div class="col-md-2">
                      <label></label>
                    <button type="button" ng-click="amRol(undefined)" class="btn btn-primary btn-flat pull-right" data-toggle="modal" data-target="#dialogoSeccion">
                        <span class="fa fa-plus">
                        </span> Nuevo indicador
                    </button>
                    </div>
                    <div class="col-md-6"></div>
                  </div><br>

                  <div class="col-md-12">
                  <div class="col-md-8">
                  <?php ?>
                      <div class="form-group <?php echo !empty(form_error('valor_porc'))? 'has-error':'';?> ">
                      <label for="valor_porc">Ingrese el Valor del Indicador</label>
                      <input class="form-control" name="valor_porc" type="text"  value="<?php echo set_value('valor_porc'); ?>" required=""
                            data-type="currency" onfocus="ShowHide('ayuda_costo_proyecto','block')" onblur="ShowHide('ayuda_costo_proyecto','none')" >										
                      <div class="alert alert-info ayuda" id="ayuda_costo_proyecto">
                        Ingrese el Valor del Indicador(Campo requerido).
                      </div>
                      <?php echo form_error("valor_porc","<span class='help-block'>","</span>"); ?>
                      </div>
                  </div>
                  </div>
                  <br>

                  <div class="col-md-12">
                  <div class="col-md-8">
                  <?php ?>
                      <div class="form-group <?php echo !empty(form_error('tipo'))? 'has-error':'';?> ">
                      <label for="tipo">Ingrese unidad del indicador</label>
                      <!--<select name="tipo" class="form-control">
                        <option value=" "<?php echo  set_select( 'tipo' , '', TRUE );?>>seleccione una opcion</option>
                        <option value="Bs" <?php echo set_select ( 'tipo' ,  'Bs' );?>>Bs.</option>
                        <option value="%" <?php echo set_select ( 'tipo', '%');?>>%</option>
                        <option value=" " <?php echo set_select ('tipo', ' ')?>>Adimensional</option>
                      </select>-->
                      <input type="text" name="tipo" class="form-control" 
                             onfocus="ShowHide('ayuda_unidad_proyecto','block')" onblur="ShowHide('ayuda_unidad_proyecto','none')" >
                      <div class="alert alert-info ayuda" id="ayuda_unidad_proyecto">
                          Escriba la unidad del Indicador(Campo requerido).
                      </div>
                      <?php echo form_error("tipo","<span class='help-block'>","</span>");?>
                    </div>
                  </div>
                  </div><br>

                  <div class="col-md-5"></div>
                   <div class="form-group">
                      <button aling="center" type="submit" class="btn btn-success btn-flat" >
                          Guardar
                      </button>
                   </div>
                   <?php //endif; ?>
                              <?php
                                if(isset($error)){
                                  if($error == 1){
                                  echo '<div class="alert alert-danger" role="alert">  <strong>El indicador esta Repetido!</strong> Ingrese otro indicador.
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                  </div>';
                                  };
                                }
                              ?>


                        </form>

                  <br><br>
                    <form action="<?php echo base_url();?>proyecto/mis_proyectos/propios"   method="POST">
                       <input type="hidden" name="id_proyecto" value="<?php echo isset($id_tramo)?$id_tramo:$id_proyecto; ?>"> <!--Se recupera el id del proyecto -->
                       <input type="hidden" name="convenio" value="<?php echo $proyecto->convenio;?>"> <!--Se recupera el id de convenio -->
                <div class="box box-solid">
                  <div class="box-body">
                    <div  class="row">
                      <div align="center" class="col-md-12">
                          <h2>Indicadores del Proyectos</h2>
                      </div>
                  <hr>
                  <div class="row">
                      <div class="col-md-12">
                          <table  class="table table-bordered btn-hover"> 
                              <thead>
                                  <tr>
                                      <th>Numero</th>
                                      <th>Nombre Indicador</th>
                                      <th>Valor Indicador</th>
                                      <th>Opciones</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <?php $c=0; ?>
                                  <?php   if(!empty($indicador)):?>
                                      <?php  foreach($indicador as $met ):?>
                                          <tr>
                                              <td><?php echo $c=$c+1; //echo $met->id_proyecto;?></td>
                                              <td><?php echo $met->nombre_indicador;?></td>
                                              <td><?php if ($met->valor_indicador==0)
                                                  {echo formato($met->valor_indicador_porc)." ".$met->tipo;}?>
                                              </td>
                                              <td>
                                                  <div class="btn-group">
                                                       <a href="<?php echo base_url();?>proyecto/indicador_controller/ingresoEdit/<?php echo $met->id;?>/<?php echo $met->id_proyecto;?>/<?php if ($met->valor_indicador==0)
                                                            {
                                                              echo "ECONOMICO";
                                                            }
                                                          ?>" class="btn btn-warning">
                                                          <span class="fa fa-pencil"title="Editar Indicador">             </span>
                                                       </a>
                                                       <a href="<?php echo base_url();?>proyecto/indicador_controller/delete/<?php echo $met->id;?>/<?php echo $met->id_proyecto;?>"  class="btn btn-danger"title="Eliminar Indicador">
                                                          <span class="fa fa-remove">
                                                          </span>

                                                      </a>
                                                  </div>
                                              </td>
                                          </tr>
                                      <?php endforeach; ?>
                                  <?php endif;?>
                              </tbody>
                          </table>
                     </div>
                  <!-- /.box-body -->
                  </div>
                </div>
              </div>
            </div>
                  <?php if ($c!=0):?>
                   <div class="col-md-5"></div>
                   <div   class="form-group">
                      <button type="submit" class="btn btn-success btn-flat" >
                          Continuar
                      </button>
                   </div>
                   <?php endif;?>
                    </form>
              </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
      </div>
  </section>
<script type="text/ng-template" id="agregar">
  <div>
      <div class="box box-primary">
          <div class="modal-header box-header with-border text-center">
              <h4 class="modal-title box-title">{{titulo}}</h4>
              <button class="close" type="button" ng-click="cerrar()">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body box-body">
              <form name="form">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group">
                        <label class="control-label">abreviatura del indicador:</label>
                        <input type="text" class="form-control" name="indicador" ng-model="indicador.abreviatura" placeholder="Abreviatura del indicador..." required onkeyup="javascript:this.value=this.value.toUpperCase();" >
                        <div ng-show="form.indicador.$touched">
                            <span class="text-help text-danger" ng-show="form.indicador.$error.required">(*) Valor requerido</span>
                            <span class="text-help text-danger" ng-show="form.indicador.$error.pattern">(*) Solo texto</span>
                        </div>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="form-group">
                        <label class="control-label">Nombre completo del indicador:</label>
                        <input type="text" class="form-control" name="indicador2" ng-model="indicador.descripcion" placeholder="Nombre completo del indicador..." required onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <div ng-show="form.indicador.$touched">
                            <span class="text-help text-danger" ng-show="form.indicador.$error.required">(*) Valor requerido</span>
                            <span class="text-help text-danger" ng-show="form.indicador.$error.pattern">(*) Solo texto</span>
                        </div>
                    </div>
                  </div>
                </div>
              </form>
          </div>
          <div class="modal-footer box-footer text-center">
              <button class="btn btn-success" ng-show="estado" ng-disabled="!form.$valid || form.$pending" ng-click="registrar(indicador)"> Registrar</button>
              <button class="btn btn-warning" ng-show="!estado" ng-disabled="!form.$valid || form.$pending" ng-click="editar(indicador)"> Editar</button>
              <a class="btn btn-default" ng-click="cerrar()"> <i class="fa fa-times"></i> Cerrar </a>
          </div>
          </div>
      </div>
  </div>
  </script>

<script type="text/ng-template" id="eliminar.html">
  <div>
      <div class="box box-danger">
          <div class="modal-header box-header with-border text-center">
              <h4 class="modal-title box-title">{{titulo}}</h4>
              <button class="close" type="button" ng-click="cerrar()">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body box-body">
              <form name="form">
                <div class="row">
                  <div class="col-lg-12">
                    <p class="text-center">{{mensaje}}</p>
                  </div>
                </div>
              </form>
          </div>
          <div class="modal-footer box-footer text-center">
              <button class="btn btn-danger" ng-click="confirmar(indicador)"> Confirmar</button>
              <a class="btn btn-default" ng-click="cerrar()"> <i class="fa fa-times"></i> Cerrar </a>
          </div>
      </div>
  </div>
</script>

</div>




<!--angularjs :) -->
<script src="<?php echo base_url();?>assets/angular.min.js"></script>
<script src="<?php echo base_url();?>assets/ui-bootstrap.min.js"></script>

<script>
let ngApp = function(){
  'use strict';
  let app = angular.module('app', ['ui.bootstrap']);
  const agregarTemplate = `
  <div>
      <div class="box box-primary">
          <div class="modal-header box-header with-border text-center">
              <h4 class="modal-title box-title">{{titulo}}</h4>
              <button class="close" type="button" ng-click="cerrar()">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body box-body">
              <form name="form">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group">
                        <label class="control-label">abreviatura del indicador:</label>
                        <input type="text" class="form-control" name="indicador" ng-model="indicador.abreviatura" placeholder="Abreviatura del indicador..." required onkeyup="javascript:this.value=this.value.toUpperCase();" >
                        <div ng-show="form.indicador.$touched">
                            <span class="text-help text-danger" ng-show="form.indicador.$error.required">(*) Valor requerido</span>
                            <span class="text-help text-danger" ng-show="form.indicador.$error.pattern">(*) Solo texto</span>
                        </div>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="form-group">
                        <label class="control-label">Nombre completo del indicador:</label>
                        <input type="text" class="form-control" name="indicador2" ng-model="indicador.descripcion" placeholder="Nombre completo del indicador..." required onkeyup="javascript:this.value=this.value.toUpperCase();">
                        <div ng-show="form.indicador.$touched">
                            <span class="text-help text-danger" ng-show="form.indicador.$error.required">(*) Valor requerido</span>
                            <span class="text-help text-danger" ng-show="form.indicador.$error.pattern">(*) Solo texto</span>
                        </div>
                    </div>
                  </div>
                </div>
              </form>
          </div>
          <div class="modal-footer box-footer text-center">
              <button class="btn btn-success" ng-show="estado" ng-disabled="!form.$valid || form.$pending" ng-click="registrar(indicador)"> Registrar</button>
              <button class="btn btn-warning" ng-show="!estado" ng-disabled="!form.$valid || form.$pending" ng-click="editar(indicador)"> Editar</button>
              <a class="btn btn-default" ng-click="cerrar()"> <i class="fa fa-times"></i> Cerrar </a>
          </div>
          </div>
      </div>
  </div>
    `;
  app.controller('ctrl', function($scope, $http, $uibModal) {



    $scope.listarIndicadores=()=>{
      $http.get("<?php echo base_url('proyecto/indicador_controller/listarIndicadores'); ?>").then(function(response) {
        console.log('1');
        $scope.indicadores = response.data;
        $scope.indicador = "";
      });
    }
    $scope.listarIndicadores();        

    $scope.amRol = (indicador) => {
        console.log('2');
        $uibModal.open({
          template: agregarTemplate,
          backdrop: 'static',
          scope:$scope,
          controller: function ($scope,$uibModalInstance) {
              if(indicador===undefined){
                $scope.titulo='Nuevo Registro';
                $scope.estado = true;
                $scope.indicador = [];
              }else{
                $scope.titulo='Editar Registro';
                $scope.indicador = indicador;
                $scope.estado = false;
              }

              $scope.registrar = (indicador)=> {
                let req = {
                   method: 'GET',
                   url: '<?php echo base_url('proyecto/indicador_controller/saveIndicador?indicador='); ?>'+indicador.abreviatura+'&indicador2='+indicador.descripcion,
                   //data: { rol:rol }
                }
                $http(req).then(function(response) {                  
                  alert("Indicador Registrado");
                  $scope.cerrar();
                  //console.log($scope.indicador);
                  $scope.listarIndicadores();

                });
              };

              $scope.editar = (rol)=> {
                let ruta = '<?php echo base_url('administrador/Rol/editRol?id=');?>'+rol.id_rol+'&nombre='+rol.nombre;
                let req = {
                   method: 'GET',
                   url: ruta
                   //data: { rol:rol }
                }
                $http(req).then(function(response) {
                  $scope.cerrar();
                  $scope.listarRoles();
                });
              };

              $scope.cerrar = () => {
                  $uibModalInstance.dismiss();
              };
          }
        });
    };


    $scope.eliminar =(rol)=>{
      $uibModal.open({
        templateUrl: 'eliminar.html',
        backdrop: 'static',
        scope:$scope,
        controller: function ($scope,$uibModalInstance) {

            $scope.titulo='Eliminar Registro';
            $scope.mensaje = `Esta seguro que desea eliminar el registro: ${rol.nombre}`;

            $scope.confirmar =()=>{
              let ruta = '<?php echo base_url('administrador/Rol/deleteRol?id_rol=');?>'+rol.id_rol;
              let req = {
                 method: 'GET',
                 url: ruta
                 //data: { rol:rol }
              }
              $http(req).then(function(response) {
                $scope.cerrar();
                $scope.listarRoles();
              });
            }

            $scope.cerrar = () => {
                $uibModalInstance.dismiss();
            };
        }
      });

    };

    console.log('funciona');
  });
}();
</script>
<!---->
