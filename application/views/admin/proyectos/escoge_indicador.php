        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->

        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Indicadores
        	        <small><?php  echo 'Proyecto:  '. $proyecto->nombre_proyecto; ?></small>
                </h1>
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <form action="<?php echo base_url();?>/proyecto/indicador_controller/nuevo"   method="POST">
                            <input type="hidden" name="id_proyecto" value="<?php echo isset($id_tramo)?$id_tramo:$id_proyecto; ?>">
                            <label>Ingrese el tipo de indicador a asignar: <input type="text" name="resp" list="listaIndicador" required=""></label>
                                <datalist id="listaIndicador">
                                  <!--<label>O elige uno de la siguiente lista</label>-->
                                  <!--<select name="resp">-->
                                    <option value="PORCENTUAL" label="Indicador Porcentual %"></option>
                                    <option value="ECONOMICO" label="Indicador Economico Bs."></option>
                                    <!-- </select>-->
                                  <!--</label>-->
                                </datalist>
                            <div>
                                    <button  type="submit" class="btn btn-success btn-flat" aling="center">
                                        Aceptar
                                    </button>
                            </div>
                        </form>


                            <!--<form action="<?php echo base_url();?>proyecto/sisin_controller/ingresoSisin" method="POST"> -->
                        <form action="<?php echo base_url();?>proyecto/mis_proyectos/propios"   method="POST">
                             <input type="hidden" value="<?php echo $id_proyecto; ?>" name="id_proyecto"> <!--Se recupera el id del proyecto -->
                         <input type="hidden" name="convenio" value="<?php echo $proyecto->convenio;?>"> <!--Se recupera el id de convenio -->
                            <div class="box box-solid">
                                 <div class="box-body">
                                    <div  class="row">
                            <div align="center" class="col-md-12">
                                <h2>Indicadores del Proyectos</h2>
                            </div>


                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <table  class="table table-bordered btn-hover">
                                    <thead>
                                        <tr>
                                            <th>Numero</th>
                                            <th>Nombre Indicador</th>
                                            <th>Valor Indicador</th>
                                                                                       <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $c=0; ?>
                                        <?php   if(!empty($indicador)):?>

                                            <?php  foreach($indicador as $met ):?>

                                                <tr>
                                                    <td><?php echo $c=$c+1; //echo $met->id_proyecto;?></td>
                                                    <td><?php echo $met->nombre_indicador;?></td>
                                                    <td><?php if ($met->valor_indicador==0)
                                                                        {echo $met->valor_indicador_porc." Bs";}else {
                                                                            echo $met->valor_indicador." %" ;
                                                                        } ?>
                                                    </td>
                                                    <td>
                                                        <div class="btn-group">
                                                             <a href="<?php echo base_url();?>proyecto/indicador_controller/ingresoEdit/<?php echo $met->id;?>/<?php echo $met->id_proyecto;?>/<?php if ($met->valor_indicador_porc==0)
                                                                  {
                                                                   echo "PORCENTUAL";
                                                                  } else
                                                                  {
                                                                    echo "ECONOMICO";
                                                                  }
                                                                ?>" class="btn btn-warning">
                                                                <span class="fa fa-pencil"title="Editar Indicador">
                                                                </span>
                                                             </a>
                                                             <a href="<?php echo base_url();?>proyecto/indicador_controller/delete/<?php echo $met->id;?>/<?php echo $met->id_proyecto;?>"  class="btn btn-danger"title="Eliminar Indicador">
                                                                <span class="fa fa-remove">
                                                                </span>                                                                
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>

                                            <?php endforeach; ?>

                                        <?php endif;?>
                                    </tbody>

                                </table>

                           </div>
                        <!-- /.box-body -->
                        </div>
                        <?php if ($c!=0):?>
                         <div class="col-md-5"></div>
                         <div   class="form-group">
                                            <button type="submit" class="btn btn-success btn-flat" >
                                                Continuar
                                            </button>
                         </div>
                         <?php endif;?>

                        </form>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
