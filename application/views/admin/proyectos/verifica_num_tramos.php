        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
       <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Numero de Paquetes
                    <small>Proyecto: <?php echo $proyecto->nombre_proyecto;?></small>
                </h1>
            </section>
           
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                     
                    <form action="<?php base_url();?>/prosye_ci/proyecto/numero_tramo_controller/store" method="POST">
                          
                          <div>
                           <input type="hidden" value="<?php echo $cont;?>" name="cont" ><!--Recupera el contador para el numero de tramo-->
                          </div>
                          <div>
                           <input type="hidden" value="<?php echo $res;?>" name="res" ><!--recupera si el proyecto tiene convenio o no-->
                          </div>
                          <div>
                           <input type="hidden" value="<?php echo $proyecto->id_proyecto;?>" name="id_proyecto" id="id_proyecto"> <!--recupera el id de proyecto-->
                          </div>
                          <div> 
                           <input type="hidden" value="<?php echo $proyecto->fase;?>" name="tipo_fase" id="tipo_fase"> <!--recupera si el proy es preinversio o Inversion-->
                           </div>
                           <div>
                           <input type="hidden" value="<?php echo $proyecto->costo_proyecto;?>" name="costo_fase" id="costo_fase"> <!--recupera el costo del proyecto-->
                           </div>
                           <div>
                           <input type="hidden" value="<?php echo $proyecto->fecha_inicio;?>" name="fecha_inicio" id="fecha_inicio"> <!--recupera la fecha inicio del proyecto-->
                           </div>
                           <div>
                           <input type="hidden" value="<?php echo $proyecto->fecha_conclusion;?>" name="fecha_conclusion" id="fecha_conclusion"> <!--recupera la fecha de fin del proyecto-->
                            </div>
                        <div class="form-group">
                            <label for="nombre_tramo">El proyecto tiene paquetes:</label>
                            <div class="form-group">
                               <input class="form-control" type=""  value="Si el proyecto tiene paquetes" name="opcion" readonly="">
                             </div>                            
                        </div>
                        <div class="form-group">
                            <label for="numero_tramo">Ingrese el Numero que tiene Paquetes del Proyecto</label>
                            <input class="form-control" name="numero_tramos" type="number" min="2" max="15" required="" />
                        </div>

                        <div > 
                                    <button  type="submit" class="btn btn-success btn-flat" aling="center">
                                        Continuar
                                    </button>
                        </div>
                   </form> 
                </div>
                    <!-- /.box-body -->
            </div>
                <!-- /.box -->

         </section>
            <!-- /.content -->
      </div>
        <!-- /.content-wrapper -->
