        
        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>Registro
                <small>Sisin</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <form action="<?php base_url();?>" method="POST">
                        <div class="form-group">
                            <input type="text" name="id_proyecto" value= "<?php echo $result->id_proyecto; ?> "/>
                           
                        </div>

                        <div class="form-group">
                            <label for="codigo_sisin">Ingrese el Codigo Sisin</label>
                            <input class="form-control" name="codigo_sisin"/>
                        </div>
                         
                        <div class="form-group">
                            <div class="row">
                               
                                     <div class="col-xs-6">
                                        <label for="fecha_inicio">
                                         Fecha de Inicio:
                                        </label>
                                        <input type="date" id="fecha_inicio" class="form-control" name="fecha_inicio"/>
                                     </div>  
                                     <div class="col-xs-6">
                                        <label for="fecha_conclusion">
                                        Fecha de Conclusion:
                                        </label>     
                                        <input type="date" id="fecha_conclusion"  class="form-control" name="fecha_conclusion"/>   
                                     </div>  
                                </div>
                           
                        </div>
                     
                        <div class="form-group">
                            <label for="tipologia">Tipologia</label>
                            <select class="form-control" name="tipologia">
                            <option value="">Seleccione una Opcion</option>
                            <option value="Sectorial">Sectorial</option>
                            <option value="Departamental">Departamental</option>
                            <option value="Local">Local</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="area_influencia">area_influencia</label>
                            <select class="form-control" name="area_influencia">
                            <option value="">Seleccione una Opcion</option>
                            <option value="Urbana">Urbana</option>
                            <option value="Rural">Rural</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tipo_inversion">tipo_inversion</label>
                            <select class="form-control" name="tipo_inversion">
                            <option value="">Seleccione una Opcion</option>
                            <option value="Capitalizable">Capitalizable</option>
                            <option value="No Capitalizable">No Capitalizable</option>
                            </select>
                        </div> 
                            <div class="form-group">
                                <label for="sector">Sector:</label>
                                    <select class="form-control" id="sector" name="sector">
                                        <option value="">Sector</option>
                                        <option value="Administracion General">Administracion General</option>
                                        <option value="Agropecuario">Agropecuario</option>
                                        <option value="Comercio y Finanzas">Comercio y Finanzas</option>
                                        <option value="Comunicaciones">Comunicaciones</option>
                                        <option value="Cultura">Cultura</option> 
                                        <option value="Defensa Nacional">Defensa Nacional</option>
                                        <option value="Deuda Publica">Deuda Publica</option>
                                        <option value="Deportes">Deportes</option> 
                                        <option value="Energia">Energia</option>
                                        <option value="Hidrocarburos">Hidrocarburos</option>
                                        <option value="Industria">Industria</option>
                                        <option value="Justicia">Justicia</option>
                                        <option value="Medio Ambiente">Medio Ambiente</option>
                                        <option value="Minero">Minero</option>
                                        <option value="Multisectorial">Multisectorial</option>  
                                        <option value="Orden Publico y Seguridad Ciud">Orden Publico y Seguridad Ciudadana</option>
                                        <option value="Recursos Hidricos">Recursos Hídricos</option>
                                        <option value="Salud">Salud</option>
                                        <option value="Saneamiento Basico">Saneamiento Básico</option>
                                        <option value="Seguridad Social">Seguridad Social</option>
                                        <option value="Turismo">Turismo</option>
                                        <option value="Transportes">Transportes</option>
                                        <option value="Urbanismo y Vivienda">Urbanismo y Vivienda</option>
                                    </select>                   
                            </div>                                                
                            <div class="form-group">
                                <label for="sub_sector">Denominacion:</label>

                                <select class="form-control"  id="sub_sector" name="sub_sector">
                                        <option value="">Denominacion</option>
                                </select>                   
                            </div> 
                            
                            <div  >
                            
                            
                            <div   class="form-group">
                                    <button aling='center' type="submit" class="btn btn-success btn-flat" >
                                        Guardar
                                    </button>
                            </div>
                            </form> 
                           
                                               
                            </div>       



                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
