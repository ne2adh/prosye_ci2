<div class="content-wrapper" id="   ">
            <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Actualizar Coordenadas (Puntos)
            <small>Proyecto: <?php echo $proyecto->nombre_proyecto;?></small>
        </h1>
    </section>
            <!-- Main content -->
    <section class="content">
                <!-- Default box -->
        <div class="box-body">
            <div class="box">
                <div class="box-header with-border">

                        <div class="row">
                            <div class="col-md-8">
                                <div class="row">
							    <form action="<?php echo base_url();?>proyecto/puntos_controller/update" method="POST">
                                    <div class="form-group">
                                      <div align="center" class="col-md-12" >
                                          <h4><strong>Coordenadas en UTM</strong></h4>
                                      </div>
						          <input type="hidden" value="<?php echo $id_proyecto; ?>" name="id_proyecto" id="" >
                                  <div class="col-md-4 <?php //echo !empty(form_error('norte'))? 'has-error':'';?>">
                                      <label for="utm">Este</label>
                                          <input type="number" name="utm" min="0.01" max="" step="0.01" id="utm"
                                          value="<?php echo $puntos->norte; ?>" class="form-control"
                                          onfocus="ShowHide('ayuda_este_proyecto','block')" onblur="ShowHide('ayuda_este_proyecto','none')">
											<div class="alert alert-info ayuda" id="ayuda_este_proyecto">
												Ingrese la coordenada en Este (Campo requerido).
											</div>
                                          <br>
                                            <?php //echo form_error("norte","<span class='help-block'>","</span>"); ?>
                                  </div>
                                  <div class="col-md-4 <?php //echo !empty(form_error('este'))? 'has-error':'';?>">
                                      <label for="utme">Norte</label>
                                          <input type="number" min="0.01" max="" step="0.01" name="utme" id="utme"
                                          value="<?php echo $puntos->este; ?>"class="form-control"
                                          onfocus="ShowHide('ayuda_norte_proyecto','block')" onblur="ShowHide('ayuda_norte_proyecto','none')">
											<div class="alert alert-info ayuda" id="ayuda_norte_proyecto">
												Ingrese la coordenada en Norte (Campo requerido).
											</div>
                                          <br>
                                          <?php //echo form_error("este","<span class='help-block'>","</span>"); ?>
                                  </div>

                                  <div class="col-md-2">
                                      <label for="norte">Zona/Hemisferio</label>
                                          <input type="text" name="zona" id="zona" class="form-control" value="19 S" readonly=""><br>
                                  </div>

    								    <input type="hidden" value="<?php echo $id_proyecto; ?>" name="id_proyecto" id="" >
                                        <input type="hidden" value="<?php echo $id_coordenada; ?>" name="id_coordenada" id="" >
                                        
                                                <input id="lon" type="hidden" class="form-control" name="longitud" min="-67.1500015" max="-67.1000015" placeholder="-67.1500015 a -67.1000015" value="<?php echo $puntos->longitud; ?>">
                                                <?php echo form_error("longitud","<span class='help-block'>","</span>"); ?>
                                            </div>
                                            <div class="col-xs-4"></div>

                                            <button  type="submit" class="btn btn btn-info" aling="center">
                                                <span class="glyphicon glyphicon-plus">Actualizar</span>
                                            </button>
                                    </div>
                                </form>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2"></div>
                                    <div class="col-xs-8">
                                        <strong>Nota:</strong> Para la correcta visualizacion de los puntos en el mapa, estos deben ser introducidos
                                        segun un orden determinado por el funcionario.
                                    </div>
                                    <div class="col-xs-2"></div>
                                </div>
                            </div>
                            <div class="col-md-6">


                            </div>
                            <br>
                            <div class="col-md-12" >
                                <div class="form-group">
                                    <div class="col-xs-1"></div>
                                        <div class="col-xs-10">
                                                <div align="center" class="col-md-12" >
                                                    <h3>Puntos del Proyectos</h3>
                                                </div>
                                                    <table class="table table-bordered btn-hover" role="grid" id="">
                                                            <thead >
                                                                <tr class="success">
                                                                    <th>Numero</th>
                                                                    <th>Este</th>
                                                                    <th>Norte</th>
                                                                    <th>Zona</th>
                                                                    <th>Hemisferio</th>

                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php $i=0;if(!empty($punto)):?>
                                                                <?php foreach($punto as $met ):?>
                                                                <tr>
                                                                    <td><?php  $i; $i=$i+1; echo $i;?></td>
                                                                    <td><?php echo $met->norte;?></td>
                                                                    <td><?php echo $met->este;?></td>
                                                                    <td>19</td>
                                                                    <td>S</td>

                                                                </tr>
                                                                <?php endforeach;?>
                                                             <?php endif;?>
                                                            </tbody>
                                                    </table>
                                        </div>
                                    </div>
                            </div>

                        </div>


                </div>
                    <!-- /.box-body -->
            </div>
        </div>
                <!-- /.box -->
    </section>
            <!-- /.content -->
</div>
 <script type="text/javascript">

const utm = document.querySelector("#utm");
        const utme = document.querySelector("#utme");
        const lat = document.querySelector("#lat");
        const lon = document.querySelector("#lon");

        utme.addEventListener("keyup", () => {
          console.log(utm.value);
          ///zona, hemisferio, norte, este
          convierteALatLon(19+" J "+utm.value+" "+utme.value);
        });
        ////
        let latitude;
        let longitude;
        const convierteALatLon = (UTM) => {
            let parts = UTM.split(" ");
            let Zone= parseInt(parts[0]);
            let Letter=parts[1].toUpperCase().charAt(0);
            let Easting= parseFloat(parts[2]);
            let Northing= parseFloat(utme.value);
            let Hem;
            if (Letter>'M')
                Hem='N';
            else
                Hem='S';
            let north;
            if (Hem == 'S')
                north = Northing - 10000000;
            else
                north = Northing;
            latitude = (north/6366197.724/0.9996+(1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)-0.006739496742*Math.sin(north/6366197.724/0.9996)*Math.cos(north/6366197.724/0.9996)*(Math.atan(Math.cos(Math.atan(( Math.exp((Easting - 500000) / (0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((Easting - 500000) / (0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2)/3))-Math.exp(-(Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*( 1 -  0.006739496742*Math.pow((Easting - 500000) / (0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2)/3)))/2/Math.cos((north-0.9996*6399593.625*(north/6366197.724/0.9996-0.006739496742*3/4*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.pow(0.006739496742*3/4,2)*5/3*(3*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996 )/2)+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/4-Math.pow(0.006739496742*3/4,3)*35/27*(5*(3*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/4+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/3))/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2))+north/6366197.724/0.9996)))*Math.tan((north-0.9996*6399593.625*(north/6366197.724/0.9996 - 0.006739496742*3/4*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.pow(0.006739496742*3/4,2)*5/3*(3*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.sin(2*north/6366197.724/0.9996 )*Math.pow(Math.cos(north/6366197.724/0.9996),2))/4-Math.pow(0.006739496742*3/4,3)*35/27*(5*(3*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/4+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/3))/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2))+north/6366197.724/0.9996))-north/6366197.724/0.9996)*3/2)*(Math.atan(Math.cos(Math.atan((Math.exp((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2)/3))-Math.exp(-(Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2)/3)))/2/Math.cos((north-0.9996*6399593.625*(north/6366197.724/0.9996-0.006739496742*3/4*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.pow(0.006739496742*3/4,2)*5/3*(3*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/4-Math.pow(0.006739496742*3/4,3)*35/27*(5*(3*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/4+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/3))/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2))+north/6366197.724/0.9996)))*Math.tan((north-0.9996*6399593.625*(north/6366197.724/0.9996-0.006739496742*3/4*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.pow(0.006739496742*3/4,2)*5/3*(3*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/4-Math.pow(0.006739496742*3/4,3)*35/27*(5*(3*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/4+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/3))/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2))+north/6366197.724/0.9996))-north/6366197.724/0.9996))*180/Math.PI;
            latitude=Math.round(latitude*10000000);
            latitude=latitude/10000000;
            longitude =Math.atan((Math.exp((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2)/3))-Math.exp(-(Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2)/3)))/2/Math.cos((north-0.9996*6399593.625*( north/6366197.724/0.9996-0.006739496742*3/4*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.pow(0.006739496742*3/4,2)*5/3*(3*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.sin(2* north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/4-Math.pow(0.006739496742*3/4,3)*35/27*(5*(3*(north/6366197.724/0.9996+Math.sin(2*north/6366197.724/0.9996)/2)+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/4+Math.sin(2*north/6366197.724/0.9996)*Math.pow(Math.cos(north/6366197.724/0.9996),2)*Math.pow(Math.cos(north/6366197.724/0.9996),2))/3)) / (0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2))))*(1-0.006739496742*Math.pow((Easting-500000)/(0.9996*6399593.625/Math.sqrt((1+0.006739496742*Math.pow(Math.cos(north/6366197.724/0.9996),2)))),2)/2*Math.pow(Math.cos(north/6366197.724/0.9996),2))+north/6366197.724/0.9996))*180/Math.PI+Zone*6-183;
            longitude=Math.round(longitude*10000000);
            longitude=longitude/10000000;
            console.log(latitude);
            console.log(longitude);
            lat.value = latitude;
            lon.value = longitude;
        }


</script>
