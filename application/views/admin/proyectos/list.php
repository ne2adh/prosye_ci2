    <!-- =============================================== -->

                 <!-- Content Wrapper. Contains page content -->
        
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Proyectos
                <small>Listado</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                
                <div class="box box-solid">
                    <div class="box-body">
                        
                        <div  class="row">
                            <div align="center" class="col-md-12" >
                                <h2>Listado General de Proyectos</h2>      
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="example1" class="table table-bordered btn-hover">
                                    <thead>
                                        <tr>
                                           
                                            <th>Proyecto</th>
                                            <th>Descripcion</th>
                                            <th>Objetivo</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(!empty($proyectos)):?>
                                        

                                            <?php foreach($proyectos as $proyecto ):?>
                                             
                                                <tr>
                                                   
                                                    <td><?php echo $proyecto->nombre_proyecto;?></td>
                                                    <td><?php echo $proyecto->descripcion_proyecto;?></td>
                                                    <td><?php echo $proyecto->objetivo_proyecto;?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a href="#" class="btn btn-info">
                                                                <span class="fa fa-eye">                 
                                                                </span>
                                                            </a>
                                                            
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach;?>  
                                        <?php endif;?>
                                    </tbody>
                                    
                                </table>
                                
                           </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>