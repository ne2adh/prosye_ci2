        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div id ="detalleMunicipioProyecto" class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>Registro Participantes
                    <small> </small>
                </h1> 
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
	                        <div class="form-group">
	                            <label>Nombre del Convenio: </label>
	                            <input class="form-control" type="" name="nombre_convenio" value="<?php echo $convenio->nombre_convenio;?>" readonly="" >
							</div>
                            <div class="form-group">
                                <label>Costo del Proyecto en Convenio: </label>
                                <input class="form-control" type="" name="costo" value="<?php echo $monto;?>" readonly="">
                            </div>
		                      <form action="<?php echo base_url();?>proyecto/participantes/store" method="POST">
    		                        <input type="hidden"  value="<?php echo $id_proyecto; ?>" name="id_proyecto">
                                    <input type="hidden"  value="<?php echo $id_convenio; ?>" name="id_convenio">
                                    <input type="hidden"  value="<?php echo $id_componente_proy; ?>" name="id_componente_proy">
                                    <input type="hidden"  value="<?php echo $monto;?>" name="costo">
                                    <div class="form-group">
    		                            <h1 align="center">Porcentaje de Co-Financiamiento</h1>
    		                        </div>
                                 <?php  $sum=0; $con=$monto;?>   
                                    <?php foreach($participa as $met ){ 
                                      $sum=$sum+($met->monto_municipio);}
                                      foreach($ente1 as $met ){
                                       $sum=$sum+($met->monto_ente);} ?>
                                    <div class="form-group <?php echo !empty(form_error("ente_finan"))? 'has-error':'';?> ">
                                         <label for="ente_finan">Financiamiento Externo al Proyecto</label>   
        		                        <select class="form-control"  id="ente_finan" name="ente_finan" >
                                                    <option value="">Ente Financiero</option>
                                                    <?php  
                                                        foreach($ente as $row)
                                                        {  
                                                            echo '<option value="'.$row->id_ente.'">'.$row->nombre.''.______.''.$row->descripcion.'</option>';
                                                        }
                                                    ?>
                                        </select> 
                                        <?php echo form_error("ente_finan","<span class='help-block'>","</span>"); ?>
                                    </div>
                                    <div class="form-group <?php echo !empty(form_error('participacion'))? 'has-error':'';?>">
                                        <label for="participacion">Monto de Participacion en el Proyecto con Financiamiento Externo</label>
                                                <input type="number"  step="0.01" class="form-control" id="participacion" placeholder="Participacion dentro Proyecto" name="participacion" max="<?php $res=$con-$sum;echo number_format($res, 2, '.', '');?>" min="00.00" value="<?php echo set_value("participacion");?>" onkeypress="return filterFloat(event,this);">
                                                <?php echo form_error("participacion","<span class='help-block'>","</span>"); ?>
                                    </div>
                                    <?php $res=$con-$sum;
                                    echo "EL MONTO QUE TIENE AUN POR ASIGNAR ES DE: $res" ?> 
                                    <div   class="form-group">
                                                <button aling='center' type="submit" class="btn btn-success btn-flat" >
                                                    Guardar 
                                                </button>
                                    </div>
                            </div>
                        </div>
                    </section>
                </div>