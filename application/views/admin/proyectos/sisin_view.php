
        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
            <h1>Registro Sisin
                <small>Proyecto: <?php echo $proyecto->nombre_proyecto; ?></small>               
            </h1>
            </section>
            <!-- Main content -->            
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <form 
                            <?php
                              if(isset($sisin)){
                                echo 'action="'.base_url().'proyecto/sisin_controller/updateSisin/'.$sisin->id.'"';
                              }
                              else{
                                echo 'action="'.base_url().'proyecto/sisin_controller/store"';
                              }
                            ?>
                         method="POST">
                         <div class="form-group  <?php echo !empty(form_error('id_proyecto'))? 'has-error':'';?>">
                         <input type="hidden"  value="<?php echo $proyecto->id_proyecto; ?>" name="id_proyecto" id="id_proyecto">
                         <?php echo form_error("id_proyecto","<span class='help-block'>","</span>"); ?>
                          </div>
                               <div class="form-group  <?php echo !empty(form_error('codigo_sisin'))? 'has-error':'';?>">
                                    <label for="codigo_sisin">Ingrese el Codigo Sisin</label>
                                    <input class="form-control" type="number" name="codigo_sisin" min="1"  value="<?php
                                        if(!isset($sisin))
                                            echo set_value('codigo_sisin');
                                        else
                                            echo $sisin->codigo_sisin;
                                    ?>" pattern="[0-9]{16}"/
                                        onfocus="ShowHide('ayuda_codigo_sisin','block')" onblur="ShowHide('ayuda_codigo_sisin','none')"
									>
                                    <div class="alert alert-info ayuda" id="ayuda_codigo_sisin">
											Escriba el Codigo Sisin del Proyecto(Campo requerido).
									</div>
                                    <?php echo form_error("codigo_sisin","<span class='help-block'>","</span>"); ?>
                                </div>
                            <div class="form-group">
                                <div class="row">

                                     <div class="col-xs-6 <?php //echo !empty(form_error('codigo_sisin'))? 'has-error':'';?>">
                                        <label for="fecha_inicio">
                                         Fecha de Inicio:
                                        </label>
                                        <input type="date" id="fecha_inicio" class="form-control" name="fecha_inicio" value="<?php echo $proyecto->fecha_inicio;?>" readonly=''/>
                                     </div>
                                     <div class="col-xs-6 <?php //echo !empty(form_error('fecha_conclusion'))? 'has-error':'';?> ">
                                        <label for="fecha_conclusion">
                                        Fecha de Conclusion:
                                        </label>
                                        <input type="date" id="fecha_conclusion"  class="form-control" name="fecha_conclusion" value="<?php echo $proyecto->fecha_conclusion; ?>" readonly='' /><?php //echo form_error("fecha_conclusion","<span class='help-block'>","</span>");?>
                                     </div>
                                </div>

                            </div>

                            <div class="form-group <?php echo !empty(form_error('tipologia'))? 'has-error':'';?> ">
                                <label for="tipologia">Tipologia</label>
                                <div class="alert alert-info ayuda" id="ayuda_tipologia_sisin">
									Seleccione la Tipologia del Proyecto(Campo requerido).
								</div>
                                    <select class="form-control" name="tipologia"
                                        onfocus="ShowHide('ayuda_tipologia_sisin','block')" onblur="ShowHide('ayuda_tipologia_sisin','none')"
                                    >
                                    <option value="" <?php  echo   set_select ( 'tipologia' ,  '',  TRUE );?>>Seleccione una Opcion</option>
                                    <option value="SECTORIAL" <?php  echo   set_select ( 'tipologia','SECTORIAL');?>
                                        <?php
                                            if(isset($sisin))
                                                if($sisin->tipologia=='SECTORIAL')
                                                    echo "selected";                                        
                                        ?>
                                    >SECTORIAL</option>
                                    <option value="DEPARTAMENTAL" <?php  echo   set_select ( 'tipologia','DEPARTAMENTAL');?>
                                        <?php
                                            if(isset($sisin))
                                                if($sisin->tipologia=='DEPARTAMENTAL')
                                                    echo "selected";                                        
                                        ?>
                                    >DEPARTAMENTAL</option>
                                    <option value="LOCAL" <?php  echo   set_select ( 'tipologia','LOCAL');?>
                                        <?php
                                            if(isset($sisin))
                                                if($sisin->tipologia=='LOCAL')
                                                    echo "selected";                                        
                                        ?>
                                    >LOCAL</option>
                                </select>
                                <?php echo form_error("tipologia","<span class='help-block'>","</span>"); ?>
                            </div>
                            <div class="form-group <?php echo !empty(form_error('area_influencia'))? 'has-error':'';?> ">
                                <label for="area_influencia">Area Influencia</label>
                                <div class="alert alert-info ayuda" id="ayuda_area_influencia_sisin">
									Seleccione la Tipologia del Proyecto(Campo requerido).
								</div>
                                <select class="form-control" name="area_influencia" 
                                    onfocus="ShowHide('ayuda_area_influencia_sisin','block')" onblur="ShowHide('ayuda_area_influencia_sisin','none')"
                                >
                                <option value="" <?php  echo   set_select ( 'area_influencia' ,  '',  TRUE );?>>Seleccione una Opcion</option>
                                <option value="URBANA"  <?php  echo   set_select ( 'area_influencia','URBANA')?>
                                    <?php
                                        if(isset($sisin))
                                            if($sisin->area_influencia=='URBANA')
                                                echo "selected";                                        
                                    ?>
                                >URBANA</option>
                                <option value="RURAL"  <?php  echo   set_select ( 'area_influencia','RURAL')?>
                                    <?php
                                        if(isset($sisin))
                                            if($sisin->area_influencia=='RURAL')
                                                echo "selected";                                        
                                    ?>
                                >RURAL</option>
                                </select>
                                <?php echo form_error("area_influencia","<span class='help-block'>","</span>"); ?>
                            </div>
                            <div class="form-group <?php echo !empty(form_error('tipo_inversion'))? 'has-error':'';?>">
                                <label for="tipo_inversion">Tipo Inversion</label>
                                <div class="alert alert-info ayuda" id="ayuda_tipo_inversion_sisin">
									Seleccione el Tipo de Inversion(Campo requerido).
								</div>
                                <select class="form-control" name="tipo_inversion"
                                    onfocus="ShowHide('ayuda_tipo_inversion_sisin','block')" onblur="ShowHide('ayuda_tipo_inversion_sisin','none')"
                                >
                                <option value="" <?php  echo   set_select ( 'tipo_inversion' ,  '',  TRUE );?>>Seleccione una Opcion</option>
                                <option value="CAPITALIZABLE" <?php  echo   set_select ( 'tipo_inversion','CAPITALIZABLE')?>
                                    <?php
                                        if(isset($sisin))
                                            if($sisin->tipo_inversion=='CAPITALIZABLE')
                                                echo "selected";
                                    ?>
                                >CAPITALIZABLE</option>
                                <option value="NO CAPITALIZABLE" <?php  echo   set_select ( 'tipo_inversion','NO CAPITALIZABLE')?>
                                    <?php
                                        if(isset($sisin))
                                            if($sisin->tipo_inversion=='NO CAPITALIZABLE')
                                                echo "selected";
                                    ?>
                                >NO CAPITALIZABLE</option>
                                </select>
                                <?php echo form_error("tipo_inversion","<span class='help-block'>","</span>"); ?>
                            </div>

                            <div class="form-group <?php echo !empty(form_error('sector'))? 'has-error':'';?> ">
                            <div class="row">
                                <div class="col-xs-1"></div>
                                <div class="col-xs-12">
                                    <label for="sector">Sector:</label>
                                    <div class="alert alert-info ayuda" id="ayuda_sector_sisin">
                                        Seleccione el Sector(Campo requerido).
                                    </div>
                                        <select class="form-control"  id="sector" name="sector"
                                                onfocus="ShowHide('ayuda_sector_sisin','block')" onblur="ShowHide('ayuda_sector_sisin','none')"
                                        >
                                            <option value="" >Sector</option>
                                            <?php
                                                foreach($sectores as $sector):?>
                                                    <option value="<?php echo $sector->id_clasificacion_sectorial;?>" <?php echo set_select("sector",$sector->id_clasificacion_sectorial);?> 
                                                    <?php
                                                        if(isset($sisin))
                                                            if($sisin->id_sector==$sector->id_clasificacion_sectorial)
                                                                echo "selected";
                                                    ?>
                                                    ><?php echo $sector->denominacion;?></option>
                                            <?php endforeach;?>

                                        </select>
                                        <?php echo form_error("sector","<span class='help-block'>","</span>"); ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                                <div class="col-xs-6">
                                        <div class="form-group <?php echo !empty(form_error('sub_sector'))? 'has-error':'';?> ">                                       
                                            <label for="sub_sector">Sub Sector:</label>
                                            <div class="alert alert-info ayuda" id="ayuda_sub_sector_sisin">
                                                Seleccione el Sub-Sector(Campo requerido).
                                            </div>
                                                <select class="form-control" id='sub_sector' name='sub_sector' 
                                                    onfocus="ShowHide('ayuda_sub_sector_sisin','block')" onblur="ShowHide('ayuda_sub_sector_sisin','none')"
                                                >
                                                    <option value="<?php if(isset($_POST['sub_sector'])){ echo $_POST['sub_sector'];} ?>"><?php echo $this->sisin_model->nombres($this->input->POST('sub_sector'));?></option>
                                                </select>
                                                <?php echo form_error("sub_sector","<span class='help-block'>","</span>");?>
                                        </div>
                                </div>
                                <div class="col-xs-6 ">
                                        <div class="form-group <?php echo !empty(form_error('denominacion'))? 'has-error':'';?>">                                                
                                                <label for="denominacion">Denominacion:</label>
                                                <div class="alert alert-info ayuda" id="ayuda_denominacion_sisin">
                                                    Seleccione la Denominacion(Campo requerido).
                                                </div>
                                                <select class="form-control" id="denominacion" name="denominacion" 
                                                    onfocus="ShowHide('ayuda_denominacion_sisin','block')" onblur="ShowHide('ayuda_denominacion_sisin','none')"
                                                >
                                                    <option value="<?php if(isset($_POST['denominacion'])){ echo $_POST['denominacion'];} ?>"><?php echo $this->sisin_model->nombres1($this->input->POST('denominacion'));?></option>
                                                </select>
                                                <?php echo form_error("denominacion","<span class='help-block'>","</span>"); ?>
                                        </div>
                                </div>
                        </div><br>
                            <!--<div class="form-group <?php //echo !empty(form_error('sector'))? 'has-error':'';?>">
                                <label for="sector">Sector:</label>
                                    <select class="form-control" id="sector" name="sector">
                                        <option value="" <?php  //echo   set_select ( 'sector' ,  '',  TRUE );?>>Sector</option>
                                        <option value="Administracion General" <?php //echo   set_select ( 'sector','Administracion General')?>>Administracion General</option>
                                        <option value="Agropecuario" <?php  //echo   set_select ( 'sector','Agropecuario')?>>Agropecuario</option>
                                        <option value="Comercio y Finanzas" <?php  //echo   set_select ( 'sector','Comercio y Finanzas')?>>Comercio y Finanzas</option>
                                        <option value="Comunicaciones" <?php  //echo   set_select ( 'sector','Comunicaciones')?>>Comunicaciones</option>
                                        <option value="Cultura" <?php  //echo   set_select ( 'sector','Cultura')?>>Cultura</option>
                                        <option value="Defensa Nacional" <?php  //echo   set_select ( 'sector','Defensa Nacional')?>>Defensa Nacional</option>
                                        <option value="Deuda Publica" <?php  //echo   set_select ( 'sector','Deuda Publica')?>>Deuda Publica</option>
                                        <option value="Deportes" <?php  //echo   set_select ( 'sector','Deportes')?>>Deportes</option>
                                        <option value="Energia" <?php  //echo   set_select ( 'sector','Energia')?>>Energia</option>
                                        <option value="Hidrocarburos" <?php // echo   set_select ( 'sector','Hidrocarburos')?>>Hidrocarburos</option>
                                        <option value="Industria" <?php  //echo   set_select ( 'sector','Industria')?>>Industria</option>
                                        <option value="Justicia" <?php  //echo   set_select ( 'sector','Justicia')?>>Justicia</option>
                                        <option value="Medio Ambiente" <?php  //echo   set_select ( 'sector','Medio Ambiente')?>>Medio Ambiente</option>
                                        <option value="Minero" <?php  //echo   set_select ( 'sector','Minero')?>>Minero</option>
                                        <option value="Multisectorial" <?php  //echo   set_select ( 'sector','Multisectorial')?>>Multisectorial</option>
                                        <option value="Orden Publico y Seguridad Ciud" <?php  //echo   set_select ( 'sector','Orden Publico y Seguridad Ciudadana')?>>Orden Publico y Seguridad Ciudadana</option>
                                        <option value="Recursos Hidricos" <?php  //echo   set_select ( 'sector','Recursos Hídricos')?>>Recursos Hídricos</option>
                                        <option value="Salud" <?php  //echo   set_select ( 'sector','Salud')?>>Salud</option>
                                        <option value="Saneamiento Basico" <?php  //echo   set_select ( 'sector','Saneamiento Básico')?>>Saneamiento Básico</option>
                                        <option value="Seguridad Social" <?php  //echo   set_select ( 'sector','Seguridad Social')?>>Seguridad Social</option>
                                        <option value="Turismo" <?php  //echo   set_select ( 'sector','Turismo')?>>Turismo</option>
                                        <option value="Transportes" <?php  //echo   set_select ( 'sector','Transportes')?>>Transportes</option>
                                        <option value="Urbanismo y Vivienda" <?php  //echo   set_select ( 'sector','Urbanismo y Vivienda')?>>Urbanismo y Vivienda</option>
                                    </select>
                                    <?php //echo form_error("sector","<span class='help-block'>","</span>"); ?>
                            </div>
                            <div class="form-group <?php //echo !empty(form_error('sub_sector'))? 'has-error':'';?>">
                                <label for="sub_sector">Denominacion:</label>

                                <select class="form-control"  id="sub_sector" name="sub_sector">
                                        <option value="<?php// if(isset($_POST['sub_sector'])){ echo $_POST['sub_sector'];} ?>"><?php /*echo $_POST['sub_sector'];*/ //echo $this->sisin_model->nombres($this->input->POST('sub_sector'));?></option>
                                </select>
                                <?php //echo form_error("sub_sector","<span class='help-block'>","</span>"); ?>
                            </div> -->
                            <div   class="form-group text-center">
                                    <?php
                                        if(isset($sisin)){
                                            echo '<button type="submit" class="btn btn-warning btn-flat" >
                                                    Actualizar
                                                </button>';
                                        }
                                        else{
                                            echo '<button type="submit" class="btn btn-success btn-flat" >
                                                    Guardar
                                                </button>';
                                        }
                                    ?>
                            </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.box-body -->
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
<script type="text/javascript">

    $(function(){ 
        let id_sub_sector = <?php 
                if( isset($sisin) ){                    
                   echo $sisin->id_sub_sector;
                }
                else{                    
                    if(isset($_POST['sub_sector']) && $_POST['sub_sector']!="" && $_POST['sub_sector']!=NULL ){                        
                        echo $_POST['sub_sector'];
                    }
                    else{                        
                        echo '""';
                    }
                }                   
            ?>;
		    var cod=$('#sector').val();                    
			if(cod!=''){
							$.ajax({
								url:"<?php echo base_url();?>proyecto/sisin_controller/get_sub_sector",
								method:"POST",
								data:{cod:cod},
                                async:false,
								success:function(data){                                    
									$('#sub_sector').html(data);                                    
                                    var sel = document.getElementById("sub_sector");                                    
                                    for (var i = 0; i < sel.length; i++) {                                                            
                                        var opt = sel[i];
                                        id_sub = opt.value;
                                        if(id_sub==id_sub_sector){                                            
                                            sel[i].setAttribute('selected', true);                                            
                                        }
                                    }
								}
							});							
			}
            cod_sub_sector = $("#sub_sector").val();
            let id_denominacion = <?php 
                if(isset($sisin)){
                   echo $sisin->id_denominacion;
                }
                else{                    
                    if(isset($_POST['denominacion']) && $_POST['denominacion']!="" && $_POST['denominacion']!=NULL ){
                        echo $_POST['denominacion'];
                    }
                    else{                        
                        echo '""';
                    }                           
                }                    
            ?>;
            if(cod!=''){
				$.ajax({
					url:"<?php echo base_url();?>proyecto/sisin_controller/get_denominacion",
					method:"POST",
					data:{cod:cod_sub_sector},
					success:function(data){
                            $('#denominacion').html(data);                            
                            var sel = document.getElementById("denominacion");
                            for (var i = 0; i < sel.length; i++) {
                                var opt = sel[i];
                                id_den = opt.value;
                                if(id_den == id_denominacion){
                                    sel[i].setAttribute('selected', true);
                                }
                            }
					}
				})										
			}            
	});  

</script>