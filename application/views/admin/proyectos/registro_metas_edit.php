
<?php
                        function formato($cantidad){
                            $formatted = number_format(sprintf('%0.2f', preg_replace("/[^0-9.]/", "", $cantidad)), 2);
                            return $cantidad < 0 ? "({$formatted})" : "{$formatted}";
                        }											                
?>
<!-- dom para angular-->
<div class="content-wrapper" ng-app="app" ng-controller="ctrl">
  <section class="content-header">
    <h1>Registro Metas
        <small>Proyecto: <?php echo $proyecto->nombre_proyecto; ?></small>
    </h1>
  </section>
  <section class="content">
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                  <div class="panel panel-info">
                    <div class="panel-heading">
                      </div>
                    <div class="panel-body">
                      <div class="row">
                          <div class="col-md-12">
                            <div class="row">
                              <div class="col-md-6">
                                </div>
                            </div>
                            <!---->
                            <form action="<?php echo base_url();?>proyecto/metas_controller/update" method="POST">
                              <input type="hidden" name="id" value="<?php echo $id; ?>" >
                              <input type="hidden" name="id_proyecto" value="<?php
                                  if(isset($proyecto->id_tramo))
                                      echo $proyecto->id_tramo;
                                  else
                                      echo $proyecto->id_proyecto;
                              ?>" >
                            <div class="form-group">
                                <div class=" <?php echo !empty(form_error('meta'))? 'has-error':'';?> ">
                                  <label for="meta">Escoja una descripcion de la meta del Proyecto:
                                  </label>
                                <select class="form-control"  id="" name="meta">
                                  <option value="<?php echo $metas2->descripcion;?>"><?php $num=$metas2->descripcion;
                                          print_r($this->metas_model->metas_proy1($num));?> </option>
                                  <?php
                                      foreach($metas as $meta):?>
                                      {
                                      echo <option value="<?php echo $meta->id_metas_proy;?>" <?php echo set_select("meta",$meta->id_metas_proy);?>> <?php echo $meta->descripcion;?><?php echo set_select("meta",$meta->descripcion);?></option>';
                                      }
                                  <?php endforeach; ?>
                                </select>
                                <?php echo form_error("meta","<span class='help-block'>","</span>"); ?>
                                </div>
                                            <br>
                                            <div class="row">
                                                <div class="<?php echo !empty(form_error('cantidad'))? 'has-error':'';?>" >
                                                    <div class="col-xs-4">
                                                        <label for="cantidad">
                                                          Ingrese Cantidad de medicion:
                                                        </label>
                                                        <input class='form-control' type='text' name='cantidad' min='1' value="<?php echo formato($metas2->cantidad);?>" data-type="currency" required>
                                                        <?php echo form_error("cantidad","<span class='help-block'>","</span>"); ?>
                                                    </div>
                                                </div>
                                                <div class="<?php echo !empty(form_error('unidad'))? 'has-error':'';?>" >
                                                    <div class="col-xs-2">
                                                      <label for="unidad">
                                                        Unidad de medicion:
                                                      </label>
                                                      <input class='form-control' type='text' name='unidad' value='<?php echo $metas2->unidad;?>' onkeyup="javascript:this.value=this.value.toUpperCase();"
                                                        onfocus="ShowHide('ayuda_unidad_meta','block')" onblur="ShowHide('ayuda_unidad_meta','none')">
                                                      <div class="alert alert-info ayuda" id="ayuda_unidad_meta">
                                                          Ingrese la Unidad de Medicion de la meta.(Campo requerido).
                                                      </div>
                                                      <?php echo form_error("unidad","<span class='help-block'>","</span>"); ?>
                                                    </div>
                                                </div>
                                            <div class="<?php echo !empty(form_error('observacion'))? 'has-error':'';?>" >
                                                    <div class="col-xs-6">

                                                        <label for="observacion">
                                                           Observaciones Adicionales :
                                                        </label>
                                                         <input class='form-control' type='' name='observacion' value='<?php echo $metas2->observacion;?>' onkeyup="javascript:this.value=this.value.toUpperCase();">
                                                            <?php /*<textarea type="text"  class="form-control" name="observacion" id="observacion" rows="1" >
                                                            <?php echo set_value("observacion");?>
                                                            </textarea>*/?>
                                                            <?php echo form_error("observacion","<span class='help-block'>","</span>"); ?>

                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div align="center" class="form-group">
                                          <button type="submit" class="btn btn-success btn-flat" >
                                            Actualizar
                                          </button>
                                      </div>
                                    </div>

                            </form>

                            <!---->
                            <form action="" method="POST">
                    <div class="box box-solid">
                        <div class="box-body">
                            <div  class="row">
                                <div align="center" class="col-md-12" >
                                    <h3>Metas del Proyectos</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                     <input type="hidden" name="id" value="<?php echo $id; ?>" >
                                    <input type="hidden" name="id_proyecto" value="<?php echo $proyecto->id_proyecto; ?>" >
                                    <table  class="table table-striped" name="tabla">
                                        <thead >
                                            <tr class="success">
                                                <th >N°</th>
                                                <th >Descripcion</th>
                                                <th >Cantidad</th>
                                                <th >Observaciones</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(!empty($metas1)):?>
                                            <?php foreach($metas1 as $met ):?>
                                            <tr id="fila">
                                                <td><?php global $i; $i=$i+1; echo $i;?></td>
                                                <td><?php $num=$met->descripcion;
                                                    print_r($this->metas_model->metas_proy1($num));?>
                                                </td>
                                                <td><?php echo formato($met->cantidad);?> </td>
                                                <td><?php echo $met->observacion;?> </td>
                                            </tr>
                                                <?php endforeach;?>
                                                <?php endif;?>
                                        </tbody>
                                    </table>
                                </div>
                                        <!-- /.box-body -->
                            </div>



                        </div>
                    </div>
                </form><!---->
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
      </div>
  </section>

<script type="text/ng-template" id="agregar.html">
  <div>
      <div class="box box-primary">
          <div class="modal-header box-header with-border text-center">
              <h4 class="modal-title box-title">{{titulo}}</h4>
              <button class="close" type="button" ng-click="cerrar()">
                <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body box-body">
              <form name="form">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group">
                        <label class="control-label">Descripcion Meta:</label>
                        <input type="text" class="form-control" name="meta" ng-model="meta.descripcion" placeholder="Descripcion de la meta..." required onkeyup="javascript:this.value=this.value.toUpperCase();" >
                        <div ng-show="form.meta.$touched">
                            <span class="text-help text-danger" ng-show="form.meta.$error.required">(*) Valor requerido</span>
                            <span class="text-help text-danger" ng-show="form.meta.$error.pattern">(*) Solo texto</span>
                        </div>
                    </div>
                  </div>
                </div>
              </form>
          </div>
          <div class="modal-footer box-footer text-center">
              <button class="btn btn-success" ng-show="estado" ng-disabled="!form.$valid || form.$pending" ng-click="registrar(meta)"> Registrar</button>
              <button class="btn btn-warning" ng-show="!estado" ng-disabled="!form.$valid || form.$pending" ng-click="editar(meta)"> Editar</button>
              <a class="btn btn-default" ng-click="cerrar()"> <i class="fa fa-times"></i> Cerrar </a>
          </div>
          </div>
      </div>
  </div>
</script>


</div>




<!--angularjs :) -->
<script src="<?php echo base_url();?>assets/angular.min.js"></script>
<script src="<?php echo base_url();?>assets/ui-bootstrap.min.js"></script>

<script>
let ngApp = function(){
  'use strict';
  let app = angular.module('app', ['ui.bootstrap']);
  app.controller('ctrl', function($scope, $http, $uibModal) {


    $scope.amRol = (meta) => {
        $uibModal.open({
          templateUrl: 'agregar.html',
          backdrop: 'static',
          scope:$scope,
          controller: function ($scope,$uibModalInstance) {
              if(meta===undefined){
                $scope.titulo='Nuevo Registro';
                $scope.estado = true;
                $scope.meta = [];
              }else{
                $scope.titulo='Editar Registro';
                $scope.meta = meta;
                $scope.estado = false;
              }

              $scope.registrar = (meta)=> {
                let req = {
                   method: 'GET',
                   url: '<?php echo base_url('proyecto/metas_controller/saveMeta?meta=');?>'+meta.descripcion,
                   //data: { rol:rol }
                }
                $http(req).then(function(response) {
                  $scope.cerrar();
                  //$scope.listarRoles();
                });
              };

              $scope.editar = (meta)=> {
                let ruta = '<?php echo base_url('proyecto/metas_controller/editarMeta?meta=');?>'+meta.id_metas_proy+'&descripcion='+meta.descripcion;
                let req = {
                   method: 'GET',
                   url: ruta
                   //data: { rol:rol }
                }
                $http(req).then(function(response) {
                  $scope.cerrar();
                  $scope.listarRoles();
                });
              };

              $scope.cerrar = () => {
                  $uibModalInstance.dismiss();
              };
          }
        });
    };


    $scope.eliminar =(rol)=>{
      $uibModal.open({
        templateUrl: 'eliminar.html',
        backdrop: 'static',
        scope:$scope,
        controller: function ($scope,$uibModalInstance) {

            $scope.titulo='Eliminar Registro';
            $scope.mensaje = `Esta seguro que desea eliminar el registro: ${rol.nombre}`;

            $scope.confirmar =()=>{
              let ruta = '<?php echo base_url('administrador/Rol/deleteRol?id_rol=');?>'+rol.id_rol;
              let req = {
                 method: 'GET',
                 url: ruta
                 //data: { rol:rol }
              }
              $http(req).then(function(response) {
                $scope.cerrar();
                $scope.listarRoles();
              });
            }

            $scope.cerrar = () => {
                $uibModalInstance.dismiss();
            };
        }
      });

    };

    console.log('funciona');
  });
}();
</script>
<!---->
