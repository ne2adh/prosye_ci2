
        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>Registro
                <small>Convenio</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <form action="<?php echo base_url();?>proyecto/convenio_controller/storeEdit" method="POST" autocomplete="off">
                       
                        <input type="hidden"  value="<?php echo $id_proyecto; ?>" name="id_proyecto">
                         <input type="hidden"  value="<?php echo $id_convenio; ?>" name="id_convenio">
                         <input type="hidden"  value="<?php echo $id_ruta; ?>" name="id_ruta">
                        
                        
                        <div class="form-group <?php echo !empty(form_error('nombre_convenio'))? 'has-error':'';?>">
                            <label for="nombre_convenio">Nombre del Convenio</label>
                            <input class="form-control" name="nombre_convenio" style="text-trasnform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" value="<?php echo $convenio->nombre_convenio;  echo set_value('nombre_convenio');?>">
                            <?php echo form_error("nombre_convenio","<span class='help-block'>","</span>");?>
                        </div>
                        <div class="form-group <?php echo !empty(form_error('descripcion_convenio'))? 'has-error':'';?>">
                            <label for="descripcion_convenio">Descripcion del Convenio</label>
                            <input class="form-control" name="descripcion_convenio" style="text-trasnform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" value="<?php echo $convenio->descripcion_convenio; echo set_value('descripcion_convenio');?>">
                            <?php echo form_error("descripcion_convenio","<span class='help-block'>","</span>");?>
                        </div>
                        <div class="form-group <?php echo !empty(form_error('fecha'))? 'has-error':'';?>">
                            <div class="row">
                                    <div class="col-xs-12">
                                        <label for="fecha">
                                         Fecha de Convenio:
                                        </label>
                                        <input type="date" id="fecha" class="form-control" name="fecha" min="<?php echo $proyecto->fecha_inicio;?>" max="<?php echo $proyecto->fecha_conclusion;?>" value="<?php echo $convenio->fecha;?>">
                                        <?php echo form_error("fecha","<span class='help-block'>","</span>");?>
                                     </div>  
                            </div>
                           
                        </div>
                        <div class="form-group <?php echo !empty(form_error('fecha'))? 'has-error':'';?>">
                            <div class="row">
                                    <div class="col-xs-12">
                                        <label for="fecha">
                                         Fecha de Ratificacion de Convenio (Opcional):
                                        </label>
                                        <input type="date" id="fecha_ratificacion" class="form-control" name="fecha_ratificacion" value="<?php echo $convenio->fecha_ratificacion;?>" min="<?php echo $proyecto->fecha_inicio;?>" max="<?php echo $proyecto->fecha_conclusion;?>">
                                        <?php echo form_error("fecha","<span class='help-block'>","</span>");?>
                                     </div>
                            </div>
                        </div>
                     
                        <div class="form-group">
                            <label for="vigencia">Vigencia del convenio (Referencial segun corresponda, en años y meses)</label>
                            <div class="row">
                                <div class="col-xs-6  <?php echo !empty(form_error('gestion'))? 'has-error':'';?>">
                                    <label>Duracion en Años</label>
                                    <input type="number" id="gestion" name="gestion" min="0" class="form-control" value="<?php echo $convenio->gestion;?>">
                                    <?php echo form_error("gestion","<span class='help-block'>","</span>");?>
                                </div>
                                <div class="col-xs-6  <?php echo !empty(form_error('mes'))? 'has-error':'';?>">
                                    <label>Duracion en meses</label>
                                    <input type="number" id="mes" name="mes" min="0" max="11" value="<?php echo $convenio->mes;?>" class="form-control">
                                    <?php echo form_error("mes","<span class='help-block'>","</span>");?>
                                </div>
                            </div>
                        </div>
                        <?php /* <div class="form-group">
                            <label for="vigencia">Monto del Convenio en Bs.</label>
                            <input class="form-control" name="monto"/>
                        </div> */?>
                        <div   class="form-group">
                                    <button aling='center' type="submit" class="btn btn-success btn-flat" >
                                       Actualizar
                                    </button>
                        </div>
                        </form> 
                           
                                               
                        </div>       



                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
