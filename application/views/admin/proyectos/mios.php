
        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Mis Proyectos
                <small>Registre su proyecto</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-12">
                                <a href="<?php echo base_url();?>proyecto/mis_proyectos/area_controller" class="btn btn-primary btn-flat">
                                <span class="fa fa-plus">
                                </span> Agregar Proyecto
                                </a>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">

                        <table id="example1" name="example1" class="table table-bordered btn-hover">
                                                <thead>
                                                    <tr>
                                                        <th>N°</th>
                                                        <!--<th>Codigo</th>-->
                                                        <th>Nombre Proyecto</th>
                                                        <th>Descripcion</th>
                                                        <th>Convenio</th>
                                                        <th>Fase de Registro</th>
                                                        <th>Opcion</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php $i=1;?>
                                                         <?php  foreach($results as $result ):?>
                                                                <tr>
                                                                    <td><?php echo $i;?></td>
                                                                    <!--<td><?php //echo $result->id_proyecto;?></td>-->
                                                                    <td><?php echo $result->nombre_proyecto;?></td>
                                                                    <td><?php echo $result->descripcion_proyecto;?></td>
                                                                    <td><?php echo $result->convenio;?></td>
                                                                    <td>
                                                                        <?php
                                                                            if($result->pant==1){
                                                                                //1-> convenio
                                                                                echo "Registro Convenio";
                                                                            }

                                                                            if($result->pant==2){
                                                                                //2-> metas
                                                                                echo "Registro de Metas";
                                                                            }

                                                                            if($result->pant==3){
                                                                                //3-> municipio
                                                                                echo "Registro de Municipio";
                                                                            }

                                                                            if($result->pant==4){
                                                                                //4-> puntos
                                                                                echo "Registro de Puntos";
                                                                            }

                                                                            if($result->pant==5){
                                                                                //5-> componente
                                                                                echo "Registro de Componentes";
                                                                            }

                                                                            if($result->pant==6){
                                                                                //6-> indicador
                                                                                echo "Registro de Indicadores";
                                                                            }
                                                                        ?>
                                                                    </td>
                                                                     <td>
                                                                       <?php
                                                           if($result->alta==0){
                                                               echo '
                                                               <div class="btn-group">
                                                                   <a  href="'.base_url().'proyecto/mis_proyectos/menu/'.$result->id_proyecto.'" class="btn btn-warning" title="Editar Proyecto">
                                                                       <span class="fa fa-pencil"> </span>
                                                                   </a>
                                                                   <a href="'.base_url().'proyecto/mis_proyectos/actualizarAlta/'.$result->id_proyecto.'" class="btn btn-danger" title="Aprobar Proyecto" >
                                                                       <span class="fa fa-check"></span>
                                                                   </a>
                                                                   <a class="btn btn-primary" href="'.base_url().'proyecto/proyecto_controller/editar/'.$result->id_proyecto.'" title="Editar datos generales del Proyecto"><i class="fa fa-wrench"></i></a>
                                                               </div>';
                                                           }else{
                                                               echo '<a href="'.base_url().'proyecto/detalle/detalle/'.$result->id_proyecto.'" class="btn btn-info" title="Detalle del Proyecto">
                                                                       <span class="fa fa-eye">
                                                                       </span>
                                                                   </a>';
                                                           }
                                                               ?>

                                                           </td>

                                                           </tr>
                                                           <?php $i++;?>
                                                    <?php endforeach;?>
                                                </tbody>
                                </table>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
