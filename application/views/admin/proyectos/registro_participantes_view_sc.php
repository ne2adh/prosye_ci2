        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div id ="detalleMunicipioProyecto" class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>Registro Participantes
                    <small> </small>
                </h1> 
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        
	                        <div class="form-group">
	                            <label>Ejecucion del paquete a cargo de: </label>
	                            <input type="" name="nombre_convenio" value="<?php echo $convenio->nombre_convenio;?>">
							</div>
                            <div class="form-group">
                                <label>Costo del Paquete en Convenio: </label>
                                <input type="" name="costo_tramo" value="<?php echo $convenio->monto_Bs;?>">
                            </div>
		                      <form action="<?php echo base_url();?>proyecto/participantes/store_sc" method="POST">
    		                        <input type=""  value="<?php echo $id_proyecto; ?>" name="id_proyecto">
                                    <input type=""  value="<?php echo $id_convenio; ?>" name="id_convenio">
                                    <input type=""  value="<?php echo $id_tipo; ?>" name="id_tipo">
                                    <input type="" name="cont" value="<?php echo $cont;?>">
                                    <input type=""  value="<?php echo $id_tramo; ?>" name="id_tramo">
                                    <input type=""  value="<?php echo $tramo->costo_proyecto; ?>" name="costo_proyecto">
                                    <div class="form-group">
    		                            <h1 align="center">Porcentaje de participacion</h1>
    		                        </div>
    		                        <?php /*<div class="form-group">
                                         <label for="ente_finan">Financiamiento Externo al Proyecto</label>   
        		                        <select class="form-control"  id="" name="ente_finan" >
                                                    <option value="">Ente Financiero</option>
                                                    <?php  
                                                        foreach($ente as $row)
                                                        {  
                                                            echo '<option value="'.$row->id_ente.'">'.$row->nombre.''.______.''.$row->descripcion.'</option>';
                                                        }
                                                    ?>
                                        </select> 
                                    </div>
                                    <div class="form-group">
                                        <label for="participacion">Monto de Participacion en el Proyecto con Financiamiento Externo</label>
                                                <input type="text" class="form-control" id="participacion" placeholder="Participacion dentro Proyecto" name="participacion">
                                    </div>*/ ?> 
                                    <div class="form-group">
                                        <label for="municipio_finan">Financiamiento por el Municipio al Proyecto</label>
                                        <select class="form-control"  id="" name="municipio_finan" >
                                                    <option value="">Municipio</option>
                                                    <?php  
                                                        foreach($municipio as $row)
                                                        {  
                                                            echo '<option value="'.$row->id_municipio.'">'.$row->municipio. '</option>';
                                                        }
                                                    ?>
                                        </select> 
                                    </div>    
                                    <?php /*<div class="form-group">
                                        <label for="participacion_muni">Monto de Participacion en el Proyecto del Municipio</label>
                                                <input type="text" class="form-control" id="participacion_muni" placeholder="Participacion dentro Proyecto" name="participacion_muni">
                                    </div>*/ ?>
                                    <div class="form-group">
                                        <label for="cant_empleo">Cantidad de Empleo Generado:</label>
                                        <input type="text" name="cant_empleo" id="cant_empleo" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="poblacion_beneficiada">Total Poblacion Beneficiada:</label>
                                        <input type="text" name="poblacion_beneficiada" id="poblacion_beneficiada" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="porcentaje_beneficio">Porcentaje de Poblacion Beneficiada:</label>
                                        <input type="text" name="porcentaje_beneficio" id="porcentaje_beneficio" class="form-control" placeholder="le falta asignar <?php $s1;?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="porcentaje_participacion">Porcentaje de Participacion:</label>
                                        <input type="text" name="porcentaje_participacion" id="porcentaje_participacion" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="por_incidencia_proy">Porcentaje de Incidencia del Proyecto:</label>
                                        <input type="text" name="por_incidencia_proy" id="por_incidencia_proy" class="form-control">
                                    </div>
                                    <div   class="form-group">
                                                <button aling='center' type="submit" class="btn btn-success btn-flat" >
                                                    Guardar
                                                </button>
                                    </div>
                               
                             </form>
                            
                            </div>
                        </div>
                    </section>
                </div>


		                        