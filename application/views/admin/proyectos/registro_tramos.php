        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
       <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                Paquetes
                    <small>Proyecto: <?php echo $proyectos->nombre_proyecto; ?></small>
                </h1>
            </section>
           
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <form action="<?php base_url();?>/prosye_ci/proyecto/tramos_controller/store/<?php echo $cont;?>" method="POST">
                        <input type="hidden"  value="<?php echo $cont; ?>" name="num_tramo"> <!--recupera el contador para numero de tramos-->
                        <input type="hidden"  value="<?php echo $proyectos->id_proyecto; ?>" name="id_proyecto"> <!--recupera el id del proyecto-->
                        <input type="hidden"  value="<?php echo $id_tipo; ?>" name="id_tipo"> <!--recupera el id de la tabla tipo-->
                        
                        
                        <div class="form-group">
                                   <label for="convenio">El Proyecto Cuenta con Convenio: </label>
                                   <input type=""  value="<?php echo $proyectos->convenio;?>" name="convenio" readonly="">
                                   <br>
                                   <label for="convenio">El Costo Total de este Proyecto es: </label>
                                   <input type=""  value="<?php echo $proyectos->costo_proyecto;?>" name="costo_proyecto" readonly="">
                                    <?php /*<select class="form-control" name="convenio">
                                        <option value="">Seleccione una Opcion</option>
                                        <option value="0">Tramo con convenio</option>
                                        <option value="1">Tramo sin convenio</option>
                                    </select> */?>
                                                                  
                        </div>
                        <div class="form-group">
                            <label for="nombre_tramo">Ingrese el Nombre del Paquete <?php echo $cont+1; ?></label>
                            <input class="form-control" style="text-trasnform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"  value="<?php echo $proyectos->nombre_proyecto;?>--" name="nombre_tramo"/>
                        </div>
                        <div class="form-group">
                            <label for="porcentaje">Ingrese el Porcentaje del Paquete</label>
                            <input class="form-control" name="porcentaje" type="number" min="1" max="100" required="" />
                        </div>
                        <div class="form-group">
                            <label for="costo_proyecto">Ingrese el Monto Asignado al Paquete en Bolivianos</label>
                            <input class="form-control" name="costo_proyecto"/>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label for="fecha_inicio">
                                    Fecha de Inicio:
                                    </label>
                                    <input type="date" id="fecha_inicio" class="form-control" name="fecha_inicio" value="<?php echo $proyectos->fecha_inicio; ?>">
                                </div>  
                                <div class="col-xs-4">
                                    <label for="fecha_fin">
                                        Fecha de Conclusion:
                                    </label>     
                                    <input type="date" id="fecha_fin"  class="form-control" name="fecha_fin"value="<?php echo $proyectos->fecha_conclusion;?>">    
                                </div>  
                                <div class="col-xs-4">
                                    <label for="duracion_anos">Duracion en Años</label>
                                    <input type="number" name="duracion_anos" class="form-control" value="<?php echo $proyectos->duracion_anos; ?>">
                                </div>
                            </div>
                        </div>
                       
                        <?php /*  <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <label for="tiempo_evaluacion">Tiempo de evaluacion del tramo</label>
                                        <input class="form-control" name="tiempo_evaluacion"/>
                                    </div>
                                    <div class="col-xs-6">
                                            <label for="tiempo_inspeccion">Tiempo de inspeccion del tramo</label>
                                        <input class="form-control" name="tiempo_inspeccion"/>
                                    </div>
                                </div>
                            </div>
                            */
                        ?>
                            
                        <div class="form-group">
                            <label for="descripcion">
                                Descripcion del Paquete:
                            </label>
                            <textarea  class="form-control" name="descripcion" type="text"  style="text-trasnform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"  value="<?php echo set_value('descripcion');?>">
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label for="objetivo">
                               Objetivo del Paquete:
                            </label>
                            <textarea  class="form-control" name="objetivo" type="text" style="text-trasnform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label for="meta">
                               Meta del Paquete:
                            </label>
                            <textarea  class="form-control" name="meta" type="text" style="text-trasnform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">
                            </textarea>
                        </div>
                        <div  class="form-group">
                                    <button aling="center" type="submit" class="btn btn-success btn-flat" >
                                        Guardar
                                    </button>
                        </div>
                   </form> 
              
                    </div>
                    <!-- /.box-body -->
            </div>
                <!-- /.box -->

         </section>
            <!-- /.content -->
      </div>
    <!-- /.content-wrapper -->
