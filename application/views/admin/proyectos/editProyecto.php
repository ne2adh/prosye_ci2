        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div id="datos1"  class="content-wrapper">
            <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Editar Proyecto
   	<small>
			<?php echo $proyecto->nombre_proyecto;?></small>
        </h1>
    </section>

            <!-- Main content -->
    <section class="content">

                <!-- Default box -->
        <div class="box box-primary">
            <div class="box-body">
				<form action="<?php echo base_url();?>proyecto/proyecto_controller/editProyecto" method="POST" ref="form" autocomplete="off">

					<input type="hidden" name="rol_id" value="<?php echo $this->session-> userdata('rol');?>" >
					<input type="hidden" name="id_proyecto" value="<?php echo $proyecto->id_proyecto;?>" >
					    <div class="form-group <?php echo !empty(form_error('nombre_proyecto'))? 'has-error':'';?> ">
							<div class="row">
								<div class="col-xs-1"></div>
									<div class="col-xs-12">
										<label for="nombre_proyecto">Nombre del Proyecto</label>
										<input type="text"  class="form-control"  placeholder="NOMBRE DEL PROYECTO"
										name="nombre_proyecto"  style="text-trasnform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"  value="<?php echo $proyecto->nombre_proyecto?>" <?php echo set_value('nombre_proyecto');?>
										onfocus="ShowHide('ayuda_nombre_proyecto','block')" onblur="ShowHide('ayuda_nombre_proyecto','none')"
										>
										<div class="alert alert-info ayuda" id="ayuda_nombre_proyecto">
											Escriba el nombre completo del proyecto (Campo requerido).
										</div>
										<?php echo form_error("nombre_proyecto","<span class='help-block'>","</span>"); ?>
									</div>
              </div>
							</div>
              <div class="col-xs-12 <?php echo !empty(form_error('modalidad'))? 'has-error':'';?>">
                <label for="mod">Modalidad del Proyecto:</label>
					<input type="text" class="form-control" value="<?php echo $proyecto->modalidad; ?>" readonly>                  	
                </select><br>
              </div>

						<div class="form-group <?php echo !empty(form_error('area'))? 'has-error':'';?> ">
							<div class="row">
								<div class="col-xs-1"></div>
								<div class="col-xs-12">
									<label for="area">Secretaria:</label>
										<select class="form-control"  id="area" name="area" readonly>
											<option value="" >Secretaria</option>
											<?php
												foreach($areas as $area):?>
												{
												echo <option value="<?php echo $area->cod;?>" <?php echo set_select("area",$area->cod, $proyecto->id_secretaria==$area->cod?TRUE:'');?> ><?php echo $area->nombre;?> <?php echo set_select("area",$area->nombre);?> </option>';
												}
											<?php endforeach;?>

										</select>
										<?php echo form_error("area","<span class='help-block'>","</span>"); ?>
								</div>
							</div>
						</div>
						<div class="form-group <?php echo !empty(form_error('subarea'))? 'has-error':'';?> <?php echo !empty(form_error('sub_area3'))? 'has-error':'';?> ">
							<div class="row">
								<div class="col-xs 1"></div>
									<div class="col-xs-6">
										<label for="subarea">Area:</label>								
								        <input type="text" class="form-control" value="<?php echo $proyecto->nombre_sub_area; ?>" readonly>
									</div>
									<div class="col-xs-6">
											<label for=" ">Sub-Area:</label>
											<input type="text" class="form-control" value="<?php echo $proyecto->nombre; ?>" readonly>
									</div>

                </div>
              </div>
						<div class="form-group <?php echo !empty(form_error('tipo_fase'))? 'has-error':'';?> <?php echo !empty(form_error('costo_proyecto'))? 'has-error':'';?> ">
							<div class="row">
									<div class="col-xs-6">
                    					<label for="fase">Fase del Proyecto</label>
										<input type="text" class="form-control" value="<?php echo $proyecto->fase; ?>" readonly>
									</div>
									<div class="col-xs-6">
									    <label for="costo_proyecto">Costo del Proyecto</label>
										<input type="text" class="form-control" id="costo_proyecto"  name="costo_proyecto" value="<?php
											function formato($cantidad)
											{
												$formatted = number_format(sprintf('%0.2f', preg_replace("/[^0-9.]/", "", $cantidad)), 2);
												return $cantidad < 0 ? "({$formatted})" : "{$formatted}";
											}											
											echo formato($proyecto->costo_proyecto );
										 	
										?>" <?php echo set_value("costo_proyecto");?> data-type="currency" 
										onfocus="ShowHide('ayuda_costo_proyecto','block')" onblur="ShowHide('ayuda_costo_proyecto','none')"
										>
										<div class="alert alert-info ayuda" id="ayuda_costo_proyecto">
											Escriba el Costo total del Proyecto(Campo requerido).
										</div>
										<?php echo form_error("costo_proyecto","<span class='help-block'>","</span>"); ?>
									</div>
							</div>
						</div>
						<div class="form-group <?php echo !empty(form_error('tipo_proyecto'))? 'has-error':'';?> <?php echo !empty(form_error('convenio'))? 'has-error':'';?> ">
							<div class="row">
									<div class="col-xs-6">
										<label for="tipo_proyecto">Tipo del Proyecto</label>
										<input type="text" class="form-control" value="<?php echo $proyecto->tipo_proyecto; ?>" readonly>                    						
									</div>
									<div class="col-xs-6">
										<label for="convenio">Proyecto con Convenio</label>
										<input type="text" class="form-control" value="<?php echo $proyecto->convenio; ?>" readonly>
									</div>

              </div>
						</div>

						<div class="col-xs-12 text-center">
								<label>Tiempo de Duracion</label>
						</div>

						<div class="form-group <?php echo !empty(form_error('fecha_inicio'))? 'has-error':'';?> <?php echo !empty(form_error('fecha_conclusion'))? 'has-error':'';?> <?php echo !empty(form_error('duracion_anos'))? 'has-error':'';?> ">
							<div class="row">
								<div class="col-xs-6">
	                				<label for="fecha_inicio">
	                					Fecha de Inicio:
	                				</label>
									<input type="month" id="fecha_inicio" class="form-control" name="fecha_inicio" value="<?php 
											$y = date("Y", strtotime($proyecto->fecha_inicio));
											$m = date("m", strtotime($proyecto->fecha_inicio));
											echo $y.'-'.$m;
										?>" <?php echo set_value("fecha_inicio");?> onChange="resta_anios();" 
										onfocus="ShowHide('ayuda_fecha_inicio','block')" onblur="ShowHide('ayuda_fecha_inicio','none')"
										>
										<div class="alert alert-info ayuda" id="ayuda_fecha_inicio">
											Seleccione el Mes y el Año de Inicio del Proyecto (Campo requerido).
										</div>
									<?php echo form_error("fecha_inicio","<span class='help-block'>","</span>" ); ?>
								</div>
								<div class="col-xs-6">
									<label for="fecha_conclusion">
									Fecha de Conclusión:
								</label>
								<input type="month" id="fecha_conclusion"  class="form-control" name="fecha_conclusion" value="<?php 
											$y = date("Y", strtotime($proyecto->fecha_conclusion));
											$m = date("m", strtotime($proyecto->fecha_conclusion));
											echo $y.'-'.$m;
										?>"  <?php echo set_value("fecha_conclusion");?>
										onfocus="ShowHide('ayuda_fecha_conclusion','block')" onblur="ShowHide('ayuda_fecha_conclusion','none')"
										>
										<div class="alert alert-info ayuda" id="ayuda_fecha_conclusion">
											Seleccione el Mes y el Año de Conclusion del Proyecto (Campo requerido).
										</div>
									<?php echo form_error("fecha_conclusion","<span class='help-block'>","</span>"); ?>
								</div>
								 <?php /*
								<div class="col-xs-4">
                						<label for="duracion_anos">Duracion en Años</label>
										<input id="duracion" type="" name="duracion_anos" class="form-control"  value="<?php $datos=$this->proyectos_model->calculaTiempo(); if ($datos[0]>0) {echo $datos[0]+1;}else{$datos='';} ?>"  readonly="">
										<?php echo form_error("duracion_anos","<span class='help-block'>","</span>"); ?>
								</div> */
								?>
							 </div>
						</div>

	            <div class="form-group <?php echo !empty(form_error('descripcion_proyecto'))? 'has-error':'';?>">
  							<div class="row">
  							<div class="col-xs-12">
  								<label for="descripcion_proyecto">
  									Descripcion del Proyecto:
  								</label>
  								<input type="text"  class="form-control"  placeholder="DESCRIPCION DEL PROYECTO"
  										name="descripcion_proyecto" id="" style="text-trasnform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"  value="<?php echo $proyecto->descripcion_proyecto;?>" <?php echo set_value("descripcion_proyecto");?> required
								    onfocus="ShowHide('ayuda_descripcion','block')" onblur="ShowHide('ayuda_descripcion','none')"
								>
								<div class="alert alert-info ayuda" id="ayuda_descripcion">
									Escriba la descripción del proyecto (Campo requerido).
								</div>

  								<?php echo form_error("descripcion_proyecto","<span class='help-block'>","</span>"); ?>
  								</div>
  							</div>
              </div>

              <div class="form-group <?php echo !empty(form_error('objetivo_proyecto'))? 'has-error':'';?> ">
    						<div class="row">
    							<div class="col-xs-12">
    								<label for="objetivo_proyecto">
    									Objetivo General del Proyecto:
    								</label>
    								<input type="text"  class="form-control"  placeholder="OBJETIVO DEL PROYECTO"
    										name="objetivo_proyecto" id="" style="text-trasnform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"  value="<?php echo $proyecto->objetivo_proyecto;?>" <?php echo set_value("objetivo_proyecto");?>
											onfocus="ShowHide('ayuda_objetivo','block')" onblur="ShowHide('ayuda_objetivo','none')"
									>
									<div class="alert alert-info ayuda" id="ayuda_objetivo">
										Escriba el objetivo del proyecto (Campo requerido).
									</div>
									<br>
    								<?php echo form_error("objetivo_proyecto","<span class='help-block'>","</span>"); ?>
    			        </div>
                </div>
            </div>

            <div>
              <center>
                <div class="col-xs-12" style="display:none;">
                  <h4>
				  	<label for="tramos">¿El proyecto tiene Tramos o Paquetes?</label><br><br>
				  </h4>
                </div>
              </center>
              <script>
                $(document).ready(function(){
                      $(".codigo_tramo").click(function(evento){

                          var valor = $(this).val();

                          if(valor == 1){
                              $("#div1").css("display", "block");
                              //$("#div2").css("display", "none");
                          }else{
                              $("#div1").css("display", "none");
                              //$("#div2").css("display", "block");
                          }
                  });
                });
            </script>
                <div class="col-xs-12" style="display:none;">
                  <center>
                    <input name="codigo_tramo" class="codigo_tramo" type="radio"
                      style="width: 25px; height: 25px" value="1"/>&nbsp;<span class="auto-style4">SI</span>
                      &nbsp;&nbsp;
                    <input checked="checked" class="codigo_tramo" name="codigo_tramo" type="radio"
                      style="width: 25px; height: 25px" value="0"/><span class="auto-style4">NO</span>
                    <p>&nbsp;</p>
                  </center>
                </div>
            <div class="col-xs-4">
            </div>
            <section aling="center" class="container">
              <div class="caja">
                <div id="div1" class="col-xs-4" style="display:none;">
                  <p class="auto-style3"><span class="auto-style4">Selecciona numero de tramos:</span></p>
                    <select class="form-control" name="numero_tramo" id="numero_tramo">
                      <?php
                      for ($i=1; $i <=100 ; $i++){
                         echo "<option value=" . $i . ">" . $i . "</option>";
                      }
                      ?>
                    </select>
                </div>
              </div>
            </section>
          </div>

          <div  align="center" class="form-group"><br>
    					<button type="submit" class="btn btn-success btn-flat" >
    						Guardar
    				</button>
   				</div>

				</form>
        </div>
    </div>
  </section>
            <!-- /.content -->
</div>
		<script>
			function filterFloat(evt,input){
			// Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
			var key = window.Event ? evt.which : evt.keyCode;
			var chark = String.fromCharCode(key);
			var tempValue = input.value+chark;
			if(key >= 48 && key <= 57){
				if(filter(tempValue)=== false){
					return false;
				}else{
					return true;
				}
			}else{
				if(key == 8 || key == 13 || key == 0) {
					return true;
				}else if(key == 46){
						if(filter(tempValue)=== false){
							return false;
						}else{
							return true;
						}
				}else{
					return false;
				}
			}
		}
		function filter(__val__){
			var preg = /^([0-9]+\.?[0-9]{0,2})$/;
			if(preg.test(__val__) === true){
				return true;
			}else{
			return false;
			}




		}
		</script>
