<div class="content-wrapper" id="   ">
            <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Registro Coordenadas (Puntos)
            <small>Proyecto: <?php echo $proyecto->nombre_proyecto;?></small>
        </h1>        
    </section>
            <!-- Main content -->
    <section class="content">
                <!-- Default box -->
        <div class="box-body">
            <div class="box">
                <div class="box-header with-border">
                    
                        <div class="row">
                            <div class="col-md-6">
							    <form action="<?php echo base_url();?>proyecto/puntos_tramo_controller/store" method="POST">
                                    <div class="form-group">
    								    <input type="" value="<?php echo $id_proyecto; ?>" name="id_proyecto" id="" >
                                        <input type="" value="<?php echo $proyecto->convenio; ?>" name="convenio" id="" >
                                        <input type="" value="<?php echo $tramo->id_tramo; ?>" name="id_tramo" id="" >
                                        <input type=""  value="<?php echo $id_tipo;?>"  name="id_tipo"><!--recupera el id del tipo del proyecto-->
                                            <div class="form-group <?php echo !empty(form_error('latitud'))? 'has-error':'';?>">
                                                <label for="latitud">
                                                    Latitud
                                                </label>
                                                <input id= "" type="" class="form-control" name="latitud" />
                                                <?php echo form_error("latitud","<span class='help-block'>","</span>"); ?>
                                            </div>
                                            <div class="form-group <?php echo !empty(form_error('longitud'))? 'has-error':'';?>">
                                                <label for="longitud">
                                                    Longitud
                                                </label>
                                                <input id="" type="" class="form-control" name="longitud" />
                                                <?php echo form_error("longitud","<span class='help-block'>","</span>"); ?>
                                            </div>
                                            <div class="col-xs-4"></div>

                                            <button  type="submit" class="btn btn btn-info" aling="center">
                                                <span class="glyphicon glyphicon-plus">Agregar puntos</span>
                                            </button>
                                    </div>
                                </form>
                            </div> 
                            <div class="col-md-6">
                               
                                <form name="frmSubirArchivo" method="POST" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-10">
                                                <input type="hidden" value="<?php echo $id_proyecto; ?>" name="id_proyecto" id="id_proyecto" >
                                                <input type="hidden" value="<?php echo $proyecto->convenio; ?>" name="convenio" id="" >
                                                <input type="hidden" value="<?php echo $tramo->id_tramo; ?>" name="id_tramo" id="" >
                                                <input type="hidden"  value="<?php echo $id_tipo;?>"  name="id_tipo"><!--recupera el id del tipo del proyecto-->
                                                <label > <strong>Seleccione un Archivo si lo tuviera en formato csv:</strong>
                                                </label>
                                            </div>
                                            <div class="col-md-12">
                                                <input type="file" name="excel">                                   
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group"><br>
                                            <div class="col-md-10"><br>
                                                <div class="col-xs-5"></div>

                                                <input  type="botton"  onclick="subirArchivoExcel();" class="btn bg-aqua" value="Subir Archivo"  data-toggle="modal" data-target="#mi_ventana"/>
                                                

                                                <div class="modal fade" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal_dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="botton" class="close" data-dismiss="modal" aria-hidden="true">&time;</button>
                                                                <h4>Esta a punto de subir datos</h4>
                                                                
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="botton" class="btn btn-primary" data-dismiss="modal" onclick="subirArchivoExcel();">Volver</button>
                                                            </div>
                                                            
                                                        </div>
                                                        
                                                    </div>
                                                </div>


                                                <?php //<button name="cancelar" id="cancelar" class="btn bg-red" style="width:80px;">Cancelar</button>?>


                                                <?php /*    <button  type="button" class="btn btn btn-warning" aling="center" v-on:click="cargarpuntos">
                                                        <span class="glyphicon glyphicon">Agregar archivo</span>
                                                    </button>*/?>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <br>
                            <div class="col-md-12" >
                                <div class="form-group">
                                    <div class="col-xs-1"></div>
                                        <div class="col-xs-10">
                                                <div align="center" class="col-md-12" >
                                                    <h3>Puntos del Paquete</h3>      
                                                </div>
                                                    <table class="table table-bordered btn-hover" role="grid" id="tablair">
                                                            <thead >
                                                                <tr class="success">
                                                                    <th>Numero</th>
                                                                    <th>Latitud</th>
                                                                    <th>Longitud</th>
                                                                    <th>Opciones</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php $i=0;if(!empty($punto)):?>
                                                                <?php foreach($punto as $met ):?>
                                                                <tr>
                                                                    <td><?php  $i; $i=$i+1; echo $i;?></td>
                                                                    <td><?php echo $met->latitud;?></td>       
                                                                    <td><?php echo $met->longitud;?></td>
                                                                    <td>
                                                                        <!-- Botón para guardar la información actualizada -->
                                                                        <span >
                                                                            <!-- Botón para mostrar el formulario de actualizar -->
                                                                            <a href=""> 
                                                                                <button type="button" id="submitBtn"  class="btn btn-warning" >Actualizar</button>
                                                                            </a>
                                                                                    <!-- Botón para borrar -->
                                                                            <a href=" ">
                                                                                <button type="button" class="btn btn-danger" " >Borrar</button>
                                                                            </a>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                <?php endforeach;?>  
                                                             <?php endif;?>									
                                                            </tbody>
                                                    </table>       
                                        </div>
                                    </div>
                            </div>

                        </div>
                                <!--button  type="submit" class="btn btn btn-info" aling="center">
                                    <span class="glyphicon glyphicon-plus">Agregar puntos</span>
                                </button-->
					
                    <form action="<?php echo base_url();?>proyecto/componente_controller/ingreso" method="POST">    
                        <input type="hidden" name="id_proyecto" value="<?php echo $id_proyecto;?>">
                        <input type="hidden" value="<?php echo $proyecto->convenio; ?>" name="convenio" id="" >
                        <input type="hidden" value="<?php echo $tramo->id_tramo; ?>" name="id_tramo" id="" >
                        <input type="hidden"  value="<?php echo $id_tipo;?>"  name="id_tipo"><!--recupera el id del tipo del proyecto-->
                            <div class="box box-solid">
                                <div class="box-body">
                                    <div  class="row"><div class="col-xs-5"></div>
                                        <div class="form-group">
                                            <button aling='center' type="submit" class="btn btn-success btn-flat" >
                                                Continuar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </form>  
                </div>
                    <!-- /.box-body -->
            </div>
        </div>
                <!-- /.box -->
    </section>
            <!-- /.content -->
</div>
<!--<script src="https://d3js.org/d3.v3.js"></script>-->
 <script type="text/javascript">
                                    function subirArchivoExcel()
                                    {
                                        if(document.frmSubirArchivo.excel.value==="")
                                        {
                                            alert("Error debe subir archivo");
                                            document.frmSubirArchivo.excel.focus();
                                            return false;

                                        }
                                        document.frmSubirArchivo.action="<?php echo base_url();?>proyecto/procesar_controller/excelProy";
                                        document.frmSubirArchivo.submit();
                                    }
</script>



