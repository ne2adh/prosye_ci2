        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
<div  class="content-wrapper">
            <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Seguimiento Avance Temporal del Proyecto
   	        <small> </small>
        </h1>
    </section>
            <!-- Main content -->
    <section class="content">
                <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
            		<!-- inicio -->
            		
            		<form action="<?php echo base_url();?>temporal/avance_controller/store" method="POST" >
            		<input type="hidden" name="id_proyecto" value="<?php echo $id_proyecto;?>">
            		<input type="hidden" name="id_tramo" value="<?php echo $id_tramo;?>">
            		<div class="form-group">
							<div class="row">
								<div class="col-xs-1"></div>
									<div class="col-xs-12">
										<label class="form-group"><h4>El proyecto al cual le asignara monto es:</h4></label>
										<input type="" name="nombre_proyecto" value="<?php echo $proyecto->nombre_proyecto;?>">
            							<br>
            							<label  class="form-group"><h4>El tramo al cual le asignara monto es:</h4></label>
            							<input  type="" name="nombre_tramo" value="<?php echo $proyecto->nombre_tramo;?>">	
            							<br>
										<label class="form-group"><h2>Seguimiento Financiero - Costo Ejecutado del Tramo</h2></label>
										<br>
										
										<div class="form-group">
											<div class="row">
											  <div class="col-xs-1"></div>
												<div class="col-xs-12">
														<label for="anio">Ingrese Año de Seguimiento</label>
														<select name='gestion'>
															  <option value="">Elija el numero de año</option>
							                                  <option value="1">Año 1</option>
							                                  <option value="2">Año 2</option>
							                                  <option value="3">Año 3</option>
							                                  <option value="4">Año 4</option>
							                                  <option value="5">Año 5</option>
							                                  <option value="6">Año 6</option>
							                                  <option value="7">Año 7</option>
							                                  <option value="8">Año 8</option>
							                                  <option value="9">Año 9</option>
							                                  <option value="10">Año 10</option>
							                                  <option value="11">Año 11</option>
							                                  <option value="12">Año 12</option>
														</select>
														<label for="mes">Ingrese mes de Seguimiento:</label>
														<select name='mes'>
															  <option value="">Elija el mes de la gestion correspondiente</option>
							                                  <option value="1">Mes 1</option>
							                                  <option value="2">Mes 2</option>
							                                  <option value="3">Mes 3</option>
							                                  <option value="4">Mes 4</option>
							                                  <option value="5">Mes 5</option>
							                                  <option value="6">Mes 6</option>
							                                  <option value="7">Mes 7</option>
							                                  <option value="8">Mes 8</option>
							                                  <option value="9">Mes 9</option>
							                                  <option value="10">Mes 10</option>
							                                  <option value="11">Mes 11</option>
							                                  <option value="12">Mes 12</option>
														</select>
														<label for="monto"  class="form-group">Monto del Tramo en Bs.</label>
														<input type="text"  id=""  name="monto">

														<label class="form-group"><h2>Seguimiento Físico - Avance Ejecutado del Tramo</h2></label>
										<br>
										<?php /*<label class="form-group">El proyecto al cual le asignara monto es:</label>
										<input type="" name="nombre_proyecto" value="<?php echo $proyecto->nombre_proyecto;?>">
            							<br>
            							<label  class="form-group">El tramo al cual le asignara monto es:</label>
            							<input type="" name="nombre_tramo" value="<?php echo $proyecto->nombre_tramo;?>"> */?>
										<div class="form-group">
											<div class="row">
											  <div class="col-xs-1"></div>
												<div class="col-xs-12">
														<label for="por_avance"  class="form-group">Ingrese Porcentaje de Avance Físico en el Tramo</label>
														<input type="text"  id=""  name="por_avance">
														
														<br>
														
														<br>
													<div  align="center" class="form-group">
													<button type="submit" class="btn btn-primary" >
									                	Guardar
									                </button>
						           					</div>
											</form>	
									</div>
								<div class="col-xs-1"></div>
							</div>
						</div>	
				</div>
			</div>
		</div>
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
		