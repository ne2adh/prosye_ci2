
        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper"  ng-app="app" ng-controller="ctrl">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Paquetes del Proyecto -
                    <small><?php echo $tramo->nombre_tramo;?></small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-header">                    
                        <nav class="navbar navbar-default">
                            <div class="container-fluid">

                                <ul class="nav navbar-nav">
                                    <li class="nav-item active" ng-show=" convenio==='SI' ">
                                        <a class="nav-link"
                                        <?php
                                            if(isset($convenio)){
                                                echo 'href="'.base_url().'proyecto/convenio_controller/ingresoEditConvenio/'.$tramo->id_tramo.'/'.$convenio.'/SI"';
                                            }
                                            else{
                                                 echo 'href="'.base_url().'proyecto/convenio_controller/ingresoConvenio/'.$tramo->id_tramo.'/SI"';
                                            }
                                        ?>

                                        >Convenio</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url();?>proyecto/metas_controller/ingresoMetas/<?php echo $tramo->id_tramo?>">
                                            Metas
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url();?>proyecto/municipio_controller/ingreso/<?php echo $tramo->id_tramo?>">Municipio</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url();?>proyecto/puntos_controller/ingresoPuntos/<?php echo $tramo->id_tramo?>">Puntos</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url();?>proyecto/componente_proy_controller/nuevo/<?php echo $tramo->id_tramo?>">Componentes</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url();?>proyecto/Indicador_controller/ingresoIndicador/<?php echo $tramo->id_tramo?>">Indicadores</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url();?>proyecto/contratos_controller/ingreso/<?php echo $tramo->id_tramo?>">Contratos</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url();?>proyecto/contratos_controller/ingreso1/<?php echo $tramo->id_tramo?>">Garantias</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url();?>proyecto/Seguir_fisico_controller/ingreso/<?php echo $tramo->id_tramo?>">Seguimiento Financiero</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url();?>proyecto/Seguir_fisico_controller/ingreso2/<?php echo $tramo->id_tramo?>">Seguimiento Fisico</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url();?>proyecto/Sye_controller/ingreso/<?php echo $tramo->id_tramo?>">Reporte</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <div class="box-body text-center">
                                        <h1>
                                        Paquetes
                                            <small>
                                                Proyecto: <?php echo $proyectos->nombre_proyecto; ?>
                                            </small>
                                        </h1>
                                        <form role="form" ng-submit="registrarTramo()" name="formulario">
                                                <input type="hidden"  value="<?php echo $cont; ?>" name="num_tramo"> <!--recupera el contador para numero de tramos-->
                                                <input type="hidden"  value="<?php echo $proyectos->id_proyecto; ?>" name="id_proyecto"> <!--recupera el id del proyecto-->
                                                <input type="hidden"  value="<?php echo $id_tipo; ?>" name="id_tipo"> <!--recupera el id de la tabla tipo-->


                                                <div class="form-group">
                                                        <label for="convenio">El Proyecto Cuenta con Convenio: </label>
                                                        <input type=""  value="<?php echo $proyectos->convenio;?>" name="convenio" readonly="">
                                                        <br>
                                                        <label for="convenio">El Costo Total de este Proyecto es: </label>
                                                        <input type=""  value="<?php echo $proyectos->costo_proyecto;?>" name="costo_proyecto" readonly="">
                                                            <?php /*<select class="form-control" name="convenio">
                                                                <option value="">Seleccione una Opcion</option>
                                                                <option value="0">Tramo con convenio</option>
                                                                <option value="1">Tramo sin convenio</option>
                                                            </select> */?>
                                                </div>
                                                <div class="form-group">
                                                   <div ng-show="convenio=='NO' || convenio==0">
                                                        Este Tramo no tiene convenio(s) <a href="#" ng-click="agregarConvenio()">Agregar Convenio</a>
                                                   </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="nombre_tramo">Ingrese el Nombre del Paquete <?php echo $cont+1; ?></label>
                                                    <input class="form-control" style="text-trasnform:uppercase;" ng-model="nombre_tramo" onkeyup="javascript:this.value=this.value.toUpperCase();" id="nombre_tramo" name="nombre_tramo"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="porcentaje">Ingrese el Porcentaje del Paquete</label>
                                                    <input class="form-control" name="porcentaje" ng-model="porcentaje" type="number" min="1" max="100" required="" />
                                                </div>
                                                <div class="form-group">
                                                    <label for="costo_proyecto">Ingrese el Monto Asignado al Paquete en Bolivianos</label>
                                                    <input class="form-control" name="costo_proyecto" ng-model="costo_proyecto" />
                                                </div>
                                                <div class="form-group" data-ng-init="duracionAnios()">
                                                    <div class="row">
                                                        <div class="col-xs-4">
                                                            <label for="fecha_inicio">
                                                            Fecha de Inicio:
                                                            </label>
                                                            <input type="date" id="fecha_inicio" ng-model="fecha_inicio"   ng-change="duracionAnios()" class="form-control" name="fecha_inicio">
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <label for="fecha_conclusion">
                                                                Fecha de Conclusion:
                                                            </label>
                                                            <input type="date" id="fecha_conclusion"  ng-model="fecha_conclusion"  ng-change="duracionAnios()" class="form-control" name="fecha_conclusion" >
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <label for="duracion_anos">Duracion en Años</label>
                                                            <input type="number" name="duracion_anos" ng-model="duracion" class="form-control" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="descripcion_proyecto">
                                                        Descripcion del Paquete:
                                                    </label>
                                                    <textarea  class="form-control" name="descripcion_proyecto" ng-model="descripcion_proyecto" type="text"  style="text-trasnform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" >
                                                    </textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="objetivo_proyecto">
                                                    Objetivo del Paquete:
                                                    </label>
                                                    <textarea  class="form-control" name="objetivo_proyecto" ng-model="objetivo_proyecto" type="text" style="text-trasnform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">
                                                    </textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="meta">
                                                    Meta del Paquete:
                                                    </label>
                                                    <textarea  class="form-control" name="meta" ng-model="meta" type="text" style="text-trasnform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();">
                                                    </textarea>
                                                </div>
                                                <div  class="form-group">
                                                            <button aling="center" type="submit" class="btn btn-success btn-flat" >
                                                                Guardar
                                                            </button>
                                                </div>
                                        </form>


                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

<script src="<?php echo base_url();?>assets/angular.min.js"></script>
<script src="<?php echo base_url();?>assets/ui-bootstrap.min.js"></script>
<script>
    let ngApp = function(){
        'use strict';
        let app = angular.module('app', ['ui.bootstrap']);
        app.controller('ctrl', function($scope, $http) {
            $scope.id_tramo = <?php echo isset($tramo->id_tramo)?$tramo->id_tramo:'""'; ?>;
            $scope.nombre_tramo = "<?php echo isset($tramo->nombre_tramo)?$tramo->nombre_tramo:''; ?>";
            $scope.porcentaje = <?php echo isset($tramo->porcentaje)?$tramo->porcentaje:'""'; ?>;
            $scope.costo_proyecto = <?php echo isset($tramo->costo_proyecto)?$tramo->costo_proyecto:'""'; ?>;
            $scope.fecha_inicio = new Date('<?php echo isset($tramo->fecha_inicio)?$tramo->fecha_inicio:Date("d/m/Y"); ?>');
            $scope.fecha_conclusion = new Date('<?php echo isset($tramo->fecha_conclusion)?$tramo->fecha_conclusion:Date("d/m/Y"); ?>');
            $scope.tiempo_evaluacion = <?php echo isset($tramo->tiempo_evaluacion)?$tramo->tiempo_evaluacion:'""'; ?>;
            $scope.tiempo_inspeccion = <?php echo isset($tramo->tiempo_inspeccion)?$tramo->tiempo_inspeccion:'""'; ?>;
            $scope.convenio = "<?php echo isset($tramo->convenio)?$tramo->convenio:''; ?>";
            $scope.id_tipo = <?php echo isset($tramo->id_tipo)?$tramo->id_tipo:'""'; ?>;
            $scope.objetivo_proyecto = "<?php echo isset($tramo->objetivo_proyecto)?$tramo->objetivo_proyecto:''; ?>";
            $scope.descripcion_proyecto = "<?php echo isset($tramo->descripcion_proyecto)?$tramo->descripcion_proyecto:''; ?>";
            $scope.meta = "<?php echo isset($tramo->meta)?$tramo->meta:''; ?>";
            $scope.id_proyecto = <?php echo isset($tramo->id_proyecto)?$tramo->id_proyecto:'""'; ?>;
            $scope.nombre_proyecto = "<?php echo isset($tramo->nombre_proyecto)?$tramo->nombre_proyecto:''; ?>";
            $scope.duracion = 0;
            $scope.duracionAnios = function(){
                let fecha_inicio = new Date($scope.fecha_inicio).getTime();
                let fecha_conclusion = new Date($scope.fecha_conclusion).getTime();
                let f = fecha_conclusion-fecha_inicio;
                f = f/(1000 * 60 * 60 * 24);
                let a = Math.floor(f/365) + parseInt(( f%365!== 0)?1:0);
                $scope.duracion = a;
            };

            $scope.registrarTramo = function(){
                $scope.verificarMenu = true;
                var req = {
                        method: 'POST',
                        url: "<?php echo base_url('proyecto/tramos_controller/storeTramo'); ?>",
                        data: {
                            id_tramo : $scope.id_tramo,
                            nombre_tramo : $scope.nombre_tramo,
                            porcentaje : $scope.porcentaje,
                            costo_proyecto : $scope.costo_proyecto,
                            fecha_inicio : $scope.fecha_inicio,
                            fecha_conclusion : $scope.fecha_conclusion,
                            tiempo_evaluacion : $scope.tiempo_evaluacion,
                            tiempo_inspeccion : $scope.tiempo_inspeccion,
                            convenio : $scope.convenio,
                            id_tipo : $scope.id_tipo,
                            objetivo_proyecto : $scope.objetivo_proyecto,
                            descripcion_proyecto : $scope.descripcion_proyecto,
                            meta : $scope.meta,
                            id_proyecto : $scope.id_proyecto,
                            nombre_proyecto : $scope.nombre_proyecto
                        }
                };

                $http(req).then( function(){
                    //Seccion registrada
                    alert("Datos registrados");
                    document.location.reload(true);
                } );
            }

            $scope.agregarConvenio = function(){
                let op = confirm("¿Desea agregar Convenio al tramo?");
                if(op){
                    //modificar convenio
                    var req = {
                        method: 'POST',
                        url: "<?php echo base_url('proyecto/tramos_controller/updateEstado'); ?>",
                        data: {
                            id_tramo : $scope.id_tramo,
                            convenio : "SI"
                        }
                };

                $http(req).then( function(){
                    //Seccion registrada
                    alert("Puede agregar convenios en el menu superior");
                    document.location.reload(true);
                } );
                }
            }
        });
    }();

</script>
