

		<div class="content-wrapper" ng-app="app" ng-controller="ctrl">
		  <section class="content-header">
		    <h1>
		        MAPEO
		    </h1>
            <div  align="center" class="form-group">
                <a href="<?php echo base_url();?>proyecto/puntos_controller/ingresoPuntos/<?php echo $id_proyecto;?>" class="btn btn-primary btn-flat" >
                    Volver Atras
                </a>
            </div>
		  </section>
		  <section class="content">
		    <div class="box box-solid">
		        <div class="box-body">
		            <div class="row" style="height:470px">
	    						<map center="{{posicion}}" zoom="7.8" style="height: 450px">
	                  <marker ng-repeat="p in puntos track by $index" position="[{{p.lat}},{{p.lng}}]"></marker>
	                </map>
	                Coordenadas:
					<p ng-repeat="p in puntos">{{p.lat}}, {{p.lng}}</p>

		            </div>
		          </div>
		      </div>
		  </section>
		</div>


		<!--angularjs :) -->
		<script src="<?php echo base_url();?>assets/angular.min.js"></script>
		<script src="<?php echo base_url();?>assets/ui-bootstrap.min.js"></script>

		<script src="//maps.google.com/maps/api/js?key=AIzaSyCWAZ49IYfc8E01KK56LhDzfC_MnMGf9SU"></script>
	  <script src="<?php echo base_url();?>assets/ng-map.min.js"></script>

		<script>
		let ngApp = function(){
		  'use strict';
		  let app = angular.module('app', ['ui.bootstrap', 'ngMap']);
		  app.controller('ctrl', function($scope, $http) {
				let id_proyecto = <?php echo $id_proyecto;?>;
				let url = '<?php echo base_url();?>' + 'proyecto/Gmaps/listarPuntos?id_proyecto=';
				let map;
        $scope.$on('mapInitialized', function(evt, evtMap) {
        	map = evtMap;
					/////

					$http.get(url+id_proyecto).then(function(response) {
						$scope.puntos = response.data;
						if($scope.puntos.length>0){
							initMap($scope.puntos);
						}
					});

					function initMap(puntos) {
						//centrado
						$scope.posicion= ''+puntos[0].lat+','+puntos[0].lng+'';
					  // se construye el poligono
					  const poligono = new google.maps.Polygon({
					    paths: puntos,
					    strokeColor: "#FF0000",
					    strokeOpacity: 0.8,
					    strokeWeight: 2,
					    fillColor: "#FF0000",
					    fillOpacity: 0.35,
							geodesic: true,
					  });
					  poligono.setMap(map);
					}
					////
		    });

		    console.log('funciona');
	  	});
	}();
	</script>

