        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">Menu proyecto</li>
                    
                    <li>
                        <a href="<?php echo base_url();?>">
                            <i class="fa fa-home"></i> <span>Inicio</span>
                        </a>
                    </li>
                    <?php
                        $id_rol = $this->session->rol;
                        $CI =& get_instance();
                        $CI->load->model('rol_model');                        
                        $result= $CI->rol_model->listRolAsignado($id_rol);
                        foreach($result as $item){
                            if(count($item["menu"])>0){
                                echo ' <li class="treeview">
                                    <a href="#">
                                        <i class="'.$item["logo"].'"></i> <span>'.$item["nombre"].'</span>
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">';
                                    foreach($item["menu"] as $menu){
                                        $url = base_url($menu["direccion"]);
                                        $v_class = explode(" ", $menu["logo"]);
                                        $color = '';                                        
                                        if(count($v_class)>1){
                                            $color = $v_class[2];
                                        }
                                        echo '<li><a  class="'.$color.'"href="'.$url.'"><i class="'.$menu["logo"].'"></i>'.$menu["nombre_menu"].'</a></l>';
                                    }
                                echo  '</ul>
                                </li>';
                            }                            
                        }                       
                    ?>
                     <li>
                        <a href="<?php echo base_url();?>auth/logout">
                            <i class="fa fa-close"></i> <span>Cerrar</span>
                        </a>
                    </li>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>
        <!-- =============================================== -->
