<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <link rel="icon" href="<?php echo base_url('assets/favicon.ico') ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ProSye</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template/bootstrap/css/bootstrap.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template/font-awesome/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template/dist/css/skins/_all-skins.min.css">
    <!-- DataTables-Export -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/template/datatables-export/css/buttons.dataTables.min.css">
    
    <link rel="stylesheet" href="<?php echo base_url();?>assets/estilos.css">
    <!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/template/jquery/jquery.min.js"></script>
    
    <!-- DataTables-Export -->
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <?php if (!$this->session->userdata('id'))
    { redirect(base_url());}
     ?>
    
    <!-- Site wrapper -->
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="<?php echo base_url();?>" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>P</b>SyE</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>ProSye</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?php echo base_url()?>assets/template/dist/img/icono.jpg" class="user-image" alt="User Image">
                                <span class="hidden-xs"><?php echo $this->session->userdata("nombre")?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-header">
                                    <img src="<?php echo base_url(); ?>assets/template/dist/img/icono.jpg" class="img-circle" alt="User Image">
                                    <p>
                                    <?php echo $this->session->userdata('nombre') ?> 
                                    <small><?php echo $this->session->userdata("nombre_rol")?></small>
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-left">
                                    <a href="<?php echo base_url(); ?>administrador/usuarios/perfil" class="btn btn-default btn-flat">Perfil</a>
                                    </div>
                                    <div class="pull-right">
                                    <a href="<?php echo base_url(); ?>auth/logout">Cerrar Sesión</a>
                                    </div>
                                </li>
                                <!--<li class="user-body">
                                    <div class="row">
                                        <div class="col-xs-12 text-center">
                                            <a href="<?php echo base_url(); ?>auth/logout">Cerrar Sesión</a>

                                        </div>
                                        <div class="col-xs-12 text-center">
                                            <a href="<?php echo base_url(); ?>auth/logout">Perfil</a>
                                        </div>
                                    </div>
                                     /.row 
                                </li>-->
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                <img src="<?php echo base_url(); ?>assets/template/dist/img/user2-160x160.jpg" >
                </div>
                <div class="pull-left info">
                <p>Proyecto</p>
                <a href="#"><i class="fa fa-circle text-success"></i>En linea proyecto</a>
                </div>
            </div>
            <?php $this->load->view('layouts/aside'); ?>
            </section>
            <!-- /.sidebar -->
        </aside>