        <!-- =============================================== -->
        <!-- Left side column. contains the sidebar -->        
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">      
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">CONTROL DE PROYECTOS</li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-share-alt"></i> <span>Proyectos</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span> 
                        </a>
                        <ul class="treeview-menu">                            
                            <li><a href="<?php echo base_url();?>proyecto/convenio_controller"><i class="fa fa-circle-o text-yellow"></i> Registro Convenio</a></li>
                            <li><a href="<?php echo base_url();?>proyecto/metas_controller"><i class="fa fa-circle-o text-green"></i> Registro Metas</a></li>    
                            <li><a href="<?php echo base_url();?>proyecto/municipio_controller"><i class="fa fa-circle-o text-red"></i> Registro Municipio</a></li>
                            <li><a href="<?php echo base_url();?>proyecto/puntos_controller"><i class="fa fa-circle-o text-blue"></i> Registro Puntos</a></li>
                             <li><a href="<?php echo base_url();?>proyecto/componente_proy_controller/mios"><i class="fa fa-circle-o text-black"></i> Registro Componentes</a></li>
                            <li><a href="<?php echo base_url();?>proyecto/indicador_controller"><i class="fa fa-circle-o"></i> Registro Indicadores</a></li>                            
                            <?php
                                if( isset($tramos) && count($tramos)>0 ):
                            ?>                           
                            <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-list"></i> <span>Paquetes</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span> 
                                </a>
                                <ul class="treeview-menu">
                                    <?php
                                       foreach($tramos as $tramo){                                            
                                            echo '<li><a class="fa fa-check-circle-o" href="'.base_url().'proyecto/tramos_controller/inicio/'.$tramo->id_tramo.'">'.$tramo->nombre_tramo.'</a></li>';
                                        }
                                    ?>
                                </ul>
                            </li>
                            <?php endif?>
                        </ul>
                      </li>
                      <li>  
                        <a href="<?php echo base_url();?>proyecto/mis_proyectos/propios">
                            <i class="fa fa-home"></i> <span>Cerrar</span>
                        </a>
                    </li>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>
        <!-- =============================================== -->