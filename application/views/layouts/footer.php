        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.5.1
            </div>
            <strong>Copyright &copy; <?php echo date("Y");?> <a href="http://www.oruro.gob.bo/" target="_blank">GADOR</a>.</strong> Derechos Reservados.
        </footer>
    </div>
    <!-- ./wrapper -->

<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/template/bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/template/jquery-slimscroll/jquery.slimscroll.min.js"></script>
 <!-- DataTables -->
<script src="<?php echo base_url();?>assets/template/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/template/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/template/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/template/dist/js/demo.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/template/fastclick/lib/fastclick.js"></script>

<!-- AdminLTE para button excel pdf print-->
<script src="<?php echo base_url();?>assets/template/datatables-export/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/template/datatables-export/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/template/datatables-export/js/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/template/datatables-export/js/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/template/datatables-export/js/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/template/datatables-export/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/template/datatables-export/js/buttons.print.min.js"></script>


<script src="<?php echo base_url();?>assets/funciones.js"></script>



<script>

$(document).ready(function ()
{
	$('#example').DataTable( {
		dom: 'Bfrtip',
        buttons: [
            //'copy', 'csv', 'excel', 'pdf', 'print'
            {
            	extend: 'excelHtml5',
				className: 'excel',		
				text: '<i class="fa fa-file-excel-o"></i>PDF',
            	title: "Listado de Proyectos",				
            	exportOptions: { 
            					columns:[0,1,2]	

            					}
            },
            {
				extend: 'pdfHtml5',
				className: 'pdf',
            	title: "Listado de Proyectos",
				text: '<i class="fa fa-file-pdf-o"></i>PDF',
            	exportOptions: { 
            					columns:[0,1,2]	

            					}
            }
            
            
        ],
        "language":{
			"lengthMenu":"Mostrar_MENU_registros por pagina",
			"zeroRecords":"No se encontraron resultados de su busqueda",
			"searchPlaceholder":"Buscar registros",
			"info":"Mostrando registros de _START_ al _END_ de un total de _TOTAL_ registros",
			"infoEmpty":"no existen registros",
			"infoFiltererd":"(filtrando un total de _MAX_ registros)",
			"search":"Buscar",
			"paginate":{
				"first":"Primero",
				"last":"Ultimo",
				"next":"Siguiente",
				"previous":"Anterior",
			},
		}
		
    } );
    
	$('#example1').DataTable({
		"language":{
			"lengthMenu":"Mostrar_MENU_registros por pagina",
			"zeroRecords":"No se encontraron resultados de su busqueda",
			"searchPlaceholder":"Buscar registros",
			"info":"Mostrando registros de _START_ al _END_ de un total de _TOTAL_ registros",
			"infoEmpty":"no existen registros",
			"infoFiltererd":"(filtrando un total de _MAX_ registros)",
			"search":"Buscar",
			"paginate":{
				"first":"Primero",
				"last":"Ultimo",
				"next":"Siguiente",
				"previous":"Anterior",
			},
		}
	});	
	$('.sidebar-menu').tree();
})
</script>
<script type="text/javascript">
				$(document).ready(function()
					{ $('#area').change(function() 
						{$('#area option:selected').each(function()
							{ 
							 var 
							 cod=$('#area').val();
							
								$.post("<?php echo base_url();?>proyecto/mis_proyectos/get_subarea",{ 
									cod:cod
									 }, function(data){ 
									 	
										$('#subarea').html(data);
										
									});
								});
							});	
						});		
											
				$(document).ready(function()
					{ $('#subarea').change(function() 
						{
							
							cod=$('#subarea').val();
							
							if(cod!='') 
								{
									$.ajax
									({
									url:"<?php echo base_url();?>proyecto/mis_proyectos/get_subarea3",
									method:"POST",
									data:{cod:cod},
									success:function(data)
										{
											$('#sub_area3').html(data);
										}
									})
														
								}
								else
								{
									//$('#area').html('<option value="'.$row->cod.'">'.$row->nombre.'</option>';)
								}
						});

					});  
</script>            						

<script>
				$(document).ready(function()
					{ $('#sector').change(function() 
						{
							var cod=$('#sector').val();
							if(cod!='') 
								{
									$.ajax
									({
									url:"<?php echo base_url();?>proyecto/sisin_controller/get_sub_sector",
									method:"POST",
									data:{cod:cod},
									success:function(data)
										{
											$('#sub_sector').html(data);
										}
									})
														
								}
								
						});

					});  


				$(document).ready(function()
					{ $('#sub_sector').change(function() 
						{
							
							cod=$('#sub_sector').val();
							
							if(cod!='') 
								{
									$.ajax
									({
									url:"<?php echo base_url();?>proyecto/sisin_controller/get_denominacion",
									method:"POST",
									data:{cod:cod},
									success:function(data)
										{
											$('#denominacion').html(data);
										}
									})
														
								}
								
						});

					});  
</script> 
<script>
				$(document).ready(function()
					{ $('#provincia').change(function() 
						{
							var cod=$('#provincia').val();
							if(cod!='') 
								{
									$.ajax
									({
									url:"<?php echo base_url();?>proyecto/municipio_controller/getMunicipio",
									method:"POST",
									data:{id_provincia:cod},
									success:function(data)
										{
											$('#municipios').html(data);
										}
									})
														
								}
								else
								{
									//$('#area').html('<option value="'.$row->cod.'">'.$row->nombre.'</option>';)
								}
						});

					}); 


</script> 
<script type="text/javascript">
function resta_anios(){
	f1=document.getElementById("fecha_inicio").value;
	f2=document.getElementById("fecha_fin").value;
	document.getElementById("duracion").value=f1;
}
</script>
								
</body>
</html>
