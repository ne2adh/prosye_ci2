    <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">      
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">Menu proyecto</li>
                    <li>
                        <a href="<?php echo base_url();?>proyecto/mis_proyectos/propios">
                            <i class="fa fa-home"></i> <span>Inicio</span>
                        </a>
                    </li>

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-share-alt"></i> <span>Proyecto</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i>En Registro</a></li>
                            <li><a href="#"><i class="fa fa-circle-o text-red"></i>En Espera Sisin</a></li>
                            <li><a href="#"><i class="fa fa-circle-o text-blue"></i>Con Sisin</a></li>
                            <li><a href="#"><i class="fa fa-circle-o text-green"></i>Finalizados</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-laptop"></i> <span>Contratacion</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href=""><i class="fa fa-circle-o text-yellow"></i>Convocatoria</a></li>
                            <li><a href=""><i class="fa fa-circle-o text-red"></i>Contrato</a></li>
                            <li><a href=""><i class="fa fa-circle-o text-blue"></i>Garantias</a></li>
                            <li><a href=""><i class="fa fa-circle-o text-green"></i>Seguimiento</a></li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-dashboard"></i> <span>Seguimiento</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i>Fisico</a></li>
                            <li><a href="#"><i class="fa fa-circle-o text-red"></i>Financiero</a></li>
                            <li><a href="#"><i class="fa fa-circle-o text-blue"></i> Convenio</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-file-o"></i> <span>Sisin</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                        </a>
                         <ul class="treeview-menu">
                            <li><a href="<?php echo base_url();?>proyecto/mis_proyectos/sisin"><i class="fa fa-circle-o text-yellow"></i>Proyectos Ingresados</a></li>
                            <li><a href="#"><i class="fa fa-circle-o text-red"></i>Proyectos Pendientes</a></li>
                            <li><a href="#"><i class="fa fa-circle-o text-blue"></i>Proyectos Con Sisin</a></li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-print"></i> <span>Reportes</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url();?>proyecto/mis_proyectos/con_convenio"><i class="fa fa-circle-o"></i> Proyectos con Convenio</a></li>
                            <li><a href="<?php echo base_url();?>proyecto/mis_proyectos/sin_convenio"><i class="fa fa-circle-o"></i> Proyectos sin Convenio</a></li>
                            <li><a href="<?php echo base_url();?>proyecto/mis_proyectos/por_provincia"><i class="fa fa-circle-o"></i> Proyectos por Provincia</a></li>
                            <li><a href="<?php echo base_url();?>proyecto/mis_proyectos/por_fecha"><i class="fa fa-circle-o"></i> Proyectos por Fecha</a></li>
                            <li><a href="<?php echo base_url();?>proyecto/sye_controller"><i class="fa fa-circle-o"></i> SyE</a></li>
                            <li><a href="../../index.html"><i class="fa fa-circle-o"></i> Municipio</a></li>
                        </ul>
                    </li>


                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-power-off"></i> <span>Temporal</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="#"><i class="fa fa-circle-o text-red"></i>Avances</a></li>
                         
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>auth/logout">
                            <i class="fa fa-close"></i> <span>Cerrar</span>
                        </a>
                    </li>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- =============================================== -->