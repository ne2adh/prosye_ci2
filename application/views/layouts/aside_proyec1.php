        <!-- =============================================== -->
        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">      
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">CONTROL DE PROYECTOS</li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-share-alt"></i> <span>Proyectos</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span> 
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url();?>proyecto/metas_controller"><i class="fa fa-circle-o text-green"></i> Registro Numero Paquetes</a></li>    
                            <li><a href="<?php echo base_url();?>proyecto/municipio_controller"><i class="fa fa-circle-o text-red"></i> Registro Paquetes</a></li>
                            <li><a href="<?php echo base_url();?>proyecto/puntos_controller"><i class="fa fa-circle-o"></i> Registro Coordenas Paquete</a></li>
                            <li><a href="<?php echo base_url();?>proyecto/indicador_controller"><i class="fa fa-circle-o"></i> Registro Puntos</a></li>
                            <li><a href="<?php echo base_url();?>proyecto/componente_controller/"><i class="fa fa-circle-o"></i> Registro Componente</a></li>
                            
                        </ul>
                      </li>
                      <li>  
                        <a href="<?php echo base_url();?>proyecto/mis_proyectos/propios">
                            <i class="fa fa-home"></i> <span>Cerrar</span>
                        </a>
                    </li>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>
        <!-- =============================================== -->