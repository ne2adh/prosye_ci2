# PROYECTO SCG v.0.0.1

_Instalacion en el servidor debian_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Da un ejemplo
```

### Instalación 🔧

_Una serie de ejemplos paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

_Dí cómo será ese paso_

```
Da un ejemplo
```

_Y repite_

```
hasta finalizar
```

_Finaliza con un ejemplo de cómo obtener datos del sistema o como usarlos para una pequeña demo_

## Ejecutando las pruebas ⚙️

_Explica como ejecutar las pruebas automatizadas para este sistema_

### Analice las pruebas end-to-end 🔩

_Explica que verifican estas pruebas y por qué_

```
Da un ejemplo
```

### Y las pruebas de estilo de codificación ⌨️

_Explica que verifican estas pruebas y por qué_

```
Da un ejemplo
```

## Despliegue 📦

_Inicialmente cabe mensionar q el despliegue sera realizado en el servidor de la institucion, estas configuraciones tambien sirven a las caracteristicas del Framework CI 3_

### Instalacion del proyecto en el servidor ⌨️

_Debemos clonar desde el repositorio de gitlab.com_

dirigirse a la siguiente direccion:

```
administrador@AppServ$ cd /var/www/html/
```
escribir la siguiente linea de comando:

```
git clone http://gitlab.com/gador/prosye_ci2.git
```

### Configuracion del proyecto en el servidor ⌨️

_Luego de clonar el proyecto del repositorio, debemos configurar los permisos de acceso del proyecto en el servidor_

```
sudo chown -R www-data:www-data /prosye_ci2
```
Crear un archivo de configuracion de apache para CodeIgniter
```
sudo vim /etc/apache2/sites-enabled/codeigniter.conf
```
inserta el siguiente codigo:
```
<Directory /var/www/html/prosye_ci2>
   AllowOverride All
</Directory>
```
y reinicia el servicio de apache
```
sudo systemctl restart apache2
```


## Construido con 🛠️

_Herramientas de construccion_

* [Codeigniter 3](https://www.codeigniter.com/userguide3/index.html) - El framework web usado
* [MySql](https://dev.mysql.com/downloads/mysql/) - Manejador de base de datos
* [Servidor HTTP Apache](https://httpd.apache.org/docs/2.4/es/) - Usado como Servidor http
* [PHP](https://www.php.net/manual/es/intro-whatis.php) - Usado como lenguaje de programacion

## Contribuyendo 🖇️

Por favor lee el [CONTRIBUTING.md](https://gist.github.com/villanuevand/xxxxxx) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Wiki](https://github.com/tu/proyecto/wiki)

## Versionado 📌

Usamos [SemVer](http://semver.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://github.com/tu/proyecto/tags).

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Andrés Villanueva** - *Trabajo Inicial* - [villanuevand](https://github.com/villanuevand)
* **Fulanito Detal** - *Documentación* - [fulanitodetal](#fulanito-de-tal)

También puedes mirar la lista de todos los [contribuyentes](https://github.com/your/project/contributors) quíenes han participado en este proyecto. 

## Licencia 📄

Este proyecto está bajo la Licencia (Tu Licencia) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

---
⌨️ con ❤️ por [Giancarlo Delgadillo](https://gitlan.com/ne2adh) 😊