/*
Funciones Control
Copyright@ GADOR Oruro Abril 2019
*/

/*
Funcion Cargar contenido en un div
*/

function cargar_contenido(div, url) //carga contenido en un  div
{ 
    
    registrar(url, 'abrir contenido');
    //url es la direccion del contenido a cargar
    //div es el nombre del div a cargar
  
    $('#'+div).load(url);
}

function registrar(menu, accion){    
    getUserIP(function(ip){        
        axios.post("../publico/registro", {                    
                    menu : menu,
                    ip : ''+ip,
                    accion : accion
                  }).then(function(response){                    
                    console.log(response.data.mensaje);                    
        });
    });

   
}

/**
 * Get the user IP throught the webkitRTCPeerConnection
 * @param onNewIP {Function} listener function to expose the IP locally
 * @return undefined
 */
function getUserIP(onNewIP) { //  onNewIp - your listener function for new IPs
    //compatibility for firefox and chrome
    var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
    var pc = new myPeerConnection({
        iceServers: []
    }),
    noop = function() {},
    localIPs = {},
    ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
    key;

    function iterateIP(ip) {
        if (!localIPs[ip]) onNewIP(ip);
        localIPs[ip] = true;
    }
 
     //create a bogus data channel
    pc.createDataChannel("");

    // create offer and set local description
    pc.createOffer().then(function(sdp) {
        sdp.sdp.split('\n').forEach(function(line) {
            if (line.indexOf('candidate') < 0) return;
            line.match(ipRegex).forEach(iterateIP);
        });
        
        pc.setLocalDescription(sdp, noop, noop);
    }).catch(function(reason) {
        // An error occurred, so handle the failure to connect
    });

    //listen for candidate events
    pc.onicecandidate = function(ice) {
        if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
        ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
    };
}




function desconectar(){
    localStorage.removeItem('id_usuario');
    localStorage.removeItem('estado');
    localStorage.removeItem('fecha_ingreso');
    localStorage.removeItem('cuenta');
    localStorage.removeItem('grado');
    localStorage.removeItem('ci_persona');
    localStorage.removeItem('nombres');
    registrar('cerrar session', 'salir aplicacion');
    alert("Session Terminada.");
}



/*
Funciones Control
*/
function verificarNumeroentero(evt){
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode > 31 && charCode!=46 && charCode!=45) && (charCode < 48 || charCode > 57)) {
        status = "Este Campo solo Acepta Numeros.";
        return false;
    }
    status = "";
    return true;
}

function verificarNumero(evt){
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode > 31  && charCode!=46 && charCode!=45 )&& (charCode < 48 || charCode > 57)) {
        status = "Este Campo solo Acepta Numeros.";
        return false;
    }
    status = "";
    return true;
}

function numeros(evt)
{
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode > 31  && charCode!=46 )&& (charCode < 48 || charCode > 57)) {
        status = "Este Campo solo Acepta Numeros.";
        return false;
    }
    status = "";
    return true;
}

function solo_numeros(evt)
{
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        status = "Este Campo solo Acepta Numeros.";
        return false;
    }
    status = "";
    return true;
}
 

function solo_letras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = [8, 37, 39, 46];
    tecla_especial = false
    for(var i in especiales) {
        if(key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}

function toUpper(control) {

    if (/[a-z]/.test(control.value)) {
        control.value = control.value.toUpperCase();
    }
}

//Modulo no disponible
function no_disponible()
{
  alert("Modulo no Disponible");
}

/*
Funciones para Dialogos
*/

function cargar_dialogo_small(url)
{
    registrar(url, 'abrir contenido');
    $('#d_small').load(url);
}

function cargar_dialogo_normal(url)
{
    registrar(url, 'abrir contenido');
    $('#d_normal').load(url);
}

function cargar_dialogo_large(url)
{
    registrar(url, 'abrir contenido');     
    $('#d_large').load(url);
}


//Decimales
function filterFloat(evt,input){
    // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
    var key = window.Event ? evt.which : evt.keyCode;    
    var chark = String.fromCharCode(key);
    var tempValue = input.value+chark;
    if(key >= 48 && key <= 57){
        if(filter(tempValue)=== false){
            return false;
        }else{       
            return true;
        }
    }else{
          if(key == 8 || key == 13 || key == 0) {     
              return true;              
          }else if(key == 46){
                if(filter(tempValue)=== false){
                    return false;
                }else{       
                    return true;
                }
          }else{
              return false;
          }
    }
}
function filter(__val__){
    var preg = /^([0-9]+\.?[0-9]{0,2})$/; 
    if(preg.test(__val__) === true){
        return true;
    }else{
       return false;
    }
    
}

/*
currency format 
https://codepen.io/559wade/pen/LRzEjj
*/

$("input[data-type='currency']").on({
    keyup: function() {
      formatCurrency($(this));
    },
    blur: function() { 
      formatCurrency($(this), "blur");
    }
});


function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");   
  
  
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val = left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}


/*
Mostrar Ocultar Div de Ayuda
https://coderanch.com/t/114199/languages/onFocus-display
*/

function ShowHide(theDiv,how){
    document.getElementById(theDiv).style.display = how;
  }